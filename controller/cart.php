<?php


class Cart extends Controller {
	public function __construct() {
		Session::init();
		parent::__construct();
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
		$this -> view -> emailSignup = "cart";
		$this -> view -> css = array('messages.css', 'cart.css');
	}	
	
	public function index() {
		Session::init();
		$this -> view -> title = "Your Cart";
		$this -> view -> mainWebsite = "";	
		$this -> view -> showFooter = "";
		$this -> view -> HTMLCART = "";
		$this -> view -> js = array(PATH . "public/js/CartController.js");
		$this -> view -> startJsFunction = array('CartController.StartDocument();');
	
		//print_r($_SESSION["cart_array"]);
		if (isset($_SESSION["cart_array"])) {
			$this -> view -> colors = $this -> model -> colors();
			$this -> view -> productCart = $this -> model -> Products();
		}
		$this -> view -> css = array('messages.css', 'cart.css');
		$this -> view -> render("cart/index");
	}
	
	public function payment() {
		Session::init();
		if (!isset($_SESSION["cart_array"])) {
			header("location: " . PATH . "cart");
		}
		$this -> view -> payment = "";
		$this -> view -> mainWebsite = "";
		$this -> view -> showFooter = "";
		$this -> view -> title = "Checkout";
		$this -> view -> productCart = $this -> model -> Products();
		$this -> view -> colors = $this -> model -> colors();
		$this -> view -> states = $this -> model -> states();
		$this -> view -> months = $this -> model -> months();
		$this -> view -> year = $this -> model -> year();
		
		$this -> view -> js = array("https://js.stripe.com/v2/", PATH . "public/js/CartController.js", PATH . "public/js/countries.js");
		$this -> view -> startJsFunction = array("Stripe.setPublishableKey('" . STRIPE_PUBLISH_KEY . "');", "CartController.PaymentPage();");
		$this -> view -> css = array('messages.css', 'cart.css');
		$this -> view -> render("cart/index");
	}

    public function confirm(){
    	if (!isset($_SESSION["cart_array"])) {
			header("location: " . PATH . "cart");
		}
        $this -> saveCartAndCheckoutInfo();
        $this -> view -> confirm = "";
        $this -> view -> mainWebsite = "";
		$this -> view -> showFooter = "";
        $this -> view -> title = "Confirm";
		$this -> view -> css = array('messages.css', 'cart.css');
        $this -> view -> colors = $this -> model -> colors();
        $this -> view -> productCart = $this -> model -> Products();
        $this -> view -> salesTax = $this -> model -> SalesTax();
        
        $this->view->shippingRates = $this->model->ShippingRates();
        
		$this -> view -> js = array(PATH . "public/js/CartController.js");

        $this -> view -> render("cart/index");
    }
	
	public function thankyou() {
		$this -> view -> title = "Thank You";
		$this -> view -> thankyou = "";
		$this -> view -> css = array('thankyou.css');
		$this -> view -> render("cart/thanks");
	}
	
	public function updateItem($id) {
		$productQuantity = array();
		$productQuantity['index'] = $id;
		$productQuantity['quantity'] = $_POST['productQuantity'];
		$productQuantity['product-id'] = $_POST['hiddenProductID'];
		$productQuantity['colorChnage'] = $_POST['colorChoices'];
		
		$this -> model -> updateItem($productQuantity);
	}

	public function saveCartAndCheckoutInfo() {
		$checkout = array();
		$i =1;		
		foreach ($_SESSION['cart_array'] as $item) {
			$checkout['productName'. $i] = $_POST['hiddenItem' . $i .'Name'];
			$checkout['productQnty'. $i] = $_POST['hiddenItem' . $i .'Qnty'];
            $checkout['productID'. $i] = $_POST['hiddenItem' . $i .'ProductID'];
            $checkout['productColorID'. $i] = $_POST['hiddenItem' . $i .'ProductColorID'];
            $checkout['price'. $i] = $_POST['hiddenItem' . $i .'Price'];
			$checkout['productImage'. $i] = $_POST['hiddenItem' . $i .'ProductImage'];

            $i ++;
		}
		
		$checkout['first-name'] 	= $_POST['firstName'];
		$checkout['last-name'] 		= $_POST['lastName'];
		$checkout['email'] 		= $_POST['email'];
		$checkout['address'] 		= $_POST['address'];
		$checkout['city'] 		= $_POST['City'];
		$checkout['state'] 		= $_POST['state'];
		$checkout['zip'] 		= $_POST['postalCode'];
		$checkout['phone'] 		= $_POST['phone'];
		
		//shipping rates
		$checkout['shipping-zip'] = $_POST['hiddenBillingZip'];
		$checkout['shipping-service'] = $_POST['hiddenShippingService'];
		$checkout['shipping-method'] = $_POST['hiddenShippingMethodValue'];
		$checkout['cart-volumne'] = $_POST['hiddenCartVolumne'];
		$checkout['cart-weight'] = $_POST['hiddenCartWeight'];
		
		if(isset($_POST['stripeToken'])) {
			$checkout['token'] 		= $_POST['stripeToken'];
		}
        $_SESSION['checkout'] = $checkout;
	}

    public function payCart(){
        $this -> model -> payCart($_SESSION['checkout']);
    }
	
	public function getShippingRates(){
        $inputJSON = file_get_contents('php://input');
        $input= json_decode($inputJSON, TRUE);
        $rates = $this->model->AllShippingRates($input['firstName'], $input['lastName'], $input['address'], $input['city'], $input['state'], $input['zip'], $input['phone'], true);
       	//this is needed for success full but for errors it doesnt display errors
       	if($rates) {
       		echo json_encode($rates);	
       	}
    }
	
	public function deleteItem($id) {
		Session::init();
		$this -> model -> deleteItem($id);
	}
	
	public function emailSignup() {
		$emailUpdates = New FeedBack();
		$emailUpdates -> email = $_POST['emailUpdates'];
		if($emailUpdates -> validateEmailUpdates()) {
			$emailUpdates ->saveEmail();
		}
	}
	
	public function clearCart() {
		Session::init();
		$this -> model -> clearCart();
		
	}
	
	
	
	
	
	

}