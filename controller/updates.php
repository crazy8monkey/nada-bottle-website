<?php

class Updates extends Controller {
		
	public function __construct() {
		Session::init();
		parent::__construct();
		$this -> view -> js = array(PATH . "public/js/UpdatesController.js");
		$this -> view -> startJsFunction = array('UpdatesController.StartDocument();');
		$this -> view -> css = array('messages.css', 'email.css');
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
	}	
	
	public function index() {
		$this -> view -> mainWebsite = "";
		$this -> view -> render("updates/index");
	}
	
	
	public function enterFeedback() {
		$feedback = New FeedBack();	
		$feedback -> feedBackDescription = $_POST['feedback'];
		$feedback -> feedBackEmail = $_POST['email'];
		$feedback -> nadaBottleEmail = $this -> model -> contactEmail;
		if($feedback -> validateFeedback()) {
			$feedback -> saveFeedback();
		}
	}
	
	

}
