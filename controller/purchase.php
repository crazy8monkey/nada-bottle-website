<?php


class purchase extends Controller {

	public function __construct() {
		Session::init();
		parent::__construct();
		$this -> view -> showFooter = "";
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
		$this -> view -> js = array(PATH . "public/js/PurchaseController.js");
		$this -> view -> emailSignup = "purchase";
	}
	
	public function index() {
		$this -> view -> mainWebsite = "";
		$this -> view -> title = isset($_GET['productType']) ? str_replace("_", " ", $_GET['productType']) : "All Products";
		if(isset($_GET['productType']))
        {
			$this -> view -> products = $this -> model -> ProductList($_GET['productType']);
        }
		else {
			$this -> view -> products = $this -> model -> ProductList();
		}
		
		$this -> view -> categories = $this -> model -> productCategories();
		
		$this -> view -> css = array('productCategory.css');
		$this -> view -> startJsFunction = array('PurchaseController.SinglePage();');
		$this -> view -> render('purchase/productCategory');
	}
	
	

	
	public function product() {
		Session::init();
		
		$this -> view -> mainWebsite = "";
		
		$this -> view -> title = "Single Product: ". str_replace("_", " ", $this -> security -> decode($_GET['productType']));
		$this -> view -> productSingle = $this -> model -> productSingle($this -> security -> decode($_GET['product']), $this -> security -> decode($_GET['productType']));
		$this -> view -> colors = $this -> model -> colors($this -> security -> decode($_GET['product']));	
		$this -> view -> preSaleVisible = $this -> model -> getPreSaleVisible();
		$this -> view -> preSaleText = $this -> model -> getPreSaleText();
		$this -> view -> css = array('messages.css', 'productSingle.css');
		$this -> view -> startJsFunction = array('PurchaseController.SinglePage();');
		$this -> view -> render('purchase/viewsingleproduct');
		
		
	}

	public function sendtocart() {
		$addToCart = new CartClass();
		$addToCart -> productID = $_POST['productID'];
		$addToCart -> quantity = $_POST['quantity'];
		$addToCart -> colorID = $_POST['colorChoices'];
		$addToCart -> cartTotalWeight = $this -> model -> totalCartWeightValue;
		
		if($addToCart -> validate()) {
			$addToCart -> sendToCart();
		}
	}
	
	public function emailSignup() {
		$emailUpdates = New FeedBack();
		$emailUpdates -> email = $_POST['emailUpdates'];
		if($emailUpdates -> validateEmailUpdates()) {
			$emailUpdates ->saveEmail();
		}
	}
	

}