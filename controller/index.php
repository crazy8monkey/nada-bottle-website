<?php


class Index extends Controller {
	function __construct() {
		Session::init();
		parent::__construct();
		//Session::getSessionVariables();
		//$loggedIn = Session::get('loggedIn');
		//if($loggedIn == true) {
		//	$this -> redirect -> redirectPage('recipes');
		//}
		
		$this -> view -> emailSignup = "index";
		$this -> view -> js = array(PATH . "public/js/IndexController.js", PATH . 'public/js/jquery.slides.min.js', PATH . 'public/js/jquery.fancybox.js');
		
		$this -> view -> startJsFunction = array('IndexController.openAboutUsVideo();');
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
	}

	function index() {
		//calls the view class, and
		//the render function
		$this -> view -> mainWebsite = "";
		$this -> view -> showFooter = "";
		$this -> view -> title = "Welcome to Nada Bottle";
		
		require_once "model/index_model.php";
		$model = New Index_Model();
		$this -> view -> css = array("homePage.css", "font-awesome.min.css", "jquery.fancybox.css");
		$this -> view -> partners = $model -> partners();
		$this -> view -> render('index/index');
	}
	public function AboutVideo() {
		$this -> view -> mainWebsite = "";
		$this -> view -> css = array("homePage.css", "font-awesome.min.css", "jquery.fancybox.css");
		$this -> view -> title = "Welcome to Nada Bottle";
		$this -> view -> render('index/video');
	}
	
	public function emailSignup() {
		$this -> view -> showFooter = "";
		$emailUpdates = New FeedBack();
		$this -> view -> css = array("homePage.css", "font-awesome.min.css", "jquery.fancybox.css");
		$emailUpdates -> email = $_POST['emailUpdates'];
		if($emailUpdates -> validateEmailUpdates()) {
			$emailUpdates ->saveEmail();
		}
	}

}
?>