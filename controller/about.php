<?php

class About extends Controller {
	public function __construct() {
		Session::init();
		parent::__construct();
		$this -> view -> showFooter = "";
		$this -> view -> mainWebsite = "";
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
		$this -> view -> emailSignup = "about";
	}	
	
	public function story() {
		$this -> view -> content = Page_Content::WithID(1);
		$this -> view -> title = $this -> view -> content -> contentHeader . ": Nada Basic";
		$this -> view -> render('about/story');
		
	}
	Public function cleanWater() {
		$this -> view -> content = Page_Content::WithID(2);
		$this -> view -> title = $this -> view -> content -> contentHeader . ": Nada Basic";
		$this -> view -> render('about/cleanWater');		
	}
	public function thankyou() {
		$this -> view -> content = Page_Content::WithID(3);
		$this -> view -> title = $this -> view -> content -> contentHeader . ": Nada Basic";
		$this -> view -> thankyoulist = $this -> model -> thankyouList();
		$this -> view -> render('about/thankyou');
	}	
	
	public function ourteam() {
		$this -> view -> teamMembers = $this -> model -> teamList();
		$this -> view -> title = "Our Team: Nada Basic";
		$this -> view -> css = array("ourTeam.css");
		$this -> view -> thankyoulist = $this -> model -> thankyouList();
		$this -> view -> render('about/ourTeam');
	}
	
	public function emailSignup() {
		$emailUpdates = New FeedBack();
		$emailUpdates -> email = $_POST['emailUpdates'];
		if($emailUpdates -> validateEmailUpdates()) {
			$emailUpdates ->saveEmail();
		}
	}


}
