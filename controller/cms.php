<?php

class CMS extends Controller {
	public function __construct() {
		parent::__construct();
		Session::init();
		$loggedIn = Session::get('adminLoggedIn');
		
		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceOrders')) {
			$this -> view -> eCommcerLink = PATH .  'cms/orders?page=1';	
			$this -> view -> eCommerceLinkTab = "";
		} else if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceCategories')) {
			$this -> view -> eCommcerLink = PATH .  'cms/categories';
			$this -> view -> eCommerceLinkTab = "";
		} else if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceProducts')) {
			$this -> view -> eCommcerLink = PATH .  'cms/products';
			$this -> view -> eCommerceLinkTab = "";
		} else if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceTaxes')) {
			$this -> view -> eCommcerLink = PATH .  'cms/taxReport';
			$this -> view -> eCommerceLinkTab = "";
		} else if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceWarranty')) {
			$this -> view -> eCommcerLink = PATH .  'cms/warranties';
			$this -> view -> eCommerceLinkTab = "";
		} else if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceInventory')) {
			$this -> view -> eCommcerLink = PATH .  'cms/inventory';
			$this -> view -> eCommerceLinkTab = "";
		}
				
		if($loggedIn == false) {
			Session::destroy();
			$this -> redirect -> redirectPage('login');
			exit();
		}
		$this -> view -> js = array("http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8/jquery.validate.min.js",
									"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js",
									"//cdn.ckeditor.com/4.5.3/full/ckeditor.js",
									PATH . "public/js/CMSController.js",
									PATH . "public/js/globals.js",
									"//code.jquery.com/ui/1.11.0/jquery-ui.js",
									PATH . "public/js/jqueryUITouchable.js",
									PATH . "public/js/farbtastic.js");
									
									//<script src=""></script>
		
	}	
	
	/*
	 * 
	 * START PAGES 
	 * 
	 */
	 
	//login
	public function index() {
		$this -> view -> title = "Nada Bottle: Administration";
		$this -> view -> adminWebsite = "";
		$this -> view -> css = array("messages.css", "admin/adminLogin.css");
		$this -> view -> render('cms/index');
	}
	
	//dashboard
	public function dashboard() {
		$this -> view -> title = "Dashboard: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> latestOrders = $this -> model -> LatestOrders();
		$this -> view -> latestContacts = $this -> model -> LatestContacts();
		$this -> view -> latestWarranties = $this -> model -> LatestWarranty();
		$this -> view -> css = array("admin/style.css", "admin/messages.css", "admin/dashboard.css");
		$this -> view -> render('cms/dashboard');
	}
	
	/*
	 * 
	 * USERS PAGES
	 *  
	 */
	
	//main users page
	public function users() {
		Session::init();
		$this -> view -> title = "Users: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> usersSection = "";
		$this -> view -> usersSelected = "";
		$this -> view -> usersList = $this -> model -> usersList();
		$this -> view -> css = array("admin/style.css", "admin/messages.css", "admin/usersList.css");
		$this -> view -> render('cms/users/index');
	}
	
	public function team() {
		Session::init();
		$this -> view -> title = "Team: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> teamSelected = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> teamMembers = $this -> model -> teamList();
		$this -> view -> startJsFunction = array('CMSController.teamList();');
		$this -> view -> css = array("admin/style.css", "admin/messages.css", "admin/teamList.css");
		$this -> view -> render('cms/users/team');
	}
	
	//add new user
	public function addNewuser() {
		$this -> view -> title = "New User: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> css = array("admin/messages.css", "admin/userSingle.css");
		$this -> view -> startJsFunction = array('CMSController.userSinglePage();');
		$this -> view -> render('cms/users/ProfileSingle');
	}
	
	public function newTeamMember() {
		$this -> view -> title = "New Team Member: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> css = array("admin/messages.css", "admin/teamSingle.css");
		$this -> view -> startJsFunction = array('Globals.textAreaAdjust(".jobDescription"); CMSController.teamSinglePage();');
		$this -> view -> render('cms/users/TeamSingle');
	}
	
	public function teamMember($id) {
		$this -> view -> teamMemberSingle = Team::WithID($id);
		$this -> view -> title = "Team Member: ". $this -> view -> teamMemberSingle -> FullName();
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> css = array("admin/messages.css", "admin/teamSingle.css");
		$this -> view -> startJsFunction = array('Globals.textAreaAdjust(".jobDescription"); CMSController.teamSinglePage();');
		$this -> view -> render('cms/users/TeamSingle');
	}
	
	//single user page
	public function profile() {
		Session::init();
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> usersSection = "";
        $this -> view -> user = User::WithID($_GET['userid']);
		if($_SESSION["user"]-> FullName() !== $this -> view -> user -> FullName()) {
			$this -> view -> title = $this -> view -> user -> FullName() . "'s Profile";
		} else {
			$this -> view -> title = "Your Profile";
		}
		
        
		$this -> view -> startJsFunction = array('CMSController.userSinglePage();');
		$this -> view -> css = array("admin/style.css", "admin/messages.css", "admin/userSingle.css");
		$this -> view -> render('cms/users/ProfileSingle');
	}
	
	/*
	 * 
	 * CATEGORIES PAGES
	 *  
	 */
	
	//category page
	public function categories() {
		Session::init();
		$this -> view -> title = "Categories: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> categories = $this -> model -> productCategories();
		$this -> view -> startJsFunction = array('CMSController.CategoriesList();');
		$this -> view -> css = array("admin/messages.css", "admin/categoryList.css");
		$this -> view -> render('cms/purchase/categoryList');
	}
	
	//single category page
	public function category($id) {
		$this -> view -> title = "Edit Category: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> categorySingle = Category::categoryID($id);
		$this -> view -> startJsFunction = array('CMSController.Categories();');
		$this -> view -> css = array("admin/messages.css", "admin/categorySingle.css");
		$this -> view -> render('cms/purchase/CategorySingle');
	}
	
	//add new category
	public function addNewCategory() {
		Session::init();
		$this -> view -> title = "New Category: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> startJsFunction = array('CMSController.Categories();');
		$this -> view -> css = array("admin/messages.css", "admin/categorySingle.css");
		$this -> view -> render('cms/purchase/CategorySingle');
	}
	
	/*
	 * 
	 * PARTNER PAGES 
	 * 
	 */
		
	//partners pages
	public function partners() {
		Session::init();
		$this -> view -> title = "Partners: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> partnersLink = "";
		$this -> view -> partners = $this -> model -> partnersList();
		$this -> view -> css = array("admin/messages.css", "admin/partners.css");
		$this -> view -> render('cms/home/partners');
	}
	
	//add new partner
	public function addNewPartner() {
		Session::init();
		$this -> view -> title = "New Partner: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> partnersLink = "";
		$this -> view -> css = array("admin/messages.css", "admin/partnerSingle.css");
		$this -> view -> startJsFunction = array('CMSController.PartnerSingle();');
		$this -> view -> render('cms/home/PartnerSingle');
	}
	
	//edit partner
	public function editPartner($id) {
		Session::init();
		$this -> view -> title = "View Partner: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> partnersLink = "";
		$this -> view -> partnerSingle = Partner::WithID($id);
		$this -> view -> css = array("admin/partnerSingle.css", "admin/partners.css", 'admin/messages.css');
		$this -> view -> startJsFunction = array('CMSController.PartnerSingle();');
		$this -> view -> render('cms/home/PartnerSingle');
	}
	
	/*
	 * 
	 * LOCATE LIFE PAGES 
	 * 
	 */
	
	//locate life page
	public function locatelife() {
		Session::init();
		$this -> view -> title = "Locate Life: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> locateLife = "";
		$this -> view -> locations = $this -> model -> locations();
		$this -> view -> css = array("admin/locateLife.css", "admin/messages.css");
		$this -> view -> startJsFunction = array('CMSController.validateLocateLifeForm();');
		$this -> view -> render('cms/locatelife/locatelife');
	}
	
	//location single page
	public function location($id) {
		Session::init();
		$this -> view -> title = "Locate Life: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> locateLife = "";
		$this -> view -> locationSingle = Locations::WithID($id);
		$this -> view -> css = array('admin/messages.css', "admin/locateLife.css");
		$this -> view -> startJsFunction = array('CMSController.validateLocateLifeForm();');
		$this -> view -> render('cms/locatelife/singleLocation');
	}
	
	//add new location
	public function addNewLocation() {
		Session::init();
		$this -> view -> title = "Locate Life: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> locateLife = "";
		$this -> view -> css = array("admin/locateLife.css", "admin/messages.css");
		$this -> view -> startJsFunction = array('CMSController.validateLocateLifeForm();');
		$this -> view -> render('cms/locatelife/singleLocation');
	}
	
	/*
	 * 
	 * PRODUCT PAGES 
	 * 
	 */
	
	//product main page
	public function products() {
		Session::init();
		$this -> view -> title = "Products: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		
		if(isset($_GET['Category'])) {
			$this -> view -> product = $this -> model -> ProductList($_GET['Category']);	
		}
		
		$this -> view -> categories = $this -> model -> productCategories();
		$this -> view -> css = array("admin/messages.css", "admin/categoryProducts.css");
		$this -> view -> render('cms/purchase/productCategory');
	}
	
	
	//edit product
	public function editproduct() {
		Session::init();
		$this -> view -> title = "Edit Product: Nada Bottle";	
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> productInfoSelectedLink = "";
		$this -> view -> css = array("admin/messages.css", "admin/productSingle.css");
		$this -> view -> productSingle = $this -> model -> productSingle($_GET['ID'], $_GET['productType']);
		$this -> view -> startJsFunction = array('CMSController.validateProductForm(); Globals.textAreaAdjust(".productDescription");');
        $this -> view -> colors = $this -> model -> productColors($_GET['ID']);
        $this -> view -> render('cms/purchase/ProductSingle');
	}
	
	//edit color list
	public function editcolorlist() {
		Session::init();
		$this -> view -> title = "Edit Color List: Nada Bottle";	
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> colorListSelectedLink = "";
		$this -> view -> css = array("admin/messages.css", "admin/productSingle.css", "admin/farbtastic.css");
		$this -> view -> productSingle = $this -> model -> productSingle($_GET['ProductID'], $_GET['productType']);
		$this -> view -> startJsFunction = array('CMSController.colorList();');
        $this -> view -> colors = $this -> model -> productColors($_GET['ProductID']);
        $this -> view -> render('cms/purchase/editColorList');
	}

	//edit product inventory
	public function editproductinventory() {
		Session::init();
		$this -> view -> title = "Edit Product Inventory: Nada Bottle";	
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> SingleInventoryList = "";
		$this -> view -> css = array("admin/messages.css", "admin/productSingle.css", "admin/inventory.css");
		$this -> view -> productSingle = $this -> model -> productSingle($_GET['ProductID'], $_GET['productType']);
		$this -> view -> colors = $this -> model -> productColors($_GET['ProductID']);
		$this -> view -> startJsFunction = array('CMSController.inventory();');
        $this -> view -> render('cms/purchase/editInventoryList');
	}
	
	//edit color
	public function editcolor() {
		Session::init();
		$this -> view -> title = "Edit Color List: Nada Bottle";	
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> colorListSelectedLink = "";
		$this -> view -> productColorListSection = "";
		$this -> view -> css = array("admin/messages.css", "admin/productSingle.css", "admin/farbtastic.css");
		$this -> view -> startJsFunction = array('CMSController.colorList();');
        $this -> view -> colorSingle = Colors::productColorID($_GET['ID']);
        $this -> view -> render('cms/purchase/editcolor');
	}
	
	//add new product
	public function addNewProduct() {
		Session::init();
		$this -> view -> title = "New Product: Nada Bottle";	
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> css = array("admin/messages.css", "admin/productSingle.css");
		$this -> view -> startJsFunction = array('CMSController.validateProductForm(); Globals.textAreaAdjust(".productDescription");');
        $this -> view -> render('cms/purchase/ProductSingle');

	}
	
	/*
	 * 
	 *	ORDER PAGES 
	 * 
	 */
	 
	//orders main page
	public function orders() {
		Session::init();
		$this -> view -> title = "Orders: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> pagination = $this -> model -> getOrderPagination();
		if(isset($_GET['status'])) {
        	$this -> view -> orders = $this -> model -> orders($_GET['page'], $_GET['status']);
		} else {
			$this -> view -> orders = $this -> model -> orders($_GET['page']);
		}
		
		$this -> view -> css = array("admin/orders.css", "admin/messages.css");
		$this -> view -> controller = $this;
		$this -> view -> render('cms/purchase/orders');
	}
	
	//transaction page
	public function transaction($id, $payment = false) {
		Session::init();
		$this -> view -> title = "Order #: " . $id;
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> singleOrder = $this -> model -> singleOrder($id);
        $ship = Shipments::WithOrderID($id);
        if($ship->responseData) {
            $this->view->shipment = Shipments::WithOrderID($id);
        }
		$this -> view -> css = array("admin/ordersSingle.css","admin/messages.css");

		if($payment) {
			$this -> view -> paymentInfoSelected = "";
			$this -> view -> paymentHistory = $this -> model -> paymentHistory($id);
			$this -> view -> render('cms/purchase/orderPaymentInfo');
		} else {
			$this -> view -> orderInfoSelected = "";	
			$this -> view -> labels = $this -> model -> orderLabels($id); 
			$this -> view -> tracking = $this -> model -> getTrackingUrl($id);
			$this -> view -> startJsFunction = array('CMSController.singleOrder();');
			$this -> view -> render('cms/purchase/singleOrder');
		}
	}

	

	public function reviewOrder($id) {
		$this -> view -> title = "Warranty / Review Order#: " . $id;
		$this -> view -> warrantyPopup = "";
		$this -> view -> singleOrder = $this -> model -> singleOrder($id);
		$this -> view -> css = array("admin/style.css", "admin/ordersSingle.css","admin/messages.css");
		
		$this -> view -> render('cms/purchase/singleOrder');
		
	}
	
	//receipt page
	public function receipt($id) {
		$this -> view -> title = "Order #: " . $id . " / Receipt";
		$this -> view -> receipt = Orders::WithID($id);
		$this -> view -> startJsFunction = array('window.print();');
		$this -> view -> render('cms/purchase/receipt');
	}
	
	//shipping label
	public function shippingLabel($id, $key){
        $this -> view -> title = "Order #:" . $id . " / Packge #:" . ($key + 1) . " / Shipping Label";
        $this -> view -> shippingLabel = $this -> model -> shippingLabel($id, $key);

        // Rather than generate the image using HTML, we are just setting the content type and returning it directly
        // This way we can do PDF for FedEx and PNG for UPS
        // The ShippingLabel() function on the model is responsible for setting content type
        echo $this -> view -> shippingLabel;
    }
	
	/*
	 * 
	 * WARRANTY PAGES
	 * 
	 */
	 
	//warranty main page
	public function warranties() {
		$this -> view -> title = "Warranties: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> warranties = $this -> model -> warranties();
		$this -> view -> css = array("admin/messages.css", "admin/warranties.css");
		$this -> view -> render('cms/warranties/warranties');
	}
	
	//warranty single page
	public function warrantySingle($id) {
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> warrantySingle = Warranty::WithID($id);
		$this -> view -> title = "Warranty #: ". $id. " Nada Bottle";
		$this -> view -> startJsFunction = array('CMSController.warrantySinglePage();');
		$this -> view -> css = array("admin/messages.css", "admin/warranties.css");
		$this -> view -> render('cms/warranties/warrantySingle');
	}
	
	//warranty photo page
	public function warrantyPhoto($id) {
		$this -> view -> warrantyPhoto = Warranty::WithID($id);
		$this -> view -> photos = $this -> model -> warrantyPhotos($id);
		$this -> view -> title = "Warranty #: " . $id . " photo / ". $this -> view -> warrantyPhoto -> getFullName();
		$this -> view -> render('cms/warranties/WarrantyPhoto');
		
	}
	
	/*
	 * 
	 * CONTACT PAGES
	 * 
	 */
	
	//contact main page
	public function contact() {
		Session::init();
		$this -> view -> title = "Contact: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> contactInquiriesLink = "";
		$this -> view -> contactInquiries = $this -> model -> contactInquiries($_GET['page']);
		$this -> view -> css = array("admin/messages.css", "admin/contactInquiries.css");
		$this -> view -> render('cms/support/contact');
	}
	
	//single contact inquirty page
	public function singleContactInquiry($id) {
		Session::init();
		$this -> view -> title = "Contact Inquiry: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> contactInquiriesLink = "";
		$this -> view -> contact = Contact::WithID($id);
		$this -> view -> css = array("admin/messages.css", "admin/contactInquiries.css");
		$this -> view -> render('cms/support/contactInquiry');
	}
	
	/*
	 * 
	 * ECOMMERCE MISC PAGES 
	 * 
	 */
	 
	 //inventory main page
	 public function inventory() {
		$this -> view -> title = "Inventory: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> product = $this -> model -> InventoryProductList();
		$this -> view -> colors = $this -> model -> InventoryProductColors();
		$this -> view -> startJsFunction = array('CMSController.inventory();');
		$this -> view -> css = array("admin/messages.css", "admin/inventoryMain.css", "admin/inventory.css");
		$this -> view -> render('cms/inventory/index');
	}
	
	//tax report
	public function taxReport() {
		$this -> view -> title = "Tax Report: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> eCommerceSection = "";
		
		if(isset($_GET['dtStart']) && isset($_GET['dtEnd']))
        {
            $this -> view -> startDate = $_GET['dtStart'];
            $this -> view -> endDate = $_GET['dtEnd'];
        }
        elseif(isset($_GET['dtStart']))
        {
            $this -> view -> startDate = $_GET['dtStart'];
        }
        elseif(isset($_GET['dtEnd']))
        {
            $this -> view -> endDate = $_GET['dtEnd'];
        }
        else
        {
            $this->view->startDate = date('Y-m-d', mktime(12,0,0,1,1,2001));
            $this->view->endDate = date('Y-m-d', mktime(12,0,0,1,1,2038));
        }
		
		$this -> view -> css = array("admin/messages.css", "admin/taxReport.css");
        $this -> view -> taxReport = $this -> model -> getTaxReport($this->view->startDate, $this->view->endDate);
        $this -> view -> render('cms/taxes/index');
    }
	
	/*
	 * 
	 * SETTINGS PAGES
	 * 
	 */
	
	//webhook list page
	public function webhooklist() {
		$this -> view -> title = "Stripe Webhook List: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> webhooks = $this -> model -> webhooks();
		$this -> view -> css = array("admin/adminWebsiteLogs.css");
		$this -> view -> render('cms/webhooks');
	} 
	
	public function phpinformation() {
		$this -> view -> title = "PHP Settings: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> render('cms/phpInfo');
	} 
	
	//settings page
	public function settings($type) {
		$this -> view -> title = "Settings: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		
		$this -> view -> css = array("admin/messages.css", "admin/settings.css");
		$this -> view -> startJsFunction = array('CMSController.Settings();');
		if($type == "shipping") {
			$this -> view -> settings = $this -> model -> Settings();
			$this -> view -> boxes = $this -> model -> ShippingBoxes();
			
			$this -> view -> shipping = "";
			$this -> view -> companyPhoneSection = $this -> model -> shippingMethodName;
			$this -> view -> render('cms/settings/shipping');	
		} else if($type == "methods") {
			$this -> view -> methods = "";
			$this -> view -> shippingCompanyNameString = $this -> model -> shippingMethodName;
			$this -> view -> shippingMethods = $this -> model -> ShippingMethods();
			$this -> view -> render('cms/settings/methods');
		} else if($type == "misc") {
			$this -> view -> misc = "";
			$this -> view -> settings = $this -> model -> Settings();
			$this -> view -> render('cms/settings/settings');
		}
		
	}
	
	//get logging page
	public function logging() {
		$this -> view -> title = "Logging: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> css = array("admin/messages.css", "admin/adminWebsiteLogs.css");
		$this -> view -> render('cms/logging/logs');
	}
	
	/*
	 * 
	 * UPDATE CONTENT PAGES
	 * 
	 */
	 
	//warranty front end page
	public function warranty() {
		Session::init();
		$this -> view -> title = "Warranty: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> warranty = Page_Content::WithID(4);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/support/warranty');
	}
	
	//contact front end page
	public function contactContent() {
		Session::init();
		$this -> view -> title = "Contact Content: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> content = Page_Content::WithID(5);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/support/contactContent');
	}
	
		//shipping and devilery front end page
	public function locatelifeContent() {
		Session::init();
		$this -> view -> title = "Locate Life";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> content = Page_Content::WithID(6);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/NadaBottle/locatelife');
	}
	
	
	//faq front end page
	public function faq() {
		Session::init();
		$this -> view -> title = "FAQ";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> content = Page_Content::WithID(7);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/support/FAQ');
	}
	
	//story front end page
	public function story() {
		Session::init();
		$this -> view -> title = "Story: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> storyContent = Page_Content::WithID(1);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/NadaBottle/story');
	}
	
	//clean water front end page
	public function cleanWater() {
		Session::init();
		$this -> view -> title = "Clean Water: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> cleanWaterContent = Page_Content::WithID(2);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/NadaBottle/cleanWater');
	}
	
	//thank you front end page
	public function thankyou() {
		Session::init();
		$this -> view -> title = "Thank You Wall: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> thankyouContent = "";
		$this -> view -> ThankYou = Page_Content::WithID(3);
		$this -> view -> css = array("admin/messages.css", "admin/pagesContent.css");
		$this -> view -> render('cms/NadaBottle/thankyou');
	}
	
	public function thankYouList() {
		Session::init();
		$this -> view -> title = "Thank You List: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> mainPagesSection = "";
		$this -> view -> thankYouList = "";
		$this -> view -> ThankYou = $this -> model -> thankYouList($_GET['page']);
		$this -> view -> css = array("admin/messages.css", "admin/contactInquiries.css");
		$this -> view -> render('cms/NadaBottle/thankyouList');
	}

	/*
	 * 
	 * Feed back logic
	 * 
	 */
	 
	 //feedbackMainPage 
	public function feedback() {
		Session::init();
		$this -> view -> title = "Feed Back";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> feedBackLink = "";
		$this -> view -> feedBackList = $this -> model -> feedBackList();
		$this -> view -> css = array("admin/messages.css", "admin/feedback.css");
		$this -> view -> render('cms/Feedback/index');
	}
	 
	public function feedbackSingle($id) {
		Session::init();
		$this -> view -> title = "Feed Back: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> feedBackLink = "";
		$this -> view -> feedback = FeedBack::WithFeedBackID($id);
		$this -> view -> css = array("admin/messages.css", "admin/feedback.css");
		$this -> view -> render('cms/Feedback/single');
	} 
	
	public function paymentLogs() {
		$this -> view -> title = "Payment Logs: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> feedBackLink = "";
		$this -> view -> eCommerceSection = "";
		$this -> view -> paymentHistory = $this -> model -> paymentHistory();
		$this -> view -> css = array("admin/adminWebsiteLogs.css");
		$this -> view -> render('cms/purchase/paymentLogs');
	}
	
	/*
	 * 
	 * NEWSLETTER PAGES 
	 * 
	 */
	
	public function emailList() {
		$this -> view -> title = "Newsletter Email List: Nada Bottle";
		$this -> view -> adminWebsite = "";
		$this -> view -> subMenu = "";
		$this -> view -> adminWebsiteHeader = "";
		$this -> view -> newsLetterLink = "";
		$this -> view -> emailList = $this -> model -> emailList();
		$this -> view -> css = array("admin/adminWebsiteLogs.css");
		$this -> view -> render('cms/NewsLetter/emailList');
	}
	
	 
	/*
	 *
	 * START CLASS LOGIC 
	 * 
	 */	
	
	public function deleteMultiple($type) {
		$idNames = "";
		switch ($type) {
			case "warranty";
				$idNames = $_POST['warranties'];
				break;
			case "contact";
				$idNames = $_POST['contacts'];
				break;
			case "feedback";
				$idNames = $_POST['feedback'];
				break;
			case "ThankYou";
				$idNames = $_POST['supporters'];
				break;
		}
				
		$ids = $idNames;
		$this -> model -> deleteMultiple($ids, $type);
	}
	
	public function delete($id, $type, $category = false) {
		switch ($type) {
			case "warranty";
				$warranty = Warranty::WithID($id);
				$warranty -> delete();
				break;	
			case "products";
				$product = Product::withID($id);
				$product -> delete();
				break;
			case "category";
				$category = Category::categoryID($id);
				$category -> delete();
				break;
			case "contact";
				$contact = Contact::WithID($id);
				$contact -> delete();
				break;
			case "partners";
				$partner = Partner::WithID($id);
				$partner -> deletePartner();
				break;
			case "locations";
				$location = Locations::WithID($id);
				$location -> delete();
				//for some odd reason
				//this method isn't working on the locations object
				$this -> model -> redirectPage('cms/locatelife');
				break;
			case "colors";
				$color = Colors::productColorID($id);
				$color -> deleteColor($id, $category);
				break;
			case "users";
				$user = User::WithID($id);
				$user -> deleteUser();
				break;
			case "feedback";
				$feedback = FeedBack::WithFeedBackID($id);
				$feedback -> delete();
				break;
			case "team";
				$team = Team::WithID($id);
				$team -> delete();
				break;
			case "thankyou";
				$supporter = ThankYou::WithID($id);
				$supporter -> delete();	
				break;
			case "boxes";
				$box = ShipmentBoxes::WithID($id);
				$box -> delete();	
				break;
		}
		
	}

	public function filter($type) {
		switch ($type) {
			case "orders";
			$orderFilter = New Filter();
			$orderFilter -> filterType = $_POST['orderStatus'];
			break;
		}
	}
	
	/*
	 * 
	 * PARTNERS LOGIC
	 * 
	 */
		
	//save partner changes
	public function savePartner($id = false) {
		if($id) {
			$partner = Partner::WithID($id);
		} else {
			$partner = New Partner();
		}
		
		$partner -> name = $_POST['partnerName'];
		if(!empty($_FILES['partnerLogoImage']['name'])) {
			$partner -> newImageName = Image::renameImage($_FILES['partnerLogoImage']['name']);	
		}
		
		$partner -> partnerLink = $_POST['partnerHyperLink'];
		$partner -> save();
	}
	
	//publish partner
	public function publishPartner($id) {
		$partner = Partner::WithID($id);
		$partner -> publishPartner();
	}
	
	//un publish partner
	public function unPublishPartner($id) {
		$partner = Partner::WithID($id);
		$partner -> unpublishPartner();
	}
	
	
	
	/*
	 * 	LOCATE LIFE LOGIC
	 * 
	 */
	
	//save new/edit location
	public function saveLocation($id = false) {
		
			
		if($id) {
			$location = Locations::WithID($id);						
		} else {
			$location = New Locations();
		}
		
		$location -> longitude = $_POST['longitude'];
		$location -> latitude = $_POST['latitude'];
		$location -> name = $_POST['locationName'];
		$location -> locationImage = $_FILES['locationImage']['name'];
		$location -> locationVideo = $_POST['locationVideo'];
		
		$location -> description = $_POST['locationInformation'];
		
		if($location -> validate()) {
			$location -> save();
		}
		

	}
	
	/*
	 * 
	 * SETTINGS LOGIC
	 * 
	 */
	 
	 //email settings
	 public function emailSettings() {
		$settings = New Settings();
		$settings -> contactEmail = $_POST['contactEmail'];
		$settings -> receiptEmail = $_POST['receiptEmail'];
		$settings -> inventoryEmail = $_POST['inventoryReminderEmail'];
		$settings -> warranties = $_POST['warrantyEmail'];
		$settings -> paymentEmail = $_POST['paymentEmail'];
		
		if($settings -> validateEmailSettings()) {
			$settings -> saveEmailSettings();
		}
	}
	
	//tracking email 
	public function saveTrackingEmail() {
		$settings = New Settings();
		$settings -> trackingEmailText = $_POST['trackingEmailText'];
		if($settings -> validateTrackingEmailText()) {
			$settings -> saveTrackingEmailText();
		}
	}
	
	//update company address
	public function updateCompanyAddress() {
		$settings = New Settings();
		$settings -> companyName = $_POST['companyName'];
		$settings -> companyShippingAddress = $_POST['companyAddress'];
		$settings -> companyShippingAddress2 = $_POST['companyAddress2'];
		$settings -> shippingCity = $_POST['companyCity'];
		$settings -> shippingState = $_POST['companyState'];
		$settings -> shippingZip = $_POST['companyZip'];
		$settings -> companyPhoneAreaCode = $_POST['companyPhoneNumberAreaCode'];
		$settings -> companyPhone1 = $_POST['companyPhoneNumber2'];
		$settings -> companyPhone2 = $_POST['companyPhoneNumber3'];
		
		if($settings -> validateCompanyInformation()) {
			$settings -> saveCompanyInformation();
		}
	}

	//update shipping company
	public function saveShippingCompany() {
		$settings = New Settings();
		$settings -> shippingCompany = $_POST['shippingCompany'];
		
		$settings -> saveShippingCompany();
	}
	 
	//locate life settings 
	public function toggleLocateLife() {
		$settings = New Settings();
		$settings -> locateLifeSettings = $_POST['toggleLocateLifePage'];
		$settings -> saveLocateLifeSettings();
	}
	
	public function updatePaymentEmailSettings() {
		$settings = New Settings();
		$settings -> paymentSucceded = $_POST['paymentSucceded'];
		$settings -> paymentFailed = $_POST['paymentFailed'];
		$settings -> paymentRefunded = $_POST['paymentRefunded'];
		$settings -> paymentCaptured = $_POST['paymentCaptured'];
		$settings -> paymentDisputeCreated = $_POST['paymentDisputeCreated'];
		$settings -> paymentDisputedFundsWithdrawn = $_POST['paymentDisputeFundsWidthdrawn'];
		$settings -> paymentDisputedFundsReinstated = $_POST['paymentDisputeFundsReinstated'];
		$settings -> paymentDisputedUpdated = $_POST['paymentDisputeUpdated'];
		$settings -> paymentDisputedClose = $_POST['paymentDisputeClosed'];
	
		$settings -> savePaymentNotifications();
	}
	
	public function updatePurchaseSettings() {
		$settings = New Settings();
		$settings -> preSaleVisible = $_POST['hiddenVisiblePreSale'];
		$settings -> preSaleText = $_POST['preSaleText'];
		
		if($settings -> validatePreSaleText()) {
			$settings -> SavePurchaseSettings();	
		}
		
		
	}
	
	public function saveShippingMethods() {
		$shippingMethod = array();
		$shippingMethod = $_POST['hiddenActiveShippingMethods'];
		$this -> model -> saveShippingMethods($shippingMethod);
	}

		//save new/edit location
	public function saveShippingBox($id = false) {
		
			
		if($id) {
			$box = ShipmentBoxes::WithID($id);						
		} else {
			$box = New ShipmentBoxes();
		}
		
		$box -> length = $_POST['boxLength'];
		$box -> width = $_POST['boxWidth'];
		$box -> height = $_POST['boxHeight'];
		$box -> itemsHold = $_POST['boxItemsHold'];
		$box -> weight = $_POST['boxWeight'];
		
		
		if($box -> validate()) {
			//echo "save";
			$box -> Save();
		}
		
					
		
		

	}

	/*
	 * 
	 * COLORS LOGIC
	 * 
	 */
	 
	//save/edit color
	public function saveColor($id) {

		$color = Colors::productColorID($id);
		$color -> Color = $_POST['colorName'];
		$color -> ColorHexacode = $_POST['color'];
        $color -> RemainingQuantity = $_POST['remainingQuantity'];
		if(!empty($_FILES['colorImage']['name'])) {
			$color -> NewColorImage = $_FILES['colorImage']['name'];
			$color -> newPhotoName = Image::renameImage($_FILES['colorImage']['name']);
		}
		if($color -> Validate(true)) {
			$color -> save();
		}
	}
	
	
	
	//update color inventory
	public function updateInventory($id) {
		$color = Colors::productColorID($id);
		$color -> RemainingQuantity = $_POST['quantity'];
		if($color -> validateQuantity()) {
			$color -> updateQuantiy();
		}
	}
	
	//add new color
	public function addProductColor($id, $category) {
        $productColor = new Colors();
        $productColor -> ProductId = $id;
        $productColor -> Color = $_POST['colorName'];
        $productColor -> ColorHexacode = $_POST['color'];
        $productColor -> ProductColorImage = $_FILES['colorImage']['name'];
		$productColor -> newPhotoName = Image::renameImage($_FILES['colorImage']['name']);
        $productColor -> RemainingQuantity = $_POST['remainingQuantity'];
        if ($productColor->validate()){
            $productColor->save();
        }
	}
	
	/*
	 * 
	 * ORDERS LOGIC
	 * 
	 */
	 
	//void order
	public function voidOrder($id) {
		$order = Orders::WithID($id);
		$order -> voidOrder();
	}
	
	//unvoid order
	public function unVoidOrder($id) {
		$order = Orders::WithID($id);
		$order -> unVoidOrder();
	}
	
	//finish order
	public function finishOrder($id) {
		$order = Orders::WithID($id);	
		$order -> finishOrder();
	}
	
	//unfinish order
	public function unFinishOrder($id) {
		$order = Orders::WithID($id);	
		$order -> unFinishOrder();
	}
	
	//edit shipping address
	public function changeShippingInfo($id) {
		$newShipping = array();
		$newShipping['id'] = $id;
		$newShipping['address'] = $_POST['changeAddress'];
		$newShipping['city'] = $_POST['changeCity'];
		$newShipping['state'] = $_POST['changeState'];
		$newShipping['zip'] = $_POST['changeZip'];
		
		$this -> model -> changeShippingInfo($newShipping);
	}
	
	//maual approve address
	public function ApproveAddress($id) {
		$order = Orders::WithID($id);	
		$order -> approveAddress();
	}
	
	/*
	 * 
	 * USERS LOGIC
	 * 
	 */
	
	//save/edit user
	public function saveUser($id = false) {
		if($id) {
			$user = User::WithID($id);	
		} else {
			$user = new User();
			$user->SetPassword($_POST['newUserPassword']);
		}

        
        $user->FirstName = $_POST['userFirstName'];
        $user->LastName = $_POST['userLastName'];
        $user->Username = $_POST['userUserName'];
        $user->Email = $_POST['userEmail'];
		
		if($_SESSION["user"]-> FullName() !== $user -> FullName()) {
       		//pages
       		if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('pages')) {
       			$user->SetPermission('pages', $_POST['chkPages'] == 1);	
       		}
	   		//eCommerce
	   	 	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceOrders')) {
	   	 		$user->SetPermission('eCommerceOrders', $_POST['chkOrders'] == 1);	
	   	 	}
	       	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceCategories')) {
	       		$user->SetPermission('eCommerceCategories', $_POST['chkCategories'] == 1);	
	       	}
	       	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceProducts')) {
	       		$user->SetPermission('eCommerceProducts', $_POST['chkProducts'] == 1);	
	       	}
	       	if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceTaxes')) {
	       		$user->SetPermission('eCommerceTaxes', $_POST['chkTaxes'] == 1);	
	       	}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceWarranty')) {
				$user->SetPermission('eCommerceWarranty', $_POST['chkWarranties'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceInventory')) {
				$user->SetPermission('eCommerceInventory', $_POST['chkInventory'] == 1);	
			}
			
			//misc
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('ContactInquiries')) {
				$user->SetPermission('ContactInquiries', $_POST['chkContactInquiries'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Partners')) {
				$user->SetPermission('Partners', $_POST['chkPartners'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('LocateLife')) {
				$user->SetPermission('LocateLife', $_POST['chkLocateLife'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Users')) {
				$user->SetPermission('Users', $_POST['chkUsers'] == 1);		
			}
			
			//$user->SetPermission('FeedBack', $_POST['chkFeedBack'] == 1);
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('newsLetterPermissions')) {
				$user->SetPermission('newsLetterPermissions', $_POST['chkNewsLetter'] == 1);	
			}
			
			//settings
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsLocateLife')) {
				$user->SetPermission('SettingsLocateLife', $_POST['chkSettingsLocateLife'] == 1);
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsEmail')) {
				$user->SetPermission('SettingsEmail', $_POST['chkSettingsEmail'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsTrackingEmailText')) {
				$user->SetPermission('SettingsTrackingEmailText', $_POST['chkSettingsTrackingEmailText'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsShippingPickupAddress')) {
				$user->SetPermission('SettingsShippingPickupAddress', $_POST['chkShippingPickupAddress'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('generalEditPermissions')) {
				$user->SetPermission('generalEditPermissions', $_POST['chkEditPermissions'] == 1);	
			}
			if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) {
				$user->SetPermission('eCommercePaymentLogs', $_POST['chkPaymentLogs'] == 1);	
			}
			
       	}
		
		
       	if($user->Validate()) {
       	    $user->Save();
       	}
	}
	

	//update user password
	public function updatePassword($id) {
		$user = User::WithID($id);
		$user -> ConfirmPassword = $_POST['userConfirmPassword'];
        $user->SetPassword($_POST['userNewPassword']);
		
		if($user -> validatePassword()) {
			$user -> SaveNewPassword();
		}
	}
	
	/*
	 * 
	 * TEAM MEMBERS
	 * 
	 */
	public function saveTeamMember($id = false) {
		if($id) {
			$team = Team::WithID($id);
			$team -> newPhoto = $_FILES["profilePic"]["name"];
		} else {
			$team = New Team();
			$team -> photo = $_FILES["profilePic"]["name"];	
		}
		
		
		$team -> firstName = $_POST['teamFirstName'];
		$team -> lastName = $_POST['teamLastName'];
		$team -> description = $_POST['teamDescription'];
		$team -> teamLink = $_POST['teamMemberLink'];
		$team -> jobTitle = $_POST['teamJobTitle'];
		if($team -> validate()) {
			$team -> save();
		}
		
	}
	
	/*
	 * 
	 * PRODUCTS
	 * 
	 */
	 
	 
	 //edit/save product
	 public function saveProduct($category, $id = false) {
	 	if($id) {
	 		$product = Product::WithID($id);	
	 	} else {
	 		$product = New Product();
			$product -> Category = $category;
	 	}
		
		if(!empty($_FILES['productImage']['name'])) {
			$product -> ProductImageMain = Image::renameImage($_FILES['productImage']['name']);	
		}
	 	
		$product -> ProductName = $_POST['productName'];
		$product -> price = $_POST['productPrice'];
		$product -> spec_outer = $_POST['specOuter'];
		
		$product -> description = $_POST['productDescription'];
		$product -> length = $_POST['productLength'];
		$product -> width = $_POST['productWidth'];
		$product -> height = $_POST['productHeight'];
		$product -> weight = $_POST['productWeight'];
		
		$product -> spec_inner = $_POST['specInner'];
		$product -> spec_strap =  $_POST['specStrap'];		
		$product -> spec_cap =  $_POST['specCap'];
		$product -> spec_grommet = $_POST['specGrommet'];
		$product -> spec_volumne =  $_POST['specVolumne'];
		
		if($product -> validate()) {
			$product -> Save();	
		}
		
	 }
	
	//publish product
	public function publishProduct($id) {
		$product = Product::WithID($id);
		$product -> publishState = 1;
		$product -> publishProduct();
	}
	
	//unpublish product
	public function unPublishProduct($id, $table) {
		$product = Product::WithID($id);
		$product -> publishState = 0;
		$product -> unpublishProduct();
	}
	

	/*
	 * 
	 * CATEGORY LOGIC
	 * 
	 */
	 
	
	//edit/save category
	public function saveCategory($id = false) {
		if($id) {
			$category = Category::categoryID($id);	
			$category -> NewCategoryName = $_POST['categoryName'];
			
		} else {
			$category = New Category();	
			$category -> CategoryName = $_POST['categoryName'];
		}
		
		if(!empty($_FILES['categoryImage']['name'])) {
			if($id) {
				$category -> newCategoryImage = $_FILES['categoryImage']['name'];	
			} else {
				$category -> categoryImage = $_FILES['categoryImage']['name'];
			}
				
		}
		
		
		
		$category -> isCustomizable = $_POST['Customizable'];
		if($category -> validate()) {
			$category -> save();
		}
	}
	
	public function deleteCategoryPhoto($id) {
		$category = Category::categoryID($id);
		$category -> deletePhoto();
	}
	
	//re order category list
	public function reOrderList($listType) {
		switch ($listType) {
			case "categories";
				$category = New Category();
				$category -> CategoryID = $_POST['categoryID'];
				$category -> reorderCategories();
				break;
			case "team";
				$team = New Team();
				$team -> teamID = $_POST['teamID'];
				$team -> reorderCategories();
				break;
		}
		
	}
	
	/*
	 * 
	 * WARRANTY LOGIC
	 * 
	 */
	
	//approve warranty
	public function approveWarranty($id) {
		$warranty = Warranty::WithID($id);
		
		$warranty -> companyName = $this -> model -> companyName;
		$warranty -> companyAddress = $this -> model -> companyAddress;
		$warranty -> companyCity = $this -> model -> companyCity;
		$warranty -> companyState = $this -> model -> companyState;
		$warranty -> companyZip = $this -> model -> companyZip;
	
		$warranty -> approveWarranty();
	}
	
	//decline warranty
	public function declineWarranty($id) {
		$warranty = Warranty::WithID($id);
		$warranty -> declineNotes = $_POST['warrantyDeclineNotes'];
		if ($warranty -> validateDeclineNotes()) {
			$warranty -> declineWarranty();	
		}
		
	}
	
	//reset warranty
	public function resetWarrantyStatus($id) {
		$warranty = Warranty::WithID($id);
		$warranty -> resetWarrantyStatus();
	}

	
	/*
	 * 
	 * MISC
	 * 
	 */
	 
    public function generateShipping($id){
        $this -> model -> generateShipping($id);
    }
	
	public function voidShipment($id) {
		$this -> model -> voidShipment($id);
	}
	
	
	public function updateContent($id, $page) {
		$content = Page_Content::WithID($id);
		$content -> contentHeader = $_POST['PageHeader'];
		$content -> content = $_POST['pageContent'];
		if($page == "Warranty") {
			$content -> isWarrantyForm = true;	
			$content -> warrantyQuestion1 = $_POST['WarrantyQuestion1'];
			$content -> warrantyQuestion2 = $_POST['WarrantyQuestion2'];
			$content -> warrantyFormDescription = $_POST['PrintWarrantyDescription'];
			
		}
		$content -> save($page);
	}

	public function uploadImage() {
		$uploadedImage = $_FILES['upload']['name'];
		$this -> model -> uploadImage($uploadedImage);
	}		
	
	public function logout() {
		Session::destroy();
		$this -> redirect -> redirectPage("login");
		exit();
	}
}