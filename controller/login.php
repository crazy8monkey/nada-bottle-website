<?php

class Login extends Controller {
	public function __construct() {
		parent::__construct();
	}	
	public function index() {
		$this -> view -> title = "Nada Bottle: Administration";
		$this -> view -> adminWebsite = "";
		$this -> view -> loginForm = "";
		$this -> view -> css = array("admin/messages.css", "admin/adminLogin.css");
		$this -> view -> js = array(PATH . "public/js/globals.js",
									PATH . "public/js/LoginController.js");
		$this -> view -> startJsFunction = array('LoginController.ValidateCredentials();');
		$this -> view -> render('cms/index');
	}
	
	public function forgotPassword() {
		$this -> view -> title = "Forgot Password";
		$this -> view -> adminWebsite = "";
		$this -> view -> forgotPassword = "";
		$this -> view -> css = array("admin/messages.css", "admin/adminLogin.css");
		$this -> view -> js = array(PATH . "public/js/globals.js",
									PATH . "public/js/LoginController.js");
		$this -> view -> startJsFunction = array('LoginController.validateEmailForgotPassword();');
		$this -> view -> render('cms/index');
	}
	
	public function resetPassword() {
		$user = new User();
		$user -> emailCheck = $_POST['forgotPasswordEmailCheck'];
        
       	if($user->validateEmail()) {
       	    $user->SendEmail();
       	}
	}
	
	public function newPassword() {
		$this -> view -> title = "Reset Password";
		$this -> view -> adminWebsite = "";
		$this -> view -> newPassword = "";
		$this -> view -> css = array("admin/messages.css", "admin/adminLogin.css");
		$this -> view -> js = array(PATH . "public/js/globals.js",
									PATH . "public/js/LoginController.js");
		
		if (isset($_GET['token']) && isset($_GET['userID']))  {		
			$this -> view -> userInfo = $this -> model -> userSingleInfo($_GET['userID']);	
			$this -> view -> startJsFunction = array('LoginController.validateResetPasswordForm();');			
		}

		$this -> view -> render('cms/index');
	}
	
	public function adminLogin() {
		$this -> model -> adminLogin();
	}
	
	public function newPasswordPost() {
		$user = User::WithID($_POST['userID']);
		$user -> ConfirmPassword = $_POST['ConfirmNewPassword'];
        $user->SetPassword($_POST['newPassword']);
		
		if($user -> validatePassword()) {
			$user -> SaveNewPassword(true);
		}	
	}
	
	
	
	
}
