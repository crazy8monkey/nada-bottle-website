<?php

class Support extends Controller {
	
	
	public function __construct() {
		Session::init();
		parent::__construct();
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
		
		$this -> view -> js = array(PATH . "public/js/SupportController.js");
		$this -> view -> emailSignup = "support";
		
	}
	
	public function contact() {
		$this -> view -> title = "Contact Nada Bottle";	
		$this -> view -> mainWebsite = "";
		$this -> view -> showFooter = "";
		$this -> view -> content = Page_Content::WithID(5);
		$this -> view -> css = array('contactForm.css', 'messages.css');
		$this -> view -> startJsFunction = array('SupportController.startContactPage();');
		$this -> view -> render('support/contact');	
	}
	
	public function warranty() {
		Session::init();
		$this -> view -> title = "Warranty";	
		$this -> view -> mainWebsite = "";	
		$this -> view -> showFooter = "";
		$this -> view -> content = Page_Content::WithID(4);//$this -> model -> content();
		$this -> view -> states = $this -> model -> states();
		$this -> view -> warrantySection = "";
		$this -> view -> css = array('warranty.css', 'messages.css');
		$this -> view -> startJsFunction = array('SupportController.startWarrantyPage();');
		$this -> view -> render('support/warranty');
	}

	public function warrantySent() {
		$this -> view -> title = "Warranty";	
		$this -> view -> mainWebsite = "";
		$this -> view -> showFooter = "";	
		$this -> view -> content = Page_Content::WithID(4);
		$this -> view -> states = $this -> model -> states();
		$this -> view -> warrantySent = "";
		$this -> view -> css = array('warranty.css', 'messages.css');
		$this -> view -> render('support/warranty');
	}
	
	public function yourWarranty() {
		$this -> view -> title = "Print Your Warranty";
		$warranty = Warranty::WithID($_GET['NBWID']);	
		
		if($_GET['token'] == $warranty -> securityTokenCheck) {
			$this -> view -> warrantyPrint = $warranty;
			$this -> view -> startJsFunction = array('window.print();');	
		}
		$this -> view -> css = array('warrantyPrint.css');
		$this -> view -> render('support/warrantyPrint');
	}
	
	public function FAQ() {
		$this -> view -> title = "FAQ";	
		$this -> view -> mainWebsite = "";	
		$this -> view -> content = Page_Content::WithID(7);
		$this -> view -> render('support/FAQ');
	}
	
	
	public function contactInquirySubmit() {
		$contact = New Contact();
		$contact -> firstName = $_POST['firstName'];
		$contact -> lastName = $_POST['lastName'];
		$contact -> email = $_POST['email'];
		$contact -> comments = $_POST['comments'];
		$contact -> nadaBottleContactEmail = $this -> model -> nadaBottleContactEmail;
		if($contact -> validate()) {
			$contact -> save();
		}
	}

	public function newWarranty() {
		$warranty = new Warranty();
		$warranty -> photos = $_FILES["warrantyPhoto"]["name"];
		$warranty -> description = $_POST['warrantDescription']; 
		$warranty -> firstName = $_POST['firstName'];
		$warranty -> lastName = $_POST['lastName'];
		$warranty -> email = $_POST['userEmail'];
		$warranty -> phoneNumber = $_POST['userPhoneNumber'];
		$warranty -> address = $_POST['shipAddress'];
		$warranty -> city = $_POST['shipCity'];
		$warranty -> state = $_POST['shipState'];
		$warranty -> zip = $_POST['shipZip'];
		$warranty -> orderNumber = $_POST['OrderNumber'];
		$warranty -> nadaBottleEmail = $this -> model -> nadaBottlEmail;
		$warranty -> verificationID = $_POST['OrderVerificationNumber'];
		
		if($warranty -> validate()) {
			$warranty -> save();
		}		
		
		
	}	

	public function emailSignup() {
		$emailUpdates = New FeedBack();
		$emailUpdates -> email = $_POST['emailUpdates'];
		if($emailUpdates -> validateEmailUpdates()) {
			$emailUpdates ->saveEmail();
		}
	}
	

}//class Newrecipe