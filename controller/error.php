<?php
class Error extends Controller {
	function __construct() {
	
		//calls the Controller class
		parent::__construct();
		
		$this -> view -> js = array(PATH . "public/js/ErrorController.js");
		$this -> view -> startJsFunction = array('ErrorPage.StartDocument();');
	}

	function index() {
		//calls the view class, and
		//the render function
		$this -> view -> css = array("messages.css", "style.css", "error/error-style.css", );
		$this -> view -> title = "404 Oops! Broken Link";
		//the $this->view->categories = $this -> model -> categories(); 
		//couldn't read it so this was the fall back
		
		$this -> view -> render('error/index');

	}
	
	public function insertError() {
		$errorLog = array();
		
		$errorLog['error-name'] = $_POST['errorName'];
		$errorLog['error-email'] = $_POST['errorEmail'];
		$errorLog['error-description'] = $_POST['errorDescription'];
				
		$this -> model -> errorLogInsert($errorLog);
	}

}
?>