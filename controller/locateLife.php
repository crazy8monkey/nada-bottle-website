<?php

Class locateLife Extends Controller {
	
	public function __construct() {
		Session::init();
		parent::__construct();
		$this -> view -> css = array('messages.css','locatelife.css', "jquery.fancybox.css");
		$this -> view -> js = array(PATH . "public/js/LocateLifeController.js", PATH . 'public/js/jquery.fancybox.js');
		$this -> view -> startJsFunction = array('LocateLifeController.StartDocument();');
		if(isset($_SESSION['cart_array'])) {
			$this -> view -> cart = parent::cartCount($_SESSION['cart_array']);
		}
		$this -> view -> emailSignup = "locateLife";
	}
	public function index() {
		$this -> view -> title = "Locate Life";
		$this -> view -> mainWebsite = "";	
		$this -> view -> showFooter = "";
		$this -> view -> content = Page_Content::WithID(6);
		$this -> view -> locateLifePermissions = $this -> model -> locateLifePermissions();
		$this -> view -> render('locateLife/index');
		
	}
	
	public function location() {
		$singleLocation = Locations::WithCoordinates($_GET['Longitude'], $_GET['Latitude']);
		$this -> view -> title = "Locate Life: " . $singleLocation -> name;
		$this -> view -> mainWebsite = "";	
		$this -> view -> showFooter = "";
		$this -> view -> singleLocation = $singleLocation;
		$this -> view -> render('locateLife/singleLocation');
	}
	
	public function getlocation() {
		$location = New Locations();
		
		$location -> longitude = $_POST['longitude'];
		$location -> latitude = $_POST['latitude'];
		
		if($location -> validateLocation()) {
			$location -> getlocation();
		}
	}
	
	public function emailSignup() {
		$emailUpdates = New FeedBack();
		$emailUpdates -> email = $_POST['emailUpdates'];
		if($emailUpdates -> validateEmailUpdates()) {
			$emailUpdates ->saveEmail();
		}
	}
	
	
	
}
