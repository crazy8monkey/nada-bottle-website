<?php

class webHook extends Controller {
	public function __construct() {
        Session::init();
        parent::__construct();

	}	
	
	public function index() {

        $body = @file_get_contents('php://input');
        $jso = json_decode($body);

        $this -> model -> processWebHook($jso);

        $type = $jso->type;
        echo $type;

		$this -> view -> mainWebsite = "";
		$this -> view -> render("ipn/index");
	}


    public function outOfStockReport(){
        $this -> view -> title = "Out Of Stock Report";
        $this -> view -> adminWebsite = "";
        $this -> view -> subMenu = "";
        $this -> view -> adminWebsiteHeader = "";
        $this -> view -> eCommerceSection = "";

        $this -> view -> css = array("admin/messages.css", "admin/taxReport.css");
        $outOfStockItems = $this -> model -> GetOutOfStockItems();
        
        
        //$outOfStockItemCheck = array_filter($outOfStockItems);
		
		
	if(!empty($outOfStockItems)) {
		$inventory = array();
		$inventory['date'] = $this -> model -> todaysDate;
		$inventoryString = "";
		foreach($outOfStockItems as $item) {
			$inventoryString .="<tr><td style='font-size:16px; font-family:arial; color: #9c9c9c; line-height: 24px; font-weight:bold'>" . $item -> ProductName ."</td></tr>";
		
			$inventoryString .="<tr><td style='padding:0px 0px 20px 0px'><table cellpadding='0' cellspacing='0' style='margin:0 auto;' width='450'>";
		
			foreach($item -> OutOfStockColors as $color) {
				$inventoryString .="<tr><td style='padding:5px 0px 0px 0px;' width='30'><div style='background:" . $color -> ColorHexacode ."; width:30px; height:30px;'></div></td><td style='font-size:16px; font-family:arial; color: #9c9c9c; padding:5px 0px 0px 10px'>". $color -> ColorName ."</td></tr>";
				
				
			}
			
			$inventoryString .="</table></td></tr>";
		
			//$inventoryString .= $item -> ProductName;
		}
		print_r($outOfStockItems);
		$inventory['list'] = $inventoryString;
			
	        // ADAM: THIS VIEW CAN BE EMPTY. ADD LOGIC HERE TO SEND THE EMAIL.
	
	        // OUT OF STOCK ITEMS IS:
	        // 1) The ProductId
	        // 2) ProductName
	        // 3) Array of OutOfStockColor
	
	        // OUT OF STOCK COLOR IS:
	        // 1) ColorName
	        // 2) ColorHexacode
	
	        // TO TEST THIS:
	        // Just go to <url>/webHook/outOfStockReport
	
	        // This page will automatically be hit every 24 hours.
	        $inventory['email'] = $this -> model -> inventoryEmail;
	        Email::Inventory($inventory);
	}
        
		
	
    }
	

}