<?php

Class JSONparse {
	
	public function outputJSON($msg, $status = 'error') {
    	die(json_encode(array(
        	'data' => $msg,
        	'status' => $status
    	)));
	}
	
	public function outputJqueryJSONObject($arrayObject, $msg) {
		$jqueryJSONObject = array(); 
		
		$jqueryJSONObject[$arrayObject] = $msg;
		echo json_encode($jqueryJSONObject);
	}
	
	public function multipleJSONOjbects($arrayObjects) {
		$jsonObjects = array($arrayObjects);	
		$jqueryOjbect = array();
		
		ksort($arrayObjects);
		
		foreach($arrayObjects as $key => $value){
		   $jqueryOjbect[$key] = $value;
		}
		echo json_encode($jqueryOjbect);
	}
	
}
