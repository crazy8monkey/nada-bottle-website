<?php

Class Email {
		
	
	public static function BodyStyles() {
		return "padding:30px 0px 30px 0px; margin:0px; background:#white";
	}
	
	public static function emailHeader() {
		return "<tr><td style='text-align: center; padding: 10px 0px 10px 0px;'><img src='" . PATH ."public/images/nadaBottleLogo.png' width='200' height='95' /></td></tr>";
	}
	
	
	public static function contactInquiryContent($content) {
	
		return "<tr><td style='padding:0px 20px 20px 20px; color:#9c9c9c'>". $content . "</td></tr>";
	}
	
	public static function whiteBox() {
		return "width:500px; background:white; border-collapse:collapse; padding:0px; margin:0px auto 0px auto; font-family:arial;";
	}
	
	public static function copyrighInfo() {
		return "<table style='margin:0 auto; padding:5px 0px 5px 0px'><tr><td style='text-align:center; color:#40aadd;'>". date("Y") ." &copy; Nada Bottle, LLC. All Rights Reserved.</td></tr></table>";
	}
	
	
	public static function Contact($to, $fullName, $email, $message) {
		
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$msg = "<html style='padding:0px; margin:0px;'><body style='". Email::BodyStyles() ."'>";
		$msg .="<table style='". Email::whiteBox() ." '>";
		$msg .= Email::emailHeader();
		$msg .= '<tr><td style="font-family:arial;font-size: 24px; color: #40aadd; padding: 0px 20px 0px 20px; font-weight: bold;" align="left">New Contact Inquiry</td>';
		$msg .= Email:: contactInquiryContent("<span style='font-size:20px'>Contact Information</span><br />" . $fullName . '<br />' . 
			$email . "</a><br /><br /><br /><br /><span style='font-size:20px'>Comments / Concerns</span><br />" .
			$message);
		$msg .="</table>";
		$msg .= Email::copyrighInfo();
		$msg .= "</body></html>";
		
		mail($to, "New Contact Inquiry", $msg, $header);
		
	}


	
	public static function receipt($receiptInfo, $toSelf = false) {
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$msg = "<html style='padding:0px; margin:0px;'><body style='backround:white; padding:30px 0px 30px 0px; margin:0px;'>";
		
		$receipt = Orders::WithID($receiptInfo['order-number']);
		
		
		if ($toSelf == true) {
			$msg .= $receipt -> generateReceipt(true, true);
		} else {
			$msg .= $receipt -> generateReceipt(false, true);
		}
		
	
		if ($toSelf == true) {
			$recieptSubject = $receipt -> getFullName() ."'s Receipt";
			$to = $receiptInfo['nada-bottle-email'];
		}
		else {
			$recieptSubject = "Your Receipt";
			$to = $receiptInfo['customer-email'];
		}
		
		mail($to, $recieptSubject, $msg, $header);
	}

	public static function tracking($trackingInfo) {

		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$msg = '<html style="padding:0px; margin:0px;"><body style="backround:white; padding:30px 0px 30px 0px; margin:0px;">';	
		
		$msg .= '<table cellpadding="0" cellspacing="0" width="350" style="margin:0 auto"><tr><td><table align="center" width="350" style="margin:0 auto">';
		$msg .=	'<tr><td style="border-bottom:1px solid #f4f4f4;font-size:16px;color:#40aadd;padding:5px;font-weight:bold;font-family:arial">Shipping / Tracking Information</td></tr>';
		$msg .=	'<tr><td style="font-size:16px;color:#9c9c9c;padding:5px;font-family:arial">Order #: '. $trackingInfo['order-id'] . '</td></tr></table></td></tr><tr>';
		$msg .=	'<td align="center" style="padding:15px 0px 20px 0px"><img src="' . PATH .'public/images/nadaBottleLogo.png" width="200" height="95" ></td></tr>';
		$msg .=	'<tr><td style="font-size:24px;color:#40aadd;font-weight:bold;font-family:arial">Hello '. $trackingInfo['customer-fullname'] .'</td></tr>';
		$msg .=	'<tr><td style="font-size:16px;color:#9c9c9c;font-family:arial;padding:0px 0px 10px 0px;border-bottom:1px solid #f4f4f4">'. $trackingInfo['tracking-email-text']  . '</td></tr>';
		$msg .=	'<tr><td style="font-size:24px;color:#40aadd;font-family:arial;padding:10px 0px 0px 0px">Shipped To</td></tr>';
		$msg .=	'<tr><td style="font-size:16px;color:#9c9c9c;font-family:arial;padding:0px 0px 20px 0px">' . $trackingInfo['customer-shipping-address'] . '</td></tr>';
		
		
		
		$packages = '';
		foreach($trackingInfo['tracking-link'] as $key => $value) {
		$packageNumber = $key + 1;
			if ($trackingInfo['shipmentType'] == 1) {
			
				$link = 'http://wwwapps.ups.com/etracking/tracking.cgi?tracknum='. $value;
				$packages .='<a href="'. $link .'" style="border: 1px solid #c9c9c9 !important; font-weight: bold; background: white !important; color: black !important; text-decoration: none; border-radius: 17px; -webkit-border-radius: 17px; -moz-border-radius: 17px; font-family:arial; font-size:12px; margin-right:5px; padding:8px 11px" target="_blank">Package ' . $packageNumber . '</a>';
			} else if ($trackingInfo['shipmentType'] == 2) {
			
				$link = 'https://www.fedex.com/apps/fedextrack/?tracknumbers='. $value . '&language=en&cntry_code=us';
				$packages .='<a href="'. $link .'" style="border: 1px solid #c9c9c9 !important; font-weight: bold; background: white !important; color: black !important; text-decoration: none; border-radius: 17px; -webkit-border-radius: 17px; -moz-border-radius: 17px; font-family:arial; font-size:12px; margin-right:5px; padding:8px 11px" target="_blank">Package ' . $packageNumber . '</a>';
			}
			
			
		} 
		
		$msg .=	'<tr><td style="padding:0px 0px 15px 0px">'. $packages . '</td></tr></table>';
		$msg .= Email::copyrighInfo();
		$msg .= '</body></html>';
		

		mail($trackingInfo['customer-email'], "Tracking Your Order", $msg, $header);
	}

	public static function NewShippingConfirm($confirmation) {
		
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$msg = '<html style="padding:0px; margin:0px;"><body style="backround:white; padding:30px 0px 30px 0px; margin:0px;">';
		$msg .= '<table cellpadding="0" cellspacing="0" width="400" style="margin:0 auto"><tr><td><table align="center" width="400" style="margin:0 auto">';
		$msg .= '<tr><td style="border-bottom:1px solid #f4f4f4;font-size:16px;color:#40aadd;font-weight:bold; font-family:arial"><table width="400"><tr>';
		$msg .= '<td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 5px 0px; font-weight: bold;" align="left">New Shipping Information</td>';
		$msg .= '<td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 5px 0px; font-weight: bold;" align="right">' . $confirmation['todays-date'] . '</td></tr></table></td></tr>';
		$msg .= '<tr><td style="font-size:16px;color:#9c9c9c;padding:5px; font-family:arial">Order #: ' . $confirmation['order-id'] . '</td></tr></table></td></tr>';
		$msg .= '<tr><td align="center" style="padding:15px 0px 20px 0px"><img src="' . PATH .'public/images/nadaBottleLogo.png" width="200" height="95" /></td></tr>';
		$msg .= '<tr><td style="font-size:24px;color:#40aadd; font-weight:bold; font-family:arial">New Shipping Address</td></tr>';
		
		
		
		if(isset($confirmation['change-shipping-rates'])) {
			$msg .= '<tr><td style="font-size:16px;color:#9c9c9c;font-family:arial; padding:0px 0px 10px 0px; border-bottom:1px solid #f4f4f4;">' . $confirmation['change-shipping-rates'] . '</td></tr>';	
			$padding = "padding:10px 0px"; 
		}
		else {
			$padding = "padding:0px 0px 10px 0px"; 
		}
		
		$msg .= '<tr><td style="font-size:16px;color:#9c9c9c;font-family:arial;' . $padding . '">' . $confirmation['new-address'] . '</td></tr></table>';	
		$msg .= Email::copyrighInfo();
		$msg .= '</body></html>';
		 
	
		mail($confirmation['customer-email'], "New Shipping Address", $msg, $header);
	}

	public static function UserCredentials($userCredentials, $userEmailType) {
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		//1 = new user
		//2 = username updated
		//3 = password updated
		//4 = user deleted 
		$emailHeader = "";
		$MainHeaderText = "";
		
		$UserCredentialsText = "";
		
		
		if($userEmailType == 1) {
			$emailHeader = "User Credentials";
			$MainHeaderText = "Your Login Information";
		} else if($userEmailType == 2) {
			$emailHeader = "Username Update";
			$MainHeaderText = "Your Updated Username";
		} else if($userEmailType == 3) {
			$emailHeader = "Password Update";
			$MainHeaderText = "Your Updated Password";
		} else if($userEmailType == 4) {
			$emailHeader = "Profile Deleted";
			$MainHeaderText = "Account Deleted";
		}
		
		if($userEmailType != 4) {
			$userNameHTML = "";
			$passwordHTML = "";
			
			if($userEmailType == 1) {
				$userNameHTML = '<tr>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c; font-weight:bold; width:120px;">Username:</td>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c;">' . $userCredentials['user-name'] .'</td>
								</tr>';
				$passwordHTML = '<tr>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c; font-weight:bold; width:120px;">Password:</td>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c;">' . $userCredentials['password'] .'</td>
								</tr>';
			
			} else if($userEmailType == 2) {
				$userNameHTML = '<tr>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c; font-weight:bold; width:120px;">Username:</td>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c;">' . $userCredentials['user-name'] .'</td>
								</tr>';
				
			} else if($userEmailType == 3) {
				$passwordHTML = '<tr>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c; font-weight:bold; width:120px;">Password:</td>
								<td style="font-size:16px; font-family:arial; color: #9c9c9c;">' . $userCredentials['password'] .'</td>
								</tr>';
			}
			
			
			$UserCredentialsText = '<tr>
									<td style="font-size:16px; font-family:arial; color: #9c9c9c;line-height: 24px; padding:0px 0px 10px 0px">
									<table>' . $userNameHTML . $passwordHTML . '</table>
									</td>
									</tr>';
		}
		
		$msg = '<html style="padding:0px; margin:0px;"><body style="backround:white; padding:30px 0px 30px 0px; margin:0px;">';
		$msg .= '<table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="500"><tr>';
		$msg .= '<td style="border-bottom: 1px solid #f4f4f4;"><table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="500"><tr><td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;" align="left">' . $emailHeader . '</td></tr></table></td></tr>'; 
		$msg .= '<tr><td style="text-align:center; padding: 20px 0px 30px 0px;" align="center"><img src="' . PATH .'public/images/nadaBottleLogo.png" width="200" height="95" /></td></tr>';
		$msg .= '<tr>
		<td style="padding:0px 0px 10px 0px;">
			<table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="500">
				<tr>
					<td style="font-size:24px;font-weight:bold;color:#40aadd; font-family:arial">' . $MainHeaderText . '</td>
				</tr>' . $UserCredentialsText . '</table></td></tr>';
	
		$msg .= Email::copyrighInfo();
		$msg .= '</body></html>';
				
	
		mail($userCredentials['customer-email'], "User Credential Information", $msg, $header);
	}

	public static function passwordReset($passwordReset) {
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$msg = '<html style="padding:0px; margin:0px;"><body style="backround:white; padding:30px 0px 30px 0px; margin:0px;">';
		
		$msg .= '<table cellpadding="0" cellspacing="0" width="400" style="margin:0 auto"><tr><td style="border-bottom:1px solid #f4f4f4;font-size:16px;color:#40aadd;font-weight:bold; font-family:arial">Reset Passowrd</td></tr>';
		$msg .= '<tr><td align="center" style="padding:15px 0px 20px 0px"><img src="' . PATH .'public/images/nadaBottleLogo.png" width="200" height="95" /></td></tr>';
		$msg .= '<tr><td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 5px 0px; font-weight: bold; font-size:16px;" align="left">Password reset requested</td></tr>';
		$msg .= '<tr><td style="font-size:16px; font-family:arial; color: #9c9c9c;">Forgot your password? No need to worry, please click <a href="' . $passwordReset['reset-link'] . '" target="_blank">here</a> to reset your password.</td></tr></table>';
		$msg .= Email::copyrighInfo();
		$msg .= '</body></html>';
		
		
		mail($passwordReset['customer-email'], "Reset Password", $msg, $header);
	}

	public static function Inventory($inventory) {
		$header = 'From: Nada Bottle <NadaBottle@Inventory.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$msg = '<html style="padding:0px; margin:0px;"><body style="backround:white; padding:30px 0px 30px 0px; margin:0px;">';
		$msg .= '<table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="450"><tr><td style="border-bottom: 1px solid #f4f4f4;">';
		$msg .= '<table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="450"><tr><td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;" align="left">Inventory</td>';
		$msg .= '<td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;" align="right">'. $inventory['date'] . '</td></tr></table></td></tr>';
		$msg .= '<tr><td align="center" style="padding: 20px 0px 30px 0px"><img src="' . PATH .'public/images/nadaBottleLogo.png" width="200" height="95" /></td></tr>';
		$msg .= '<tr><td style="padding:0px 0px 5px 0px;"><table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="450"><tr><td style="font-size:24px;font-weight:bold;color:#40aadd; font-family:arial">Out of Stock</td></tr></table></td></tr>';
		$msg .= '<tr><td style="padding:0px 0px 0px 0px;"><table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="450">' . $inventory['list'] . '</table></td></tr>';
		$msg .= '</table>';
		$msg .= Email::copyrighInfo();
		$msg .= '</body></html>';
		
		
		mail($inventory['email'], "Inventory", $msg, $header);
	}


	public static function StripeError($to, $msg) {
		
		$header = 'From: Nada Bottle <NadaBottle@stripeerror.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$messageError = "<table cellpadding='0' cellspacing='5'>" . $msg ."</table>";
		
		mail($to, "Nada Bottle Website Error", $messageError, $header);
	}
	
	public static function WebHook($to, $msg, $subject) {
		
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$messageError = "<table cellpadding='0' cellspacing='5'>" . $msg ."</table>";
		
		mail($to, "Payment Details - Order #:" . $subject, $messageError, $header);
	}

	public static function UPSValidationError($to, $msg) {
		
		$header = 'From: Nada Bottle <NadaBottle@UPSError.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$messageError = "<table cellpadding='0' cellspacing='5'>" . $msg ."</table>";
		
		mail($to, "Nada Bottle Checkout Error", $messageError, $header);
	}
	
	public static function sendErrorEmail($msg) {
		$header = 'From: Nada Bottle <NadaBottle@404Error.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$messageError = "<html><body><table cellpadding='0' cellspacing='5'>" . $msg ."</table></body></html>";
		
		mail(Email::EmailErrorRecipients(), "Nada Bottle 404 error", $messageError, $header);
	}
	
	public static function warrantyEmail($warranty, $warrantyEmailType, $toSelf = false) {
		$emailHeader = "";	
		$headerText = "";
		$emailText = "";
		$showEmail = false;
		$showDeclineNotes = false;
		$padding ="";
		switch ($warrantyEmailType) {
			//if 0 warranty request sent
			case 0;
				$emailHeader = "Your Warranty";
				$headerText = "Warranty Request Sent";	
				$emailText = "Thank you for submitting your evidence to your warranty, We will look into this for further review, and you will be notified on to the next step.<br /><br />";
				break;
			//if 1 warranty request approved
			case 1;
				$emailHeader = "Warranty Approved";
				$headerText = "Warranty Approved";	
				$emailText = "Your warranty has been approved! The next following step is to mail your product back to Nada Bottle.<br />";
				$showEmail = true;
				$padding = "padding:0px 0px 10px 0px";
				break;
			//if 2 warranty request decline
			case 2;
				$emailHeader = "Warranty Declined";
				$headerText = "Warranty Declined";
				$emailText = "Your warranty has been declined. Here are the comments from our reviewer";
				$showDeclineNotes = true;
				
				if($showDeclineNotes) {
					$emailText .= '<table cellpadding="0" cellspacing="0" style="margin:10px auto" width="350"><tr><td style="border-top:1px solid #e9e9e9; border-bottom:1px solid #e9e9e9; padding:10px 0px;font-size:16px;font-family:arial;color:#9c9c9c;line-height:24px"><strong>Reason for decling:</strong><br />'. $warranty['declined-notes'] .'</td></tr></table>';
				}
				
					
				$padding = "padding:0px 0px 10px 0px";
				break;
			//if 3 warranty request reset
			case 3;
				$emailHeader = "Warranty being Reviewed";
				$headerText = "Warranty Re-Reviewing";	
				$emailText = "Your warranty is being reviewed again to make sure nothing was missed<br />";
				break;
		}
		
		$header = 'From: Nada Bottle <NadaBottle@DoNotReply.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$msg = '<html style="padding:0px; margin:0px;"><body style="backround:white; padding:0px; margin:0px;">';
		
		if ($toSelf == true) {
			$msg .= "<table width='350' align='center' sytle='padding: 5px 0px 10px 0px;'><tr><td style='border:2px dashed #40aadd; text-align:center; padding:5px; color:#40aadd; background: #DAF3FF; font-weight: bold; font-size: 16px;font-family:arial'> Warranty #:". $warranty['warranty-id'] . " / New Warranty</td></tr></table>";
		}
		$msg .= '<table width="350" align="center" style="padding: 5px 0px 10px 0px;">';
		$msg .= '<tr><td style="border-bottom: 1px solid #f4f4f4;"><table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="350">';
		$msg .= '<tr><td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;" align="left">' . $emailHeader .'</td>';
		$msg .= '<td style="font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;" align="right">' . $warranty['todays-date'] . '</td></tr></table></td></tr>';
		$msg .= '<tr><td style="font-family:arial; font-size: 16px; color: #9c9c9c; padding: 6px;">Warranty #: ' . $warranty['warranty-id'] . '</td></tr>';
		$msg .= '<tr><td style="text-align:center; padding: 20px 0px 30px 0px;" align="center"><img src="' . PATH .'public/images/nadaBottleLogo.png" width="200" height="95" /></td></tr>';	
		$msg .= '<tr><td style="padding:0px 0px 10px 0px;"><table cellpadding="0" cellspacing="0" style="margin:0 auto;" width="350">';
		$msg .= '<tr><td style="font-size:24px;font-weight:bold;color:#40aadd; font-family:arial">'. $headerText .'</td></tr>';
		$msg .= '<tr><td style="font-size:16px; font-family:arial; color: #9c9c9c;line-height: 24px; ' . $padding . '">Hi ' . $warranty['warrantee-name'] . ', ' . $emailText . '<br />Thank you for being a Nada Bottle Customer!</td></tr></table></td></tr>';	
		if($warrantyEmailType == 0) {
			$msg .=	'<tr><td style="padding:25px 0px 15px 0px; border-top: 1px solid #f4f4f4;"><a href="'. $warranty['warrantyPrintLink'] .'" style="background:#40aadd;border-radius:4px;text-decoration:none;padding:10px 20px; display:inline; font-size:16px;color:white;font-family:arial" target="_blank">Print Warranty</a></td></tr>';	
		}
		
		
		
		if($showEmail) {
			$msg .= '<tr>
							<td style="border-top:1px solid #e9e9e9; padding:10px 0px 0px 0px; font-size:16px; font-family:arial; color: #9c9c9c;line-height: 24px;">
								<strong>Mail your product to the following address:</strong><br />'.
								 $warranty['company-name'] . '<br />' .
								$warranty['company-address'] .'<br />' .
								$warranty['company-city'] . ', ' . $warranty['company-state'] . ' ' . $warranty['company-zip'] .
							'</td>
						</tr>';
			
		}
		
		$msg .= Email::copyrighInfo();
		$msg .= '</body></html>';	
		
		if ($toSelf) {
			$to = $warranty['nada-bottome-email'];
			$finalEmailHeader = "New Warranty";
		}
		else {
			$to = $warranty['email'];
			 $finalEmailHeader = $emailHeader;
		}
		
		mail($to, $finalEmailHeader, $msg, $header);
	}
	
	public static function FeedBack($email) {
		$header = 'From: Nada Bottle <NadaBottle@Feedback.com>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$messageError = "<html><body><table cellpadding='0' cellspacing='5'>Yout got a feedback entry</table></body></html>";
		
		mail($email, "Nada Bottle New FeedBack", $messageError, $header);
	}
	
	public static function EmailErrorRecipients() {
		return "adamschmidt22@gmail.com";
		//, dan.j.drews@gmail.com
	}
	
	public static function verifyEmailAddress($emailInput) {
		return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$emailInput);
	}
}