<?php

class Controller {

    public function __construct() {
        $this->view = new View();
		$this -> security = new Security();
		$this -> redirect = new Redirect();		
    }
    
	public function cartCount($cartCount) {
		
		$cartCountString = NULL;	
		if(count($cartCount) > 1) {
			$cartCountString = count($cartCount) . " Items";
		}
		else {
			$cartCountString = count($cartCount) . " Item";
		}				
		
		return $cartCountString;
	}
	

	
	
    /**
     * 
     * @param string $name Name of the model
     * @param string $path Location of the models
     */
    public function loadModel($name, $modelPath = 'models/') {
        
        $path = $modelPath . $name.'_model.php';
        
        if (file_exists($path)) {
            require $modelPath .$name.'_model.php';
            
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }        
    }

}