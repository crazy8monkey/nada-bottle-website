<?php

class UPS
{
	private $accessNumber;
	private $userid;
   	private $passwd;
	private $UPSShipperNumber;
	//address validation
	private $AddressValidationWsdl;
	private $AddressValidationEndpointurl;
	//shipping rate
	private $shippingRateWsdl;
	private $ShippingRatesEndpointurl;
	//create shipment
	private $createShipmentWsdl;
	private $createShipmentEndpointurl;
	//void shipment
	private $voidShipmentWsdl;
	private $voidShipmentEndpointurl;

	//nada bottle company address
	private $companyName;
	private $companyAddress;
	private $companyCity;
	private $companyState;
	private $companyZip;
	
	private $cartItems;
	
	private $soapClient;

    public function shipmentType(){
        return 1;
    }

	//set Configuration
	public function __construct($companyName, $companyAddress, $companyCity, $companyState, $companyZip, $companyPhone) {
		$this -> accessNumber = UPS_ACCESS_KEY;
		$this -> userid = UPS_USERNAME;
		$this -> passwd = UPS_PASSWORD;
		$this -> UPSShipperNumber = UPS_SHIPPER_NUMBER;
		//address validation
		$this -> AddressValidationWsdl = PATH . "public/UPSWsdl/XAV.wsdl";
		$this -> AddressValidationEndpointurl = UPS_ENDPOINTURL_ADDRESS_VALIDATION;
		//shipping rates
		$this -> shippingRateWsdl = PATH . "public/UPSWsdl/RateWS.wsdl";
		$this -> ShippingRatesEndpointurl = UPS_ENDPOINTURL_SHIPPING_RATES;
		
		$this -> createShipmentWsdl = PATH . "public/UPSWsdl/Ship.wsdl";
		$this -> createShipmentEndpointurl = UPS_ENDPOINTURL_CREATE_SHIPMENT;
		
		$this -> voidShipmentWsdl = PATH . "public/UPSWsdl/Void.wsdl";
		$this -> voidShipmentEndpointurl = UPS_ENDPOINTURL_VOID_SHIPMENT;
		
		$this -> companyName = $companyName;
		$this -> companyAddress = $companyAddress;
		$this -> companyCity = $companyCity;
		$this -> companyState = $companyState;
		$this -> companyZip = $companyZip;
	}
	
	private function initiateSOAPClient($wsdl, $endpointURL) {
		$mode = array ('soap_version' => 'SOAP_1_1',  // use soap 1.1 client
	            	   'trace' => 1);
					   
		 // initialize soap client
	     $this -> soapClient = new SoapClient($wsdl , $mode);
		 
		 //set endpoint url
	     $this -> soapClient ->__setLocation($endpointURL);
		 
		 //create soap header
	     $usernameToken['Username'] = $this -> userid;
	     $usernameToken['Password'] = $this -> passwd;
	     $serviceAccessLicense['AccessLicenseNumber'] = $this -> accessNumber;
	     $upss['UsernameToken'] = $usernameToken;
	     $upss['ServiceAccessToken'] = $serviceAccessLicense;
		 
		 $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
	     $this -> soapClient ->__setSoapHeaders($header);
		 
	}

	// Should be called "IsValidEnough" address. UPS's service kind of sucks. Especially when it comes to apartments
	public function isValidAddress($address1, $address2, $City, $State, $Zip) {		    
	    $operation = "ProcessXAV";
	    $outputFileName = "XOLTResultValidation.xml";
		
	    $result = 0;
	    try {
	    	
			$this -> initiateSOAPClient($this -> AddressValidationWsdl, $this -> AddressValidationEndpointurl);
		
	        //get response
	        $resp = $this -> soapClient ->__soapCall($operation ,array($this -> processXAV($address1, $address2, $City, $State, $Zip)));
		
	        //get status
	        if($resp->Response->ResponseStatus->Description == "Success") {		
		    	if(empty($resp->Candidate)) {
		        	$result = 0;
		        } else {
		        	// It is an empty (but existent) XML node. This serializes via JSON to be ""
		        	if(json_encode($resp->ValidAddressIndicator) == '""') {
		            	$result = 1;
		            // Ambiguous Address
					} else {
		            	$result = 2; // We will call this good for now and twe
		            }
		    	}
		    } else {
		    	$result = 3;
			}
		
			//save soap request and response to file
			$fw = fopen($outputFileName , 'w');
			fwrite($fw , "Request: \n" . $this -> soapClient ->__getLastRequest() . "\n");
			fwrite($fw , "Response: \n" . $this -> soapClient ->__getLastResponse() . "\n");
			fclose($fw);
		
		} catch(Exception $ex) {
			$result = 3;
			
			$TrackError = new EmailServerError();
			$TrackError -> message = var_export($ex, true);
			$TrackError -> type = "UPS VALIDATION ERROR";
			$TrackError -> SendMessage();

		}
		
		return $result;
	}
	//part of UPS Address Validation
	private function processXAV($address1, $address2, $City, $State, $Zip) {
	    //create soap request
	    $option['RequestOption'] = '1';
	    $request['Request'] = $option;
	
	
	    $addrkeyfrmt['AddressLine'] = array
	    (
	        $address1
	    );
	
	    if($address2 === NULL)
	    {
	        $addrkeyfrmt['AddressLine'] = array
	        (
	          $address1,
	            $address2
	        );
	    }
	
	    //$addrkeyfrmt['Region'] = 'GRETNA,NE,68028';
	    $addrkeyfrmt['PoliticalDivision1'] = $State;
	    $addrkeyfrmt['PoliticalDivision2'] = $City;
	    $addrkeyfrmt['PostcodePrimaryLow'] = $Zip;
	    $addrkeyfrmt['CountryCode'] = 'US';
	    $request['AddressKeyFormat'] = $addrkeyfrmt;
	
	    return $request;
	}
	
	//get Shipping Rate Section
	public function getShippingRate($shipToName, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $shipToPhone, $boxes = array(), $cmsChange = false, $serviceCode = false) {
        $operation = "ProcessRate";
        $outputFileName = "XOLTResultRate.xml";

		$this -> initiateSOAPClient($this -> shippingRateWsdl, $this -> ShippingRatesEndpointurl);

        // Call UPS API to get response
        $resp = $this -> soapClient ->__soapCall($operation, array($this -> processRate($shipToName, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $boxes)));

	if($cmsChange == true) {
		$result = "";
	} else {
		$result = array();
	}
        
         if ($resp->Response->ResponseStatus->Description == 'Success') {
             // Loop through each Result that UPS gives us. Find the matching code in the Database
             // If it is active in the database, add it to the Response list
            foreach ($resp->RatedShipment as $shipment) {
                try {
                        if($cmsChange == true) {
                            if($shipment->Service->Code == $serviceCode) {
                                $result = $shipment->TotalCharges->MonetaryValue;
                            }
                        } else {
                            $serviceCode = Shipments::WithServiceCode($shipment->Service->Code, "UPS");
                            if ($serviceCode->shippingMethodActive == 1) {
                                                array_push($result, array('code' => $shipment->Service->Code,
                                                    'description' => $serviceCode->shippingMethodName,
                                                    'price' => $shipment->TotalCharges->MonetaryValue));
                            }
                        }
                }catch (Exception $e){
                	 // I want to ignore this case. Likely a new service code was added that is not in our database.
                    // We don't want this call to fail because of it
                    // We can set it up to send an email if necessary
                	$TrackError = new EmailServerError();
					$TrackError -> message = var_export($e, true);
					$TrackError -> type = "UPS RATES ERROR";
					$TrackError -> SendMessage();
                }
            }
        }

        //save soap request and response to file. This is for logging purposes.
        $fw = fopen($outputFileName, 'w');
        fwrite($fw, "Request: \n" . $this -> soapClient ->__getLastRequest() . "\n");
        fwrite($fw, "Response: \n" . $this -> soapClient ->__getLastResponse() . "\n");
        fclose($fw);
        return $result;
    }

	private function processRate($shipToName, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $boxes = array()) {
        //create soap request
        $option['RequestOption'] = 'Shop';
        $request['Request'] = $option;

        $pickuptype['Code'] = '01';
        $pickuptype['Description'] = 'Daily Pickup';
        $request['PickupType'] = $pickuptype;

        $customerclassification['Code'] = '00';
        $customerclassification['Description'] = 'Classfication';
        $request['CustomerClassification'] = $customerclassification;

        $shipper['Name'] = $this -> companyName;
        $shipper['ShipperNumber'] = $this -> UPSShipperNumber;
        $address['AddressLine'] = array
        (
        	$this -> companyAddress
            //'516 3rd St'
        );
        $address['City'] = $this -> companyCity;//"Crested Butte";
        $address['StateProvinceCode'] = $this -> companyState;//"CO";
        $address['PostalCode'] = $this -> companyZip; //"81224"; 
        $address['CountryCode'] = 'US';
        $shipper['Address'] = $address;
        $shipment['Shipper'] = $shipper;

        $shipto['Name'] = $shipToName;
        if (is_null($shipToAddress2)) {
            $addressTo['AddressLine'] = $shipToAddress1;
        } else {
            $addressTo['AddressLine'] = array($shipToAddress1, $shipToAddress2);
        }

        $addressTo['City'] = $shipToCity;
        $addressTo['StateProvinceCode'] = $shipToState;
        $addressTo['PostalCode'] = $shipToZip;
        $addressTo['CountryCode'] = 'US';
        $addressTo['ResidentialAddressIndicator'] = '';
        $shipto['Address'] = $addressTo;
        $shipment['ShipTo'] = $shipto;

        $service['Code'] = '03';
        $service['Description'] = 'Service Code';
        $shipment['Service'] = $service;

		
		
		
		$packagesFinal = array();
		foreach($boxes as $value) {
			$packaging['Code'] = '02';
	        $packaging['Description'] = 'Rate';
	        $package['PackagingType'] = $packaging;
	        $dunit['Code'] = 'IN';
	        $dunit['Description'] = 'inches';
	        $dimensions['Length'] = $value['length'];
	        $dimensions['Width'] = $value['width'];
	        $dimensions['Height'] = $value['height'];
	        $dimensions['UnitOfMeasurement'] = $dunit;
	        $package['Dimensions'] = $dimensions;
	        $punit['Code'] = 'LBS';
	        $punit['Description'] = 'Pounds';
	        $packageweight['Weight'] = $value['weight'];
	        $packageweight['UnitOfMeasurement'] = $punit;
	        $package['PackageWeight'] = $packageweight;


            array_push($packagesFinal, $package);

		}
        
	    $shipment['Package'] = $packagesFinal;
	    $shipment['ShipmentServiceOptions'] = '';
        $shipment['LargePackageIndicator'] = '';
        $request['Shipment'] = $shipment;
        return $request;
    }

	//create shipment section
	public function processShipment($name, $addressLine1, $addressLine2, $city, $state, $zip, $serviceCode, $boxes = array()) {
	    $operation = "ProcessShipment";
	    $outputFileName = "XOLTResult.xml";
	
	    try {
	    		
	    	$this -> initiateSOAPClient($this -> createShipmentWsdl, $this -> createShipmentEndpointurl);
			
		
	        if (strcmp($operation, "ProcessShipment") == 0) {
	            //get response
	            $resp = $this -> soapClient ->__soapCall('ProcessShipment', array($this -> createShipment($name, $addressLine1, $addressLine2, $city, $state, $zip, $serviceCode, $boxes)));

	            //save soap request and response to file
	            $fw = fopen($outputFileName, 'w');
	            fwrite($fw, "Request: \n" . $this -> soapClient ->__getLastRequest() . "\n");
	            fwrite($fw, "Response: \n" . $this -> soapClient ->__getLastResponse() . "\n");
	            fclose($fw);
	            return $this -> soapClient ->__getLastResponse();
	        } else if (strcmp($operation, "ProcessShipConfirm") == 0) {
	            //get response
	            $resp = $this -> soapClient->__soapCall('ProcessShipConfirm', array(processShipConfirm()));
	
	            //get status
	            echo "Response Status: " . $resp->Response->ResponseStatus->Description . "\n";
	
	            //save soap request and response to file
	            $fw = fopen($outputFileName, 'w');
	            fwrite($fw, "Request: \n" . $this -> soapClient ->__getLastRequest() . "\n");
	            fwrite($fw, "Response: \n" . $this -> soapClient ->__getLastResponse() . "\n");
	            fclose($fw);
	            return $this -> soapClient ->__getLastResponse();
	        } else {
	            $resp = $this -> soapClient ->__soapCall('ProcessShipeAccept', array(processShipAccept()));
	
	            //get status
	            echo "Response Status: " . $resp->Response->ResponseStatus->Description . "\n";
				
	            //save soap request and response to file
	            $fw = fopen($outputFileName, 'w');
	            fwrite($fw, "Request: \n" . $this -> soapClient ->__getLastRequest() . "\n");
	            fwrite($fw, "Response: \n" . $this -> soapClient ->__getLastResponse() . "\n");
	            fclose($fw);
	            return $this -> soapClient ->__getLastResponse();
	        }
	
	    } catch (Exception $ex) {
	    	$TrackError = new EmailServerError();
			$TrackError -> message = var_export($ex, true);
			$TrackError -> type = "UPS CREAT SHIPMENT ERROR";
			$TrackError -> SendMessage();
			
			
	        print_r($ex);
	    }
	}

	private function getShipFrom(){
	    $shipfrom['Name'] = $this -> companyName;
	    $addressFrom['AddressLine'] = $this -> companyAddress;
	    $addressFrom['City'] = $this -> companyCity;
	    $addressFrom['StateProvinceCode'] = $this -> companyState;
	    $addressFrom['PostalCode'] = $this -> companyZip;
	    $addressFrom['CountryCode'] = 'US';
	    $shipfrom['Address'] = $addressFrom;
	    return $shipfrom;
	}
	
	private function createShipment($name, $addressLine1, $addressLine2, $city, $state, $zip, $serviceCode, $boxes = array()) {
	    // create soap request
	    $requestoption['RequestOption'] = 'nonvalidate';
	    $request['Request'] = $requestoption;
	
	    $shipment['Description'] = $this -> companyName;
	    $shipper['Name'] = $this -> companyName; //'Nada Bottle';
	    $shipper['ShipperNumber'] = $this -> UPSShipperNumber;
	    $address['AddressLine'] = $this -> companyAddress;
	    $address['City'] = $this -> companyCity;
	    $address['StateProvinceCode'] = $this -> companyState;
	    $address['PostalCode'] = $this -> companyZip;
	    $address['CountryCode'] = 'US';
	    $shipper['Address'] = $address;
	    $shipment['Shipper'] = $shipper;
	
	    $shipto['Name'] = $name;
	    $addressTo['AddressLine'] = array($addressLine1, $addressLine2);
	    $addressTo['City'] = $city;
	    $addressTo['StateProvinceCode'] = $state;
	    $addressTo['PostalCode'] = $zip;
	    $addressTo['CountryCode'] = 'US';
	    $shipto['Address'] = $addressTo;
	    $shipment['ShipTo'] = $shipto;
	
	    $shipment['ShipFrom'] = $this -> getShipFrom();
	
	    $shipmentcharge['Type'] = '01';
	    $billshipper['AccountNumber'] = $this -> UPSShipperNumber;
	    $shipmentcharge['BillShipper'] = $billshipper;
	    $paymentinformation['ShipmentCharge'] = $shipmentcharge;
	    $shipment['PaymentInformation'] = $paymentinformation;

        $code = Shipments::WithServiceCode($serviceCode, "UPS");
	    $service['Code'] = $serviceCode;
	    $service['Description'] = $code->shippingMethodName;
	    $shipment['Service'] = $service;
	
	
		
	
		
		$packagesFinal = array();
		foreach($boxes as $value) {
				
			$package['Description'] = '';
	    	$packaging['Code'] = '02';
	    	$packaging['Description'] = 'Nails';
	    	$package['Packaging'] = $packaging;
	    	$unit['Code'] = 'IN';
	    	$unit['Description'] = 'Inches';
	    	$dimensions['UnitOfMeasurement'] = $unit;
	    	$dimensions['Length'] = $value['length'];
	    	$dimensions['Width'] = $value['width'];
	    	$dimensions['Height'] = $value['height'];
	    	$package['Dimensions'] = $dimensions;
	    	$unit2['Code'] = 'LBS';
	    	$unit2['Description'] = 'Pounds';
	    	$packageweight['UnitOfMeasurement'] = $unit2;
	    	$packageweight['Weight'] = $value['weight'];
	    	$package['PackageWeight'] = $packageweight;
					
			
			array_push($packagesFinal, $package);
		}
		
		
		
		
	    
	    $shipment['Package'] = $packagesFinal;
	
	    $labelimageformat['Code'] = 'GIF';
	    $labelimageformat['Description'] = 'GIF';
	    $labelspecification['LabelImageFormat'] = $labelimageformat;
	    $labelspecification['HTTPUserAgent'] = 'Mozilla/4.5';
	    $shipment['LabelSpecification'] = $labelspecification;
	    $request['Shipment'] = $shipment;
	
	    return $request;
		
		
		//   $shipment['Package'] = $packagesFinal;
	    //$shipment['ShipmentServiceOptions'] = '';
        //$shipment['LargePackageIndicator'] = '';
        //$request['Shipment'] = $shipment;
        //return $request;
		
	}


	public function voidShipment($trackingNumber) {
		//Configuration
  		$operation = "ProcessVoid";
  		$outputFileName = "XOLTVoidShipment.xml";
		
		
	  	try
		  {
			
			$this -> initiateSOAPClient($this -> voidShipmentWsdl, $this -> voidShipmentEndpointurl);
		
		
		    //get response
		  	$resp = $this -> soapClient ->__soapCall($operation ,array($this -> processVoid($trackingNumber)));
		
		    //get status
		    echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
		
		    //save soap request and response to file
		    $fw = fopen($outputFileName , 'w');
		    fwrite($fw , "Request: \n" . $this -> soapClient ->__getLastRequest() . "\n");
		    fwrite($fw , "Response: \n" . $this -> soapClient ->__getLastResponse() . "\n");
		    fclose($fw);
		
		  }
		  catch(Exception $ex)
		  {
		  	
			$TrackError = new EmailServerError();
			$TrackError -> message = var_export($ex, true);
			$TrackError -> type = "UPS CREAT SHIPMENT ERROR";
			$TrackError -> SendMessage();
			
		  	print_r ($ex);
		  }
	}

	private function processVoid($trackingNumber) {
	      //create soap request
	    $tref['CustomerContext'] = 'Add description here';
	    $req['TransactionReference'] = $tref;
	    $request['Request'] = $req;
		
		//tracking number
	    $voidshipment['ShipmentIdentificationNumber'] = $trackingNumber;
	    $request['VoidShipment'] = $voidshipment;
	
	    echo "Request.......\n";
		print_r($request);
	    echo "\n\n";
	    return $request;
	  }

	
	
}