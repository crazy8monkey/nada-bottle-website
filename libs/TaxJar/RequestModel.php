<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 4/27/15
 * Time: 5:46 PM
 */

class TaxJarRequest
{
    function GetTaxes($amount, $shipping, $country, $state, $city, $zip)
    {
        $ch = curl_init();
        $url = 'https://api.taxjar.com/sales_tax/?amount=' . $amount . '&shipping=' . $shipping . '&from_city=&from_state=&from_country=&from_zip=&to_city=' .
        urlencode($city) . '&to_state=' . urlencode($state) . '&to_zip=' . urlencode($zip) . '&to_country=' . urlencode($country);
        curl_setopt($ch, CURLOPT_URL, $url);
        $headers = array();
        $headers[] = 'Authorization: Token token="' . TAXJAR_API . '"';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

}