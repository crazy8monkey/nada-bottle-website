<?php

//http://www.pontikis.net/tip/?id=18

class Time {
	
	public $timeZoneString;	
	private $coloradoTime;
	
	public function getTimeZone($state, $zip) {
		//Eastern ........... America/New_York
		//Central ........... America/Chicago
		//Mountain .......... America/Denver
		//Mountain no DST ... America/Phoenix
		//Pacific ........... America/Los_Angeles
		//Alaska ............ America/Anchorage
		//Hawaii ............ America/Adak
		//Hawaii no DST ..... Pacific/Honolulu
		
					
					 
		
		
		switch ($state) {
				
			//case "OR";
			//oregon has two
			
			//case "ID";
			//idao has two
			
			//case "ND";
			//north dakota has two
			
			//case "SD";
			//south dakota has two
			
			//case "NE"
			//nebraska has two
			
			//case "TX"
			//texas has two
			
			//case "IN"
			//indiana has two
			
			//case "TN"
			//tennesse has two
			
			//case "FL"
			//florida has two
			
			//case "KY"
			//kentucky has two
			
			//pacific time
			case "CA"; case "WA"; case "NV";
				$this -> timeZoneString = "America/Los_Angeles";
				break;
			//Mountain time
			case "MT"; case "WY"; case "UT"; case "NM"; case "CO";
				$this -> timeZoneString = "America/Denver";
				break;
			//Central time
			case "KS"; case "OK"; case "MN"; case "IA"; case "MO"; case "AR";
			case "LA"; case "WI"; case "IL"; case "MS"; case "AL";
				$this -> timeZoneString = "America/Chicago";
				break;
			//Eastern time
			case "MI"; case "OH"; case "GA"; case "SC"; case "NC"; case "WV";
			case "PA"; case "MA"; case "MD"; case "DC"; case "ME"; case "NH";
			case "NJ"; case "RI"; case "VT"; case "VA"; case "CT"; case "DE";
				$this -> timeZoneString = "America/New_York";
				break;
			//Mountain no Daylight Savings
			case "AZ";
				$this -> timeZoneString = "America/Phoenix";
				break;
			//Alaska
			case "AK";
				$this -> timeZoneString = "America/Anchorage";
				break;
			case "HI";
				$this -> timeZoneString = "Pacific/Honolulu";
				break;
		}
		
	}
		
	public function coloradoTime() {
		$this -> coloradoTime = new DateTime("now", new DateTimeZone('America/Denver'));		
		return $this -> coloradoTime -> getTimestamp() + $this -> coloradoTime -> getOffset();
	}
	
	public function formatDate($value) {
		$myDateTime = DateTime::createFromFormat('Y-m-d', $value);
		$formatedDate = $myDateTime->format('F j, Y');
		return $formatedDate;
	}
	
	public function getYear($pdate) {
	    $date = DateTime::createFromFormat("Y-m-d", $pdate);
	    return $date->format("Y");
	}
	
	public function now() {
 
  		// set timezone to user timezone
  		date_default_timezone_set('America/Denver');
 
	  	$date = new DateTime('now');
	  	$date->setTimezone(new DateTimeZone(CONST_SERVER_TIMEZONE));
	  	$str_server_now = $date->format(CONST_SERVER_DATEFORMAT);
	 
	  	// return timezone to server default
	  	//date_default_timezone_set(CONST_SERVER_TIMEZONE);
	 
	  	return $str_server_now;
	}
	
	
	
}

?>