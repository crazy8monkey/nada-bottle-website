<?php

require_once('FedexCommon.php');

class FedEx
{
    //address validation
    private $AddressValidationWsdl;
    private $AddressValidationEndpointurl;
    //shipping rate
    private $ShippingRateWsdl;
    private $ShippingRatesEndpointurl;
    //create shipment
    private $CreateShipmentWsdl;
    private $CreateShipmentEndpointurl;
    //void shipment
    private $voidShipmentWsdl;
    private $voidShipmentEndpointurl;

    //nada bottle company address
    private $companyName;
    private $companyAddress;
    private $companyCity;
    private $companyState;
    private $companyZip;
    private $companyPhone;
   

    private $soapClient;
    private $req;

    public function shipmentType(){
        return 2;
    }

    //set Configuration
    public function __construct($companyName, $companyAddress, $companyCity, $companyState, $companyZip, $companyPhone) {
        $this -> AddressValidationWsdl = PATH . "public/FedexWsdl/AddressValidationServiceTEST.wsdl";
        $this -> AddressValidationEndpointurl = FEDEX_ENDPOINTURL;
        //shipping rates
        $this -> ShippingRateWsdl = PATH . "public/FedexWsdl/RateServiceTEST.wsdl";
        $this -> ShippingRatesEndpointurl = FEDEX_ENDPOINTURL;

        $this -> CreateShipmentWsdl = PATH . "public/FedexWsdl/ShipServiceTEST.wsdl";
        $this -> CreateShipmentEndpointurl = FEDEX_ENDPOINTURL;

        $this->companyName = $companyName;
        $this->companyAddress = $companyAddress;
        $this->companyCity = $companyCity;
        $this->companyState = $companyState;
        $this->companyZip = $companyZip;
      	$this -> companyPhone = $companyPhone;
    }

    private function getClient($wsdl){
        return new SoapClient($wsdl, array('trace' => 1));
    }

    private function setupAuthentication(){
        $this->req['WebAuthenticationDetail'] = array(
            'UserCredential' => array(
                'Key' => getProperty('key'),
                'Password' => getProperty('password')
            )
        );
        $this->req['ClientDetail'] = array(
            'AccountNumber' => getProperty('shipaccount'),
            'MeterNumber' => getProperty('meter')
        );
    }

    public function isValidAddress($address1, $address2, $City, $State, $Zip) {
        $client = $this->getClient($this->AddressValidationWsdl);
        $this->setupAuthentication();

        $this->req['TransactionDetail'] = array('CustomerTransactionId' => ' *** Address Validation Request using PHP ***');
        $this->req['Version'] = array(
            'ServiceId' => 'aval',
            'Major' => '4',
            'Intermediate' => '0',
            'Minor' => '0'
        );
        $this->req['InEffectAsOfTimestamp'] = date('c');

        $this->req['AddressesToValidate'] = array(
            0 => array(
                'ClientReferenceId' => 'ClientReferenceId1',
                'Address' => array(
                    'StreetLines' => array($address1, $address2),
                    'PostalCode' => $Zip,
                    'City' => $City,
                    'StateOrProvinceCode' => $State,
                    'CountryCode' => 'US'
                )
            )
        );

        $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        $response = $client ->addressValidation($this->req);
        if(!json_encode($response->HighestSeverity == 'SUCCESS')){
            return 3;
        }
        if (!is_null($response) && !is_null($response->AddressResults) && !is_null($response->AddressResults->Attributes)){
            $validAddress = false;
            $needsApartmentNumber = false;
            $validSuiteNumber = true;
            foreach($response->AddressResults->Attributes as $value){
                if($value->Name == 'SuiteRequiredButMissing' && $value->Value == 'true'){
                    $needsApartmentNumber = true;
                }
                if($value->Name == 'InvalidSuiteNumber' && $value->Value == 'true'){
                    $validSuiteNumber = false;
                }
                if($value->Name == 'Resolved'){
                    if($value->Value == 'true'){
                        $validAddress = true;
                    }
                }
            }
        }

        if(!$validAddress){
            return 0;
        }
        if(($validAddress && $needsApartmentNumber) || ($validAddress && !$validSuiteNumber)){
            // Same as ambiguous in UPS. Just leave it for now
            return 2;
        }
        return 1;
    }


    private function addShipper(){
        $shipper = array(
            'Contact' => array(
                'CompanyName' => $this->companyName,
                'PhoneNumber' => $this -> companyPhone
            ),
            'Address' => array(
                'StreetLines' => array($this->companyAddress),
                'City' => $this->companyCity,
                'StateOrProvinceCode' => $this->companyState,
                'PostalCode' => $this->companyZip,
                'CountryCode' => 'US'
            )
        );
        return $shipper;
    }

    private function addRecipient($shipToName, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $shipToPhone){
        $recipient = array(
            'Contact' => array(
                'PersonName' => $shipToName,
                'PhoneNumber' => $shipToPhone
            ),
            'Address' => array(
                'StreetLines' => array($shipToAddress1, $shipToAddress2),
                'City' => $shipToCity,
                'StateOrProvinceCode' => $shipToState,
                'PostalCode' => $shipToZip,
                'CountryCode' => 'US'
            )
        );
        return $recipient;
    }

    private function addShippingChargesPayment(){
        $shippingChargesPayment = array(
            'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor' => array(
                'ResponsibleParty' => array(
                    'AccountNumber' => getProperty('shipaccount'),
                    'CountryCode' => 'US'
                )
            )
        );
        return $shippingChargesPayment;
    }

    private function addPackageLineItems($boxes = array()){

      $result = array();
      $counter = 1;
      
      foreach ($boxes as $box) {	
      	$packageLineItem = $this->addPackageLineItem($box, $counter);
        array_push($result, $packageLineItem);
        $counter++;
      }
      
      return $result;

    }

    private function addPackageLineItem($box, $sequenceNumber){


            $packageLineItem = array(
                'SequenceNumber' => $sequenceNumber,
                'GroupPackageCount' => 1,
                'Weight' => array(
                    'Value' => (float)$box["weight"],
                    'Units' => 'LB'
                ),
                'Dimensions' => array(
                    'Length' => (int)$box["length"],
                    'Width' => (int)$box["width"],
                    'Height' => (int)$box["height"],
                    'Units' => 'IN'
                )
            );



        return $packageLineItem;

    }

    public function getShippingDate(){

        return date('c', strtotime('now +1 Weekdays 10 PM'));
    }

    //get Shipping Rate Section
    public function getShippingRate($shipToName, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $shipToPhone, $boxes = array(), $cmsChange = false, $serviceCode = false) {
        $client = $this->getClient($this->ShippingRateWsdl);
        $this->setupAuthentication();
        $this->req['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request using PHP ***');
        $this->req['Version'] = array(
            'ServiceId' => 'crs',
            'Major' => '18',
            'Intermediate' => '0',
            'Minor' => '0'
        );

        $this->req['ReturnTransitAndCommit'] = true;
        $this->req['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        $this->req['RequestedShipment']['ShipTimestamp'] = $this->getShippingDate(); // date('c');

        $this->req['RequestedShipment']['Shipper'] = $this->addShipper();
        $this->req['RequestedShipment']['Recipient'] = $this->addRecipient($shipToName, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $shipToPhone);
        $this->req['RequestedShipment']['ShippingChargesPayment'] = $this->addShippingChargesPayment();
        $this->req['RequestedShipment']['PackageCount'] = count($boxes);
        $this->req['RequestedShipment']['RequestedPackageLineItems'] = $this -> addPackageLineItems($boxes);
        $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        $resp = $client -> getRates($this->req);

        echo json_encode($this -> addPackageLineItems($boxes));
		echo json_encode($this->req);
		
        if($cmsChange == true) {
			$result = "";
		} else {
			$result = array();
		}
        
        if ($resp->HighestSeverity == 'SUCCESS') {
            foreach ($resp->RateReplyDetails as $shipment) {
                try {
                	if($cmsChange == true) {
                		if($shipment->ServiceType == $serviceCode) {
                                $result = $shipment->RatedShipmentDetails->ShipmentRateDetail->TotalNetChargeWithDutiesAndTaxes->Amount;
                            }
					} else {
						$serviceCode = Shipments::WithServiceCode($shipment->ServiceType, "FedEx");
                    	if ($serviceCode -> shippingMethodActive == 1) {
                        	array_push($result,
                        	    array('code' => $shipment->ServiceType,
                        	    	  'description' => $serviceCode -> shippingMethodName,
                        	    	  'price' => str_replace(" ", "", money_format("%10.2n", $shipment->RatedShipmentDetails->ShipmentRateDetail->TotalNetChargeWithDutiesAndTaxes->Amount))));

                    	}		
					}
                    
                }catch (Exception $e){
                    // I want to ignore this case. Likely a new service code was added that is not in our database.
                    // We don't want this call to fail because of it
                    // We can set it up to send an email if necessary
                    $TrackError = new EmailServerError();
               		$TrackError -> message = var_export($e, true);
               		$TrackError -> type = "FEDEX RATES ERROR";
               		$TrackError -> SendMessage();
                }
            }
        } else {
           
         
            // How Can We Handle a failed Call? I've been getting errors all morning when trying to retrieve rates
            // I cannot actually do anymore coding until that's fixed

        }
        return $result;
    }


    private function addLabelSpecification(){
        $labelSpecification = array(
            'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
            'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
            'LabelStockType' => 'PAPER_7X4.75'
        );
        return $labelSpecification;
    }
    //create shipment section
    public function processShipment($name, $addressLine1, $addressLine2, $city, $state, $zip, $serviceCode, $phone, $boxes = array()) {
        $masterId = 0;
        $r = array();
        $counter = 1;
        foreach ($boxes as $box) {
            $resp = $this->processShipmentBox($name, $addressLine1, $addressLine2, $city, $state, $zip, $serviceCode, $phone, $box, count($boxes), $counter, $masterId);
            if ($masterId == 0){
                // Get the actual Master Id
                $masterId = $resp->CompletedShipmentDetail->MasterTrackingId;
            }
            array_push($r, $resp);

            $counter++;
        }
        return $r;
    }

    private function processShipmentBox($name, $addressLine1, $addressLine2, $city, $state, $zip, $serviceCode, $phone, $box, $boxCount, $index, $masterShipmentId){
        $client = $this->getClient($this->CreateShipmentWsdl);
        $this->setupAuthentication();

        $this->req['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request using PHP ***');
        $this->req['Version'] = array(
            'ServiceId' => 'ship',
            'Major' => '17',
            'Intermediate' => '0',
            'Minor' => '0'
        );

        //http://stackoverflow.com/questions/14040137/fedex-api-shipping-label-multiple-package-shipments
        $this->req['RequestedShipment'] = array(
            'ShipTimestamp' => $this->getShippingDate(),
            'DropoffType' => 'REGULAR_PICKUP',
            'ServiceType' => $serviceCode,
            'PackagingType' => 'YOUR_PACKAGING',
            'Shipper' => $this->addShipper(),
            'Recipient' => $this->addRecipient($name, $addressLine1, $addressLine2, $city, $state, $zip, $phone),
            'ShippingChargesPayment' => $this->addShippingChargesPayment(),
            'LabelSpecification' => $this->addLabelSpecification(),
            'PackageCount' => $boxCount,
            'RequestedPackageLineItems' => array(
              '0' => $this->addPackageLineItem($box, $index)
            ),
            'MasterTrackingId' => null
        );

        if ($masterShipmentId != 0){
            $this->req['RequestedShipment']['MasterTrackingId'] = $masterShipmentId;
        }

        $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        $response = $client->processShipment($this->req);  // FedEx web service invocation
        return $response;
    }


    public function voidShipment($trackingNumber) {
        $client = $this->getClient($this->CreateShipmentWsdl);
        $this->setupAuthentication();

        $this->req['TransactionDetail'] = array('CustomerTransactionId' => ' *** Cancel Shipment Request using PHP ***');
        $request['Version'] = array(
            'ServiceId' => 'ship',
            'Major' => '17',
            'Intermediate' => '0',
            'Minor' => '0'
        );
        $this->req['ShipTimestamp'] = date('c');
        $this->req['TrackingId'] = array(
            'TrackingIdType' =>'GROUND', // valid values EXPRESS, GROUND, USPS, etc
            'TrackingNumber'=>$trackingNumber
        );
        $this->req['DeletionControl'] = 'DELETE_ONE_PACKAGE'; // Package/Shipment
        $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        $response = $client ->deleteShipment($request);

    }

}