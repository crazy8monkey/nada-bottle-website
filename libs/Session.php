<?php

class Session 
{
	//starts login session
	public static function init() {
		@session_start();
	}

	public static function set($key, $value) {
		$_SESSION[$key] = $value;
	}
	
	public static function get($key) {
		//if an empty session
		//return session 
		if (isset($_SESSION[$key])) return $_SESSION[$key];
	}
	//ends login session
	public static function destroy() {
		session_destroy();
	}
	
	public static function token() {
		$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

		$finalToken = substr(str_shuffle($alpha_numeric), 0, 25);



		return $finalToken;
	}
	
}
