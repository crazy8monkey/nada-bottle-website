<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Payments extends BaseObject {
	
	public $orderID;
	public $amount;
	public $paymentStatus;
	public $stripeChargeID;
	public $reason;
	public $refundID;
	public $paymentEmail;
	public $sendEmail;
	public $refundedAmount;
	
	public $previousTotalRefunded;
	private $stripePaymentID;
	
	
	
	public static function WithPaymentID($paymentID) {		
        	$instance = new self();
        	$instance-> stripePaymentID = $paymentID;
        	$instance->loadByPartiallyRefundedStatus();
        	return $instance;
    	}
	
    protected function loadByPartiallyRefundedStatus() {
        $sth = $this -> db -> prepare('SELECT * FROM paymentLogs WHERE stripeCheckoutID = :stripe_checkout_id AND paymentStatus="Partially Refunded" ORDER BY id DESC LIMIT 1');
        $sth->execute(array(':stripe_checkout_id' => $this-> stripePaymentID));
        $record = $sth -> fetch();
		
        if($sth->rowCount() > 0) {
        	$this->fill($record);
        }
        
    }	
 

   
	
    protected function fill(array $row) {
	$this -> previousTotalRefunded = $row['amount'];    	
    }
	
	
	
	

	public function Save() {
	
		if(!empty($this -> orderID)) {
			$this -> updateOrderPaymentStatus();
		
			if($this -> refundedAmount !=0) {
				$postData = array('orderID' => $this -> orderID,
						  'paymentStatus' => $this -> paymentStatus,
						  'stripeCheckoutID' => $this -> stripeChargeID,
						  'date' => date("Y-m-d", $this -> currentTime -> coloradoTime()),
						  'time' => date('h:i:s a', $this -> currentTime -> coloradoTime()),
						  'reason' => $this -> reason,
						  'amount' => ($this -> amount / 100),
						  'refundedAmount'=> ($this -> refundedAmount));
			
			
			} else {
			
				$postData = array('orderID' => $this -> orderID,
						  'paymentStatus' => $this -> paymentStatus,
						  'stripeCheckoutID' => $this -> stripeChargeID,
						  'date' => date("Y-m-d", $this -> currentTime -> coloradoTime()),
						  'time' => date('h:i:s a', $this -> currentTime -> coloradoTime()),
						  'reason' => $this -> reason,
						  'amount' => ($this -> amount / 100));
				       			
			}
			
					
			$this -> db -> insert("paymentLogs", $postData);
			
			
			//if send email call method
			if($this -> sendEmail == true) {
				$this -> EmailPaymentNotification();
			}
			
			
			if($this -> paymentStatus == 'Partially Refunded' || $this -> paymentStatus == 'dispute closed: lost') {
				$this -> refundOrder();
			}
		}
		
	
		
		
	}	
	
	private function EmailPaymentNotification() {
		setlocale(LC_MONETARY,"en_US");
	
		//email
		$disputedLink = "<tr><td style='font-size:12px;'><strong>Payment Link:</strong></td><td style='font-size:12px;'><a href=" . STRIPE_PAYMENT_LINK . $this -> stripeChargeID . " style='text-decoration:underline'>Click Here.</a></td></tr>";
		$msg = "<tr><td style='font-size:16px;' colspan='2'><strong>CHARGE: " . $this -> paymentStatus ."</strong></td></tr>" .
			   "<tr><td style='font-size:12px;'><strong>Order #:</strong></td><td style='font-size:12px;'>" . $this -> orderID ."</td></tr>" . $disputedLink;
		        
		if($this -> paymentStatus == 'dispute closed: lost') {
			$msg .= "<tr><td style='font-size:12px;'><strong>Reason:</strong></td><td style='font-size:12px;'>" . $this -> reason ."</td></tr>";
		}
		        
		if($this -> amount) {
			
			if($this -> paymentStatus == 'dispute: funds withdrawn') {
				$label = "Dispute Fee:";
			} else if ($this -> paymentStatus == 'Partially Refunded') {
				
				$msg .= "<tr><td style='font-size:12px;'><strong>Amount Refunded: </strong></td><td style='font-size:12px;'>" . money_format("%10.2n", $this -> refundedAmount) ."</td></tr>";
			
				$label = "Total Refunded:";
			} else {
				$label = "Amount:";
			}
		
			$msg .= "<tr><td style='font-size:12px;'><strong>". $label . "</strong></td><td style='font-size:12px;'>" . money_format("%10.2n", $this -> amount / 100) ."</td></tr>";
		}
		        	
		Email::WebHook($this -> paymentEmail, $msg, $this -> orderID);        
	}

	private function updateOrderPaymentStatus() {
		//update payment status
		$order = Orders::WithID($this -> orderID);
		
		if($this -> paymentStatus != 'dispute: funds withdrawn' || $this -> paymentStatus != 'succeeded' || $this -> paymentStatus != 'refunded' || $this -> paymentStatus != 'refunded: lost dispute' || $order -> stripeStatus != 'refunded') {
			$order -> stripeStatus = $this -> paymentStatus;
			$order -> updateStripeStatus();
		}
			
				
	}
	
	private function refundOrder() {
		try {
			$order = Orders::WithID($this -> orderID);
		
			if($order -> cartTotal == ($this -> amount / 100)) {				
				$this -> updateStripeStatus('refunded', "Fully Refunded"); 				
			} else if ($this -> paymentStatus == 'dispute closed: lost') {
				$this -> updateStripeStatus('refunded: lost dispute', "Fully Refunded: Lost Dispute");
				
			}
			   
		 } catch (Exception $e) {
	    	//email
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
	 	}    
	}
	
	private function updateStripeStatus($stripeStatus, $fullyRefundedText) {
		setlocale(LC_MONETARY,"en_US");
		$order = Orders::WithID($this -> orderID);
	
		$order -> stripeStatus = $stripeStatus;
	
		
		$order -> updateStripeStatus();
		$order -> voidOrder();
		
		//need to get all payments that state succedded and the payment id
		//and put this into a foreach loop
		
		
		$paymentLogInsert = array('orderID' => $order -> orderID,
					  'paymentStatus' => $fullyRefundedText,
					  'stripeCheckoutID' => $this -> stripeChargeID,
					  'date' => date("Y-m-d", $this -> currentTime -> coloradoTime()),
					  'time' => date('h:i:s a', $this -> currentTime -> coloradoTime()),
					  'amount' => $this -> amount / 100);
									  
		$this -> db -> insert("paymentLogs", $paymentLogInsert);
				
		$disputedLink = "<tr><td style='font-size:12px;'><strong>Payment Link:</strong></td><td style='font-size:12px;'><a href=" . STRIPE_PAYMENT_LINK . $this -> stripeChargeID . " style='text-decoration:underline'>Click Here.</a></td></tr>";
			
	    $msg = "<tr><td style='font-size:16px;' colspan='2'><strong>CHARGE: " . $fullyRefundedText ."</strong></td></tr>" .
		   	   "<tr><td style='font-size:12px;'><strong>Order #:</strong></td><td style='font-size:12px;'>" . $order -> orderID ."</td></tr>" . $disputedLink;
				
		$msg .= "<tr><td style='font-size:12px;'><strong>Amount:</strong></td><td style='font-size:12px;'>" . money_format("%10.2n", $order -> cartTotal) ."</td></tr>";
				
		Email::WebHook($this -> paymentEmail, $msg, $this -> orderID);
	}
	
	




}