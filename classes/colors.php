<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Colors extends BaseObject {

	public $ProductColorID;
	public $Color;
	public $ProductId;
	public $NewColorImage;
	public $ProductColorImage;
	public $ColorHexacode;
	public $purchasedQuantity;
    public $RemainingQuantity = 2;
	public $newPhotoName;
	
	public $productCategory;

    public function __sleep()
    {
        parent::__sleep();
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }

    public static function productColorID($productColorId){
        $instance = new self();
        $instance -> ProductColorID = $productColorId;
        $instance -> loadByProductID();
        return $instance;
    }

	

    protected function loadByProductID() {
        $sth = $this -> db -> prepare('SELECT * FROM ProductColors WHERE productColor_id = :productColor_id');
        $sth->execute(array(':productColor_id' => $this -> ProductColorID));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> ProductColorID = $row['productColor_id'];
        $this -> ProductColorImage = $row['productColorImage'];
        $this -> ProductId = $row['productId'];
        $this -> ColorHexacode = $row['hexacode'];
        $this -> Color = $row['Color'];
        $this -> RemainingQuantity = $row['quantityRemaining'];
    }

	private function redirectSave() {
		$product = Product::withID($this -> ProductId);
		return $redirect = PATH . "cms/editcolor?ID=". $this->ProductColorID . "&ProductID=" . $this -> ProductId . "&productType=" . $product -> Category . "&productID=" . $this -> ProductId . "&productName=" . $product -> ProductName;
	}

	public function Validate($editColor = false) {
		if($editColor == true) {
			if(!empty($this -> NewColorImage)) {
				if($_FILES['colorImage']['error'] !== UPLOAD_ERR_OK) {
					$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
					return false;
				} 				
			} else {
				if($this -> validate -> emptyInput($this -> Color)) {
					$this -> json -> outputJqueryJSONObject('emptyColorName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Color Name")));
					return false;
				} else if ($this -> validate -> emptyInput($this -> ColorHexacode)) {
					$this -> json -> outputJqueryJSONObject('hexacodeProblem', $this -> msg->flashMessage('message error', "Color didn't read correctly, please try again"));	
					return false;
				} else if ($this -> validate -> emptyInput($this -> RemainingQuantity)) {
					$this -> json -> outputJqueryJSONObject('emptyQuantity', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Quantity")));
					return false;
				}
			}

		} else {
			if($this -> validate -> emptyInput($this -> Color)) {
				$this -> json -> outputJqueryJSONObject('emptyColorName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Color Name")));
				return false;
			} else if ($this -> validate -> emptyInput($this -> ProductColorImage)) {
				$this -> json -> outputJqueryJSONObject('emptyColorPhoto', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Color Photo")));
				return false;
			} else if($_FILES['colorImage']['error'] !== UPLOAD_ERR_OK) {
				$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
				return false;
			} else if ($this -> validate -> emptyInput($this -> ColorHexacode)) {
				$this -> json -> outputJqueryJSONObject('hexacodeProblem', $this -> msg->flashMessage('message error', "Color didn't read correctly, please try again"));	
				return false;
			} else if ($this -> validate -> emptyInput($this -> RemainingQuantity)) {
				$this -> json -> outputJqueryJSONObject('emptyQuantity', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Quantity")));
				return false;
			}	
		}
		
		
		return true;
		
	}

	public function save() {
		try {
			if(isset($this -> ProductColorID)) {
				if(empty($this -> NewColorImage)) {
						$this->db->update('ProductColors', array('Color' => $this -> Color,
	                				  'hexacode' => $this -> ColorHexacode,
	                                    'quantityRemaining' => $this -> RemainingQuantity),
	                				array('productColor_id' => $this->ProductColorID));
					
				} else {
					Image::moveImage($this -> newPhotoName, $_FILES["colorImage"]["tmp_name"], "view/purchase/product-images/");
					Image::cropImage($this -> newPhotoName, DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_WIDTH, "view/purchase/product-images/");
					
					unlink("view/purchase/product-images/" . $this -> ProductColorImage);
					
					$this->db->update('ProductColors', array('Color' => $this -> Color,
	                				  'productColorImage' => $this -> newPhotoName,
	                				  'hexacode' => $this -> ColorHexacode,
	                                  'quantityRemaining' => $this -> RemainingQuantity),
	                				array('productColor_id' => $this->ProductColorID));
				}
				$product = Product::withID($this -> ProductId);
				
				$redirect = "cms/editcolor?ID=" . $this -> ProductColorID;
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Color updated"));
				//$this -> redirect -> redirectPage($this-> redirectSave());
				$this -> json -> outputJqueryJSONObject('redictNewColor', $this-> redirectSave());
				
			}else{
	            // New color
	            Image::moveImage($this -> newPhotoName, $_FILES["colorImage"]["tmp_name"], "view/purchase/product-images/");
	            Image::cropImage($this -> newPhotoName, DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_WIDTH, "view/purchase/product-images/");
	
	            $colorData = array( "Color"=> $this -> Color,
	                "productId"          => $this -> ProductId,
	                "productColorImage"  => $this -> newPhotoName,
	                "hexacode" => $this->ColorHexacode,
	                "quantityRemaining" => $this -> RemainingQuantity);
	
	            $this->ProductColorID = $this -> db -> insert("ProductColors", $colorData);
	
	            $this -> msg -> set($this -> msg -> flashMessage('message success', "Color Added"));
				
				$this -> json -> outputJqueryJSONObject('redictNewColor', $this-> redirectSave());
	            //$this -> redirect -> redirectPage();
	        }
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySQLError', $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
		
	}
	
	public function validateQuantity() {
		if($this -> validate -> emptyInput($this -> RemainingQuantity)) {
			$this->json->outputJqueryJSONObject("emptyQuantity", $this->msg->flashMessage('message error', $this->msg->isRequired("Quantity")));
            return false;
		} else if(!is_numeric($this -> RemainingQuantity)) {
			$this->json->outputJqueryJSONObject("onlyNumbers", $this->msg->flashMessage('message error', "Only numbers are allowed"));
            return false;
		}
		return true;
	}
	
	public function updateQuantiy() {
		try {
		
			$postData = array('quantityRemaining' => preg_replace('/[^0-9]/','',$this -> RemainingQuantity));
			$this->db->update('ProductColors', $postData, array('productColor_id' => $this -> ProductColorID));
			
			$this -> json -> multipleJSONOjbects(array("quantityUpdated"	=> $this->msg->flashMessage('message success', $this -> Color . " quantity updated"),
										                "quantityNumber"	=> $this -> RemainingQuantity,
														'colorID' => $this -> ProductColorID));
		
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this->json->outputJqueryJSONObject("MySqlError", $this->msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	
	public function deleteColor($id, $category) {
		try {
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Color Removed"));
			
			$sth = $this -> db -> prepare("UPDATE ProductColors SET IsActive=0 WHERE productColor_id = :productColor_id");
			$sth -> execute(array(':productColor_id' => $id));
			
			//unlink("view/purchase/product-images/" . $this->ProductColorImage);
			$this -> unPublishProduct();
			
			$this -> redirect -> redirectPage('cms/editcolorlist?ProductID=' . $this -> ProductId . "&productType=" . $category);
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));	
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/editcolorlist?ProductID=' . $this -> ProductId . "&productType=" . $category);
		}
		
	}
	
	private function unPublishProduct() {
		$colorCount = $this -> db -> prepare('SELECT * FROM ProductColors WHERE productId = :productId AND isActive = 1');
        $colorCount->execute(array(':productId' => $this -> ProductId));
		
		if(count($colorCount -> fetchAll()) == 0) {
			$this->db->update('Products', array('publishState' => 0), array('product_id' => $this -> ProductId));
		}
		
	}
	
	public function updateQuantity() {
		$quantiyRemaining = $this -> RemainingQuantity - $this -> purchasedQuantity;
		
		$postData = array('quantityRemaining' => $quantiyRemaining);
		
		$this->db->update('ProductColors', $postData, array('productColor_id' => $this -> ProductColorID));
	}



}