<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class FeedBack extends BaseObject {
	
	
	public $emailID;	
	public $email;
	public $feedBackID;
	public $feedBackEmail;
	public $feedBackDescription;
	public $feedBackDate;
	public $feedBackTime;
	public $nadaBottleEmail;
	
	
	public static function WithFeedBackID($feedBackID) {
		$instance = new self();
        $instance -> feedBackID = $feedBackID;
        $instance -> loadByFeedBackID();
        return $instance;
	}
	
	protected function loadByFeedBackID() {
        $sth = $this -> db -> prepare('SELECT * FROM FeedBack WHERE id = :id');
        $sth->execute(array(':id' => $this -> feedBackID));
        $record = $sth -> fetch();
        $this->fillFeedback($record);
    }

    protected function fillFeedback(array $row) {
		$this -> feedBackEmail = $row['email'];
		$this -> feedBackDescription = $row['description'];
		$this -> feedBackDate  = $row['date'];
		$this -> feedBackTime = $row['time'];
    }
	
	public function validateFeedback() {
		if($this -> validate -> emptyInput($this -> feedBackEmail)) {
			$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Email")));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> feedBackEmail)) {
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
			return false;
		} else if($this -> validate -> emptyInput($this -> feedBackDescription)) {
			$this -> json -> outputJqueryJSONObject('emptyFeedback', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Feedback")));
			return false;
		}
		return true;
	}
	
	public function validateEmailUpdates() {
		$emailCheck = $this->db->prepare('SELECT registeredEmail FROM userEmails WHERE registeredEmail = ?');
        $emailCheck->execute(array($this -> email));
		
		
		if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Email")));
			return false;
		} else if ($this -> validate -> correctEmailFormat($this -> email)){
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
			return false;
		} else if(count($emailCheck -> fetchAll())) {
            $this->json->outputJqueryJSONObject("duplicateEmail", $this->msg->flashMessage('message error', "There is already an email entered in our email signup"));
            return false;
        }
		return true;
	}

	public function saveFeedback() {
		try {
			$this -> db -> insert("FeedBack", array('description' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> feedBackDescription)), 
							       				'date' => date("Y-m-d", $this -> currentTime -> coloradoTime()), 
							       				'time' => date('h:i:s a', $this -> currentTime -> coloradoTime()),
												'email' => $this -> feedBackEmail));
		
			Email::FeedBack($this -> nadaBottleEmail);
		
			$this -> json -> multipleJSONOjbects(array("complete"	=> $this -> msg -> flashMessage('message success', "Thank you for submitting your feedback!"), 
													   "redirect"	=> PATH));	
		} catch (Exception $e) {
			$this -> json -> outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
													   
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
		}	
		
		
	}
	
	public function saveEmail() {
		try {
			$this -> db -> insert("userEmails", array('registeredEmail' => $this -> email));
			$this -> json -> outputJqueryJSONObject("complete", $this -> msg -> flashMessage('message success', "Your email has been entered"));
		} catch (Exception $e) {
			$this -> json -> outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
													   
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
		}	
	}
	
	public function delete() {
		try {
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Feed Back Removed"));
				
			$sth = $this -> db -> prepare("UPDATE FeedBack SET isActive = 0 WHERE id = :id");
			$sth -> execute(array(':id' => $this -> feedBackID));
		
			$this -> redirect -> redirectPage('cms/feedback');
		
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/feedback');
		}
	}
	
	
	

}