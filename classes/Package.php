<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Package extends BaseObject {
	
	private $packageID;
	public $width;
	public $height;
	public $length;
	public $weight;
	public $orderID;
	public $shippingMethodCode;
	
	public static function WithOrderID($orderID) {
		$instance = new self();
        $instance-> orderID = $orderID;
        return $instance;
	}
	
	public function LoadPackages() {
        $sth = $this -> db -> prepare('SELECT * FROM packageItems WHERE orderID = :orderID');
        $sth->execute(array(':orderID' => $this-> orderID));
        return $sth -> fetchAll();
    }

    
	
	public function newPackage() {
		$this -> db -> insert("packageItems", array("orderID" => $this -> orderID,
								  "width" => $this -> width,
			  				      "length" => $this -> length,
							      "height" => $this -> height, 
								  "weight" => $this -> weight,
					    	      "shippingMethod" => $this -> shippingMethodCode));
	}

}