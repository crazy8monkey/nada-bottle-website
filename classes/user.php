<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class User extends BaseObject {

    public function __sleep()
    {
        parent::__sleep();
        return array('_userId', 'FirstName', 'LastName', 'Username', 'Email', '_permissions', '_shouldSavePermissions', '_password');
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }

    public static function WithID($userId){
        $instance = new self();
        $instance->_userId = $userId;
        $instance->loadById();
        return $instance;
    }


	public function deleteUser() {
		try {
		
			//delete user
			$sth = $this -> db -> prepare("DELETE FROM adminUsers WHERE userID = :id");
			$sth -> execute(array('id' => $this->_userId));
			
			//delete referrences from permissions table
			$sth = $this -> db -> prepare("DELETE FROM permissions WHERE userID = :id");
			$sth -> execute(array('id' => $this->_userId));
			
			$userCredentials = array();
			$userCredentials['customer-email'] = $this->Email;
				
	        Email::UserCredentials($userCredentials, 4);		
			
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "User Deleted"));
			$this -> redirect -> redirectPage('cms/users');
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/users');
		}

	}
	
    protected function loadByID()
    {
        $sth = $this -> db -> prepare('SELECT * FROM adminUsers WHERE userID = :userID');
        $sth->execute(array(':userID' 	 => $this->_userId));
        $record = $sth -> fetch();
        $this->fill($record);
        $this->LoadPermissions();
    }

    protected function fill(array $row){
        $this->FirstName = $row['firstName'];
        $this->LastName = $row['lastName'];
        $this->Username = $row['userName'];
        $this->Email = $row['email'];
        $this->Salt = $row['salt'];
        $this->_password = $row['password'];
    }

    // USER ID is read-only, so it is private, and the function below is to get the value.
    public $_userId;
    public function GetUserId()
    {
        return $this->_userId;
    }

    public $FirstName;
    public $LastName;
	public $ConfirmPassword;
    public $Salt;

    public function FullName(){
        return $this->FirstName . ' ' . $this->LastName;
    }

    public $Username;
    public $Email;
	public $emailCheck;

    private $_permissions;
    public function HasPermission($permissionName){
        if (!isset($this->_permissions))
        {
            if (isset($userId))
            {
                $this->LoadPermissions();
            }
            else
            {
                $this->_permissions = array();
            }
        }
        foreach($this->_permissions as $row)
        {
            if ($row['PermissionsName'] == $permissionName){
                return $row['Enabled'] == 1;
            }
        }
        return false;
    }

    private $_shouldSavePermissions = false;
    public function SetPermission($permissionName, $value){
        $this->_shouldSavePermissions = true;
        if (!isset($this->_permissions))
        {
            if (isset($userId))
            {
                $this->LoadPermissions();
            }
            else
            {
                $this->_permissions = array();
            }
        }
        foreach ($this->_permissions as &$row)
        {
            if ($row['PermissionsName'] == $permissionName){
                $row['Enabled'] = $value ? 1 : 0;
                return;
            }
        }
        array_push($this->_permissions, array(
           'PermissionsName' => $permissionName,
            'Enabled' => $value ? 1 : 0
        ));
    }

    private function LoadPermissions()
    {
        $sth = $this -> db -> prepare('SELECT PermissionsName, Enabled FROM permissions WHERE userID = :userID');
        $sth->execute(array(':userID' 	 => $this->_userId));
        $this->_permissions = $sth -> fetchAll();
    }

    public function Validate()
    {
        if(!isset($this->_userId)) {
            $emailCheck = $this->db->prepare('SELECT email FROM adminUsers WHERE email = ?');
            $emailCheck->execute(array($this->Email));


            $userNameCheck = $this->db->prepare('SELECT userName FROM adminUsers WHERE userName = ?');
            $userNameCheck->execute(array($this->Username));
        }else{
            $emailCheck = $this->db->prepare('SELECT email FROM adminUsers WHERE email = :email and userID <> :userId');
            $emailCheck->execute(array(':userId' => $this->_userId, ':email' => $this->Email));


            $userNameCheck = $this->db->prepare('SELECT userName FROM adminUsers WHERE userName = :userName and userID <> :userId');
            $userNameCheck->execute(array(':userName' => $this->Username, ':userId' => $this->_userId));
        }

        if($this -> validate -> emptyInput($this->FirstName)) {
            $this->json->outputJqueryJSONObject("userFirstName", $this->msg->flashMessage('message error', $this->msg->isRequired("First Name")));
            return false;
        }else if($this -> validate -> emptyInput($this->LastName)) {
            $this->json->outputJqueryJSONObject("userLastName", $this->msg->flashMessage('message error', $this->msg->isRequired("Last Name")));
            return false;
        }else if($this -> validate -> emptyInput($this->Username)) {
            $this->json->outputJqueryJSONObject("userNameEmpty", $this->msg->flashMessage('message error', $this->msg->isRequired("Username")));
            return false;
        }else if(count($userNameCheck -> fetchAll())) {
            $this->json->outputJqueryJSONObject("duplicateUserName", $this->msg->flashMessage('message error', $this->msg->duplicateUsername()));
            return false;
        }else if($this -> validate -> emptyInput($this->Email)) {
            $this -> json -> outputJqueryJSONObject("emptyEmail", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Email")));
            return false;
        } else if($this -> validate -> correctEmailFormat($this->Email)) {
            $this -> json -> outputJqueryJSONObject("wrongEmailFormat", $this -> msg -> flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
            return false;
        }else if (count($emailCheck -> fetchAll())) {
            $this->json->outputJqueryJSONObject("duplicateEmail", $this->msg->flashMessage('message error', $this->msg->duplicateEmail()));
            return false;
        }
        return true;
    }

    public function Save() {
    	try {
		
	        if(isset($this->_userId))
	        {
	            $this->db->update('adminUsers', array('firstName' => $this->FirstName,
	                'lastName' => $this->LastName,
	                'userName' => $this->Username,
	                'email' => $this->Email),
	                array('userID' => $this->_userId));
	
	            
					
				$userCredentials = array();
	            $userCredentials['user-name'] = $this->Username;
				$userCredentials['customer-email'] = $this->Email;
				
	            Email::UserCredentials($userCredentials, 2);
	            
	            $this -> json -> multipleJSONOjbects(array("firstNameURL"	=> $this->FirstName,
	                "lastNameURL"	=> $this->LastName,
	                "passed" => $this -> msg -> flashMessage('message success', "User Updated"),
					"userEmail" => $this->Email));
					
	
	        }
	        else
	        {
	            $this->_userId = $this->db->insert('adminUsers', array('firstName' => $this->FirstName,
	                'lastName' => $this->LastName,
	                'userName' => $this->Username,
	                'email' => $this->Email,
	                'password' => $this->_password,
	                'salt' => $this->Salt));
	
	            $userCredentials = array();
	            $userCredentials['user-name'] = $this->Username;
				$userCredentials['password'] = $this->_initialPassword;
				$userCredentials['customer-email'] = $this->Email;
				
	            Email::UserCredentials($userCredentials, 1);
	            $this -> msg -> set($this -> msg -> flashMessage('message success', "User: " . $this->FirstName . " " . $this->LastName ." Added"));
	            $this -> json -> outputJqueryJSONObject("redirect", PATH . "cms/profile?userid=" . $this->_userId );
				
				
				
	        }
	        $this->SavePermissions();
		} catch (Exception $e) {
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			if(isset($this->_userId)) {
				$this -> json -> outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} else {
				$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				$this -> json -> outputJqueryJSONObject("redirect", PATH . "cms/users");
			}
			
			
			
			//$this -> redirect -> redirectPage(PATH . 'cms/users');
		}

    }

	public function validatePassword() {
		if($this -> validate -> emptyInput($this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject('emptyPassword', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Password")));
			return false;
		} else if($this -> validate -> passwordLength($this->_initialPassword, 6)) {
			$this -> json -> outputJqueryJSONObject("userNewPassword", $this -> msg -> flashMessage('message error', $this -> msg -> passwordLengthMessage("6")));
			return false;
		} else if ($this -> validate -> notMatchingPasswords($this -> ConfirmPassword, $this->_initialPassword)) {
			$this -> json -> outputJqueryJSONObject("userConfirmPasswordNotMatch", $this -> msg -> flashMessage('message error', $this -> msg -> notMatchingPasswordsMessage()));
			return false;
		} 
		return true;
	}
	
	public function validateEmail() {
			
		$getAdminCredentials = $this -> db -> prepare("SELECT * FROM adminUsers WHERE email = :Email");
		$getAdminCredentials -> execute(array(":Email" => $this -> emailCheck));
		$getCredentialObject = $getAdminCredentials -> fetch();
		
		$this->_userId = $getCredentialObject['userID'];
		
		if($this -> validate -> emptyInput($this -> emailCheck)) {
			$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailRequiredRetriveCredentials()));
			return false;
		} else if(!Email::verifyEmailAddress($this -> emailCheck)) {
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
			return false;
		} else if($this -> validate -> noEmailMatch($this -> emailCheck, $getCredentialObject['email'])) {
			$this -> json -> outputJqueryJSONObject('errorEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailNotMatchingMessage()));
			return false;
		} 
		return true;
		
	}
	
	public function SendEmail() {
		$securityToken = Hash::generateSecurityToken();
		
		$this -> json -> multipleJSONOjbects(array("success" => $this -> msg -> flashMessage('message success', "Success!, please check your email to reset your password"),
                "redirect"	=> PATH . 'login'));
		
		$passwordReset = array();
		$passwordReset['customer-email'] = $this -> emailCheck;
		$passwordReset['reset-link'] = PATH . "login/newPassword?userID=" . $this->_userId . "&token=" . $securityToken;
		
		Email::passwordReset($passwordReset);
			
		$this->db->update('adminUsers', array('securityToken' => $securityToken), array('userID' => $this->_userId));
	}
	
	public function SaveNewPassword($passwordReset = false) {
	
		try {
			
			
			
			
			$userCredentials = array();
	        	$userCredentials['user-name'] = $this->Username;
			$userCredentials['password'] = $this->_initialPassword;
			$userCredentials['customer-email'] = $this->Email;
			
			if($passwordReset) {
				$postData = array('password' => $this->_password, 'salt' => $this->Salt, 'securityToken' => '');
			} else {
				$postData = array('password' => $this->_password, 'salt' => $this->Salt);
			}
			
			$this->db->update('adminUsers', $postData, array('userID' => $this->_userId));
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Password Updated"));
				
	        	Email::UserCredentials($userCredentials, 3);	
        	} catch (Exception $e) {
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}		
			
	}

    private function SavePermissions()
    {
        if($this->_shouldSavePermissions)
        {
            $this->db->delete('permissions', 'userID = ' . $this->_userId, 10000);
            foreach($this->_permissions as $row)
            {
                $this->db->insert("permissions", array('userId' => $this->_userId,
                                                        'PermissionsName' => $row['PermissionsName'],
                                                        'Enabled' => $row['Enabled']));
            }

        }
    }

    public function ValidatePasswordAttempt($pass)
    {
        return  Hash::create('sha256', $pass, $this->Salt) == $this->_password;
    }

    private $_password;
    private $_initialPassword;
    public function SetPassword($password)
    {
        $this->Salt = Hash::generateSalt();
        $this->_initialPassword = $password;
        $this->_password = Hash::create('sha256', $password, $this->Salt);
    }

}