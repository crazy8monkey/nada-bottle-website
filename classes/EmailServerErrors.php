<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class EmailServerError extends BaseObject {
	
	public $message;
	public $type;

	
	public function SendMessage() {
			
		$time = date("M. j Y / g:i a", $this -> currentTime -> coloradoTime()); 
		$ip = getenv('REMOTE_ADDR');
		$userAgent = getenv('HTTP_USER_AGENT');
		$referrer = getenv('HTTP_REFERER');
		$query = getenv('QUERY_STRING');
 
			//COMBINE VARS INTO OUR LOG ENTRY
		$StripeErrorMessage = 
			   $this -> type .
			   "\nIP: " . "<a href='" . IP_LOOKUP . $ip . "' target='_blank'>" . $ip ."</a>"  . 
			   "\nTIME: " . $time . 
			   "\nREFERRER: " . $referrer . 
			   "\nSEARCH STRING: " . $query . 
			   "\nUSER AGENT: " . $userAgent . 
			   "\nPOST DATA: <pre>" . var_export($_POST, true) . "</pre>" . 
			   "\nMESSAGE: <pre>" . $this -> message . "</pre>\n\n";
		//implode()
		//
		
		Log::UserAgent($StripeErrorMessage);
		
		$StripeEmailMessage = 
				   "<tr><td style='font-size:16px;' colspan='2'><strong>" .$this -> type ."</strong></td></tr>" .
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>IP:</strong></td><td style='font-size:12px; vertical-align:top'><a href='" . IP_LOOKUP . $ip ."' target='_blank'>". $ip ."</a></td></tr>" .
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>TIME:</strong></td><td style='font-size:12px; vertical-align:top'>" . $time ."</td></tr>" . 
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>REFERRER:</strong></td><td style='font-size:12px; vertical-align:top'>". $referrer . "</td></tr>" . 
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>SEARCH STRING:</strong></td><td style='font-size:12px; vertical-align:top'>" . $query . "</td></tr>" . 
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>USER AGENT:</strong></td><td style='font-size:12px; vertical-align:top'>" . $userAgent . "</td></tr>" .
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>POST DATA:</strong></td><td style='font-size:12px; vertical-align:top'>" . var_export($_POST, true) . "</td></tr>" .
				   "<tr><td style='font-size:12px; vertical-align:top'><strong>MESSAGE:</strong></td><td style='font-size:12px; vertical-align:top'>" . $this -> message . "</td></tr>";
				
		//email to me and dan?
		Email::StripeError(Email::EmailErrorRecipients(), $StripeEmailMessage);
		
		
	}
	

}