<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Partner extends BaseObject {
	
	public $partnerID;
	public $name;
	public $partnerLink;
	public $newImageName;
	public $partnerPhoto;
	public $publishState;

	
	public static function WithID($partnerID) {
		$instance = new self();
        $instance -> partnerID = $partnerID;
        $instance -> loadByPartnerID();
        return $instance;
	}
	
	protected function loadByPartnerID() {
        $sth = $this -> db -> prepare('SELECT * FROM partners WHERE partnerID = :partnerID');
        $sth->execute(array(':partnerID' => $this -> partnerID));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
    	$this -> name = $row['partnerName'];
 		$this -> partnerPhoto = $row['partnerImage'];
		$this -> partnerLink = $row['partnerLink'];
		$this -> publishState = $row['publishState'];
    }
	
	
	public function save() {
		
		try {
			if(isset($this -> partnerID)) {
				if(!empty($this -> newImageName)) {
					ini_set('memory_limit', '-1');
					
					if(!empty($this -> partnerPhoto)) {
						if(file_exists("view/index/partnersImages/" . $this -> partnerPhoto)) {
							unlink("view/index/partnersImages/" . $this -> partnerPhoto);
						}	
					}
					
					
					$kaboom = explode(".", $this -> newImageName); // Split file name into an array using the dot
					$fileExt = end($kaboom);
				
			
					Image::moveImage($this -> newImageName, $_FILES["partnerLogoImage"]["tmp_name"], "view/index/partnersImages/");
					Image::resizeImage("view/index/partnersImages/" . $this -> newImageName, "view/index/partnersImages/" . $this -> newImageName, "500", "500", $fileExt);
			
					$postData = array("partnerName" => $this -> name,
									  "partnerImage" => $this -> newImageName,
									  "partnerLink" => $this -> partnerLink);
												  	
						
				} else {
					$postData = array("partnerName" => $this -> name,
									  "partnerLink" => $this -> partnerLink);
					
				}
					
				$where = array('partnerID' => $this -> partnerID);
				$this -> db -> update("partners", $postData, $where);
					
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Partner Updated"));
		
				$this -> redirect -> redirectPage('cms/editPartner/' .  $this -> partnerID);
				
				
			} else {
				if(!empty($this -> newImageName)) {
					ini_set('memory_limit', '-1');
		
					$kaboom = explode(".", $this -> newImageName); // Split file name into an array using the dot
					$fileExt = end($kaboom);
					Image::moveImage($this -> newImageName, $_FILES["logoImage"]["tmp_name"], "view/index/partnersImages/");
					Image::resizeImage("view/index/partnersImages/" . $this -> newImageName, "view/index/partnersImages/" . $this -> newImageName, "500", "500", $fileExt);	
				
					$this -> db -> insert("partners", array('partnerName' => $this -> name,
														'partnerImage' => $this -> newImageName, 
				             							'partnerLink' => $this -> partnerLink));
				} else {
					$this -> db -> insert("partners", array('partnerName' => $this -> name,
				             							'partnerLink' => $this -> partnerLink));
				}
				
				$this -> msg -> set($this -> msg -> flashMessage('message success', $this -> name . " Added"));
				$this -> redirect -> redirectPage('cms/editPartner/' . $this -> db -> lastInsertId());
				
			}
			
			
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = "MySQL Error: " . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/partners');
		}
		
	}

	public function publishPartner() {
		try {
			$postData = array("publishState" => 1);
			$where = array('partnerID' => $this -> partnerID); 
					
			$this -> db -> update("partners", $postData, $where);	
		
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Partner Published"));
			$this -> redirect -> redirectPage('cms/editPartner/' . $this -> partnerID);
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/editPartner/' . $this -> partnerID);
		}
			
		
	}

	public function unpublishPartner() {
		try {
			$postData = array("publishState" => 0);
			$where = array('partnerID' => $this -> partnerID); 
					
			$this -> db -> update("partners", $postData, $where);	
			
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Partner Un-Published"));
			$this -> redirect -> redirectPage('cms/editPartner/' . $this -> partnerID);	
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/editPartner/' . $this -> partnerID);	
			
			
		}
		
	}
	
	public function deletePartner() {
		try {
			if(!empty($this -> partnerPhoto)) {
				if(file_exists("view/index/partnersImages/" . $this -> partnerPhoto)) {
					unlink("view/index/partnersImages/" . $this -> partnerPhoto);
				}	
			}
			
			$sth = $this -> db -> prepare("DELETE FROM partners WHERE partnerID = :id");
			$sth -> execute(array('id' => $this -> partnerID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Partner Deleted"));
			$this -> redirect -> redirectPage('cms/partners');	
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/partners');	
		}
		
	}
	

	

	


	
	
	

}