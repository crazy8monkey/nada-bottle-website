<?php

require CLASSES . 'BaseObject.php';
require CLASSES . 'colors.php';
require CLASSES . 'user.php';
require CLASSES . 'category.php';
require CLASSES . 'product.php';
require CLASSES . 'settings.php';
require CLASSES . 'orders.php';
require CLASSES . 'warranty.php';
require CLASSES . 'contact.php';
require CLASSES . 'locations.php';
require CLASSES . 'partner.php';
require CLASSES . 'pageContent.php';
require CLASSES . 'feedback.php';
require CLASSES . 'outOfStockColor.php';
require CLASSES . 'outOfStockProduct.php';
require CLASSES . 'cartClass.php';
require CLASSES . 'EmailServerErrors.php';
require CLASSES . 'Payments.php';
require CLASSES . 'Shipments.php';
require CLASSES . 'Package.php';
require CLASSES . 'Team.php';
require CLASSES . 'ThankYou.php';
require CLASSES . 'ShipmentBoxes.php';