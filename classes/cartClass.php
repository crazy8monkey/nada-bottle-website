<?php


class CartClass extends BaseObject {
		
	private $maxCartWeight = 100;
		
	public $productID;
	public $quantity;
	public $colorID;
	
	public $cartTotalWeight;

	public function validate() {
		if($this -> validate -> emptyInput($this -> quantity)) {
			$this -> json -> outputJqueryJSONObject('emptyQuantity', $this -> msg->flashMessage('message error', $this -> msg -> isRequired('Quantity')));
			return false;
		} else if($this -> validate -> emptyInput($this -> colorID)) {
			$this -> json -> outputJqueryJSONObject('productColor', $this -> msg->flashMessage('message error', "Please select a color"));
			return false;
		} else if($this -> remaningQuantity() < 2) {
			$this -> json -> outputJqueryJSONObject('OutOfStock', $this -> msg->flashMessage('message error', "There is not enough in stock for that quantity to be ordered"));
			return false;
		//for person ordered quantity extends the limit
		} else if($this -> extendedQuantity() < 2) {
			$this -> json -> outputJqueryJSONObject('quantityExtendsStock', $this -> msg->flashMessage('message error', "There is not enough in stock for that quantity to be ordered"));
			//$this -> json -> outputJqueryJSONObject('quantityExtendsStock', $this -> msg->flashMessage('message error', $this -> extendedQuantity()));
			return false;
		} else if($this -> generateTotalWeightWithPurchase() > $this -> maxCartWeight) {
			$this -> json -> outputJqueryJSONObject('exceedsMaxWeight', $this -> msg->flashMessage('message error', "You can not exceed the maximum weight limit in your cart.<br /> Max Weight: 100 lbs <br /> Current Weight: ". $this -> generateTotalWeightWithPurchase() . " lbs"));
			return false;
		}
		
		return true;
	}

	private function generateTotalWeightWithPurchase() {
		$finalWeight = null;
		$product = Product::withID($this -> productID);
		
		$goingToPurchaseWeight = ($product -> weight) * $this -> quantity;
		$finalWeight = 	((int)$this -> cartTotalWeight) + $goingToPurchaseWeight;
		
		return $finalWeight;
	}
	private function remaningQuantity() {
		$color = Colors::productColorID($this -> colorID);
		return $color -> RemainingQuantity;
	}

	private function extendedQuantity() {
		$color = Colors::productColorID($this -> colorID);
		return $color -> RemainingQuantity - $this -> quantity;
	}
	

	public function sendToCart() {
		Session::init();
		
		$index = 0;
			// If the cart session variable is not set or cart array is empty
			if (!isset($_SESSION['cart_array']) || count($_SESSION['cart_array']) < 1) {
			    // RUN IF THE CART IS EMPTY OR NOT SET
				$_SESSION["cart_array"] = array(0 => array("index" => $index,
														   "product_ID" => $this -> productID, 
														   "quantity" => preg_replace('#[^0-9]#i', '', $this -> quantity),
														   "color" => $this -> colorID));
														   
				$this -> json -> outputJqueryJSONObject('redirect', PATH . 'cart');
			} else {
				// RUN IF THE CART HAS AT LEAST ONE ITEM IN IT
                $index = count($_SESSION['cart_array']);

               array_push($_SESSION['cart_array'], array("index" => $index,
                                                         "product_ID" => $this -> productID,
                                                         "quantity"   => preg_replace('#[^0-9]#i', '', $this -> quantity),
                                                         "color" => $this -> colorID));

               $this -> json -> outputJqueryJSONObject('redirect', PATH . 'cart');
			}
		
	
	}
}