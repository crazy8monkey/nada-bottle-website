<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Shipments extends BaseObject {
	
	private $shipmentID;
	public $orderID;
	public $responseData;
	public $serviceCode;
	public $shippingMethodActive;
	public $shippingMethodName;
    public $shipmentType;
    public $shipmentImage;
	public $shippingCompany;
	//public $date;
	
	
	public static function WithOrderID($orderID) {
		$instance = new self();
        $instance-> orderID = $orderID;
        $instance->loadByOrderId();
        return $instance;
	}
	
	public static function WithServiceCode($serviceCode, $company = false) {
		$instance = new self();
        $instance -> serviceCode = $serviceCode;
		if($company) {
			$instance -> shippingCompany = $company;	
		}
		
        $instance->loadByServiceCode();
        return $instance;
	}
	
	protected function loadByOrderId() {
        $sth = $this -> db -> prepare('SELECT * FROM shipments WHERE orderId = :orderId');
        $sth->execute(array(':orderId' => $this-> orderID));
        $record = $sth -> fetch();
        if($record != false) {
            $this->fill($record);
        }
    }
	
	protected function loadByServiceCode() {
		if(isset($this -> shippingCompany)) {
			$sth = $this -> db -> prepare('SELECT * FROM ShippingMethods WHERE code = :code AND Company = :Company');
        	$sth->execute(array(':code' => $this-> serviceCode, ':Company' => $this -> shippingCompany));	
		} else {
			$sth = $this -> db -> prepare('SELECT * FROM ShippingMethods WHERE code = :code');
        	$sth->execute(array(':code' => $this-> serviceCode));
		}
        
        $record = $sth -> fetch();
        if($record) {
            $this->fillShippingMethod($record);
        }
    }
    
    protected function fill(array $row) {
    	$this -> responseData = $row['responseData'];
        $this -> shipmentID = $row['shipmentId'];
        $this -> shipmentType = $row['shipmentType'];
        $this -> shipmentImage = $row['shipmentImage'];
    }
	
	protected function fillShippingMethod(array $row) {
    	//$this -> serviceCode = $row['code'];
		$this -> shippingMethodActive = $row['isActive'];
		$this -> shippingMethodName = $row['Name'];
		$this -> shippingCompany = $row['Company'];
    }
	
	public function newShipment() {
		try {
			$this -> db -> insert("shipments", array('orderId' => $this -> orderID,
				   	              'responseData' => 'view/shipmentResponses/' . $this -> responseData,
	        	      			  "shipmentDate" => date("Y-m-d", $this -> currentTime -> coloradoTime()),
                                  'shipmentType' => $this -> shipmentType,
                                  'shipmentImage' => $this -> shipmentImage));
							  
		} catch (Exception $e) {
    		$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
    	
    		$TrackError = new EmailServerError();
			$TrackError -> message = NORMAL_CHECKOUT_EXCEPTION . $e-> getMessage();
			$TrackError -> type = TRACKING_EMAIL_EXCEPTION;
			$TrackError -> SendMessage();
			
			
			$this -> redirect -> redirectPage('cms/transaction/' . $this -> orderID); 
		}
		
	}
	
	public function voidShipment() {
		//need to refund shipping charges...or maybe not?
		try {
			unlink($this -> responseData);
			$sth = $this -> db -> prepare("DELETE FROM shipments WHERE orderId = :orderId");
			$sth -> execute(array(':orderId' => $this -> orderID));
			
			
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/transaction/' . $this -> orderID); 
		}
		
	}
	
	public function getTrackingNumber() {
		$label = simplexml_load_string($this -> $responseData);
    	$label->registerXPathNamespace('ship', 'http://www.ups.com/XMLSchema/XOLTWS/Ship/v1.0');
     	$val = $label->xpath('//ship:ShipmentResponse/ship:ShipmentResults/ship:PackageResults/ship:TrackingNumber[1]');
        return $val[0];		
	}
	



}