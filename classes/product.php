<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Product extends BaseObject {

	public $ProductID;
	public $Category;
	public $ProductName;
	private $ProductColorImages = array();
	public $ProductImageMain;
	public $newProductImageMain;
	private $currentProductImage;
	public $publishState;
	public $price;
	public $length;
	public $width;
	public $height;
	public $weight;	
	public $spec_outer;
	public $spec_inner;
	public $spec_strap;
	public $spec_cap;
	public $spec_grommet;
	public $spec_volumne;
	public $description;
	
	
    public function __sleep()
    {
        parent::__sleep();
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }

    public static function withID($productID) {
        $instance = new self();
        $instance -> ProductID = $productID;
        $instance -> loadByProductID();
		$instance -> loadColors();
        return $instance;
    }
	
	private function loadByProductID() {
		$sth = $this -> db -> prepare('SELECT * FROM Products WHERE product_id = :product_id');
        $sth->execute(array(':product_id' => $this -> ProductID));
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
	protected function fill(array $row){
        $this -> Category = $row['product_category'];
		$this -> ProductImageMain = $row['product_image'];
		$this -> ProductName = $row['product_name'];
		$this -> currentProductImage = $row['product_image'];
		$this -> weight = $row['weight'];
    }
	
	public function validate() {
		
		if(isset($this -> ProductID)) {
			if(!empty($_FILES['productImage']['name'])) {
				if($_FILES['productImage']['error'] !== UPLOAD_ERR_OK) {
					$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
					return false;
				} 				
			} else {
				if($this -> validate -> emptyInput($this -> ProductName)) {
					$this->json->outputJqueryJSONObject("emptyProductName", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Product Name")));
					return false;
				} else if($this -> validate -> emptyInput($this -> price)) {
					$this->json->outputJqueryJSONObject("emptyPrice", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Price")));
					return false;
				} else if($this -> validate -> emptyInput($this -> spec_outer)) {
					$this->json->outputJqueryJSONObject("emptySpecOuter", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Outer")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_inner)) {
					$this->json->outputJqueryJSONObject("emptySpecInner", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Inner")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_strap)) {
					$this->json->outputJqueryJSONObject("emptySpecStrap", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Strap")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_cap)) {
					$this->json->outputJqueryJSONObject("emptySpecCap", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Cap")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_grommet)) {
					$this->json->outputJqueryJSONObject("emptySpecGrommet", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Grommet")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> width)) {
					$this->json->outputJqueryJSONObject("emptySpecWidth", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Width")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> height)) {
					$this->json->outputJqueryJSONObject("emptySpecHeight", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Height")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> length)) {
					$this->json->outputJqueryJSONObject("emptySpecLength", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Depth")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> weight)) {
					$this->json->outputJqueryJSONObject("emptySpecWeight", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Weight")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_volumne)) {
					$this->json->outputJqueryJSONObject("emptySpecVolume", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Volumne")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> description)) {
					$this->json->outputJqueryJSONObject("emptyDescription", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Product Description")));
					return false;	
				}
			}
		} else {
			if(!empty($this -> ProductImageMain)) {
				if($_FILES['productImage']['error'] !== UPLOAD_ERR_OK) {
					$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
					return false;
				} 				
			} else {
				if($this -> validate -> emptyInput($this -> ProductName)) {
					$this->json->outputJqueryJSONObject("emptyProductName", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Product Name")));
					return false;
				} else if($this -> validate -> emptyInput($this -> price)) {
					$this->json->outputJqueryJSONObject("emptyPrice", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Price")));
					return false;
				} else if($this -> validate -> emptyInput($this -> spec_outer)) {
					$this->json->outputJqueryJSONObject("emptySpecOuter", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Outer")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_inner)) {
					$this->json->outputJqueryJSONObject("emptySpecInner", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Inner")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_strap)) {
					$this->json->outputJqueryJSONObject("emptySpecStrap", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Strap")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_cap)) {
					$this->json->outputJqueryJSONObject("emptySpecCap", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Cap")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_grommet)) {
					$this->json->outputJqueryJSONObject("emptySpecGrommet", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Grommet")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> width)) {
					$this->json->outputJqueryJSONObject("emptySpecWidth", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Width")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> height)) {
					$this->json->outputJqueryJSONObject("emptySpecHeight", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Height")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> length)) {
					$this->json->outputJqueryJSONObject("emptySpecLength", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Depth")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> weight)) {
					$this->json->outputJqueryJSONObject("emptySpecWeight", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Weight")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> spec_volumne)) {
					$this->json->outputJqueryJSONObject("emptySpecVolume", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Specifications: Volumne")));
					return false;	
				} else if($this -> validate -> emptyInput($this -> description)) {
					$this->json->outputJqueryJSONObject("emptyDescription", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Product Description")));
					return false;	
				}	
			}
			
			
		}
		return true;
	}
	
	public function Save() {
		try {
			
		
			if(isset($this -> ProductID)) {
				
				if(!empty($_FILES['productImage']['name'])) {
					ini_set('memory_limit', '-1');
			
					$kaboom = explode(".", $this -> ProductImageMain); // Split file name into an array using the dot
					$fileExt = end($kaboom);
				
			
					Image::moveImage($this -> ProductImageMain, $_FILES["productImage"]["tmp_name"], "view/purchase/product-images/");
					Image::cropImage($this -> ProductImageMain, DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_WIDTH, "view/purchase/product-images/");
					
					Image::receiptImage("view/purchase/product-images/" . $this -> ProductImageMain, "view/purchase/product-images/reciept_" . $this -> ProductImageMain, 100, 100);
					
					$postData = array("product_name" => $this -> ProductName,
									  "price" => $this -> price,
									  "product_image" => $this -> ProductImageMain,
									  "product_description" => str_replace("\n", '<br />',$this -> description),
									  "weight" => $this -> weight,
									  "length" => $this -> length,
									  "width" => $this -> width,
									  "height" => $this -> height,
									  "spec_outer" => $this -> spec_outer,
									  "spec_inner" => $this -> spec_inner,
									  "spec_strap" => $this -> spec_strap,
									  "spec_cap" => $this -> spec_cap,
									  "spec_grommet" => $this -> spec_grommet,
									  "spec_volumne" =>	$this -> spec_volumne);	
								
						if(isset($this -> currentProductImage)) {
							unlink("view/purchase/product-images/" . $this -> currentProductImage);		
						}			  
						
				} else {
					$postData = array("product_name" => $this -> ProductName,
									   "price" => $this -> price,
									   "product_description" => str_replace("\n", '<br />',$this -> description),
									   "weight" => $this -> weight,
									   "length" => $this -> length,
									   "width" => $this -> width,
									   "height" => $this -> height,
									   "spec_outer" => $this -> spec_outer,
									   "spec_inner" => $this -> spec_inner,
									   "spec_strap" => $this -> spec_strap,
									   "spec_cap" => $this -> spec_cap,
									   "spec_grommet" => $this -> spec_grommet,
									   "spec_volumne" =>	$this -> spec_volumne);
				}
				
				$where = array('product_id' => $this -> ProductID,
							   'product_category' => $this -> Category);
				
				$this -> db -> update("Products", $postData, $where);
				$this -> msg -> set($this -> msg -> flashMessage('message generic', "Product Updated"));
				
				$this->json->outputJqueryJSONObject("redirect", PATH . 'cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
				
				//$this -> redirect -> redirectPage('cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
				
				
				
			} else {
				if(!empty($_FILES['productImage']['name'])) {
					ini_set('memory_limit', '-1');
			
					$kaboom = explode(".", $this -> ProductImageMain); // Split file name into an array using the dot
					$fileExt = end($kaboom);
				
					Image::moveImage($this -> ProductImageMain, $_FILES["productImage"]["tmp_name"], "view/purchase/product-images/");
					Image::cropImage($this -> ProductImageMain, DESIRED_IMAGE_WIDTH, DESIRED_IMAGE_WIDTH, "view/purchase/product-images/");
				
					Image::receiptImage("view/purchase/product-images/" . $this -> ProductImageMain, "view/purchase/product-images/reciept_" . $this -> ProductImageMain, 100, 100);
				
					$postData = array('product_name' => $this -> ProductName,
									  'price' => $this -> price, 
					    	          'product_image' => $this -> ProductImageMain,
									  'product_description' => str_replace("\n", '<br />',$this -> description),
									  'product_category' => $this -> Category,
									  'length' => $this -> length,
									  'width' => $this -> width,
									  'height' => $this -> height,
									  'weight' => $this -> weight,
									  'spec_outer' => $this -> spec_outer,
									  'spec_inner' => $this -> spec_inner,
									  'spec_strap' => $this -> spec_strap,
									  'spec_cap' => $this -> spec_cap,
									  'spec_grommet' => $this -> spec_grommet,
									  'spec_volumne' => $this -> spec_volumne);	
															
				} else {
					$postData = array('product_name' => $this -> ProductName,
									  'price' => $this -> price, 
									  'product_description' => str_replace("\n", '<br />',$this -> description),
									  'product_category' => $this -> Category,
								      'length' => $this -> length,
									  'width' => $this -> width,
									  'height' => $this -> height,
								      'weight' => $this -> weight,
									  'spec_outer' => $this -> spec_outer,
									  'spec_inner' => $this -> spec_inner,
									  'spec_strap' => $this -> spec_strap,
								      'spec_cap' => $this -> spec_cap,
									  'spec_grommet' => $this -> spec_grommet,
									  'spec_volumne' => $this -> spec_volumne);
				}
					
				
				$this -> db -> insert("Products", $postData);
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Product Added"));
			
				//$this -> redirect -> redirectPage('cms/editProduct?ID='. $this -> db -> lastInsertId() . "&productType=" . $this -> Category);
				
				$this->json->outputJqueryJSONObject("redirect", PATH . 'cms/editProduct?ID='. $this -> db -> lastInsertId() . "&productType=" . $this -> Category);
				
			}
		
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			if(isset($this -> ProductID)) {
				$this -> redirect -> redirectPage('cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
			} else {
				$this -> redirect -> redirectPage('cms/addNewProduct?productType=' . $this -> Category);
			//addNewProduct?productType=Nada_Basic
			}
			//$this -> redirect -> redirectPage('cms/products');
		}
		
	}
	
	public function loadColors() {
		$sth = $this -> db -> prepare('SELECT * FROM ProductColors WHERE productId = :productId');
        $sth->execute(array(':productId' => $this -> ProductID));
		$this -> ProductColorImages = $sth->fetchAll();
	}
	
	public function delete() {
		try {
	        //$this -> deleteColorImages();
	        	$this -> deleteProductColors();
	        	$this -> deleteProductItem();
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Product Deleted"));
			$this -> redirect -> redirectPage('cms/products?Category=' . $this-> Category);
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/products');
		}
	}

    private function deleteColorImages() {
        /* Not sure if we want to still delete the images since we are just going to just mark it as deleted*/
		foreach($this -> ProductColorImages as $colorImage) {
			if (file_exists("view/purchase/product-images/" . $colorImage['productColorImage'])) {
				unlink("view/purchase/product-images/" . $colorImage['productColorImage']);
			}
		}
        
	}

    private function deleteProductColors() {
		$sth = $this -> db -> prepare("UPDATE ProductColors SET IsActive=0 WHERE productId = :productId");
		$sth -> execute(array(':productId' => $this -> ProductID));
	}
	
	private function deleteProductItem() {
		
		$sth = $this -> db -> prepare("UPDATE Products set IsActive = 0 WHERE product_id = :product_id");
		$sth -> execute(array(':product_id' => $this -> ProductID));
		/* Not sure if we want to still delete the images since we are just going to just mark it as deleted*/
		if(!empty($this -> ProductImageMain)) {
			if (file_exists("view/purchase/product-images/" . $this -> ProductImageMain)) {
				unlink("view/purchase/product-images/" . $this -> ProductImageMain);
			}	
		}
		
    }
	
	public function publishProduct() {
		try {
		
			$postData = array("publishState" => $this -> publishState);
			
			
			$where = array('product_id' => $this -> ProductID,
						   'product_category' => $this -> Category);
			
			$this -> db -> update("Products", $postData, $where);
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Product Published"));
			$this -> redirect -> redirectPage('cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
		}
	}
	
	public function unpublishProduct() {
		try {
			$postData = array("publishState" => $this -> publishState);
			
			
			$where = array('product_id' => $this -> ProductID,
						   'product_category' => $this -> Category);
			
			$this -> db -> update("Products", $postData, $where);
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Product Unpublished"));
			$this -> redirect -> redirectPage('cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/editproduct?ID=' . $this -> ProductID . '&productType=' . $this -> Category);
		}
	}
}