<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */
class Settings extends BaseObject {
	
	public $companyName;
	public $companyShippingAddress;
	public $companyShippingAddress2;
	public $shippingCity;
	public $shippingState;
	public $shippingZip;
	public $trackingEmailText;
	public $locateLifeSettings;
	
	public $contactEmail;
	public $receiptEmail;
	public $inventoryEmail;
	public $warranties;
	public $paymentEmail;
	
	public $paymentSucceded;
	public $paymentFailed;
	public $paymentRefunded;
	public $paymentCaptured;
	public $paymentDisputeCreated;
	public $paymentDisputedFundsWithdrawn;
	public $paymentDisputedFundsReinstated;
	public $paymentDisputedUpdated;
	public $paymentDisputedClose;
	public $shippingCompany;
	
	public $shippingMethodID;
	public $shippingMethodActive;
	
	public $companyPhoneAreaCode;
	public $companyPhone1;
	public $companyPhone2;
	
	public $preSaleVisible;
	public $preSaleText;
	
	private $companyPhone;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }

    private static $singletonSettings;
    private static $initialValue = 0;
    public static function GetSettings(){
        if(empty(self::$singletonSettings)){
            $s = new Settings();
            $sth = $s -> db -> prepare('SELECT * FROM settings WHERE settingsID = :settingsID');
            $sth->execute(array(':settingsID' => 1));
            $record = $sth -> fetch();
            $s->fill($record);
            self::$singletonSettings = $s;
        }
        return self::$singletonSettings;
    }
	
	protected function fill(array $row){
		$this -> companyName = $row['CompanyName'];
		$this -> companyShippingAddress = $row['ShippingAddress'];
		$this -> companyShippingAddress2 = $row['ShippingAddress2'];
		$this -> shippingCity = $row['ShippingCity'];
		$this -> shippingState = $row['ShippingState'];
		$this -> shippingZip = $row['ShippingZip'];		
		$this -> trackingEmailText = $row['TrackingEmailText'];
		
		$this -> contactEmail = $row['contactEmail'];
		$this -> receiptEmail = $row['ReceiptEmail'];
		$this -> inventoryEmail = $row['InventoryEmail'];
		$this -> warranties = $row['WarrantiesEmail'];
		$this -> paymentEmail = $row['PaymentsEmail'];
				
		$this -> paymentSucceded = $row['paymentSucceded'];
		$this -> paymentFailed = $row['paymentFailed'];
		$this -> paymentRefunded = $row['paymentRefunded'];
		$this -> paymentCaptured = $row['paymentCaptured'];
		$this -> paymentDisputeCreated = $row['paymentDisputeCreated'];
		$this -> paymentDisputedFundsWithdrawn = $row['paymentDisputeFundsWithdrawn'];
		$this -> paymentDisputedFundsReinstated = $row['paymentDisputeFundsReinstated'];
		$this -> paymentDisputedUpdated = $row['paymentDisputeUpdated'];
		$this -> paymentDisputedClose = $row['paymentDisputeClosed'];
		$this -> shippingCompany = $row['ShippingCompany'];
		$this -> companyPhone = $row['shippingPhoneNumber'];
		
		$this -> preSaleVisible = $row['PreSaleShow'];
		$this -> preSaleText = $row['PreSaleText'];
		
    }
	
	public function getCompanyName() {
		return $this -> companyName;
	}
	
	public function getCompanyAddress() {
		return $this -> companyShippingAddress;
	}
	
	public function getCompanyCity() {
		return $this -> shippingCity;
	}
	
	public function getCompanyState() {
		return $this -> shippingState;
	}
	
	public function getCompanyZip() {
		return $this -> shippingZip;
	}
	
	public function getContactEmail() {
		return $this -> contactEmail;
	}
	
	public function getReceiptEmail() {
		return $this -> receiptEmail;
	}
	
	public function getWarrantyEmail() {
		return $this -> warranties;
	}
	
	public function getTrackingEmailText() {
		return $this -> trackingEmailText;
	}
	
	public function getPaymentEmail() {
		return $this -> paymentEmail;
	}
	
	public function getInventoryEmail() {
		return $this -> inventoryEmail;
	}
	
	public function getCompanyPhoneNumber() {
		return $this -> companyPhone;
	}
	
	public function validateEmailSettings() {
		if($this -> validate -> emptyInput($this -> contactEmail)) {
			$this -> json -> outputJqueryJSONObject('ContactEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Contact Email")));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> contactEmail)) {
			$this -> json -> outputJqueryJSONObject('verifyContactEmail', $this -> msg -> flashMessage('message error', 'Contact Email: ' . $this -> msg -> emailFormatRequiredMessage()));
			return false;
		//receipt email
		} else if($this -> validate -> emptyInput($this -> receiptEmail)) {
			$this -> json -> outputJqueryJSONObject('ReceiptEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Receipt Email")));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> receiptEmail)) {
			$this -> json -> outputJqueryJSONObject('verifyReceiptEmail', $this -> msg -> flashMessage('message error', 'Receipt Email: ' . $this -> msg -> emailFormatRequiredMessage()));
			return false;
		//Inventory Email
		} else if($this -> validate -> emptyInput($this -> inventoryEmail)) {
			$this -> json -> outputJqueryJSONObject('InventoryEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Inventory Email")));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> inventoryEmail)) {
			$this -> json -> outputJqueryJSONObject('verifyInventoryEmail', $this -> msg -> flashMessage('message error', 'Inventory Email: ' . $this -> msg -> emailFormatRequiredMessage()));
			return false;
		//warranty email
		} else if($this -> validate -> emptyInput($this -> warranties)) {
			$this -> json -> outputJqueryJSONObject('WarrantyEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Warranty Email")));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> warranties)) {
			$this -> json -> outputJqueryJSONObject('verifyWarrantyEmail', $this -> msg -> flashMessage('message error', 'Warranty Email: ' . $this -> msg -> emailFormatRequiredMessage()));
			return false;
		//payment email
		} else if($this -> validate -> emptyInput($this -> paymentEmail)) {
			$this -> json -> outputJqueryJSONObject('PaymentEmail', $this -> msg -> flashMessage('message error', 'Payments Email: ' . $this -> msg -> emailFormatRequiredMessage()));
			return false;
		} 
		return true;
	}

	public function validateTrackingEmailText() {
		if($this -> validate -> emptyInput($this -> trackingEmailText)) {
			$this -> json -> outputJqueryJSONObject('trackingEmailText', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Tracking Email Text")));
			return false;
		}
		return true;
	}
	
	public function validatePreSaleText() {
		if($this -> validate -> emptyInput($this -> preSaleText)) {
			$this -> json -> outputJqueryJSONObject('emptyPreSaleText', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Pre-Sale Text")));
			return false;
		}
		return true;
	}
	
	public function saveTrackingEmailText() {
		try {
			$this -> db -> update("settings", array('TrackingEmailText' => str_replace("\n", '<br />', $this -> trackingEmailText)), array("settingsID" => 1));
			$this -> json -> outputJqueryJSONObject('trackingEmailTextSaved', $this -> msg->flashMessage('message success', "Tracking Email Updated"));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	
	public function SavePurchaseSettings() {
		try {
			$this -> db -> update("settings", array('PreSaleShow' => $this -> preSaleVisible,
													'PreSaleText' => str_replace("\n", '<br />', $this -> preSaleText)), array("settingsID" => 1));
			$this -> json -> outputJqueryJSONObject('purchaseSettingsSaved', $this -> msg->flashMessage('message success', "Purchase Settings Updated"));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}

	public function saveEmailSettings() {
		try {
		
			
			$postData = array("contactEmail" => $this -> contactEmail,
							  "ReceiptEmail" => $this -> receiptEmail,
							  "InventoryEmail" => $this -> inventoryEmail,
							  "WarrantiesEmail" => $this -> warranties,
							  "PaymentsEmail" => $this -> paymentEmail);
					
			$this -> db -> update("settings", $postData,  array('settingsID' => 1));
			
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Email Settings Updated"));
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
		
	}
	
	public function validateCompanyInformation() {
		if($this -> validate -> emptyInput($this -> companyName)) {
			$this -> json -> outputJqueryJSONObject('companyName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Company Name")));
			return false;
		} else if($this -> validate -> emptyInput($this -> companyShippingAddress)) {
			$this -> json -> outputJqueryJSONObject('companyAddress', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Company Address")));
			return false;
		} else if($this -> validate -> emptyInput($this -> shippingCity)) {
			$this -> json -> outputJqueryJSONObject('companyCity', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Company City")));
			return false;
		} else if($this -> validate -> emptyInput($this -> shippingState)) {
			$this -> json -> outputJqueryJSONObject('companyState', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Company State")));
			return false;
		} else if($this -> validate -> emptyInput($this -> shippingZip)) {
			$this -> json -> outputJqueryJSONObject('companyZip', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Company Zip")));
			return false;
		} else if($this -> validate -> emptyInput($this -> shippingZip)) {
			$this -> json -> outputJqueryJSONObject('companyZip', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Company Zip")));
			return false;
		} else if($this -> validate -> emptyInput($this -> companyPhoneAreaCode)) {
			$this -> json -> outputJqueryJSONObject('areaCodeMissing', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Area Code")));
			return false;
		} else if($this -> validate -> emptyInput($this -> companyPhone1)) {
			$this -> json -> outputJqueryJSONObject('emptyPhonePart1', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Phone Part 1:")));
			return false;
		} else if($this -> validate -> emptyInput($this -> companyPhone2)) {
			$this -> json -> outputJqueryJSONObject('emptyPhonePart2', $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Phone Part 2:")));
			return false;
		}
		 
		return true;
	}

	public function saveCompanyInformation() {
		try {
			
			$postData = array("CompanyName" => $this -> companyName,
						      "ShippingAddress" => $this -> companyShippingAddress,
						      "ShippingAddress2" => $this -> companyShippingAddress2,
							  "ShippingCity" => $this -> shippingCity,
							  "ShippingState" => $this -> shippingState,
							  "ShippingZip" => $this -> shippingZip, 
							  "shippingPhoneNumber" => $this -> companyPhoneAreaCode . $this -> companyPhone1 . $this -> companyPhone2);
								  
							  
			$where = array("settingsID" => 1);
			
			$this -> db -> update("settings", $postData, $where);
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Company Information Updated"));
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	
	public function saveShippingCompany() {
		try {
			
			
			$postData = array("ShippingCompany" => $this -> shippingCompany);
							  
			$where = array("settingsID" => 1);
			
			$this -> db -> update("settings", $postData, $where);
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Shipping Company Updated"));
			
		} catch (Exception $e) {
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	
	public function saveLocateLifeSettings() {
		try {
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Locate Life settings updated"));
			$where = array('settingsID' => 1);
			$postData = array('locateLifeSettings' => $this -> locateLifeSettings);
			$this -> db -> update("settings", $postData, $where);
			
		} catch (Exception $e) {
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/settings');
		}
	}
	
	public function savePaymentNotifications() {
		try {
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Payment Notifications Updated"));
			$where = array('settingsID' => 1);
			
			$postData = array('paymentSucceded' => $this -> paymentSucceded,
							  'paymentFailed' => $this -> paymentFailed,
							  'paymentRefunded' => $this -> paymentRefunded,
							  'paymentCaptured' => $this -> paymentCaptured,
							  'paymentDisputeCreated' => $this -> paymentDisputeCreated,
							  'paymentDisputeFundsWithdrawn' => $this -> paymentDisputedFundsWithdrawn,
							  'paymentDisputeFundsReinstated' => $this -> paymentDisputedFundsReinstated,
							  'paymentDisputeUpdated' => $this -> paymentDisputedUpdated,
							  'paymentDisputeClosed' => $this -> paymentDisputedClose);
			
			
			$this -> db -> update("settings", $postData, $where);
			
		} catch (Exception $e) {
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/settings');
		}
	}

	public function saveShippingMethods() {
		try {
			
			$where = array('id' => $this -> shippingMethodID);
			
			$postData = array('isActive' => $this -> shippingMethodActive);
			
			
			$this -> db -> update("ShippingMethods", $postData, $where);
			$this -> msg -> set($this -> msg -> flashMessage('message success', "Shipping Methods Saved"));
			
			$this -> redirect -> redirectPage('cms/settings/methods');
		} catch (Exception $e) {
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			$this -> redirect -> redirectPage('cms/settings/methods');
		}
	}
	

}