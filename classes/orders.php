<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Orders extends BaseObject {
	
	public $orderID;
	public $firstName;
	public $lastName;
	public $email;
	
	public $address;
	public $city;
	public $state;
	public $zip;
	public $country;
	public $countryAbbr;
	public $phone;
	public $orderDate;
	public $orderYear;
	public $getStatus;
	public $stripeStatus;
	public $taxRate;
	public $orderTime;
	public $cartTotal;
	
	public $stripeCustomerID;
	public $stripeCheckoutID;
	public $shippingMethodName;
	public $shippingMethodCode;
	public $currentShippingRates;
	public $updatedShippingRates;
	public $stripeToken;
	public $addressValidation;
	public $cartItems = array();
	public $packageDimensions = array();
	public $getReceiptEmail;
	public $warrantyVerificatonID;
	
	public $preSaleTextVisible;
	public $preSaleText;
	
	public $receiptAddres;
	public $receiptCity;
	public $receiptState;
	public $receiptZip;
	public $receiptCountry;
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }
	
    public static function WithID($orderId) {		
        $instance = new self();
        $instance-> orderID = $orderId;
        $instance->loadById();
        return $instance;
    }
    
    public static function WithPaymentStatus($status) {		
        $instance = new self();
        $instance-> getStatus = $status;
        $instance->loadByStatus();
        return $instance;
    }
	
    protected function loadByStatus() {
        $sth = $this -> db -> prepare('SELECT * FROM orders WHERE stripeCheckoutID = :stripe_checkout_id');
        $sth->execute(array(':stripe_checkout_id' => $this-> getStatus));
        $record = $sth -> fetch();
        $this->fill($record);
    }	
 

    protected function loadByID() {
        $sth = $this -> db -> prepare('SELECT * FROM orders WHERE orderID = :orderID');
        $sth->execute(array(':orderID' => $this-> orderID));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
    protected function fill(array $row) {
    	$this -> orderID = $row['orderID'];
        $this -> firstName = $row['firstName'];
        $this -> lastName = $row['lastName'];
		$this -> email = $row['email'];
		$this -> address = $row['address'];
		$this -> city = $row['city'];
		$this -> state = $row['state'];
		$this -> zip = $row['zip'];
		$this -> phone = $row['telephone'];
		$this -> stripeCustomerID = $row['stripeCustomerID'];
		$this -> stripeCheckoutID = $row['stripeCheckoutID'];
		$this -> shippingMethodCode = $row['shippingMethodCode'];
		$this -> currentShippingRates = $row['shippingRate'];
		$this -> orderDate = $row['date'];
		$this -> taxRate = $row['taxRate'];
		$this -> cartTotal = $row['cartTotal'];
		$this -> orderTime = $row['time'];
		$this -> updatedShippingRates = $row['newShippingRate'];
		$this -> warrantyVerificatonID = $row['VerificationWarrantyID'];
		$this -> preSaleTextVisible = $row['preSaleTextVisible'];
		$this -> preSaleText = $row['preSaleText'];
		
		$this -> receiptAddres = $row['receiptAddress'];
		$this -> receiptCity = $row['receiptCity'];
		$this -> receiptState = $row['receiptState'];
		$this -> receiptZip = $row['receiptZip'];
		$this -> receiptCountry = $row['receiptCountry'];
		
		
    }

	public function getFullName() {
		return $this->firstName . " " . $this->lastName;
	}
	
	public function getShippingAddress() {
		return $this -> address . '<br />' . $this -> city . ', ' .  $this -> state . ' ' . $this -> zip;
	}
	
	public function finishOrder() {
		try {
			$this->db->update('orders', array('isFinished' => 1), array('orderID' => $this->orderID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Order #: ". $this->orderID . " Completed"));
			$this -> redirect -> redirectPage('cms/transaction/'. $this -> orderID);	
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/transaction/'. $this -> orderID);
		}
		
	}

	public function unFinishOrder() {
		try {
			$this->db->update('orders', array('isFinished' => 0), array('orderID' => $this->orderID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Order #: ". $this->orderID . " Not Completed"));
			$this -> redirect -> redirectPage('cms/transaction/'. $this -> orderID);			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/transaction/'. $this -> orderID);
		}

	}
	
	public function voidOrder() {
		try {
			$postData = array("isVoided" => 1,
					  "isFinished" => 3);
			$where = array('orderID' => $this-> orderID); 
						
			$this -> db -> update("orders", $postData, $where);	
			
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Transaction Voided"));
			$this -> redirect -> redirectPage('cms/transaction/' .$this-> orderID);	
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/transaction/'. $this -> orderID);
		}	
		
	}
	
	public function unVoidOrder() {
		try {
			$postData = array("isVoided" => 0,
					  "isFinished" => 0);
			$where = array('orderID' => $this-> orderID); 
					
			$this -> db -> update("orders", $postData, $where);	
		
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Transaction Unvoided"));
			$this -> redirect -> redirectPage('cms/transaction/' .$this-> orderID);	
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/transaction/'. $this -> orderID);
		}
		
	}
	
	public function updateStripeStatus() {
		try {
			$postData = array("stripeStatus" => $this -> stripeStatus);
			$where = array('orderID' => $this-> orderID); 
					
			$this -> db -> update("orders", $postData, $where);	
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
		}
	}
	
	public function approveAddress() {
		try {
			$postData = array("isValidAddress" => 4);
		
		
			$where = array('orderID' => $this-> orderID);
			
			$this -> db -> update("orders", $postData, $where);
			$this -> msg -> set($this -> msg -> flashMessage('message success', "Address approved"));
			$this -> redirect -> redirectPage('cms/transaction/' . $this-> orderID);	
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			//email
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
				
			$this -> redirect -> redirectPage('cms/transaction/' . $this-> orderID);
		}
	}
	
	public function saveOrder() {
		
		//stripe interaction
		\Stripe\Stripe::setApiKey(STRIPE_TEST_KEY);
		
		
				
		$customer = \Stripe\Customer::create(array(
  			"source" => $this -> stripeToken,
			"email" => $this -> email
		));
			
		$charge = \Stripe\Charge::create(array(
			"amount" => $this -> cartTotal * 100, // amount in cents, again
		  	"currency" => "usd",
  			"customer" => $customer['id'],
		));
		
		
		$insertedId = $this -> db -> insert("orders", array('firstName' => $this -> firstName,
				  						        'lastName' => $this -> lastName,
									       		'email' => $this -> email, 
									            'address' => $this -> address,
										   		'city' => $this -> city,
												'state' => $this -> state,
												'zip' => $this -> zip,
												'receiptAddress' => $this -> address,
										   		'receiptCity' => $this -> city,
												'receiptState' => $this -> state,
												'receiptZip' => $this -> zip,
												'receiptCountry' => $this -> country,
												'telephone' => preg_replace('/[^0-9]/','',$this  -> phone),
												'Country' => $this -> country,
												'countryAbbr' => $this -> countryAbbr,
												'date' => date("Y-m-d", $this -> currentTime -> coloradoTime()),
												'time' => date('h:i:s a', $this -> currentTime -> coloradoTime()),
	                                            'stripeCheckoutID'=> $charge['id'],
	                                            'stripeCustomerID' => $customer['id'], 
	                                            'taxRate' => $this -> taxRate, //$taxes
	                                            'shippingRate' => $this -> currentShippingRates, //$shippingRates // Just a random number chosen for now
	                                            'cartTotal' => $this -> cartTotal,
	                                            'stripeStatus' =>  $charge['status'],
												'shippingMethod' =>$this -> shippingMethodName,
												'shippingMethodCode'=> $this -> shippingMethodCode,
												'isValidAddress' => $this -> addressValidation,
												'userAgent' => getenv('HTTP_USER_AGENT'),
												'customerIDAddress' => getenv('REMOTE_ADDR'), 
												'VerificationWarrantyID' => $this -> warrantyVerificatonID,
												'preSaleTextVisible' => $this -> preSaleTextVisible,
												'preSaleText' => $this -> preSaleText));
												
		$stripeDescription = "Customer: " . $this -> firstName . " " . $this -> lastName . " / Date purchased: " . date("F jS, Y", $this -> currentTime -> coloradoTime()) . " / Order #: " . $insertedId;
		
		//update customer description
		$customerDescription = \Stripe\Customer::retrieve($customer['id']);
		$customerDescription -> description = $stripeDescription;	
		$customerDescription -> save();
								
		//update payment description			
		$checkoutIDDescription = \Stripe\Charge::retrieve($charge['id']);
		$checkoutIDDescription -> description = $stripeDescription;	
		$checkoutIDDescription -> save();
		
		
		$this -> saveOrderItems($insertedId);	
		$this -> savePackageDimensions($insertedId);		
		$this -> emailPurchasee($insertedId);
		
		unset($_SESSION['cart_array']);

		$this -> redirect -> redirectPage("cart/thankyou");										
	}

	private function saveOrderItems($insertedId) {
		//record into orderItems table
			$i =1;
	        foreach($this -> cartItems as $item) {
	        	//updating quantity
	        	$colorID = $item['productColorID'];
				$quantity = $item['quantity'];
	        	$colors = Colors::productColorID($colorID); 
				$colors -> purchasedQuantity = $quantity;
				$colors -> updateQuantity();


                $this->db->insert("orderItems", array('orderID' => $insertedId,
	                                                  'productID' => $item['productID'],
	                                                  'productColorID' => $colorID,
	                                                  'quantity' => $quantity,
	                                                  'unitPrice' => $item['unitPrice'],
	                                                  'productImage' => $item['productImage']));				  
	            $i ++;
				
				
				
	
				
				
	        }
		// UPDATE QUANTITY WHEN ORDER GOES THROUGH
        //this is working on local but on go daddy it is not
        //so I'm commeting this stuff out just in case we need it back
		//$quantityUpdateQuery = $this->db->prepare('UPDATE ProductColors AS PC INNER JOIN orderItems AS OI ON OI.productColorID = PC.productColor_id SET PC.quantityRemaining = pc.quantityRemaining - OI.quantity WHERE OI.orderID = :id');
        //$quantityUpdateQuery -> execute(array(':id'=>$insertedId));
        //$quantityUpdateQuery -> fetchAll();	

	}
	private function savePackageDimensions($insertedId) {
		 foreach($this -> packageDimensions as $item) {
		 	$newPackage = new Package();
			$newPackage -> width = $item['width'];
			$newPackage -> height = $item['height'];
			$newPackage -> length = $item['length'];
			$newPackage -> weight = $item['weight'];
			$newPackage -> orderID = $insertedId;
			$newPackage -> shippingMethodCode = $this -> shippingMethodCode;
			 
			$newPackage -> newPackage(); 		 	
		 }

	}
	
	private function emailPurchasee($insertedId) {
		setlocale(LC_MONETARY, "en_US");
		
		
		//email info to sent to receipt method	 
		//this info needs to be in this method shown below   			    				    			    
		$receiptInfo = array();
	    $receiptInfo['nada-bottle-email'] = $this -> getReceiptEmail;
		$receiptInfo['customer-email'] = $this -> email;
		$receiptInfo['order-number'] = $insertedId;
		
		
		//this is used to send a copy of the users receipt to the owners		
		Email::receipt($receiptInfo, true);
				   
		
		Email::receipt($receiptInfo);
	}

	public function generateReceipt($toSelf = false, $printPage = false) {
		
		$cartTotal = "";
		setlocale(LC_MONETARY,"en_US");
		
		$receipt = "";
		if($toSelf == true) {
			$receipt .="<table width='500' align='center' style='padding: 5px 0px 10px 0px;'><tr><td style='border:2px dashed #40aadd; text-align:center; padding:5px; color:#40aadd; background: #DAF3FF; font-weight: bold; font-size: 16px; font-family:arial'>";
			$receipt .= $this -> firstName . " " . $this -> lastName ."'s Receipt / " . $this -> orderTime ."</td></tr></table>";	
		}
		$receipt .= "<table cellpadding='0' cellspacing='0' style='margin:0 auto;' width='500'>";
		$receipt .= "<tr><td style='border-bottom: 1px solid #f4f4f4;'><table cellpadding='0' cellspacing='0' style='margin:0 auto;' width='500'><tr>";
		$receipt .= "<td style='font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;' align='left'>Your Receipt</td>";
		$receipt .= "<td style='font-family:arial;font-size: 16px; color: #40aadd; padding: 6px; font-weight: bold;' align='right'>" . $this -> currentTime -> formatDate($this -> orderDate) . "</td></tr></table></td></tr>";
		$receipt .= "<tr><td style='font-family:arial; font-size: 16px; color: #9c9c9c; padding: 6px;'>Order #: " .  $this -> orderID . "</td></tr>";
		$receipt .= "<tr><td style='text-align:center; padding: 20px 0px 30px 0px;' align='center'><img src='" . PATH . "public/images/nadaBottleLogo.png' width='200' height='95' /></td></tr>";
		$receipt .= "<tr><td style='padding:0px 0px 0px 0px;'><table cellpadding='0' cellspacing='0' style='margin:0 auto;' width='500'>";
		$receipt .= "<tr><td style='font-size:24px;font-weight:bold;color:#40aadd; font-family:arial'>Thank you for your Purchase!</td></tr>";
		$receipt .= "<tr><td style='font-size:16px; font-family:arial; color: #9c9c9c;line-height: 24px;'>Thank you for shopping with us. Returns are easy, go to our <br />";
		$receipt .= "<a style='color: #9c9c9c;' href='" . PATH  ."support/warranty'>warranty and return information</a> to fill out a warranty!<br /><br />Don’t forget to print off your receipt! You will need your Order Verification ID handy when submitting your warranty!";
		if ($this -> preSaleTextVisible == 1) {
			$receipt .= "<table cellpadding='0' cellspacing='0' style='margin:10px auto; background: #c30000;' width='500'><tr><td style='padding:10px 0px; color:white; text-align:center'>" . $this -> preSaleText ."</td></tr></table>";
		}
		
		$receipt .= "<table cellpadding='0' cellspacing='0' style='margin:10px auto; background: #f4f4f4;' width='500'><tr><td style='padding:10px 0px 10px 10px;'>Your Order Verification ID<br /><span style='font-size:20px;font-weight:bold;color: #9c9c9c; font-family:arial;'>" . $this -> warrantyVerificatonID . "</span></td>";
		if($printPage == true) {
			$receipt .= "<td style='text-align: center;' valign='middle'><a href='". PATH . "receipt?receipt=". $this -> security -> encode($this -> orderID) ."'><img src='". PATH."public/images/warrantyPrintButton.png' width='26' height='29' /></a></td></tr>";	
		}
		
		$receipt .= "</table></td></tr>";
		$receipt .= "<tr><td style='padding:0px 0px 0px 0px;'><table cellpadding='0' cellspacing='0' style='margin:0 auto;' width='500'><tr>";
		$receipt .= "<td style='font-size:16px;color:#40aadd; font-family:arial; padding:0px 0px 3px 0px'>Shipping To</td></tr><tr>";
		$receipt .= "<td style='font-size:16px; font-family:arial; color: #9c9c9c; line-height: 24px;'>" .$this -> receiptAddres. "<br />";
		$receipt .= $this -> receiptCity . ", " .$this -> receiptState . " " . $this -> receiptZip . "<br />";
		$receipt .=	 $this -> receiptCountry. "<br />" . "<a style='color:#40aadd;font-size: 12px;' href='mailto:nadawaterbottle@gmail.com?subject=Shipping Address Change'>Need a change of address?</a></td></tr></table></td></tr>";
		$receipt .= "<tr><td style='padding:30px 0px 0px 0px'><table cellpadding='0' cellspacing='0' style='margin:0 auto;' width='500'>";
		$receipt .= "<tr><td style='border-bottom: 1px solid #f4f4f4; padding:0px 0px 8px 0px'></td><td style='border-bottom: 1px solid #f4f4f4; padding:0px 0px 8px 0px'></td><td style='border-bottom: 1px solid #f4f4f4; padding:0px 0px 8px 0px; font-family:arial; font-size: 16px; color: #9c9c9c; text-align:center;'>Qnty</td><td style='border-bottom: 1px solid #f4f4f4; padding:0px 5px 8px 0px; font-family:arial; font-size: 16px; color: #9c9c9c; text-align:right'>Total</td></tr>";
		foreach ($this -> getOrderList() as $key => $value) {
			
			//generate the item amounts amount
			$priceString =  money_format("%10.2n", $value['unitPrice']);
			$quantityTotalPrice = $value['unitPrice'] * $value['quantity'];
			$itemTotal = money_format("%10.2n", $quantityTotalPrice);
			$cartTotal += $quantityTotalPrice;
				
			$receipt .= "<tr>";
			$receipt .= "<td align='left' width='100' style='padding:8px; border-bottom: 1px solid #f4f4f4;'>
							<img src='" . PATH . "view/purchase/product-images/receipt_" . $value['productImage'] ."' width='100' />
						</td>";
			$receipt .= "<td align='left' style='font-family:arial; padding:8px; border-bottom: 1px solid #f4f4f4;'>
							<strong>" . $value['product_name'] . "</strong><br />
							Color: " . $value['Color'] . "<br />
							Price: " . str_replace(' ', '', $priceString) .
						"</td>";
			$receipt .= "<td style='text-align:center; border-bottom: 1px solid #f4f4f4; padding:0px 0px 8px 0px; font-family:arial; font-size: 16px;'>" . $value['quantity'] . "</td>";
			$receipt .= "<td style='text-align:right; border-bottom: 1px solid #f4f4f4; padding:0px 5px 8px 0px; font-family:arial; font-size: 16px;'>" . str_replace(' ', '', $itemTotal) . "</td></tr>";
						
			
		}
		
		//generate total amounts
		$cartTotalPriceFormat = money_format("%10.2n", $cartTotal);
		$taxRateTotal = money_format("%10.2n", $this -> taxRate);
		$shippingRates = money_format("%10.2n", $this -> currentShippingRates);
		$grandTotal = $cartTotal + $this -> taxRate + $this -> currentShippingRates;
		$grandTotalString = money_format("%10.2n", $grandTotal);
				
		$receipt .= "</table></td></tr>";
		$receipt .= "<tr><td><table cellpadding='0' cellspacing='0' style='float: right; margin: 20px 0px 10px 0px;' width='400'>";
		
		$receipt .= "<tr><td style='border-bottom:1px solid #f4f4f4;font-size:16px'>";
		$receipt .= "<table width='100%'><tr><td style='color:#9c9c9c; font-family:arial;'>Cart Total</td>";
		$receipt .= "<td align='right' style='padding:5px;color:black; font-family:arial;'>";    
		$receipt .=	 str_replace(' ', '', $cartTotalPriceFormat) . "</td></tr></table></td></tr>";
		$receipt .= "<tr><td style='border-bottom:1px solid #f4f4f4;font-size:16px'><table width='100%'><tr><td style='color:#9c9c9c; font-family:arial;'>Sales Tax</td>";
		$receipt .= "<td align='right' style='padding:5px;color:black; font-family:arial;'>";
		$receipt .= str_replace(' ', '', $taxRateTotal) . "</td></tr></table></td></tr><tr>";
		$receipt .= "<td style='border-bottom:1px solid #f4f4f4;font-size:16px'><table width='100%'><tr><td style='color:#9c9c9c; font-family:arial;'>Shipping</td>";
		$receipt .= "<td align='right' style='padding:5px;color:black; font-family:arial;'>";
		$receipt .= str_replace(' ', '', $shippingRates) . "</td></tr></table></td></tr>";
		$receipt .= "<tr><td style='padding:5px;font-size:20px;color:black;text-align:right; font-family:arial;'>Your Total: "; 
		$receipt .= str_replace(' ', '', $grandTotalString) . "</td></tr>";
		
		$receipt .= "</table></td></tr>";
		$receipt .= "<tr><td style='text-align:center;color:#40aadd; font-size:12px; font-family:arial; padding:0px 0px 15px 0px'>" . $this -> currentTime -> getYear($this -> orderDate) . " © Nada Bottle, LLC. All Rights Reserved.</td></tr>";
		
		$receipt .= "</table>";
		
		
		
		return $receipt;
	}

	
	private function getOrderList() {
		$itemQuery = $this -> db -> prepare('SELECT ProductColors.Color, Products.product_name, orderItems.quantity, orderItems.unitPrice, orderItems.productImage FROM orderItems '
	          									.'INNER JOIN Products on orderItems.productID = Products.product_id '
	           									.'INNER JOIN ProductColors on orderItems.productColorID = ProductColors.productColor_id '
	           									.' WHERE orderID = :id');

        $itemQuery -> execute(array(':id' => $this -> orderID));
		
		return $itemQuery -> fetchAll();
	}

		

}