<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class ShipmentBoxes extends BaseObject {
	
	public $boxID;
	public $width;
	public $height;
	public $length;
	public $itemsHold;
	public $weight;
	
	public static function WithID($boxID) {
		$instance = new self();
        $instance-> boxID = $boxID;
        $instance->loadById();
        return $instance;
	}
	
	public static function WithLargestItem($items) {
		$instance = new self();
        $instance -> itemsHold = $items;
        $instance ->loadByItems();
        return $instance;
	}
	

	
	protected function loadById() {
        $sth = $this -> db -> prepare('SELECT * FROM ShippingBoxes WHERE id = :id');
        $sth->execute(array(':id' => $this-> boxID));
        $record = $sth -> fetch();
        //$this->fill($record);
    }
	
	protected function loadByItems() {
        $sth = $this -> db -> prepare('SELECT * FROM ShippingBoxes WHERE items = :items');
        $sth->execute(array(':items' => $this-> itemsHold));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	

    
    protected function fill(array $row) {
    	$this -> boxID = $row['id'];
		$this -> width = $row['width'];
		$this -> height = $row['height'];
		$this -> length = $row['length'];
		$this -> itemsHold = $row['items'];
		$this -> weight = $row['weight'];

    }
	
	
	public function validate() {
		$packageConstrants = 165;
		
		$packageValue = ($this -> width * 2) + ($this -> height * 2) + $this -> length;
		
		
		if($this -> validate -> emptyInput($this -> length)) {
			$this -> json -> outputJqueryJSONObject('boxLenthEmpty', $this -> msg->flashMessage('message error', "Length is required"));	
			return false;
		} else if($this -> validate -> HasLetters($this -> length)) {
			$this -> json -> outputJqueryJSONObject('boxLengthNumbers', $this -> msg->flashMessage('message error', "Length: Only Numbers"));	
			return false;
		} else if($this -> validate -> emptyInput($this -> width)) {
			$this -> json -> outputJqueryJSONObject('boxWidthEmpty', $this -> msg->flashMessage('message error', "Width is required"));	
			return false;
		} else if($this -> validate -> HasLetters($this ->width)) {
			$this -> json -> outputJqueryJSONObject('boxWidthNumbers', $this -> msg->flashMessage('message error', "Width: Only Numbers"));	
			return false;
		} else if($this -> validate -> emptyInput($this -> height)) {
			$this -> json -> outputJqueryJSONObject('boxHeightEmpty', $this -> msg->flashMessage('message error', "Height is required"));	
			return false;
		} else if($this -> validate -> HasLetters($this -> height)) {
			$this -> json -> outputJqueryJSONObject('boxHeightNumbers', $this -> msg->flashMessage('message error', "Height: Only Numbers"));	
			return false;
		} else if($this -> validate -> emptyInput($this -> itemsHold)) {
			$this -> json -> outputJqueryJSONObject('boxItemsHoldEmpty', $this -> msg->flashMessage('message error', "Items Hold is required"));	
			return false;
		} else if($this -> validate -> HasLetters($this -> itemsHold)) {
			$this -> json -> outputJqueryJSONObject('boxItemsHoldNumbers', $this -> msg->flashMessage('message error', "Items Hold: Only Numbers"));	
			return false;
		} else if($this -> validate -> emptyInput($this -> weight)) {
			$this -> json -> outputJqueryJSONObject('boxWeightEmpty', $this -> msg->flashMessage('message error', "Weight is required"));	
			return false;
		} else if($this -> validate -> HasLetters($this -> weight)) {
			$this -> json -> outputJqueryJSONObject('boxWeightNumbers', $this -> msg->flashMessage('message error', "Weight: Only Numbers"));	
			return false;
		} else if($this -> GirthLength() > $packageConstrants) {
			$this -> json -> outputJqueryJSONObject('MaxPackageConstrants', $this -> msg->flashMessage('message error', "Package exceeds the maximum size total constraints<br /> of 165 inches (length + girth, where girth is 2 x width plus 2 x height)"));	
			return false;
		}
		return true;
	}
	
	private function GirthLength() {
		$packageValue = ($this -> width * 2) + ($this -> height * 2) + $this -> length;
		return $packageValue;
	}
	
	public function Save() {
		try {
			if($this -> boxID) {
				$where = array('id' => $this -> boxID); 
						
				$postData = array('length' => $this -> length,
								  'width' => $this -> width,
								  'height' => $this -> height,
								  'items' => $this -> itemsHold,
								  'weight' => $this -> weight);		
						
				$this -> db -> update("ShippingBoxes", $postData, $where);	
			
				
				$this -> json -> multipleJSONOjbects(array("success" => $this -> msg -> flashMessage('message success', "Shipping Box Updated"),
														   "newLenth"	=> $this -> length, 
														   "newWidth" => $this -> width,
														   "newHeight" => $this -> height,
														   "newItems" => $this -> itemsHold,
														   "newWeight" => $this -> weight,
														   "newPackageGirthValue" => $this -> GirthLength()));
				
				
			} else {
				$this->db->insert('ShippingBoxes', array('length' => $this -> length,
														 'width' => $this -> width,
												         'height' => $this -> height,
												         'items' => $this -> itemsHold,
												         'weight' => $this -> weight));	
														 
														 
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Shipping Box Entered"));														 
				$this -> json -> outputJqueryJSONObject('newBox', PATH . 'cms/settings/shipping?newBox=true');										 
			}
			
							  
		} catch (Exception $e) {
    		$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
    	
    		$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e-> getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			
			$this -> redirect -> redirectPage('cms/settings/shipping'); 
		}
		
	}

	public function generateCartBoxes() {
		
	}
	
	public function TotalShippingBoxes() {
        $ret = array();
		$results = $this -> db -> select('SELECT * FROM ShippingBoxes');
        foreach($results as $box) {
            $b = new ShipmentBoxes();
            $b->fill($box);
            array_push($ret, $b);
        }
        return $ret;
	}
	
	
	public function delete() {
		try {
			$sth = $this -> db -> prepare("DELETE FROM ShippingBoxes WHERE id = :id");
			$sth -> execute(array(':id' => $this -> boxID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Shipping Box Removed"));	
			
			$this -> redirect -> redirectPage('cms/settings/shipping?newBox=true');
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error',  SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
				
			
		}
	}
	

	

	



}