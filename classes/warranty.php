<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */



 
 
 

class Warranty extends BaseObject {
	
	public $_warrantyID;
	public $photos = array();
	public $description;
	public $firstName;
	public $lastName;
	public $email;
	public $phoneNumber;
	public $address;
	public $city;
	public $state;
	public $zip;
	public $submittedDate;
	public $submittedTime;
	public $warrantyStatus;
	public $declineNotes;
	public $newPhotoNames = array();
	public $securityToken;
	public $securityTokenCheck;
	public $orderNumber;
	public $verificationID;
	
	//this might change, but with settings being converted into the singleton pattern we wont need these
	public $companyName;
	public $companyAddress;
	public $companyCity;
	public $companyState;
	public $companyZip;
	
	public $nadaBottleEmail;

	
	public function validate() {
		
		$orderIDCheck = $this->db->prepare('SELECT orderID FROM orders WHERE orderID = ?');
        $orderIDCheck->execute(array($this -> orderNumber));
		
		
		$validOrderNumber = $this->db->prepare('SELECT  orderID FROM orders WHERE orderID = :orderID AND VerificationWarrantyID = :VerificationWarrantyID');
        $validOrderNumber->execute(array(':orderID' => $this -> orderNumber, ':VerificationWarrantyID' => str_replace(' ', '', $this -> verificationID)));
		
		//count($validOrderNumber -> fetchAll())
		
		$photoArrayCheck = array_filter($this -> photos);
		
		
		if($this -> validate -> emptyInput($this -> description)) {
			$this -> json -> outputJqueryJSONObject('warrantyDescription', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Warranty description")));
			return false;
		} else if($this -> validate -> emptyInput($this -> orderNumber)) {
			$this -> json -> outputJqueryJSONObject('orderNumber', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Order Number")));
			return false;
		} else if($this -> validate -> emptyInput($this -> verificationID)) {
			$this -> json -> outputJqueryJSONObject('orderNumberVerification', $this -> msg->flashMessage('message error', "Order # Verification ID is required (Check your receipt to find it)"));
			return false;
		} else if(!count($orderIDCheck -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('orderNumberNotExist', $this -> msg->flashMessage('message error', "That order number is not in our records"));
			return false;
		} else if(!count($validOrderNumber -> fetchAll())) {
			$this -> json -> outputJqueryJSONObject('verificationNumberNotMatching', $this -> msg->flashMessage('message error', "The Order # Verification ID you typed in does not match with your order number you entered."));
			return false;
		} else if(empty($photoArrayCheck)) {
			$this -> json -> outputJqueryJSONObject('emptyPhotos', $this -> msg->flashMessage('message error', "There must be one photo uploaded"));	
			return false;
		} else if($this -> validatePhotos()) {
			$this -> json -> outputJqueryJSONObject('photoUploadError', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK) . ": photo Name - " . $photoArrayCheck[0]));			
			//$this -> json -> outputJqueryJSONObject('photoUploadError', $this -> msg->flashMessage('message error', $photoArrayCheck[0]));
			//$this -> json -> outputJqueryJSONObject('photoUploadError', $this -> msg->flashMessage('message error', print_r($photoArrayCheck)));	
			//$this -> json -> outputJqueryJSONObject('photoUploadError', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK) . ": photo Name - " . var_dump($photoArrayCheck)));
			return false;
		} else if($this -> validate -> extendsMaximumFileSize($photoArrayCheck)) {
			$this -> json -> outputJqueryJSONObject('emptyPhotos', $this -> msg->flashMessage('message error', "There must be one photo uploaded"));	
			return false;
		} else if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('firstName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("First Name")));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('lastName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Last Name")));
			return false;
		} else if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('Email', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Email")));
			return false;
		} else if($this -> validate -> correctEmailFormat($this -> email)) {
			$this -> json -> outputJqueryJSONObject('correctEmailFormat', $this -> msg->flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
			return false;
		} else if($this -> validate -> emptyInput($this -> phoneNumber)) {
			$this -> json -> outputJqueryJSONObject('emptyPhone', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Phone Number")));
			return false;
		} else if($this -> validate -> emptyInput($this -> address)) {
			$this -> json -> outputJqueryJSONObject('emptyAddress', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Address")));
			return false;
		} else if($this -> validate -> emptyInput($this -> city)) {
			$this -> json -> outputJqueryJSONObject('emptyCity', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("City")));
			return false;
		} else if($this -> validate -> emptyInput($this -> state)) {
			$this -> json -> outputJqueryJSONObject('emptyState', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("State")));
			return false;
		} else if($this -> validate -> emptyInput($this -> zip)) {
			$this -> json -> outputJqueryJSONObject('emptyZip', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Zip")));
			return false;
		}
		return true;
	}
	
	private function validatePhotos() {
		foreach ($this -> photos as $key => $photo) {
			if(!empty($photo)) {
				return	$_FILES["warrantyPhoto"]["error"][$key] !== UPLOAD_ERR_OK;
			}
		}
	}
	
	public function save() {
		try {
		
			$securityToken = Hash::generateSecurityToken();
			$newWarranty = $this->db->insert('warranties', array('orderID' => $this -> orderNumber,
																 'firstName' => $this->firstName,
												                 'lastName' => $this->lastName,
												                 'description' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this->description)),
												                 'email' => $this->email,
												                 'phone' => preg_replace('/[^0-9]/','',$this->phoneNumber),
												                 'address' => $this->address,
												                 'city' => $this->city,
												                 'state' => $this->state,
												                 'zip' => $this->zip,
																 'date' => date("Y-m-d", $this -> currentTime -> coloradoTime()),
																 'time' => date('h:i:s a', $this -> currentTime -> coloradoTime()),
																 'securityToken' => $securityToken
																 
																 
			));
			
			
				Folder::CreateWarrantyFolder($newWarranty);
			
				foreach ($this -> photos as $key => $photo) {
					if(!empty($photo)) {
						$photoRename = Image::renameImage($photo);
						
						$this->db->insert('warrantyPhotos', array('warrantyID' => $newWarranty,
																  'warrantyPhoto' => $photoRename
						));
						Image::copyImage($_FILES["warrantyPhoto"]["tmp_name"][$key], "view/cms/warranties/images/" . $newWarranty . "/" . $photoRename, 90);
						//copy($_FILES["warrantyPhoto"]["tmp_name"][$key], "view/cms/warranties/images/" . $newWarranty . "/" . $photoRename);
					}
				}
			
	
			
			
			
			
			//$this -> redirect -> redirectPage('support/warrantySent');
			
			$warranty = array();
			$warranty['warranty-id'] = $newWarranty;
			$warranty['todays-date'] = date("M. j Y", $this -> currentTime -> coloradoTime());
			$warranty['warrantee-name'] = $this->firstName . ' ' . $this->lastName;
			$warranty['email'] = $this -> email;
			$warranty['nada-bottome-email'] = $this -> nadaBottleEmail;
			$warranty['warrantyPrintLink'] = PATH . 'support/yourWarranty?token=' . $securityToken . '&NBWID=' . $newWarranty;
			
			
			Email::warrantyEmail($warranty, 0);
			//email to self
			Email::warrantyEmail($warranty, 0, true);
			
			$this -> json -> outputJqueryJSONObject('redirect', PATH . 'support/warrantySent');
			
		} catch (Exception $e) {
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', true);
		}
	}
	
	public static function WithID($warrantyID) {
		$instance = new self();
        $instance -> _warrantyID = $warrantyID;
        $instance -> loadWarranty();
        return $instance;
	}
	
	protected function loadWarranty() {
        $sth = $this -> db -> prepare('SELECT * FROM warranties WHERE warrantyID = :warrantyID');
        $sth->execute(array(':warrantyID' => $this -> _warrantyID));			

		$record = $sth -> fetch();	
		$this->fill($record);
    }

    protected function fill(array $row){
    	$this -> _warrantyID = $row['warrantyID'];
    	$this -> firstName = $row['firstName'];
		$this -> lastName = $row['lastName'];
		$this -> email = $row['email'];
		$this -> description = $row['description'];
		$this -> phoneNumber = $row['phone'];
		$this -> address = $row['address'];
		$this -> city = $row['city'];
		$this -> state = $row['state'];
		$this -> zip = $row['zip'];
		$this -> submittedDate = $row['date'];
		$this -> submittedTime = $row['time'];
		$this -> warrantyStatus = $row['isApproved'];
		$this -> declineNotes = $row['disapprovalComments'];
		$this -> securityTokenCheck = $row['securityToken'];
		$this -> orderNumber = $row['orderID'];
    }
	
	
	public function getFullName() {
		return $this -> firstName . " " . $this -> lastName;
	}
	
	public function delete() {
		try {
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Warranty Removed"));
					
			$sth = $this -> db -> prepare("UPDATE warranties SET isActive = 0 WHERE warrantyID = :id");
			$sth -> execute(array(':id' => $this -> _warrantyID));
			
			//we will wait and see if they want to view all warranties or not
			//unlink("view/images/warranty/" . $this -> photo);
			
			$this -> redirect -> redirectPage('cms/warranties');
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/warranties');
		}
	}
	
	public function approveWarranty() {
		try {
			$postData = array('isApproved' => 1);
			
			$this->db->update('warranties', $postData, array('warrantyID' => $this-> _warrantyID));
			$this -> msg -> set($this -> msg -> flashMessage('message success', "Warranty #: ". $this-> _warrantyID . " Approved"));
			
			
			
			$warranty = array();
			$warranty['warranty-id'] = $this-> _warrantyID;
			$warranty['todays-date'] = date("M. j Y", $this -> currentTime -> coloradoTime());
			$warranty['warrantee-name'] = $this->firstName . ' ' . $this->lastName;
			$warranty['company-name'] = $this -> companyName;
			$warranty['company-address'] = $this -> companyAddress;
			$warranty['company-city'] = $this -> companyCity;
			$warranty['company-state'] = $this -> companyState;
			$warranty['company-zip'] = $this -> companyZip;
			$warranty['email'] = $this -> email;
			
			Email::warrantyEmail($warranty, 1);
			$this -> redirect -> redirectPage('cms/warrantySingle/'. $this-> _warrantyID);
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/warrantySingle/'. $this-> _warrantyID);
		}
		
	}
	
	public function declineWarranty() {
		try {
			$postData = array('isApproved' => 2, 'disapprovalComments' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> declineNotes)));
			
			$this->db->update('warranties', $postData, array('warrantyID' => $this-> _warrantyID));
			$this -> msg -> set($this -> msg -> flashMessage('message error', "Warranty #: ". $this-> _warrantyID . " Declined"));
			
			$warranty = array();
			$warranty['warranty-id'] = $this-> _warrantyID;
			$warranty['todays-date'] = date("M. j Y", $this -> currentTime -> coloradoTime());
			$warranty['warrantee-name'] = $this->firstName . ' ' . $this->lastName;
			$warranty['declined-notes'] = str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> declineNotes));
			$warranty['email'] = $this -> email;
			
			Email::warrantyEmail($warranty, 2);
			
			$this->json->outputJqueryJSONObject("redirect", PATH . 'cms/warrantySingle/'. $this-> _warrantyID);
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this->json->outputJqueryJSONObject("redirect", PATH . 'cms/warrantySingle/'. $this-> _warrantyID);
		}

	}
	
	public function resetWarrantyStatus() {
		try {
			$postData = array('isApproved' => 0);
			
			$this->db->update('warranties', $postData, array('warrantyID' => $this-> _warrantyID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Warranty #: ". $this-> _warrantyID . " Reset Complete"));

			
			$warranty = array();
			$warranty['warranty-id'] = $this-> _warrantyID;
			$warranty['todays-date'] = date("M. j Y", $this -> currentTime -> coloradoTime());
			$warranty['warrantee-name'] = $this->firstName . ' ' . $this->lastName;
			$warranty['email'] = $this -> email;
			
			Email::warrantyEmail($warranty, 3);
			$this -> redirect -> redirectPage('cms/warrantySingle/'. $this-> _warrantyID);

		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/warrantySingle/'. $this-> _warrantyID);
		}
		
		
	}

	public function validateDeclineNotes() {
		if($this -> validate -> emptyInput($this -> declineNotes)) {
			$this->json->outputJqueryJSONObject("emptyDeclineNotes", $this->msg->flashMessage('message error', $this->msg->isRequired("Decline Notes")));
			return false;
		} 
		return true;
	}
	
	

}