<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Team extends BaseObject {
	
	public $teamID;
	public $photo;
	public $newPhoto;
	public $firstName;
	public $lastName;
	public $description;
	public $jobTitle;
	public $teamLink;
	
	public static function WithID($teamID) {
		$instance = new self();
        $instance-> teamID = $teamID;
        $instance->loadById();
        return $instance;
	}
	
	protected function loadById() {
        $sth = $this -> db -> prepare('SELECT * FROM TeamMembers WHERE id = :id');
        $sth->execute(array(':id' => $this -> teamID));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){
		$this -> firstName = $row['firstName'];
		$this -> lastName = $row['lastName'];
		$this -> description = $row['description'];
		$this -> photo = $row['photo'];
		$this -> teamLink = $row['teamLink'];
		$this -> jobTitle = $row['jobTitle'];
    }
	
	public function FullName() {
		return $this -> firstName . " " . $this -> lastName;
	}
	public function validate() {
		
		if($this -> validate -> emptyInput($this -> photo)) {
			$this -> json -> outputJqueryJSONObject('emptyPhoto', $this -> msg->flashMessage('message error', "Photo is required"));	
			return false;
		} else if($_FILES['profilePic']['error'] !== UPLOAD_ERR_OK) {
			$this -> json -> outputJqueryJSONObject('extendsFileSize', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
			return false;
		} else if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('emptyFirstName', $this -> msg->flashMessage('message error', "First Name is required"));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('emptyLastName', $this -> msg->flashMessage('message error', "Last Name is required"));
			return false;
		} else if($this -> validate -> emptyInput($this -> jobTitle)) {
			$this -> json -> outputJqueryJSONObject('emptyJobTitle', $this -> msg->flashMessage('message error', "Job Title is required"));
			return false;
		} else if($this -> validate -> emptyInput($this -> description)) {
			$this -> json -> outputJqueryJSONObject('emptyDescription', $this -> msg->flashMessage('message error', "Description is required"));
			return false;
		}
		
		return true;
	}
	
	public function save() {
		try {
			
			if($this -> teamID) {
				
				if(!empty($_FILES['profilePic']['name'])) {
					unlink("view/cms/users/team-images/" . $this -> photo);	

					$newPhoto = Image::renameImage($this -> newPhoto);
										
					Image::copyImage($_FILES["profilePic"]["tmp_name"], "view/cms/users/team-images/" . $newPhoto, 80);
					
					
					$postData = array("firstName" => trim($this -> firstName),
									  "lastName" => trim($this -> lastName),
									  "description" => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this->description)),
									  "jobTitle" => $this -> jobTitle,
									  "photo" => $newPhoto, 
									  "teamLink" =>$this->teamLink);			
				}
				else {
					$postData = array("firstName" => trim($this -> firstName),
									  "lastName" => trim($this -> lastName),
									  "description" => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this->description)),
									  "jobTitle" => $this -> jobTitle,
									  "teamLink" =>$this->teamLink);
				}
				
				
								  
				$where = array('id' => $this -> teamID); 
						
				$this -> db -> update("TeamMembers", $postData, $where);	
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Team Member Saved"));
				$this -> json -> outputJqueryJSONObject('savedEdits', PATH . 'cms/teamMember/' . $this -> teamID);
				
			} else {
				
				$teamMemberPhoto = Image::renameImage($this -> photo);
						
				Image::copyImage($_FILES["profilePic"]["tmp_name"], "view/cms/users/team-images/" . $teamMemberPhoto, 80);
				
				//copy($_FILES["profilePic"]["tmp_name"], "view/cms/users/team-images/" . $teamMemberPhoto);
						
				//Image::compress("view/cms/users/team-images/" . $teamMemberPhoto, "view/cms/users/team-images/" . $teamMemberPhoto, 80);
						
				$newTeamMember = $this->db->insert('TeamMembers', array('firstName' => trim($this -> firstName),
																		'lastName' => trim($this -> lastName),
				  					                   'description' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this->description)),
													   'photo' => $teamMemberPhoto,
													   "jobTitle" => $this -> jobTitle,
													   'teamLink' => $this->teamLink
				));
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Team Member Added"));
				$this -> json -> outputJqueryJSONObject('redirect', PATH . 'cms/teamMember/' . $newTeamMember);
				
			}
			
			
			
			
		} catch (Exception $e) {
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));	
			
			
			
		}
	}

	public function delete() {
		try {
			
			unlink("view/cms/users/team-images/" . $this -> photo);				
			
			$sth = $this -> db -> prepare("DELETE FROM TeamMembers WHERE id = :id");
			$sth -> execute(array(':id' => $this -> teamID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Team Member Deleted"));	
			
			$this -> redirect -> redirectPage('cms/team');
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error',  SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
				
			
		}
	}
	
	public function reorderCategories() {
		try {
		
			$count = 1;
			foreach ($this -> teamID as $value) {
				$sth = $this -> db -> prepare('UPDATE TeamMembers SET position = ' . $count . ' WHERE id = :id');
				$sth -> execute(array(':id' => $value));
				$count++;
			}
				
			$this->json->outputJqueryJSONObject("teamListUpdated", $this -> msg -> flashMessage('message success', 'Team List Updated'));
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this->json->outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	


}