<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Contact extends BaseObject {
	
	public $_contactID;
	public $firstName;
	public $lastName;
	public $email;
	public $comments;
	public $contactDate;
	public $contactTime;
	public $submittedDate;
	
	public $nadaBottleContactEmail;
	
	
	public static function WithID($contactID) {
		$instance = new self();
        $instance -> _contactID = $contactID;
        $instance -> loadByProductID();
        return $instance;
	}
	
	protected function loadByProductID() {
        $sth = $this -> db -> prepare('SELECT * FROM ContactInquiries WHERE contactID = :contactID');
        $sth->execute(array(':contactID' => $this -> _contactID));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row){    	
		$this -> firstName = $row['FirstName'];
		$this -> lastName = $row['LastName'];
		$this -> email = $row['Email'];
		$this -> comments = $row['Comments'];
		$this -> contactDate = $row['date'];
		$this -> contactTime = $row['time'];
    }
	
	public function fullName() {
		return $this -> firstName . ' ' . $this -> lastName;
	}
	
	public function delete() {
		try {
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Contact Inquiry Removed"));
				
			$sth = $this -> db -> prepare("UPDATE ContactInquiries SET isActive = 0 WHERE contactID = :id");
			$sth -> execute(array(':id' => $this -> _contactID));
		
			$this -> redirect -> redirectPage('cms/contact');	
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/contact');
		}
		
	}
	
	public function validate() {
		if($this -> validate -> emptyInput($this -> firstName)) {
			$this -> json -> outputJqueryJSONObject('firstName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("First Name")));
			return false;
		} else if($this -> validate -> emptyInput($this -> lastName)) {
			$this -> json -> outputJqueryJSONObject('lastName', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Last Name")));
			return false;
		} else if($this -> validate -> emptyInput($this -> email)) {
			$this -> json -> outputJqueryJSONObject('emptyEmail', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Email")));
			return false;
		} else if ($this -> validate -> correctEmailFormat($this -> email)){
			$this -> json -> outputJqueryJSONObject('verifyEmail', $this -> msg -> flashMessage('message error', $this -> msg -> emailFormatRequiredMessage()));
			return false;
		} else if ($this -> validate -> emptyInput($this -> comments)){
			$this -> json -> outputJqueryJSONObject('emptyComments', $this -> msg -> flashMessage('message error', "Please fill out your concerns"));
			return false;		
		}
		return true; 
	}
	
	public function save() {
		
		try {
			$date = date("Y-m-d", $this -> currentTime -> coloradoTime());
			$time = date('h:i:s a', $this -> currentTime -> coloradoTime());
			
			
			
			$this -> db -> insert("ContactInquiries", array('FirstName' => $this -> firstName, 
							       						'LastName' => $this -> lastName, 
							       						'Email' => $this -> email, 
							       						'Comments' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> comments)),
							       						'date' => $date,
							       						'time' => $time));
														
														
			$this -> json -> outputJqueryJSONObject('complete', $this -> msg -> flashMessage('message success', "Thank you for filling out this form, someone will be in contact with you shortly"));
			Email::Contact($this -> nadaBottleContactEmail, $this -> fullName(), $this -> email, str_replace("\n", '<br />',$this -> comments));
			
			$time = date("M. j Y / g:i a", $this -> currentTime -> coloradoTime()); 
			$ip = getenv('REMOTE_ADDR');
			$userAgent = getenv('HTTP_USER_AGENT');
			
			
			$contactInquiryLogUserAgent = 
				   "CONTACT INQUIRY" .
				   "\nIP: " . $ip . 
				   "\nTIME: " . $time . 				   
				   "\nUSER AGENT: " . $userAgent . "\n\n";
			
			Log::UserAgent($contactInquiryLogUserAgent);			
		} catch (Exception $e) {
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			
		}
				

	}
	

}