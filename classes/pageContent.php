<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Page_Content extends BaseObject {
	
	
	public $pageID;
	public $contentHeader;
	public $content;
	public $isWarrantyForm;	
	public $warrantyFormDescription;
	public $warrantyQuestion1;	
	public $warrantyQuestion2;	
	public $warrantyQuestion3;	
	
	public static function WithID($pageID) {
		$instance = new self();
        $instance -> pageID = $pageID;
        $instance -> loadByID();
        return $instance;
	}
	
	protected function loadByID() {
        $sth = $this -> db -> prepare('SELECT * FROM PageContent WHERE pageID = :pageID');
        $sth->execute(array(':pageID' => $this -> pageID));
        $record = $sth -> fetch();
        $this->fill($record);
    }

    protected function fill(array $row) {
    	$this -> contentHeader = $row['title'];
		$this -> content = $row['content'];
		//if($this -> isWarrantyForm == true) {
			$this -> warrantyFormDescription = $row['warrantyFormDescription'];
			$this -> warrantyQuestion1 = $row['warrantyFormQuestion1'];	
			$this -> warrantyQuestion2 = $row['warrantyFormQuestion2'];	
		//}
    }
	
	public function validate() {
	
	}
	
	public function save($page) {
		try {
			if($this -> isWarrantyForm == true) {
				$postData = array('title' => $this -> contentHeader,
						  'content' => $this -> content,
						  'warrantyFormDescription' => str_replace("\n", '<br />', $this -> warrantyFormDescription),
						  'warrantyFormQuestion1' => str_replace("\n", '<br />', $this -> warrantyQuestion1),
						  'warrantyFormQuestion2' => str_replace("\n", '<br />', $this -> warrantyQuestion2));						  
			} else {
				$postData = array('title' => $this -> contentHeader,
								  'content' => $this -> content);	
			}
				
			
			
		
			$where = array('pageID' => $this -> pageID);
								  
			$this -> db -> update("PageContent", $postData, $where);
				
			$this -> msg -> set($this -> msg -> flashMessage('message generic', $page. " Page Updated"));
			
			$this -> redirect -> redirectPage("cms/". $page);
				
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this -> redirect -> redirectPage('cms/' . $page);
		}
		
	}
	
	
	
	
	

}