<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class ThankYou extends BaseObject {
	
	//private $_supporterID;
	public $supporterID;	

	public static function WithID($supporterID) {
		$instance = new self();
        $instance -> supporterID = $supporterID;
        $instance -> loadWarranty();
        return $instance;
	}
	
	protected function loadWarranty() {
        $sth = $this -> db -> prepare('SELECT * FROM ThankYouList WHERE supporterID = :supporterID');
        $sth->execute(array(':supporterID' => $this -> supporterID));			

		$record = $sth -> fetch();	
		$this->fill($record);
    }

    protected function fill(array $row){
    	$this -> supporterID = $row['supporterID'];
    }
	
	public function delete() {
		try {
			$sth = $this -> db -> prepare("DELETE FROM ThankYouList WHERE supporterID = :supporterID");
			$sth -> execute(array(':supporterID' => $this -> supporterID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Supporter removed"));	
			
			$this -> redirect -> redirectPage('cms/thankYouList?page=1');
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error',  SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
				
			
		}
	}

	


}