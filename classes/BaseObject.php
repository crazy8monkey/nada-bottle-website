<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:58 PM
 */

class BaseObject {
    function __construct()
    {
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
        $this -> redirect = new Redirect();
		$this -> currentTime = new Time();
		$this -> security = new Security();
		
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }

    public function __sleep()
    {
        return array();
    }

    public function __wakeup()
    {
        $this -> msg = new Message();
        $this -> json = new JSONparse();
        $this -> validate = new Validation();
        $this -> redirect = new Redirect();
		$this -> currentTime = new Time();
        $this->db = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PASS);
    }
	
	public function codeToMessage($code) {
		try {
	        switch ($code) { 
	            case UPLOAD_ERR_INI_SIZE: 
	                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini (" . ini_get('upload_max_filesize') . ")"; 
	                break; 
	            case UPLOAD_ERR_FORM_SIZE: 
	                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
	                break; 
	            case UPLOAD_ERR_PARTIAL: 
	                $message = "The uploaded file was only partially uploaded"; 
	                break; 
	            case UPLOAD_ERR_NO_FILE: 
	                $message = "No file was uploaded"; 
	                break; 
	            case UPLOAD_ERR_NO_TMP_DIR: 
	                $message = "Missing a temporary folder"; 
	                break; 
	            case UPLOAD_ERR_CANT_WRITE: 
	                $message = "Failed to write file to disk"; 
	                break; 
	            case UPLOAD_ERR_EXTENSION: 
	                $message = "File upload stopped by extension"; 
	                break; 
	            default: 
	                $message = "Unknown upload error, please try a different photo"; 
	                break; 
	        } 
	        return $message; 
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = "FILE UPLOAD ERROR" . $e->getMessage();
			$TrackError -> type = "File Upload error: ";
			$TrackError -> SendMessage();
			
		}
    }

}