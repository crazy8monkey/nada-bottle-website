<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Category extends BaseObject {

	public $CategoryID;
	public $CategoryName;
	public $isCustomizable;
	public $NewCategoryName;
	private $ProductImageImages = array();
	public $categoryImage;
	public $newCategoryImage;
	
	private $ProductID = array();
	private $AllColors = array();
	
    public function __sleep() {
        parent::__sleep();
    }

    public function __wakeup() {
        parent::__wakeup();
    }


    public function __construct() {
        parent::__construct();
    }

    public static function categoryID($categoryID) {
        $instance = new self();
        $instance -> CategoryID = $categoryID;
        $instance -> loadByCategoryID();
		$instance -> loadProductIDByColor();
        return $instance;
    }
	
	public static function categoryName($categoryName) {
	 	$instance = new self();
        $instance -> CategoryName = $categoryName;
		$instance -> loadByCategoryID(true);
		return $instance;
	}

	private function loadByCategoryID($category = false) {
		if($category == true) {
			$sth = $this -> db -> prepare('SELECT * FROM productCategories WHERE ProductCategory = :ProductCategory');
     		$sth->execute(array(':ProductCategory' => $this -> CategoryName));
		} else {
			$sth = $this -> db -> prepare('SELECT * FROM productCategories WHERE categoryID = :categoryID');
        	$sth->execute(array(':categoryID' => $this -> CategoryID));	
		}
		
		
        $record = $sth -> fetch();
        $this->fill($record);
	}
	
	protected function fill(array $row){
        $this-> CategoryName = $row['ProductCategory'];
		$this -> isCustomizable = $row['isCustomizable'];
		$this -> categoryImage = $row['CategoryImage'];
    }
	
	private function loadProductIDByColor() {
		$sth = $this -> db -> prepare('SELECT * FROM Products WHERE product_category = :product_category');
        $sth->execute(array(':product_category' => $this-> CategoryName));
        $this -> ProductImageImages = $sth -> fetchAll();
	}
	
	public function delete() {
		$this -> deleteProductsByCategory();
		$this -> deleteCategory();
		
		
		$this -> msg -> set($this -> msg -> flashMessage('message generic', "Category Deleted"));
		$this -> redirect -> redirectPage('cms/categories');
	}

	private function deleteProductsByCategory() {		
		foreach($this -> ProductImageImages as $product) {
        	$product = Product::withID($product['product_id']);
            $product->delete();
		}
	}

	private function deleteCategory() {		
		$sth = $this -> db -> prepare("UPDATE productCategories SET IsActive=0 WHERE categoryID = :id");
		$sth -> execute(array('id' => $this -> CategoryID));
	}

	public function validate() {
		if(isset($this -> CategoryID)) {
			if(!empty($this -> newCategoryImage)) {
				if($_FILES['categoryImage']['error'] !== UPLOAD_ERR_OK) {
					$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
					return false;
				} 				
			} else {
				if($this -> validate -> emptyInput($this -> NewCategoryName)) {
					$this->json->outputJqueryJSONObject("categoryNameError", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Category Name")));
					return false;
				}	
			}
		} else {
			if(!empty($this -> categoryImage)) {
				if($_FILES['categoryImage']['error'] !== UPLOAD_ERR_OK) {
					$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
					return false;
				} 				
			} else {
				if($this -> validate -> emptyInput($this -> CategoryName)) {
					$this->json->outputJqueryJSONObject("categoryNameError", $this -> msg -> flashMessage('message error', $this -> msg -> isRequired("Category Name")));
					return false;
				} 					
			}
		}
		return true;		
	}

	public function save() {
		try {
			if(isset($this -> CategoryID)) {
				if(!empty($this -> newCategoryImage)) {
					ini_set('memory_limit', '-1');
					
					if(!empty($this -> categoryImage)) {
						if(file_exists("view/purchase/category-images/" . $this -> categoryImage)) {
							unlink("view/purchase/category-images/" . $this -> categoryImage);
						}	
					}
					$newPhotoRename = Image::renameImage($this -> newCategoryImage);	
					
					$postData = array("ProductCategory" => str_replace(" ", "_", $this -> NewCategoryName),
								  "CategoryImage" => $newPhotoRename,
								  "isCustomizable" => $this  -> isCustomizable);
								  
							  
					
					Image::copyImage($_FILES["categoryImage"]["tmp_name"], "view/purchase/category-images/" . $newPhotoRename, 90);
					
				
				
				} else {
					$postData = array("ProductCategory" => str_replace(" ", "_", $this -> NewCategoryName),
								  	  "isCustomizable" => $this  -> isCustomizable);
				}
				
				//update category product names
				$changeCategory = $this -> db -> prepare("UPDATE Products SET product_category = '" . str_replace(" ", "_",$this -> NewCategoryName) ."' 
														  WHERE product_category = '" . $this ->  CategoryName . "'");
				$changeCategory -> execute();
					
				
								  
				$where = array('categoryID' => $this -> CategoryID); 
						
				$this -> db -> update("productCategories", $postData, $where);	
				
				$this -> msg -> set($this -> msg->flashMessage('message success', "Category updated"));
				
				$this -> json -> multipleJSONOjbects(array("categoryUpdated"	=> $this -> msg->flashMessage('message success', "Category Updated"), 
														   "NewCategoryName" => PATH . "cms/category/" . $this -> CategoryID));
							
			} else {
				if(!empty($this -> categoryImage)) {
					ini_set('memory_limit', '-1');
					
					
					$newPhotoRename = Image::renameImage($this -> categoryImage);
					
					Image::copyImage($_FILES["categoryImage"]["tmp_name"], "view/purchase/category-images/" . $newPhotoRename, 90);
					
					$this -> db -> insert("productCategories", array('ProductCategory' => str_replace(" ", "_", $this -> CategoryName),
																 'CategoryImage' => $newPhotoRename,
																 'isCustomizable' => $this -> isCustomizable));
					
				} else {
					$this -> db -> insert("productCategories", array('ProductCategory' => str_replace(" ", "_", $this -> CategoryName),
																 'isCustomizable' => $this -> isCustomizable));
				}
					
				$this -> msg -> set($this -> msg->flashMessage('message success', "Category Added"));
																 
				$this->json->outputJqueryJSONObject("redirectLink", PATH .'cms/category/' . $this -> db -> lastInsertId());
			}
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_GENERAL_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			if(isset($this -> CategoryID)) {
				$this->json->outputJqueryJSONObject("redirectLink", PATH .'cms/category/' . $this -> CategoryID);	
			} else {
				
				$this->json->outputJqueryJSONObject("redirectLink", PATH .'cms/categories');
			}
			
			
		}
	}

	public function reorderCategories() {
		try {
		
			$count = 1;
			foreach ($this -> CategoryID as $value) {
				$sth = $this -> db -> prepare('UPDATE productCategories SET position = ' . $count . ' WHERE categoryID = :id');
				$sth -> execute(array(':id' => $value));
				$count++;
			}
				
			$this->json->outputJqueryJSONObject("categoryListUpdated", $this -> msg -> flashMessage('message success', 'Category Links Updated'));
			
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_UPDATE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this->json->outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	
	public function deletePhoto() {
		try {
			
			
			unlink("view/purchase/category-images/" . $this -> categoryImage);
				
			$postData = array("CategoryImage" => "");
			$where = array('categoryID' => $this -> CategoryID); 
					
			$this -> db -> update("productCategories", $postData, $where);	
			
			
			$this -> msg -> set($this -> msg->flashMessage('message generic', "Category photo removed"));
			
			$this -> redirect -> redirectPage('cms/category/' . $this -> CategoryID);
		} catch (Exception $e) {
				
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
			
			$this->json->outputJqueryJSONObject("MySqlError", $this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
		}
	}
	
	

}