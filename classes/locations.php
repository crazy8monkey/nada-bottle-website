<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 8/2/15
 * Time: 12:07 PM
 */

class Locations extends BaseObject {
	
	
	public $locationID;	
	public $longitude;
	public $latitude;
	public $name;
	public $description;
	public $locationImage;
	public $currentImage;
	public $locationVideo;
	

    public function __sleep()
    {
        parent::__sleep();
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


    public function __construct(){
        parent::__construct();
    }
	
	
	
	public static function WithID($locationID) {
		$instance = new self();
        $instance -> locationID = $locationID;
        $instance -> loadByLocationID();
        return $instance;
	}
	
	public static function WithCoordinates($long, $lat) {
		$instance = new self();
		$instance -> longitude = $long;
		$instance -> latitude = $lat;
        $instance -> loadLocationByCoordinates();
		return $instance;
		//$_GET['Longitude'], $_GET['Latitude']
	}
	
	protected function loadByLocationID() {
        $sth = $this -> db -> prepare('SELECT * FROM locateLife WHERE id = :id');
        $sth->execute(array(':id' => $this -> locationID));
        $record = $sth -> fetch();
        $this->fill($record);
    }
	
	protected function loadLocationByCoordinates() {
		$sth = $this -> db -> prepare('SELECT * FROM locateLife WHERE longitude = :longitude AND latitude = :latitude');
        $sth->execute(array(':longitude' => $this -> longitude, ':latitude' => $this -> latitude));
        $record = $sth -> fetch();
        $this->fill($record);
	}

	

    protected function fill(array $row) {
    	$this -> locationID = $row['id'];
		$this -> longitude = $row['longitude'];
		$this -> latitude = $row['latitude'];
		$this -> name = $row['name'];
		$this -> locationImage = $row['image'];
		$this -> currentImage = $row['image'];
		$this -> locationVideo = $row['video'];
		$this -> description = $row['description'];
    }
	
	private function redirectSave() {
		$redirectString = "";
		if(isset($this -> locationID)) {
			$redirectString = "cms/location/" . $this -> locationID;
		} else {
			$redirectString = "cms/addNewLocation";
		}
		return $redirectString;
	}
	
	public function validate() {
		if(isset($this -> locationID)) {
			if(!empty($this -> locationImage)) {
				if($_FILES['locationImage']['error'] !== UPLOAD_ERR_OK) {
					$this -> json -> outputJqueryJSONObject('uploadImageProblem', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
					return false;
				} 				
			} else if($this -> validate -> emptyInput($this -> longitude)) {
	            $this->json->outputJqueryJSONObject("emptyLongitude", $this->msg->flashMessage('message error', $this->msg->isRequired("Longitude")));
	            return false;
	        } else if($this -> validate -> HasLetters($this -> longitude)) {
	            $this->json->outputJqueryJSONObject("longitudeOnlyNumbers", $this->msg->flashMessage('message error', "Longitude: Only Numbers are allowed"));
	            return false;
	        } else if($this -> validate -> emptyInput($this -> latitude)) {
	        	$this->json->outputJqueryJSONObject("emptyLatitude", $this->msg->flashMessage('message error', $this->msg->isRequired("Latitude")));
	            return false;
	        } else if($this -> validate -> HasLetters($this -> latitude)) {
	        	$this->json->outputJqueryJSONObject("latitudeOnlyNumbers", $this->msg->flashMessage('message error', "Latitude: Only Numbers are allowed"));
	            return false;
	        } else if($this -> validate -> emptyInput($this -> name)) {
	        	$this->json->outputJqueryJSONObject("emptyName", $this->msg->flashMessage('message error', $this->msg->isRequired("Location Name")));
	            return false;
	        } else if($this -> validate -> emptyInput($this -> locationVideo)) {
				$this -> json -> outputJqueryJSONObject('emptyVideo', $this -> msg->flashMessage('message error', $this->msg->isRequired("Video")));
				return false;
			}
		} else {
			if($this -> validate -> emptyInput($this -> longitude)) {
	            $this->json->outputJqueryJSONObject("emptyLongitude", $this->msg->flashMessage('message error', $this->msg->isRequired("Longitude")));
	            return false;
	        } else if($this -> validate -> HasLetters($this -> longitude)) {
	            $this->json->outputJqueryJSONObject("longitudeOnlyNumbers", $this->msg->flashMessage('message error', "Longitude: Only Numbers are allowed"));
	            return false;
	        } else if($this -> validate -> emptyInput($this -> latitude)) {
	        	$this->json->outputJqueryJSONObject("emptyLatitude", $this->msg->flashMessage('message error', $this->msg->isRequired("Latitude")));
	            return false;
	        } else if($this -> validate -> HasLetters($this -> latitude)) {
	        	$this->json->outputJqueryJSONObject("latitudeOnlyNumbers", $this->msg->flashMessage('message error', "Latitude: Only Numbers are allowed"));
	            return false;
	        } else if($this -> validate -> emptyInput($this -> name)) {
	        	$this->json->outputJqueryJSONObject("emptyName", $this->msg->flashMessage('message error', $this->msg->isRequired("Location Name")));
	            return false;
	        } else if($this -> validate -> emptyInput($this -> locationImage)) {
				$this -> json -> outputJqueryJSONObject('emptyLocationPhoto', $this -> msg->flashMessage('message error', "Location Photo is required"));	
				return false;
			} else if($_FILES['locationImage']['error'] !== UPLOAD_ERR_OK) {
				$this -> json -> outputJqueryJSONObject('extendsFileSize', $this -> msg->flashMessage('message error', parent::codeToMessage(UPLOAD_ERR_OK)));
				return false;
			} else if($this -> validate -> emptyInput($this -> locationVideo)) {
				$this -> json -> outputJqueryJSONObject('emptyVideo', $this -> msg->flashMessage('message error', $this->msg->isRequired("Video")));
				return false;
			}
		}
		
		
		return true;
	}
	
	public function save() {
		try {
			if(isset($this -> locationID)) {
				
				if(!empty($this -> locationImage)) {
					unlink("view/cms/locatelife/location/" . $this -> locationID . "/" . $this -> currentImage);	

					$newPhoto = Image::renameImage($this -> locationImage);
										
					Image::copyImage($_FILES["locationImage"]["tmp_name"], "view/cms/locatelife/location/" . $this -> locationID . "/" . $newPhoto, 80);
					
					
					$postData = array('longitude' => $this -> longitude,
					  				  'latitude'  => $this -> latitude, 
					             	  'name' => $this -> name,
									  'image' => $newPhoto,
									  'video' => $this -> locationVideo,
									  'description' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> description)));
					
					
				} else {
					$postData = array("longitude" =>  $this -> longitude,
							  "latitude" => $this -> latitude,
							  "name" => $this -> name,
							  'video' => $this -> locationVideo,
							  'description' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> description)));	
				}
				
				
				
							  
							  
				$where = array("id" => $this -> locationID);
			
				$this -> db -> update("locateLife", $postData, $where);
				
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Location Updated"));
				//$this -> json -> multipleJSONOjbects(array("success" => $this -> msg->flashMessage('message success', "Location Updated"), 
				//										   "newLatitude" => $this -> latitude,
				//										   "newLongitude" => $this -> longitude));
				
				//$this -> redirect -> redirectPage("cms/location/". $this -> locationID);
				$this->json->outputJqueryJSONObject("redirectLink", PATH .'cms/location/' . $this -> locationID);
			} else {
				$locationPhoto = Image::renameImage($this -> locationImage);
				
				$this -> db -> insert("locateLife", array('longitude' => $this -> longitude,
					  									  'latitude'  => $this -> latitude, 
					             						  "name" => $this -> name,
														  'image' => $locationPhoto,
														  'video' => $this -> locationVideo,
														  'description' => str_replace("\n", '<br />', $this -> validate -> SpecialCharacters($this -> description))));
				
				Folder::CreateLocateLifeFolder($this -> db -> lastInsertId());
				
				
						
				Image::copyImage($_FILES["locationImage"]["tmp_name"], "view/cms/locatelife/location/" . $this -> db -> lastInsertId() . "/". $locationPhoto, 80);
			
					
				
				
					             						  
				$this -> msg -> set($this -> msg -> flashMessage('message success', "Location Added"));
													
				$this->json->outputJqueryJSONObject("redirectLink", PATH .'cms/location/' . $this -> db -> lastInsertId());
				//$this -> redirect -> redirectPage("cms/location/". $this -> db -> lastInsertId());
			}
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error',  SYSTEM_ERROR_MESSAGE));
			
			
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
				
			$this->json->outputJqueryJSONObject("redirectLink", PATH .'cms/locatelife');
		}
		
	}
	
	public function delete() {
		try {
			$sth = $this -> db -> prepare("DELETE FROM locateLife WHERE id = :id");
			$sth -> execute(array(':id' => $this -> locationID));
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Location Deleted"));	
			
			Folder::DeleteUserDirectory("view/cms/locatelife/location/" . $this -> locationID . "/");
			
		} catch (Exception $e) {
			$this -> msg -> set($this -> msg -> flashMessage('message error',  SYSTEM_ERROR_MESSAGE));
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_DELETE_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
				
			
		}
		
	}
	
	public function validateLocation() {
		if($this -> validate -> emptyInput($this -> longitude)) {
			$this -> json -> outputJqueryJSONObject('longitutde', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Longitude")));
			return false;
		} else if($this -> validate -> emptyInput($this -> latitude)) {
			$this -> json -> outputJqueryJSONObject('latitude', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Latitude")));
			return false;
		}
		return true;
	}
	
	public function getlocation() {
		try {
			$sth = $this->db->prepare("SELECT * FROM locateLife WHERE 
								longitude = :longitude AND latitude = :latitude");
			$sth->execute(array(':longitude' 	 => $this -> longitude,
								':latitude'  => $this -> latitude,
							));
			$redirect = $sth -> fetch();
			$count =  $sth->rowCount();
			
			if ($count >0) {
				$this -> json -> outputJqueryJSONObject('redirect', PATH . 'locateLife/location?Longitude=' . $this -> longitude. '&Latitude=' . $this -> latitude);	
			} else {
				$this -> json -> outputJqueryJSONObject('noLocation', $this -> msg->flashMessage('message error', "Location does not exist"));
			}
		} catch (Exception $e) {
			$this -> json -> outputJqueryJSONObject('MySqlError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));	
			
			$TrackError = new EmailServerError();
			$TrackError -> message = MYSQL_SELECT_LABEL . $e->getMessage();
			$TrackError -> type = MYSQL_ERROR_TYPE;
			$TrackError -> SendMessage();
		}
		
			
			
			
			
	}
	
	

}