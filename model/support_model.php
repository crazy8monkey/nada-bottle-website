<?php

class Support_Model extends Model {
	
	public $companyName;
	public $companyAddress;
	public $companyCity;
	public $companyState;
	public $companyZip;
	public $nadaBottlEmail;
	
	public $nadaBottleContactEmail;

	public function __construct() {
		parent::__construct();
		$this -> companyName = $this -> settings -> getCompanyName();
		$this -> companyAddress = $this -> settings -> getCompanyAddress();
		$this -> companyCity = $this -> settings -> getCompanyCity();
		$this -> companyState = $this -> settings -> getCompanyState();
		$this -> companyZip = $this -> settings -> getCompanyZip();
		$this -> nadaBottlEmail = $this -> settings -> getWarrantyEmail();
		$this -> nadaBottleContactEmail = $this -> settings -> getContactEmail();
	}
	
	public function Settings() {
		return $this -> db -> select('SELECT * FROM settings');
	}

	public function states() {
		return array('AL'=>'Alabama',
					 'AK'=>'Alaska', 
					 'AR'=>'Arkansas',
					 'AZ'=>'Arizona',
					 'CA'=>'California',
					 'CO'=>'Colorado',
					 'CT'=>'Connecticut',
					 'DE'=>'Delaware', 
					 'DC'=>'District of Columbia',
					 'FL'=>'Florida', 
					 'GA'=>'Georgia',
					 'HI'=>'Hawaii',
					 'IA'=>'Iowa',  
					 'ID'=>'Idaho',
					 'IL'=>'Illinois',
					 'IN'=>'Indiana', 
					 'KS'=>'Kansas', 
					 'KY'=>'Kentucky',
					 'LA'=>'Louisiana',
					 'ME'=>'Maine', 
					 'MD'=>'Maryland', 
					 'MA'=>'Massachusetts', 
					 'MI'=>'Michigan', 
					 'MN'=>'Minnesota', 
					 'MO'=>'Missouri',
					 'MS'=>'Mississippi', 
					 'MT'=>'Montana',
					 'NE'=>'Nebraska', 
					 'NM'=>'New Mexico', 
					 'NV'=>'Nevada',
					 'NH'=>'New Hampshire', 
					 'NJ'=>'New Jersey', 
					 'NY'=>'New York', 
					 'NC'=>'North Carolina', 
					 'ND'=>'North Dakota',
					 'OH'=>'Ohio', 
					 'OK'=>'Oklahoma', 
					 'OR'=>'Oregon', 
					 'PA'=>'Pennsylvania',
					 'RI'=>'Rhode Island', 
					 'SC'=>'South Carolina',
					 'SD'=>'South Dakota',
					 'TX'=>'Texas', 
					 'TN'=>'Tennessee',
					 'UT'=>'Utah',  
					 'VT'=>'Vermont', 
					 'VA'=>'Virginia',
					 'WA'=>'Washington', 
					 'WV'=>'West Virginia',
					 'WI'=>'Wisconsin',
					 'WY'=>'Wyoming');
	}



}