<?php

class locateLife_Model extends Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function locateLifePermissions() {
		$sth = $this -> db -> prepare('SELECT * FROM settings WHERE settingsID = :id');
		$sth -> execute(array(':id' => 1));
		return $sth -> fetch();
	}
	
	
	public function singleLocation($longitude, $latitude) {
        $sth = $this -> db -> prepare('SELECT * FROM locateLife WHERE longitude = :longitude AND latitude = :latitude');
        $sth->execute(array(':longitude' => $longitude, ':latitude' => $latitude));
        return $sth -> fetch();
    }





}