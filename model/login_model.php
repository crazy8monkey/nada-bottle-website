<?php

class Login_Model extends Model {
	
	public function __construct() {
		parent::__construct();
	}
	public function adminLogin() {
		
		$sth = $this->db->prepare("SELECT * FROM adminUsers WHERE 
							userName = :username");
		$sth->execute(array(':username' 	 => $_POST['admin-username']));
							
		$data = $sth->fetch();	
		$count =  $sth->rowCount();
		if ($count >0)  
		{
            // User Exists
            $user = User::WithID($data['userID']);
            if($user->ValidatePasswordAttempt($_POST['admin-password'])) {

                // login
                Session::init();
                Session::set('adminLoggedIn', true);
                Session::set('user', User::WithID($data['userID']));
                $this->json->outputJqueryJSONObject('redirect', $this->redirect->JSONRedirect('cms/dashboard'));
            }else{
                // Valid Login. Incorrect Password. Handle it the same either way so they don't know what is a valid login.
                $this -> json -> outputJqueryJSONObject('loginErrorMessage', $this -> msg->flashMessage('message error', $this -> msg -> wrongCredentialMatch()));
            }
		} else {
			$this -> json -> outputJqueryJSONObject('loginErrorMessage', $this -> msg->flashMessage('message error', $this -> msg -> wrongCredentialMatch()));
		}		
	}

	public function userSingleInfo($id) {
		$sth = $this -> db -> prepare('SELECT * FROM adminUsers WHERE userID = :userID');
        $sth -> execute(array(':userID' => $id));
        return $sth->fetch();
	}

	
}