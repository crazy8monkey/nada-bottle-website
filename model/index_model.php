<?php

Class Index_Model extends Model {
	
	public function __construct() {
		parent::__construct();		
	}
	
	public function partners() {
		return $this -> db -> select('SELECT * FROM partners WHERE publishState = 1');
	}
	
	public function LatestProducts() {
		return $this -> db -> select('SELECT * FROM Products ORDER BY product_id AND publishState AND IsActive DESC LIMIT 8');
	}
}

?>