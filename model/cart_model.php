<?php

class Cart_Model extends Model {

	private $shippingRates;
	private $shippingHeight;
	private $shippingWidth;
	private $shippingLength;
	
	private $shippingRateRecord;
	private $cartItems;
	
	
	public function __construct() {
		parent::__construct();	
		if(isset($_SESSION['cart_array'])) {
			parent::getTotalItems();	
		}
		
	}	
	
	
	public function products() {
		
		$selectedProductIDS = NULL;
		foreach ($_SESSION['cart_array'] as $item) {
			$selectedProductIDS .= 	$item['product_ID'] . ',';
		}
		$selectedProductIDS = rtrim($selectedProductIDS, ',');
		$products = $this -> db -> select("SELECT * FROM Products WHERE product_id IN ($selectedProductIDS)");

        $returned = Array();
        foreach ($_SESSION['cart_array'] as $item){

            foreach ($products as $key => $value){
                if($item["product_ID"] == $value['product_id']){
                    $product = $value; // Copy the product
                    $product['quantity'] = $item['quantity'];
                    $product['color'] = Colors::productColorID($item['color']);
                    $product['index'] = $item['index'];
                    array_push($returned, $product);
                }

            }
        }
        return $returned;
	}
	
	public function colors() {
		return $this -> db -> select('SELECT * FROM ProductColors');
	}

	public function validateAddress() {
		$checkout = $_SESSION['checkout'];
		$shippingAddressValidation = $this -> shipping -> isValidAddress($checkout['address'], '', $checkout['city'], $checkout['state'], $checkout['zip']);
		
		return $shippingAddressValidation;
	}

    private function getProductTotal(){
        $cartTotal = 0;
        foreach($this->products() as $key => $value) {
            $quantity = $value['quantity'];
            setlocale(LC_MONETARY, "en_US");
            $totalPrice = $value['price'] * $quantity;
            $cartTotal +=  $totalPrice;
        }
        return $cartTotal;
    }


	public function SalesTax() {
		try {
        	$checkout = $_SESSION['checkout'];
        	$taxJar = new TaxJarRequest();
        	$response = json_decode($taxJar -> GetTaxes($this->getProductTotal(), 0, 'US', $checkout['state'], $checkout['city'], $checkout['zip']), true);
        	if(isset($response['amount_to_collect'])) {
        		return $response['amount_to_collect'];
        	} else {
        		Session::init();
				$data_id = md5(time().microtime().rand(0,100));
				$_SESSION["POSTDATA_$data_id"] = $_POST;		
				
				$TrackError = new EmailServerError();
				$TrackError -> message = TAXJAR_API_LABEL. "Something wrong with your taxjar account" . "(". $response . ")";
				$TrackError -> type = TAXJAR_API_TYPE;
				$TrackError -> SendMessage();
				
        		$this -> msg -> set($this -> msg -> flashMessage('message error CardError', 'Tax rates could not be generated, please try again'));
            	$this -> redirect -> redirectPage("cart/payment?data_id=$data_id");
        	}
        	
		} catch (Exception $e) {
			Session::init();
			$data_id = md5(time().microtime().rand(0,100));
			$_SESSION["POSTDATA_$data_id"] = $_POST;
			
			//email
			$TrackError = new EmailServerError();
			$TrackError -> message = TAXJAR_API_LABEL . $e->getMessage();
			$TrackError -> type = TAXJAR_API_TYPE;
			$TrackError -> SendMessage();
			
			$this -> msg -> set($this -> msg -> flashMessage('message error CardError', 'Could not generate tax rates, please try again'));
            $this -> redirect -> redirectPage("cart/payment?data_id=$data_id");
		}
			
	}
	
	public function ShippingRates() {
        $checkout = $_SESSION['checkout'];
        
		$rates = $this -> shipping -> getShippingRate($checkout['first-name'] . ' ' . $checkout['last-name'], 
													  $checkout['address'], 
													  '', 
													  $checkout['city'], 
													  $checkout['state'], 
													  $checkout['zip'], 
													  $checkout['phone'], 
													  parent::getTotalBoxes(), 
													  true, 
													  $checkout['shipping-service']);
		return $rates;
        
        
        //foreach ($this->shippingRates as $rate){
         //   if($rate['code'] == $checkout['shipping-service']) {
         //       $this -> shippingRateRecord = $rate['price'];
         //       return $rate['price'];
		//		
        //    }
        //}
	}

    public function AllShippingRates($firstName, $lastName, $address, $city, $state, $zip, $phone, $async = false) {
        try {
            $this -> shippingHeight = 10;
            $this -> shippingWidth = 10;
            $this -> shippingLength = 10;
			
			$fullName = $firstName . ' ' . $lastName;
            $rates = $this -> shipping -> getShippingRate($fullName, $address, '', $city, $state, $zip, $phone, parent::getTotalBoxes());

            
            if($rates){
                return $rates;
            }else{
                if($async == true){ // This seems to happen if there is a UPS or FedEx error
                    $this -> json -> outputJqueryJSONObject('ShippingRateError', "There was an error loading shipping rates");
                }
            }
			

        } catch (Exception $e) {
        	
        	$TrackError = new EmailServerError();
			$TrackError -> message = var_export($e, true);
			$TrackError -> type = "UPS VALIDATION ERROR";
			$TrackError -> SendMessage();
			
        	if($async == false) {
        		Session::init();
				$data_id = md5(time().microtime().rand(0,100));
				$_SESSION["POSTDATA_$data_id"] = $_POST;	
			}
			

            if(isset($e->detail) && isset($e->detail->Errors) && isset($e->detail->Errors->ErrorDetail) && isset($e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description)){
                if(strpos($e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description, 'The postal code')){
                    // INVALID POSTAL CODE FOR STATE
                    
                    $TrackError = new EmailServerError();
                    $TrackError -> message = SHIPPING_RATE_LABEL . var_export($e, true);
                    $TrackError -> type = SHIPPING_ERROR_TYPE;
                    $TrackError -> SendMessage();
                    
                    if($async == true) {
                    	$this -> json -> outputJqueryJSONObject('ShippingRateError', $e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description);	
                    } else {
                    	$this -> msg -> set($this -> msg -> flashMessage('message error CardError', $e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description));  
						$this -> redirect -> redirectPage("cart/payment?data_id=$data_id");                  	
                    }
                    
                } else {
                    // SOMETHING ELSE WENT WRONG... NOT SURE WHAT IS POSSIBLE HERE.... EMAIL US AND SHOW FRIENDLY ERROR?
					$TrackError = new EmailServerError();
                    $TrackError -> message = SHIPPING_RATE_LABEL . var_export($e, true);
                    $TrackError -> type = SHIPPING_ERROR_TYPE;
                    $TrackError -> SendMessage();
					
					if($async == true) {
						$this -> json -> outputJqueryJSONObject('ShippingRateError', $e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description);	
					} else {
						$this -> msg -> set($this -> msg -> flashMessage('message error CardError', $e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description));
						$this -> redirect -> redirectPage("cart/payment?data_id=$data_id");	
					}
					
                }

            }
        }



    }
	

	
	public function states() {
		return array('AL'=>'Alabama',
					 'AK'=>'Alaska', 
					 'AR'=>'Arkansas',
					 'AZ'=>'Arizona',
					 'CA'=>'California',
					 'CO'=>'Colorado',
					 'CT'=>'Connecticut',
					 'DE'=>'Delaware', 
					 'DC'=>'District of Columbia',
					 'FL'=>'Florida', 
					 'GA'=>'Georgia',
					 'HI'=>'Hawaii',
					 'IA'=>'Iowa',  
					 'ID'=>'Idaho',
					 'IL'=>'Illinois',
					 'IN'=>'Indiana', 
					 'KS'=>'Kansas', 
					 'KY'=>'Kentucky',
					 'LA'=>'Louisiana',
					 'ME'=>'Maine', 
					 'MD'=>'Maryland', 
					 'MA'=>'Massachusetts', 
					 'MI'=>'Michigan', 
					 'MN'=>'Minnesota', 
					 'MO'=>'Missouri',
					 'MS'=>'Mississippi', 
					 'MT'=>'Montana',
					 'NE'=>'Nebraska', 
					 'NM'=>'New Mexico', 
					 'NV'=>'Nevada',
					 'NH'=>'New Hampshire', 
					 'NJ'=>'New Jersey', 
					 'NY'=>'New York', 
					 'NC'=>'North Carolina', 
					 'ND'=>'North Dakota',
					 'OH'=>'Ohio', 
					 'OK'=>'Oklahoma', 
					 'OR'=>'Oregon', 
					 'PA'=>'Pennsylvania',
					 'RI'=>'Rhode Island', 
					 'SC'=>'South Carolina',
					 'SD'=>'South Dakota',
					 'TX'=>'Texas', 
					 'TN'=>'Tennessee',
					 'UT'=>'Utah',  
					 'VT'=>'Vermont', 
					 'VA'=>'Virginia',
					 'WA'=>'Washington', 
					 'WV'=>'West Virginia',
					 'WI'=>'Wisconsin',
					 'WY'=>'Wyoming');
	}

	public function months() {
		return array('01 - January'=>'01',
					 '02 - February'=>'02',
					 '03 - March'=>'03',
					 '04 - April'=>'04',
					 '05 - May'=>'05',
					 '06 - June'=>'06',
					 '07 - July'=>'07',
					 '08 - August'=>'08',
					 '09 - September'=>'09',
					 '10 - October'=>'10',
					 '11 - November'=>'11',
					 '12 - December'=>'12');
	}

	public function year() {
			
		$currentYear = date("Y");
	
		return array($currentYear => $currentYear,
					 $currentYear + 1 => $currentYear + 1,
					 $currentYear + 2 => $currentYear + 2,
					 $currentYear + 3 => $currentYear + 3,
					 $currentYear + 4 => $currentYear + 4,
					 $currentYear + 5 => $currentYear + 5,
					 $currentYear + 6 => $currentYear + 6,
					 $currentYear + 7 => $currentYear + 7,
					 $currentYear + 8 => $currentYear + 8,
					 $currentYear + 9 => $currentYear + 9,
					 $currentYear + 10 => $currentYear + 10,
					 $currentYear + 11 => $currentYear + 11,
					 $currentYear + 12 => $currentYear + 12,
					 $currentYear + 13 => $currentYear + 13,
					 $currentYear + 14 => $currentYear + 14,
					 $currentYear + 15 => $currentYear + 15,
					 $currentYear + 16 => $currentYear + 16,
					 $currentYear + 17 => $currentYear + 17,
					 $currentYear + 18 => $currentYear + 18,
					 $currentYear + 19 => $currentYear + 19,
					 $currentYear + 20 => $currentYear + 20);
	}

	
	public function clearCart() {
		unset($_SESSION['cart_array']);
		$this -> redirect -> redirectPage("cart");
	}	
	
	public function updateItem($productQuantity) {
	
		//echo $productQuantity['id'];
		
		foreach ($_SESSION["cart_array"] as $item) { 
	    	while (list($key, $value) = each($item)) {
				if ($key == "index" && $value == $productQuantity['index']) {
					// That item is in cart already so let's adjust its quantity using array_splice()
					array_splice($_SESSION["cart_array"], $item['index'], 1, array(array("index" => $productQuantity['index'],
																			"product_ID" => $productQuantity['product-id'], 
																			"quantity" => preg_replace('#[^0-9]#i', '', $productQuantity['quantity']),
																			"color" => $productQuantity['colorChnage'])));
				
			  	} // close if condition
	      	} // close while loop
		} // close foreach loop
		
		$this -> redirect -> redirectPage("cart");
		
	}
	
	public function deleteItem($id) {
		
		if (count($_SESSION["cart_array"]) <= 1) {
			unset($_SESSION["cart_array"]);
		} else {
			unset($_SESSION["cart_array"][$id]);
			sort($_SESSION["cart_array"]);
		}
		
				
		$this -> redirect -> redirectPage("cart");
	}
	
	public function generateShipping($shippingInfo) {
		
		$this -> redirect -> redirectPage("cart/payment");
	}
	
	public function getShippingRate() {
		return $this -> shippingRates;
	}
	
	private function logErrorAndRedirect($label, $errorMessage) {
		Session::init();
		$data_id = md5(time().microtime().rand(0,100));
		$_SESSION["POSTDATA_$data_id"] = $_POST;
			
		//email
		$TrackError = new EmailServerError();
		$TrackError -> message = $label;
		$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE;
		$TrackError -> SendMessage();
					
		// The card has been declined
		$this -> msg -> set($this -> msg -> flashMessage('message error CardError', $errorMessage));
		$this -> redirect -> redirectPage("cart/payment?data_id=$data_id");
		
		
	}
	public function payCart($checkout) {
		
		

								
		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			
			//record and email info
			$taxes = $this->SalesTax();
			$shippingRates = $this -> ShippingRates();
			$cartTotal = ($this -> getProductTotal() * 100) + ($taxes * 100) + ($shippingRates * 100);
			$emailCartTotal = $this -> getProductTotal() + $taxes + $shippingRates;
			
				
			$newOrder = new	Orders();		
			$newOrder -> stripeToken = $checkout['token'];
			$newOrder -> firstName = $checkout['first-name'];
			$newOrder -> lastName = $checkout['last-name'];
			$newOrder -> email = $checkout['email'];
			$newOrder -> address = $checkout['address'];
			$newOrder -> city = $checkout['city'];
			$newOrder -> state = $checkout['state'];
			$newOrder -> zip = $checkout['zip'];
			$newOrder -> country = "United States";
			$newOrder -> phone = $checkout['phone'];
			$newOrder -> countryAbbr = "USA";
			$newOrder -> taxRate = $taxes;
			$newOrder -> currentShippingRates = $this -> ShippingRates();
			$newOrder -> cartTotal = $emailCartTotal;
			$newOrder -> shippingMethodName = $checkout['shipping-method'];
			$newOrder -> shippingMethodCode = $checkout['shipping-service'];
			$newOrder -> addressValidation = $this -> validateAddress();			
			$newOrder -> getReceiptEmail = $this -> settings -> getReceiptEmail();
			$newOrder -> warrantyVerificatonID = Hash::createRandomString("warranty");
			
			$i =1;
			foreach($_SESSION['cart_array'] as $item) {
				array_push($newOrder -> cartItems, array("productID" => $checkout['productID'. $i],
                                                         "productColorID" => $checkout['productColorID'. $i],
                                                         "quantity"   => $checkout['productQnty'. $i],
                                                         "unitPrice" => $checkout['price'. $i],
														 "productImage" => $checkout['productImage'. $i]));
				 
				$i ++;
			}
			
			foreach(parent::getTotalBoxes() as $boxSingle) {
				array_push($newOrder -> packageDimensions, array("length" => $boxSingle['length'],
                                                         		 "width" => $boxSingle['width'],
                                                         		 "height" => $boxSingle['height'],
                                                         		 "weight" => $boxSingle['weight']));	
			}
			
			$newOrder -> preSaleTextVisible = $this -> settings -> preSaleVisible;
			$newOrder -> preSaleText = $this -> settings -> preSaleText;
			
			$newOrder -> saveOrder();
					   
		} catch(\Stripe\Error\Card $e) {
						
			$e_json = $e->getJsonBody();
			$err = $e_json['error'];
			$errors['stripe'] = $err['message'];							
			$this -> logErrorAndRedirect(STRIPE_CARD_EXCEPTION_LABEL . implode(" - ", $e_json['error']), $errors['stripe']);
		} catch (\Stripe\Error\RateLimit $e) {
  			// Too many requests made to the API too quickly  		
			$e_json = $e->getJsonBody();
  			$this -> logErrorAndRedirect(STRIPE_RATELIMIT_EXCEPTION_LABEL . implode(" - ", $e_json['error']), SYSTEM_ERROR_MESSAGE);  		
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$e_json = $e->getJsonBody();
			$this -> logErrorAndRedirect(STRIPE_INVALID_REQUEST_EXCEPTION_LABEL . implode(" - ", $e_json['error']), SYSTEM_ERROR_MESSAGE);
		} catch (\Stripe\Error\Authentication $e) {			
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$e_json = $e->getJsonBody();									
			$this -> logErrorAndRedirect(STRIPE_AUTHENTICATION_EXCEPTION_LABEL . implode(" - ", $e_json['error']), SYSTEM_ERROR_MESSAGE);
		} catch (\Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			$e_json = $e->getJsonBody();						
			$this -> logErrorAndRedirect(STRIPE_API_EXCEPTION_LABEL . implode(" - ", $e_json['error']), SYSTEM_ERROR_MESSAGE);
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$e_json = $e->getJsonBody();	
			$this -> logErrorAndRedirect(STRIPE_BASE_EXCEPTION_LABEL . implode(" - ", $e_json['error']), SYSTEM_ERROR_MESSAGE);
		} catch (Exception $e) {
			$this -> logErrorAndRedirect(NORMAL_CHECKOUT_EXCEPTION . $e-> getMessage(), SYSTEM_ERROR_MESSAGE);
		}
	}

}