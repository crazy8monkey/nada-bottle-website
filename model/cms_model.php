<?php
require 'libs/Pagination.php';
class CMS_Model extends Model {
	
	public $companyName;
	public $companyAddress;
	public $companyCity;
	public $companyState;
	public $companyZip;
	
	private $start;
	private $countQuery;
	private $userLimit;
	private $finalPaginationLinks;
	
	private $shippingCompany;
	
	public $shippingMethodName;

	public function __construct() {
		parent::__construct();
		$this -> companyName = $this -> settings -> getCompanyName();
		$this -> companyAddress = $this -> settings -> getCompanyAddress();
		$this -> companyCity = $this -> settings -> getCompanyCity();
		$this -> companyState = $this -> settings -> getCompanyState();
		$this -> companyZip = $this -> settings -> getCompanyZip();	
		$this -> shippingMethodName	= $this -> settings -> shippingCompany;
	}
	
	public function redirectPage($page) {
		return $this -> redirect -> redirectPage($page);
	}
	
	public function contactInquiries($pageVariable) {
		$page = $pageVariable;
		$this -> userLimit = "10";
		
		if($page) {
			//First Item to display on this page
			$this -> start = ($page - 1) * $this -> userLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> start = 0;
		}
		
		$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM ContactInquiries") -> fetchColumn();	
		$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/contact", $page);
		return $this -> db -> select('SELECT * FROM ContactInquiries where isActive=1 ORDER BY contactID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);
	
	
	}
	public function ShippingBoxes() {
		return $this -> db -> select('SELECT * FROM ShippingBoxes');	
	}
	
	public function paymentHistory($id = false) {
		if($id) {
			return $this -> db -> select('SELECT * FROM paymentLogs WHERE orderID = ' . $id . ' ORDER BY id DESC');	
		} else {
			return $this -> db -> select('SELECT * FROM paymentLogs ORDER BY id DESC');		
		}
		
	}
	
	
	public function feedBackList() {
		return $this -> db -> select('SELECT * FROM FeedBack where isActive=1 ORDER BY id DESC');	
	}
	
	public function webhooks() {
		return $this -> db -> select('SELECT * FROM WebHooks');	
	}
	
	public function warrantyPhotos($id) {
		return $this -> db -> select('SELECT * FROM warrantyPhotos WHERE warrantyID = ' . $id);	
	}
	
	public function partnersList() {
		return $this -> db -> select('SELECT * FROM partners');	
	}
	public function Settings() {
		return $this -> db -> select('SELECT * FROM settings');
	}
	public function locations() {
		return $this -> db -> select('SELECT * FROM locateLife');
	}
	public function productCategories() {
		return $this -> db -> select('SELECT * FROM productCategories where IsActive=1 ORDER BY position ASC');
	}
	public function usersList() {
		return $this -> db -> select('SELECT * FROM adminUsers');
	}
	
	public function teamList() {
		return $this -> db -> select('SELECT * FROM TeamMembers ORDER BY position ASC');
	}
	
	public function LatestOrders() {
		return $this -> db -> select('SELECT * FROM orders ORDER BY orderID DESC LIMIT 10');
	}
	
	public function LatestContacts() {
		return $this -> db -> select('SELECT * FROM ContactInquiries where isActive=1 ORDER BY contactID DESC LIMIT 10');
	}
	public function warranties() {
		return $this -> db -> select('SELECT * FROM warranties where isActive=1 ORDER BY warrantyID DESC');
	}
	
	public function LatestWarranty() {
		return $this -> db -> select('SELECT * FROM warranties where isActive=1 ORDER BY warrantyID DESC LIMIT 10');
	}
	
	public function emailList() {
		return $this -> db -> select('SELECT * FROM userEmails ORDER BY emailID DESC');
	}
	
	public function ShippingMethods() {
		$sth = $this -> db -> prepare('SELECT * FROM ShippingMethods WHERE Company = :Company');
        $sth -> execute(array(':Company' => $this -> shippingMethodName));
        return $sth->fetchAll();
	}
	

	public function getTaxReport($dtStart, $dtEnd)
    {
        $sth = $this -> db -> prepare('select sum(taxRate), state from orders WHERE isVoided=0 and date >= :startDate and date <= :endDate GROUP BY state');
        $sth -> execute(array(':startDate' => $dtStart, ':endDate' => $dtEnd));
        return $sth -> fetchAll();
    }

    public function productColors($id){
        $sth = $this -> db -> prepare('SELECT * FROM ProductColors WHERE productId = :productId AND isActive = 1');
        $sth -> execute(array(':productId' => $id));
        return $sth->fetchAll();
    }
	
	public function ProductList($productType = false) {
		if($productType) {
			$sth = $this -> db -> prepare('SELECT * FROM Products WHERE product_category = :product_category AND IsActive = 1');
			$sth -> execute(array(':product_category' => $productType));	
			return $sth -> fetchAll();
		}
		else {
			return $this -> db -> select('SELECT * FROM Products WHERE isActive=1');
		}
	}
	
	
	public function InventoryProductList() {		
		return $this -> db -> select('SELECT * FROM Products WHERE isActive=1 AND publishState=1');
	}
	
	public function InventoryProductColors() {
		return $this -> db -> select('SELECT * FROM ProductColors WHERE isActive = 1');
	}
	
	public function thankYouList($pageVariable) {
		$page = $pageVariable;
		$this -> userLimit = "20";
		
		if($page) {
			//First Item to display on this page
			$this -> start = ($page - 1) * $this -> userLimit;
		} else {
			//if no page variable is given, set start to 0
			$this -> start = 0;
		}
		
		$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM ThankYouList") -> fetchColumn();	
		$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/thankYouList", $page);
		return $this -> db -> select('SELECT * FROM ThankYouList ORDER BY supporterName LIMIT ' . $this -> start. ', ' . $this -> userLimit);
		
		
		
		
	}
	
	public function orders($pageVariable, $status = false) {
	
	//$_GET['page']
	$page = $pageVariable;
			$this -> userLimit = "8";
		
			if($page) {
				//First Item to display on this page
				$this -> start = ($page - 1) * $this -> userLimit;
			}
			else {
				//if no page variable is given, set start to 0
				$this -> start = 0;
			}
	
		//$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders");
		if($status) {
			switch ($status) {
				case "finished";
					$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM orders WHERE isFinished = 1") -> fetchColumn();
					$sth = $this -> db -> prepare('SELECT * FROM orders WHERE isFinished = :isFinished ORDER BY orderID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);
					$sth -> execute(array(':isFinished' => 1));
					$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders", $page, "finished");
					return $sth -> fetchAll();	
					break;
				case "unfinished";
					$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM orders WHERE isFinished = 0") -> fetchColumn();
					$sth = $this -> db -> prepare('SELECT * FROM orders WHERE isFinished = :isFinished ORDER BY orderID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);
					$sth -> execute(array(':isFinished' => 0));
					$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders", $page, "unfinished");
					return $sth -> fetchAll();	
					break;
				case "invalidAddress";
					$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM orders WHERE isValidAddress IN (0, 2, 3)") -> fetchColumn();	
					$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders", $page, "invalidAddress");
					return $this -> db -> select('SELECT * FROM orders WHERE isValidAddress IN (0, 2, 3) ORDER BY orderID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);	
					break;
				case "disputed";
					$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM orders WHERE stripeStatus = 'disputed'") -> fetchColumn();
					$sth = $this -> db -> prepare('SELECT * FROM orders WHERE stripeStatus = :stripeStatus ORDER BY orderID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);
					$sth -> execute(array(':stripeStatus' => "disputed"));
					$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders", $page, "disputed");
					return $sth -> fetchAll();
					break;
				case "void";
					$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM orders WHERE isVoided = 1") -> fetchColumn();
					$sth = $this -> db -> prepare('SELECT * FROM orders WHERE isVoided = :isVoided ORDER BY orderID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);
					$sth -> execute(array(':isVoided' => 1));
					$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders", $page, "void");
					return $sth -> fetchAll();	
					break;
					
			}
		} else {		
			$this -> countQuery = $this -> db -> query("SELECT COUNT(*) FROM orders") -> fetchColumn();	
			$this -> pagination -> paginationLinks($this -> countQuery, $this -> userLimit, PATH . "cms/orders", $page);
			return $this -> db -> select('SELECT * FROM orders ORDER BY orderID DESC LIMIT ' . $this -> start. ', ' . $this -> userLimit);	
		}
		
	}

	public function getOrderPagination() {
		return $this -> pagination -> getPaginationLinks();
	}

	private function getShipmentObject($id) {
		$shipment = Shipments::WithOrderID($id);
		return $shipment; 
	}

    public function shippingLabel($id, $keyElement) {
        if($this -> getShipmentObject($id) -> shipmentType == 1) { // UPS
        	
			$images = array();	
			
            // Fetch the proper node of the XML that contains the label
            $label = simplexml_load_file(PATH . $this -> getShipmentObject($id) -> responseData);
			$label -> registerXPathNamespace('ship', 'http://www.ups.com/XMLSchema/XOLTWS/Ship/v1.0');	
			
			foreach($label -> xpath('//ship:ShipmentResponse/ship:ShipmentResults/ship:PackageResults/ship:ShippingLabel/ship:GraphicImage[1]') as $picture) {
				array_push($images, $picture);
			}
            header('Content-Type: image/png');
			return base64_decode($images[$keyElement]);
            
        }
        if($this -> getShipmentObject($id) -> shipmentType == 2){ // FedEx
			$fedexJSONString= file_get_contents(PATH . $this -> getShipmentObject($id)-> responseData);
            
            $response = json_decode($fedexJSONString);
            $images = array();
            foreach($response as $package){
                array_push($images, $package->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image);
            }
            header('Content-Type: application/pdf');
            return $images[$keyElement];
        }
    }

    public function orderLabels($id) {
        if($this -> getShipmentObject($id) -> shipmentType == 1) { // UPS
        	
			$images = array();	
			
            // Fetch the proper node of the XML that contains the label
            $label = simplexml_load_file(PATH . $this -> getShipmentObject($id)->responseData);
			$label -> registerXPathNamespace('ship', 'http://www.ups.com/XMLSchema/XOLTWS/Ship/v1.0');	
			
			foreach($label -> xpath('//ship:ShipmentResponse/ship:ShipmentResults/ship:PackageResults/ship:ShippingLabel/ship:GraphicImage[1]') as $picture) {
				array_push($images, $picture);
			}
			return $images;		
        }
        if($this -> getShipmentObject($id) -> shipmentType == 2){ // FedEx
            $images = array();
			
			$fedexJSONString = file_get_contents(PATH . $this -> getShipmentObject($id) -> responseData);
            $packages = json_decode($fedexJSONString);
			
			
            foreach($packages as $s){
                array_push($images, $s->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image);
            }
            return $images;
        }
		
    }	
		
	public function getTrackingUrl($orderID){
		if($this -> getShipmentObject($orderID) -> shipmentType == 1) { // UPS
			$trackingURLS = array();
		
			$getTracking = simplexml_load_file(PATH . $this -> getShipmentObject($orderID) -> responseData);
			$getTracking -> registerXPathNamespace('ship', 'http://www.ups.com/XMLSchema/XOLTWS/Ship/v1.0');	
			
			foreach($getTracking -> xpath('//ship:ShipmentResponse/ship:ShipmentResults/ship:PackageResults/ship:TrackingNumber[1]') as $tracking) {
				array_push($trackingURLS, $tracking);
			}

			return $trackingURLS;
		
	    } else if($this -> getShipmentObject($orderID) -> shipmentType == 2){ // Fedex
	    	
	    	$fedExTrackingURL = array();
			$fedexJSONString= file_get_contents(PATH . $this -> getShipmentObject($orderID) -> responseData);
			
	    	$d = json_decode($fedexJSONString);  
			foreach($d as $tracking) {
				array_push($fedExTrackingURL, $tracking -> CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber);
			}
			
		    return $fedExTrackingURL;
	    }
	        
	       
	}
	
	
	
	public function productSingle($id, $category) {
		$sth = $this -> db -> prepare('SELECT * FROM Products WHERE product_id = :id AND product_category = :product_category');
		$sth -> execute(array(':id' => $id, ':product_category' => $category));
		return $sth -> fetch();
	}
		
	
				
	public function singleOrder($id) {
		//LEFT OUTER JOIN shipments on orders.OrderId = shipments.OrderId
		$sth = $this -> db -> prepare('SELECT * FROM orders LEFT OUTER JOIN  shipments on orders.OrderId = shipments.OrderId WHERE orders.orderID = :id');
		
		
		//$sth = $this -> db -> prepare('SELECT * FROM orders WHERE orderID = :id');
		$sth -> execute(array(':id' => $id));

        $itemQuery = $this -> db -> prepare('SELECT ProductColors.Color, Products.product_name, orderItems.quantity FROM orderItems '
            .'INNER JOIN Products on orderItems.productID = Products.product_id '
            .'INNER JOIN ProductColors on orderItems.productColorID = ProductColors.productColor_id '
            .' WHERE orderID = :id');

        $itemQuery -> execute(array(':id' => $id));
		
		$packageQuery = $this -> db -> prepare('SELECT * FROM packageItems '
            .'INNER JOIN orders ON orders.orderID = packageItems.orderID '
            .' WHERE packageItems.orderID = :id');
			
		$packageQuery -> execute(array(':id' => $id));


        $returnValue = array();
        $returnValue['order'] = $sth -> fetch();
        $returnValue['items'] = $itemQuery -> fetchAll();
		$returnValue['package'] = $packageQuery -> fetchAll();

		return $returnValue;
	}
	
	
	private function ShippingRates($shippingRates, $boxes = array()) {
		$rates = "";
		
		$code = Shipments::WithServiceCode($shippingRates['shipping-service']);
		if($code -> shippingCompany == "UPS") {
			parent::setShippingCompany("UPS");
		} else if($code -> shippingCompany == "FedEx") {
			parent::setShippingCompany("FedEx");
		}
        $rates = $this -> shipping -> getShippingRate($shippingRates['first-name'] . ' ' . $shippingRates['last-name'], $shippingRates['address'], '', $shippingRates['city'], $shippingRates['state'], $shippingRates['zip'], $shippingRates['phone'], $boxes, true, $shippingRates['shipping-service']);

    	return $rates;
	}

	private function generateShippingBoxesByOrderID($id) {
		$package = new Package();
		$package ->	orderID = $id;
		$shippingBoxes = array();
		
		foreach($package -> LoadPackages() as $box) {
			//echo "package id: ". $box['packageItemID'];
			array_push($shippingBoxes, array("length" => $box['length'],
                                           	  "width" => $box['width'],
                                              "height" => $box['height'],
                                              "weight" => $box['weight']));
		}
		
		return $shippingBoxes;
	}
	
    public function generateShipping($id){
    	try {
	    	$order = Orders::WithID($id);
			
			$code = Shipments::WithServiceCode($order->shippingMethodCode);
			$newShipment = new Shipments();
			$newShipment -> orderID = $id;
			
			$shipmentType = "";
		
			
			
			if($code -> shippingCompany == "UPS") {
				$shipmentType = 1;
            	
				parent::setShippingCompany("UPS");
				$shipment = $this -> shipping -> processShipment($order -> getFullName(), 
				 											 $order -> address, 
				 											 '', 
				 											 $order -> city,
               												 $order -> state, 
               												 $order -> zip, 
               												 $order -> shippingMethodCode,
															 $this -> generateShippingBoxesByOrderID($id));
															 
				$newShipment -> responseData = 'UPS_ShipmentResponse_OrderNumber_' .$id . '.xml';
				$shipmentXML = fopen('view/shipmentResponses/UPS_ShipmentResponse_OrderNumber_' .$id . '.xml', 'w');
				fwrite($shipmentXML, $shipment);
				fclose($shipmentXML);
															 
				
			} else if($code -> shippingCompany == "FedEx") {
            			$shipmentType = 2;
				parent::setShippingCompany("FedEx");
				$shipment = $this -> shipping -> processShipment($order -> getFullName(), 
				 											 $order -> address, 
				 											 '', 
				 											 $order -> city,
               												 $order -> state, 
               												 $order -> zip, 
                											 $order -> shippingMethodCode,
															 $order -> phone,
															 $this -> generateShippingBoxesByOrderID($id));
				
				
				$newShipment -> responseData = 'FedEx_ShipmentResponse_OrderNumber_' .$id . '.json';
				$shipmentJson = fopen('view/shipmentResponses/FedEx_ShipmentResponse_OrderNumber_' .$id . '.json', 'w');
				fwrite($shipmentJson, json_encode($shipment));
				fclose($shipmentJson);
				
            	
            }
            
			
            
			
           
            $newShipment -> shipmentType = $shipmentType;
			$newShipment -> newShipment();
			
			//send tracking email
			$trackingInfo = array();	
			$trackingInfo['order-id'] = $id;
			$trackingInfo['customer-fullname'] = $order -> getFullName();
			$trackingInfo['tracking-email-text'] = $this -> settings -> getTrackingEmailText();
			$trackingInfo['customer-shipping-address'] = $order -> getShippingAddress();
			$trackingInfo['shipmentType'] = $shipmentType;
			$trackingInfo['tracking-link'] = $this -> getTrackingUrl($id);
			$trackingInfo['customer-email'] = $order -> email;
			
			Email::tracking($trackingInfo);
	
	        $this -> msg -> set($this -> msg -> flashMessage('message generic', "Shipment Generated"));
	        $this -> redirect -> redirectPage('cms/transaction/' .$id);    		
    	} catch (Exception $e) {
    		$this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
    	
    		$TrackError = new EmailServerError();
			$TrackError -> message = NORMAL_CHECKOUT_EXCEPTION . var_export($e, true);
			$TrackError -> type = TRACKING_EMAIL_EXCEPTION;
			$TrackError -> SendMessage();
			
			
			$this -> redirect -> redirectPage('cms/transaction/' .$id); 
		}

    }

	public function voidShipment($id) {
        $track = '';
		try {
			$this -> msg -> set($this -> msg -> flashMessage('message generic', "Shipment Voided"));
			$shipment = Shipments::WithOrderID($id);
            $trackingNumbers = $this->getTrackingUrl($id);
			if($shipment -> shipmentType == 1) {
				parent::setShippingCompany("UPS");
			} else {
				parent::setShippingCompany("FedEx");
			}
				
			
			$this -> shipping -> voidShipment($trackingNumbers[0]);
			$shipment -> voidShipment();
			
			$this -> redirect -> redirectPage('cms/transaction/' .$id); 
		} catch (Exception $e) {
            $this -> msg -> set($this -> msg -> flashMessage('message error', SYSTEM_ERROR_MESSAGE));
            $TrackError = new EmailServerError();
			$TrackError -> message = "UPS void shipment error: " . $e-> getMessage();
			$TrackError -> type = "UPS API ERROR";
			$TrackError -> SendMessage();
			
			
			$this -> redirect -> redirectPage('cms/transaction/' .$id); 
		}
	}

	
	public function deleteMultiple($ids, $type) {
		switch ($type) {
			case "warranty";
				foreach ($ids as $id) {
					$warranty = Warranty::WithID($id);
					$warranty -> delete();
				}
				break;
			case "contact";
				foreach ($ids as $id) {
					$contact = Contact::WithID($id);
					$contact -> delete();					
				}
				break;
			case "feedback";
				foreach ($ids as $id) {
					$feedback = FeedBack::WithFeedBackID($id);
					$feedback -> delete();					
				}
				break;
			case "ThankYou";
				foreach ($ids as $id) {
					$supporter = ThankYou::WithID($id);
					$supporter -> delete();					
				}
				break;
		}
	}
	public function saveShippingMethods($shippingMethod) {
		
		foreach ($shippingMethod as $value) {
			$shippingMethodContent = explode(", ", $value);
			$settings = New Settings();
			$settings -> shippingMethodID = $shippingMethodContent[1];
			$settings -> shippingMethodActive = $shippingMethodContent[0];
			$settings -> saveShippingMethods();	
        }
		
	
	}
		

	
	public function changeShippingInfo($newShipping) {
		
		if($this -> validate -> emptyInput($newShipping['address'])) {
			$this -> json -> outputJqueryJSONObject('emptyNewAddress', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Address")));
		} else if($this -> validate -> emptyInput($newShipping['city'])) {
			$this -> json -> outputJqueryJSONObject('emptyCity', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("City")));
		} else if($this -> validate -> emptyInput($newShipping['state'])) {
			$this -> json -> outputJqueryJSONObject('emptyState', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("State")));
		} else if($this -> validate -> emptyInput($newShipping['zip'])) {
			$this -> json -> outputJqueryJSONObject('emptyZip', $this -> msg->flashMessage('message error', $this -> msg -> isRequired("Zip")));
		} else {
			
			\Stripe\Stripe::setApiKey(STRIPE_TEST_KEY);	
			try {
				$order = Orders::WithID($newShipping['id']);
				
				//email string to send to the user	
				$confirmation = array();
				$confirmation['todays-date'] = date("F j, Y", $this -> currentTime -> coloradoTime());
				$confirmation['order-id'] = $order -> orderID;
				$confirmation['new-address'] = $newShipping['address'] . "<br />" . $newShipping['city'] . ", " . $newShipping['state'] . " ". $newShipping['zip'] . "<br /> United States" ;
				$confirmation['customer-email'] = $order -> email;	
				
				//New Shipping rate logic
				$shippingRates = array();
				$shippingRates['first-name'] = $order -> firstName;
				$shippingRates['last-name'] = $order -> lastName;
				$shippingRates['address'] = $newShipping['address'];
				$shippingRates['city'] = $newShipping['city'];
				$shippingRates['state'] = $newShipping['state'];
				$shippingRates['zip'] = $newShipping['zip'];
				$shippingRates['phone'] = $order -> phone;
				$shippingRates['shipping-service'] = $order -> shippingMethodCode;
				
				if($order -> updatedShippingRates == 0) {
					$latestShippingRates = $order -> currentShippingRates;
				} else {
					$latestShippingRates = $order -> updatedShippingRates;
				}
				
	
				
				$stripeDescription = "Activity Time: " . date("F jS, Y", $this -> currentTime -> coloradoTime()) . " / Order #: " . $newShipping['id'] . " / Details: Shipping rate changes"; 
				
				$newShippingRates = $this -> ShippingRates($shippingRates, $this -> generateShippingBoxesByOrderID($newShipping['id']));
				//echo $newShippingRates;
				
				//if new shipping rates is higher than original
				if($newShippingRates > $order -> currentShippingRates) {
					$partialCharge = ((int)$newShippingRates) - ($order -> currentShippingRates);
					
					//apparently it has to be more than 50 cents
					if (($partialCharge * 100) > 50) {
						$charge = \Stripe\Charge::create(array(
							"amount" => $partialCharge * 100, // amount in cents, again
				  			"currency" => "usd",
  							"customer" => $order -> stripeCustomerID,
				  			"description" => $stripeDescription
						));
					
						$partialChargeEmail = money_format("%10.2n", $partialCharge / 100);
					
					
						$confirmation['change-shipping-rates'] = "Due to a change in your shipping address, your shipping rate has changed to <strong>$" . str_replace(" ", "", $newShippingRates) ."</strong>. ". "This is an additional <strong>$" . str_replace(" ", "", $partialCharge)  . "</strong> from the original shipping rate on the date of purchase.";
		
		
					
						
					}
					
					
				
				}
				//if new shipping rates is lower than original
				if($latestShippingRates > $newShippingRates) {
					$partialRefund = $latestShippingRates - $newShippingRates;
						
					$checkoutID = \Stripe\Charge::retrieve($order -> stripeCheckoutID);
			
					$checkoutID->refunds->create(array(
						'amount' => $partialRefund * 100
					));
					
					
					$partialRefundEmail = money_format("%10.2n", $partialRefund / 100);
					
					
				
					$confirmation['change-shipping-rates'] = "Due to a change in the shipping address, your shipping rate has changed to <strong>$" . str_replace(" ", "", $newShippingRates) ."</strong>. You will be refunded <strong>$" . str_replace(" ", "", $partialRefund)  . "</strong> from the original shipping rate at the date of purchase.";
					
				}
				
				
				$newMapItButton = "http://maps.google.com/maps?q=" . str_replace(" ", "+",$newShipping['address']) . ",+" . $newShipping['city'] . ",+" . $newShipping['state'] . ",+" .$newShipping['zip'] . "&z=14";
				

				//$newMapItButton = "http://maps.google.com/maps?q=" . str_replace(" ", "+",$newShipping['address']) . "," .$newShipping['zip'] . "&amp;hl=en&amp;t=k";	
			
				$newShippingRatesString = str_replace(" ", "", money_format("%10.2n",$newShippingRates));
			
				$this -> json -> multipleJSONOjbects(array("success" => $this -> msg->flashMessage('message success', "Shipping address changed"), 
														   "shippingAddress" => $newShipping['address'],
														   "shippingCity" => $newShipping['city'],
														   "shippingState" => $newShipping['state'],
														   "shippingZip" => $newShipping['zip'],
														   "changedDate"	=> date("F j, Y / g:i a", $this -> currentTime -> coloradoTime()),
														   "newShippingRates" => $newShippingRatesString, 
														   "updateStripeValue" => $newShippingRates,
														   "updateMapItButton" => $newMapItButton,
														   "ServiceCode" => $order -> shippingMethodCode));	
			
				$postData = array("address" => $newShipping['address'],
								  "city" => $newShipping['city'],
								  "state" => $newShipping['state'],
								  "zip" => $newShipping['zip'],
								  "shippingChange" => 1, 
								  "shippingChangeDate" => date("F j, Y / g:i a", $this -> currentTime -> coloradoTime()),
								  "newShippingRate" => $newShippingRates);
							  
							  
				$where = array('orderID' => $order -> orderID); 
					
				$this -> db -> update("orders", $postData, $where);	
			
				Email::NewShippingConfirm($confirmation);
			
			} catch(\Stripe\Error\Card $e) {
				$e_json = $e->getJsonBody();
			  
				//email
				$TrackError = new EmailServerError();
				$TrackError -> message = STRIPE_CARD_EXCEPTION_LABEL . implode(" - ", $e_json['error']);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
				
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} catch (\Stripe\Error\RateLimit $e) {
	  			// Too many requests made to the API too quickly
	  			Session::init();
				$data_id = md5(time().microtime().rand(0,100));
				$_SESSION["POSTDATA_$data_id"] = $_POST;
				
				$e_json = $e->getJsonBody();
				
				//email
				$TrackError = new EmailServerError();
				$TrackError -> message = STRIPE_RATELIMIT_EXCEPTION_LABEL . implode(" - ", $e_json['error']);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
						
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} catch (\Stripe\Error\InvalidRequest $e) {
				$e_json = $e->getJsonBody();
			  
				//email
				$TrackError = new EmailServerError();
				$TrackError -> message = STRIPE_INVALID_REQUEST_EXCEPTION_LABEL . implode(" - ", $e_json['error']);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
					
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} catch (\Stripe\Error\Authentication $e) {
				$e_json = $e->getJsonBody();
			  
				///email
				$TrackError = new EmailServerError();
				$TrackError -> message = STRIPE_AUTHENTICATION_EXCEPTION_LABEL . implode(" - ", $e_json['error']);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
					
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} catch (\Stripe\Error\ApiConnection $e) {
				$e_json = $e->getJsonBody();
			  
				//email
				$TrackError = new EmailServerError();
				$TrackError -> message = STRIPE_API_EXCEPTION_LABEL . implode(" - ", $e_json['error']);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
							
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} catch (\Stripe\Error\Base $e) {
				$e_json = $e->getJsonBody();
			  
				//email
				$TrackError = new EmailServerError();
				$TrackError -> message = STRIPE_BASE_EXCEPTION_LABEL . implode(" - ", $e_json['error']);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
						
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Stripe
				
				//email
				$TrackError = new EmailServerError();
				$TrackError -> message = "Non Stripe Exception: " . var_export($e, true);
				$TrackError -> type = STRIPE_CHECKOUT_ERROR_TYPE . "(CMS CHANGE ADDRESS METHOD)";
				$TrackError -> SendMessage();
				
				$this -> json -> outputJqueryJSONObject('stripeError', $this -> msg->flashMessage('message error', SYSTEM_ERROR_MESSAGE));
			}
		}
	}
	
		
	public function uploadImage($uploadedImage) {
		
		$funcNum = $_GET['CKEditorFuncNum'] ;
		$url = '../view/images/' . $uploadedImage;
		$message = '';
		
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		
		Image::moveImage($uploadedImage, $_FILES["upload"]["tmp_name"], "view/images/");
	}

}