<?php

class About_Model extends Model {

	public function __construct() {
		parent::__construct();		
	}
	
	public function thankyouList() {
		return $this -> db -> select('SELECT * FROM ThankYouList ORDER BY supporterName');	
	}
	
	public function teamList() {
		return $this -> db -> select('SELECT * FROM TeamMembers ORDER BY position ASC');	
	}
	


}