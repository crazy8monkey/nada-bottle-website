<?php

Class Purchase_Model extends Model {

	public $totalCartWeightValue;

	public function __construct() {
		parent::__construct();
		if(isset($_SESSION['cart_array'])) {
			parent::getTotalItems();	
			$this -> totalCartWeightValue = parent::getTotalCartWeight();
		}
	}
	
	public function colors($productId) {
		$sth = $this -> db -> prepare('SELECT * FROM ProductColors WHERE productId = :productId and IsActive = 1');
        $sth -> execute(array(':productId' => $productId));
        return $sth-> fetchAll();
	}
	
	public function ProductList($productCategory = false) {
		if($productCategory == true) {
			$sth = $this -> db -> prepare('SELECT * FROM Products WHERE product_category = :product_category AND publishState = 1 and IsActive = 1');
			$sth -> execute(array(':product_category' => $productCategory));
			return $sth -> fetchAll();	
		}
		else {
			return $this -> db -> select('SELECT * FROM Products WHERE publishState = 1 and IsActive = 1');
		}
			
	}
	
	public function productCategories() {
		return $this -> db -> select('SELECT * FROM productCategories WHERE IsActive = 1 ORDER BY position ASC');
	}
	
	public function productSingle($id, $category) {
		$sth = $this -> db -> prepare('SELECT * FROM Products WHERE product_id = :id AND product_category = :product_category');
		
							  
		
		$sth -> execute(array(':id' => $id,
							  ':product_category' => $category));
							  
		if($sth) {
			return $sth -> fetch();		
		} else {
			echo "blak";
		}
		
		
							  
		
		
	}
	
	public function getPreSaleVisible() {
		return $this -> settings -> preSaleVisible;
	}
	public function getPreSaleText() {
		return $this -> settings -> preSaleText;
	}
	
}
?>
