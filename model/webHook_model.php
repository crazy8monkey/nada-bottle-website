<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 4/8/15
 * Time: 6:40 PM
 */

class WebHook_Model extends Model{

	public $inventoryEmail;
	public $todaysDate;
	private $amount; 
	private $refundedAmount;

    public function __construct() {
        parent::__construct();
		$this -> inventoryEmail = $this -> settings ->  getInventoryEmail();
		$this -> todaysDate = date("F j, Y", $this -> currentTime -> coloradoTime());
    }

    function processWebHook($val){
    
    	$webHookCheck = $this->db->prepare('SELECT FormPostValue FROM WebHooks WHERE FormPostValue = ?');
        $webHookCheck->execute(array(json_encode($val)));
    
    	//if event is not in the webHook talbe
    	if (count($webHookCheck -> fetchAll()) <=0 ) {
	        switch($val->type){
	            case 'charge.succeeded':
	                echo "succeeded: ";
	                echo $val->data->object->id;
	                $this -> amount = $val->data->object->amount;
	                $this->updateOrder($val->data->object->customer, $val->data->object->id, true, 'succeeded', false, $val->data->object->amount);
	                break;
	            case 'charge.failed':
	                echo "failed: ";
	                echo $val->data->object->id;
	                $this->updateOrder($val->data->object->customer, $val->data->object->id, true, 'failed', $val->data->object->failure_message);
	                break;
	            case 'charge.refunded':
	                echo "refunded: ";
	                echo $val->data->object->id;
	                
	                $payment = Payments::WithPaymentID($val->data->object->id);
	                
	                
	                $this -> amount = $val->data->object->amount_refunded;
	                $this -> refundedAmount = ($val->data->object->amount_refunded / 100) - $payment -> previousTotalRefunded;
	                $this->updateOrder($val->data->object->customer, $val->data->object->id, true, 'Partially Refunded', 'refunded', $val->data->object->amount_refunded);
	                
	                break;
	            case 'charge.captured':
	                echo "captured: ";
	                echo $val->data->object->id;
	                $this->updateOrder($val->data->object->customer, $val->data->object->id, true, 'captured');
	                break;
	            case 'charge.dispute.created':
	                echo "disputed: ";
	                //need charge id
	                echo $val->data->object->charge;
	                $this->updateOrder(false, $val->data->object->charge, true, 'disputed');
	                break;
	            case 'charge.dispute.funds_reinstated':
	                echo "disputed: ";
	                //need charge id
	                echo $val->data->object->charge;
	                $this->updateOrder(false, $val->data->object->charge, true, 'dispute: payment reinstated');
	                break;
		    case 'charge.dispute.funds_withdrawn':
	                echo "disputed: ";
	                //need charge id
	                $this -> amount = 1500;
	                echo $val->data->object->charge;
	                $this->updateOrder(false, $val->data->object->charge, true, 'dispute: funds withdrawn', "Dispute Fee");
	                break;
	            case 'charge.dispute.updated':
	                echo "disputed: ";
	                //need charge id
	                echo $val->data->object->charge;
	                $this->updateOrder(false, $val->data->object->charge, true, 'dispute updated');
	                break;
	            case 'charge.dispute.closed':
	                echo "disputed: ";
	                //need charge id
	                echo $val->data->object->charge;
	                $this->updateOrder(false, $val->data->object->charge, true, 'dispute closed: ' . $val->data->object->status, $val->data->object->reason);
	                break;
	
				
					
	        }
	
	        $this -> db -> insert("WebHooks", array('FormPostValue' => json_encode($val)));
	        
        }
    }

    function getOrder($customerID, $status, $chargeID){
    	if (preg_match('/dispute/',$status) || preg_match('/refunded/',$status)) {
   		$sth = $this -> db -> prepare('SELECT * FROM orders WHERE stripeCheckoutID = :stripeCheckoutID');
        	$sth -> execute(array(':stripeCheckoutID' => $chargeID)); 		
    	} else {
   		$sth = $this -> db -> prepare('SELECT * FROM orders WHERE stripeCustomerID = :stripe_customer_id');
        	$sth -> execute(array(':stripe_customer_id' => $customerID));
    	
    	}
    	
    	//$OrderArrayCheck = array_filter($sth -> fetchAll());
		
	$orderID = $sth -> fetch(PDO::FETCH_OBJ);	
	
    	
    	
    	if(!empty($orderID)) {
    		
		//gets the order id
		return $orderID -> orderID;
    	}
    
   	
    }

    function updateOrder($customerID = false, $chargeID, $email = false, $status, $reason = false, $payment = false){
	    try {
	    	$payment = new Payments();
		$payment -> orderID = $this -> getOrder($customerID, $status, $chargeID);
			
		$payment -> sendEmail = $email;
		$payment -> paymentEmail = $this -> settings -> getPaymentEmail();
		$payment -> amount = $this -> amount;
		$payment -> refundedAmount = $this -> refundedAmount;
		$payment -> reason = $reason;
		$payment -> paymentStatus = $status;
    		$payment -> stripeChargeID = $chargeID;
	    	
	    	$payment -> Save();
			
			
	    } catch (Exception $e) {
	    	//email
		$TrackError = new EmailServerError();
		$TrackError -> message = MYSQL_INSERT_LABEL . $e->getMessage();
		$TrackError -> type = MYSQL_ERROR_TYPE;
		$TrackError -> SendMessage();
	    }
    	
    	
				
    }

    
    public function GetOutOfStockItems(){
        // Get All Products And Colors
        $productQuery = $this->db->prepare('SELECT * FROM Products AS P INNER JOIN ProductColors AS PC ON P.product_id = PC.productId WHERE PC.quantityRemaining < :quantity AND PC.IsActive = 1 AND P.IsActive=1');
        $productQuery -> execute(array(':quantity' => 3));
        $getProductInfo = $productQuery -> fetchAll();

        $products = array();

        foreach($getProductInfo as $row){
            $p = null;
            $foundProduct = false;
            foreach($products as $product){
                if(!$foundProduct && $product->ProductId == $row['product_id']){
                    $p = $product;
                    $foundProduct = true;
                }
            }
            if(!$foundProduct){
                $p = new OutOfStockProduct();
                $p->ProductId = $row['product_id'];
                $p->ProductName = $row['product_name'];
                $p->OutOfStockColors = array();
                array_push($products, $p);
            }
            $color = new OutOfStockColor();
            $color->ColorHexacode = $row['hexacode'];
            $color->ColorName = $row['Color'];
            array_push($p->OutOfStockColors, $color);
        }
        return $products;
    }




}