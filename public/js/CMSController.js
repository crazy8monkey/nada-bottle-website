jQuery.fn.extend({
    toggleText: function (a, b){
        var that = this;
            if (that.text() != a && that.text() != b){
                that.text(a);
            }
            else
            if (that.text() == a){
                that.text(b);
            }
            else
            if (that.text() == b){
                that.text(a);
            }
        return this;
    }
});
var CMSController = function() {
	
	function validateColorForm() {
		$("#ColorForm").validate({
			onfocusout : function(element) {
				jQuery(element).valid()
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			}
		});
	}
	
	
	
	function validateEditPartnerForm() {
		$("#editPartnerForm").validate({
			messages : {
				partnerName : {
					required : "Please enter a partner mame"
				},
				partnerHyperLink : {
					required : "Please enter partner website link"
				}
			},
			onfocusout : function(element) {
				jQuery(element).valid()
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			}
		});
	}
	
	
		
	function categoryForm() {
		$("#CategoryForm").submit(function(e){
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			var formURL = $(this).attr("action");
			//var postData = $(this).serialize();

	
			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: formData,
		        async: true,
		        dataType: 'json',
		        beforeSend: function(){
		            $(".loadingCategory").show();
    		    },
				success :function(data) {
					if(data.categoryNameError) {
						Globals.loadAjaxFlashMessage(data.categoryNameError, false, false, true);
						$(".loadingCategory").hide();
					}
					if(data.uploadImageProblem) {
						Globals.loadAjaxFlashMessage(data.uploadImageProblem, false, false, true);
						$(".loadingCategory").hide();
					}
					if(data.categoryUpdated) {
						window.location = data.NewCategoryName;
					}
					if(data.redirectLink) {
						window.location = data.redirectLink;
					}
				},
				cache: false,
		        contentType: false,
		        processData: false
			});					
		});
	}
	
	
	function AddColorForm() {
		$("#ProductColorForm").submit(function(e){
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			var formURL = $(this).attr("action");

	
			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: formData,
		        async: true,
		        dataType: 'json',
		        beforeSend: function(){
		            $(".loadingColor").show();
    		    },
				success :function(data) {
					if(data.emptyColorName) {
						Globals.loadAjaxFlashMessage(data.emptyColorName, false, false, true);
						$(".loadingColor").hide();
					}
					if(data.emptyColorPhoto) {
						Globals.loadAjaxFlashMessage(data.emptyColorPhoto, false, false, true);
						$(".loadingColor").hide();
					}
					if(data.uploadImageProblem) {
						Globals.loadAjaxFlashMessage(data.uploadImageProblem, false, false, true);
						$("#color-upload").val();
						
						$(".loadingColor").hide();
					}
					if(data.hexacodeProblem) {
						Globals.loadAjaxFlashMessage(data.hexacodeProblem, false, false, true);
						$(".loadingColor").hide();
					}
					if(data.emptyQuantity) {
						Globals.loadAjaxFlashMessage(data.emptyQuantity, false, false, true);
						$(".loadingColor").hide();
					}
					if(data.redictNewColor) {
						window.location = data.redictNewColor;
					}
					if(data.MySQLError) {
						Globals.loadAjaxFlashMessage(data.MySQLError, false, false, true);
						$(".loadingColor").hide();
					}
					
				},
				cache: false,
		        contentType: false,
		        processData: false
			});					
		});
	}
	
	function emailSettings() {
		$("#emailSettings").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingEmail").show();
    		    },
				success :function(data) {
					if(data.ContactEmail) {
						Globals.loadAjaxFlashMessage(data.ContactEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						 $(".loadingEmail").hide();
					}
					if(data.verifyContactEmail) {
						Globals.loadAjaxFlashMessage(data.verifyContactEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					
					if(data.ReceiptEmail) {
						Globals.loadAjaxFlashMessage(data.ReceiptEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					if(data.verifyReceiptEmail) {
						Globals.loadAjaxFlashMessage(data.verifyReceiptEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					
					if(data.InventoryEmail) {
						Globals.loadAjaxFlashMessage(data.InventoryEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					if(data.verifyInventoryEmail) {
						Globals.loadAjaxFlashMessage(data.verifyInventoryEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					
					if(data.WarrantyEmail) {
						Globals.loadAjaxFlashMessage(data.WarrantyEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					if(data.verifyWarrantyEmail) {
						Globals.loadAjaxFlashMessage(data.verifyWarrantyEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					if(data.PaymentEmail) {
						Globals.loadAjaxFlashMessage(data.PaymentEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					
					if(data.verifyPaymentEmail) {
						Globals.loadAjaxFlashMessage(data.verifyPaymentEmail, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						$("#ajaxImage").delay(1500).fadeOut(3500);
						$(".loadingEmail").hide()
					}
					
				}
			});					
		});
	}
	
	function shippingBoxes() {
		$("#shippingBoxes, .editShippingBox").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			var formID = formURL.split("/");
		  	
		  	var idNumber = formID[formID.length - 1];
		  
			var loadingBox;
			if($(this).hasClass("editShippingBox")) {
				loadingBox = $("#"+ idNumber + ".editBoxLoading");	
			} else {
				loadingBox = $(".loadingBox");
			}
			
			

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            loadingBox.show();
    		    },
				success :function(data) {
					if(data.boxLenthEmpty) {Globals.loadAjaxFlashMessage(data.boxLenthEmpty, false, false, true); loadingBox.hide();}
					if(data.boxLengthNumbers) {Globals.loadAjaxFlashMessage(data.boxLengthNumbers, false, false, true); loadingBox.hide();}
					
					if(data.boxWidthEmpty) {Globals.loadAjaxFlashMessage(data.boxWidthEmpty, false, false, true); loadingBox.hide();}
					if(data.boxWidthNumbers) {Globals.loadAjaxFlashMessage(data.boxWidthNumbers, false, false, true); loadingBox.hide();}
					
					if(data.boxHeightEmpty) {Globals.loadAjaxFlashMessage(data.boxHeightEmpty, false, false, true); loadingBox.hide();}
					if(data.boxHeightNumbers) {Globals.loadAjaxFlashMessage(data.boxHeightNumbers, false, false, true); loadingBox.hide();}
					
					if(data.boxItemsHoldEmpty) {Globals.loadAjaxFlashMessage(data.boxItemsHoldEmpty, false, false, true); loadingBox.hide();}
					if(data.boxItemsHoldNumbers) {Globals.loadAjaxFlashMessage(data.boxItemsHoldNumbers, false, false, true); loadingBox.hide();}
					
					if(data.boxWeightEmpty) {Globals.loadAjaxFlashMessage(data.boxWeightEmpty, false, false, true); loadingBox.hide();}
					if(data.boxWeightNumbers) {Globals.loadAjaxFlashMessage(data.boxWeightNumbers, false, false, true); loadingBox.hide();}
					
					if(data.MaxPackageConstrants) {Globals.loadAjaxFlashMessage(data.MaxPackageConstrants, false, false, true); loadingBox.hide();}
					
					if(data.success) {
						Globals.loadAjaxFlashMessage(data.success, false, false, true); loadingBox.hide();
						$("#" + idNumber + ".dimensionEdit, #" + idNumber + ".specificationEdit, #" + idNumber + ".button").hide();
						$("#" + idNumber + ".dimensionedSave, #" + idNumber + ".specificationSave").show();
			
						$("#" + idNumber + ".whiteButton").html("Edit");
						$("#" + idNumber + ".whiteButton").parent().attr('onclick', 'CMSController.editShippingBox(' + idNumber + ', this)');
						
						$("#" + idNumber + ".dimensionedSave span.length").html(data.newLenth + "''");
						$("#" + idNumber + ".dimensionedSave span.width").html(data.newWidth + "''");
						$("#" + idNumber + ".dimensionedSave span.height").html(data.newHeight + "''");
						
						var itemsHoldText;
						
						if(data.newItems > 1) {
							itemsHoldText = "items";
						} else {
							itemsHoldText = "item";
						}
						
						$("#" + idNumber + ".specificationSave span.itemsHold").html(data.newItems + ' ' + itemsHoldText);
						$("#" + idNumber + ".specificationEdit span.itemsHold").html(itemsHoldText);
						$("#" + idNumber + ".specificationSave span.weight").html(data.newWeight);
						
						$("#" + idNumber + ".specificationSave span.girth").html(data.newPackageGirthValue + "''");
						
					}
					
					if(data.newBox) {window.location = data.newBox}
					
				}
			});					
		});
	}
	
	function purchaseSettings() {
		$("#purchaseSettingForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
					$(".loadingPurchase").show();
		            //loadingBox.show();
    		    },
				success :function(data) {
					if(data.emptyPreSaleText) {Globals.loadAjaxFlashMessage(data.emptyPreSaleText, false, false, true); $(".loadingPurchase").hide();}
					if(data.purchaseSettingsSaved) {Globals.loadAjaxFlashMessage(data.purchaseSettingsSaved, false, false, true); $(".loadingPurchase").hide();}
					
					
					
					if(data.MySqlError) {Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true); $(".loadingPurchase").hide();}
				

					
				}
			});					
		});
	}
	
	
	function TrackingEmailText() {
		$("#TrackingEmailText").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingTracking").show();
    		    },
				success :function(data) {
					if(data.trackingEmailText) {
						Globals.loadAjaxFlashMessage(data.trackingEmailText, false, false, true);
						 $(".loadingTracking").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$(".loadingTracking").hide();
					}
					if(data.trackingEmailTextSaved) {
						Globals.loadAjaxFlashMessage(data.trackingEmailTextSaved, false, false, true);
						$(".loadingTracking").hide();
					}
				}
			});					
		});
	}
	
	function ShippingCompany() {
		$("#ShippingCompany").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingTracking").show();
    		    },
				success :function(data) {
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						 $(".loadingTracking").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$(".loadingTracking").hide();
					}
				}
			});					
		});
	}	
	
	function passwordChange() {
		$("#ChangePassword").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingPasswordForm").show();
    		    },
				success :function(data) {
					if(data.emptyPassword) {
						Globals.loadAjaxFlashMessage(data.emptyPassword, false, false, true);
						$(".loadingPasswordForm").hide();
					}
					if(data.passwordLenth) {
						Globals.loadAjaxFlashMessage(data.passwordLenth, false, false, true);
						$(".loadingPasswordForm").hide();
					}
					if(data.userNewPassword) {
						Globals.loadAjaxFlashMessage(data.userNewPassword, false, false, true);
						$(".loadingPasswordForm").hide();
					}
					if(data.userConfirmPasswordNotMatch) {
						Globals.loadAjaxFlashMessage(data.userConfirmPasswordNotMatch, false, false, true);
						$(".loadingPasswordForm").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$(".loadingPasswordForm").hide();
					}
					
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						$(".message, #ajaxImage").delay(1500).fadeOut(3500);
						$("#newPasswordConfirm, #newPassword").val("");
						$(".loadingPasswordForm").hide();
					}
				}
			});					
		});
	}
	
	function FileUploadPartners() {
		//http://stackoverflow.com/questions/8903854/check-image-width-and-height-on-upload-with-javascript
		var _URL = window.URL || window.webkitURL;
		
		var errorMessage = "<div class='message error'>" +
								"Image needs to be at least 500px wide." +
								"<a href='javascript:void(0);' onclick='Globals.closeFlashMessage()'>" + 
									"<div class='message-close'></div>" + 
								"</a>" + 
							"</div>"
							
		var errorMessageFileExt = "<div class='message error'>" +
								  	  "jpg, jpeg, png, gif file formats are allowed." +
									  "<a href='javascript:void(0);' onclick='Globals.closeFlashMessage()'>" + 
									  	  "<div class='message-close'></div>" + 
									  "</a>" + 
								  "</div>"
		
		$("#image-upload").change(function(e) {
			//validating image width
		    var file, img;
		    if ((file = this.files[0])) {
		        img = new Image();
				img.onload = function () {
					if(this.width < 500) {
						Globals.loadAjaxFlashMessage(errorMessage, "#image-upload");
						//$("#image-upload").val('');
						$("a.clearFileInput").hide();
					} 
				}	
				img.src = _URL.createObjectURL(file);
		
		    }
			//validate file extension
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				Globals.loadAjaxFlashMessage(errorMessageFileExt, "#image-upload")
    			$("a.clearFileInput").hide();
			} 
			
			if($("input[type=file]").val() != "") {
				$("a.clearFileInput").show();
			} 
		});					
	}
	
	function validateNewProductForm(productType) {
		$("#newProductForm").validate({
			messages : {
				productName : {
					required : "Please enter a partner name"
				},
				productPrice : {
					required : "Please enter a price"
				}
			},
			onfocusout : function(element) {
				jQuery(element).valid()
			},
			errorElement : "div",
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			}
		});
	}
	
	function validateProductForm() {
		$("#editProductForm").submit(function(e){
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			var formURL = $(this).attr("action");
			//var postData = $(this).serialize();
			
			var loadingProduct = $('.loadingProduct');
			
			
	
			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: formData,
		        async: true,
		        dataType: 'json',
		        beforeSend: function(){
		            loadingProduct.show();
    		    },
				success :function(data) {
					if(data.uploadImageProblem) {
						Globals.loadAjaxFlashMessage(data.uploadImageProblem, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptyProductName) {
						Globals.loadAjaxFlashMessage(data.emptyProductName, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptyPrice) {
						Globals.loadAjaxFlashMessage(data.emptyPrice, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecOuter) {
						Globals.loadAjaxFlashMessage(data.emptySpecOuter, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecInner) {
						Globals.loadAjaxFlashMessage(data.emptySpecInner, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecStrap) {
						Globals.loadAjaxFlashMessage(data.emptySpecStrap, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecCap) {
						Globals.loadAjaxFlashMessage(data.emptySpecCap, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecGrommet) {
						Globals.loadAjaxFlashMessage(data.emptySpecGrommet, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecWidth) {
						Globals.loadAjaxFlashMessage(data.emptySpecWidth, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecHeight) {
						Globals.loadAjaxFlashMessage(data.emptySpecHeight, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecLength) {
						Globals.loadAjaxFlashMessage(data.emptySpecLength, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecWeight) {
						Globals.loadAjaxFlashMessage(data.emptySpecWeight, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptySpecVolume) {
						Globals.loadAjaxFlashMessage(data.emptySpecVolume, false, false, true);
						loadingProduct.hide();
					}
					if(data.emptyDescription) {
						Globals.loadAjaxFlashMessage(data.emptyDescription, false, false, true);
						loadingProduct.hide();
					}
					if(data.redirect) {
						window.location = data.redirect;
					}
					
				},
				cache: false,
		        contentType: false,
		        processData: false
			});					
		});
	}


	function toggleLocateLife() {
		$("#locateLifeForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingLocateLife").show();
    		    },
				success :function(data) {
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						$(".loadingLocateLife").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$(".loadingLocateLife").hide();
					}
				}
			});					
		});
	}
	
	function newShippingAddress() {
		$("#newShippingAddress").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingShippingForm").show();
    		    },
				success :function(data) {
					if(data.emptyNewAddress) {
						Globals.loadAjaxFlashMessage(data.emptyNewAddress, false, false, true);
						$(".loadingShippingForm").hide();
					}
					if(data.emptyCity) {
						Globals.loadAjaxFlashMessage(data.emptyCity, false, false, true);
						$(".loadingShippingForm").hide();
					}
					if(data.emptyState) {
						Globals.loadAjaxFlashMessage(data.emptyState, false, false, true);
						$(".loadingShippingForm").hide();
					}
					if(data.emptyZip) {
						Globals.loadAjaxFlashMessage(data.emptyZip, false, false, true);
						$(".loadingShippingForm").hide();
					}
					if(data.stripeError) {
						Globals.loadAjaxFlashMessage(data.stripeError, false, false, true);
						$("#currentShipping, .chngShipping, #changeStatusIndicator, .shipButtonContainer").show();
						$(".loadingShippingForm, #editShippingForm").hide();
					}
					if(data.success) {
						Globals.loadAjaxFlashMessage(data.success, false, false, true);
						//client side update customer info
						$("span#changedDate").html(data.changedDate);
						$("span#shippingAddress").html(data.shippingAddress);
						$("span#shippingCity").html(data.shippingCity);
						$("span#shippingState").html(data.shippingState);
						$("span#shippingZip").html(data.shippingZip);
						$("span#newShippingRates").html(data.newShippingRates + " USD");
						$(".loadingShippingForm, #editShippingForm").hide();
						$("#currentShipping, .chngShipping, #changeStatusIndicator, .shipButtonContainer, .changedShippingRate").show();
						//alert(data.updateStripeValue);
						//update value on form
						$(".newAddress").val(data.shippingAddress);
						$(".newCity").val(data.shippingCity);
						$(".newState").val(data.shippingState);
						$(".newZip").val(data.shippingZip);
						
						$("#originalShippingLabel").show();
						//update Map it Button
						$("a#mapItLink").attr("href", data.updateMapItButton);
					}
				}
			});					
		});
	}
	
	function validateUserSinglePage() {
		$("#updateUser").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingUserSettings").show();
    		    },
				success :function(data) {
					if(data.userFirstName) {
						Globals.loadAjaxFlashMessage(data.userFirstName, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.userLastName) {
						Globals.loadAjaxFlashMessage(data.userLastName, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.userNameEmpty) {
						Globals.loadAjaxFlashMessage(data.userNameEmpty, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.duplicateUserName) {
						Globals.loadAjaxFlashMessage(data.duplicateUserName, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.emptyEmail) {
						Globals.loadAjaxFlashMessage(data.emptyEmail, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.wrongEmailFormat) {
						Globals.loadAjaxFlashMessage(data.wrongEmailFormat, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.duplicateEmail) {
						Globals.loadAjaxFlashMessage(data.duplicateEmail, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingUserSettings").hide();
					}
					if(data.redirect) {
						window.location = data.redirect;
					}
					
					if(data.passed) {
						Globals.loadAjaxFlashMessage(data.passed, false, false ,true);
						$(".loadingUserSettings").hide();
						changeUrlParam('firstName', data.firstNameURL);
						changeUrlParam('lastName', data.lastNameURL);
						$("#currentUser, .adminContent .header .text").html(data.firstNameURL + " " + data.lastNameURL);
						$("#userEmail").val(data.userEmail);
					}
					
				}
			});					
		});
	}
	
	function locateLifeForm() {
		$("#LocationslocateLifeForm").submit(function(e){
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			var formURL = $(this).attr("action");
			//var postData = $(this).serialize();

			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: formData,
		        async: true,
		        dataType: 'json',
				beforeSend: function(){
		            $(".loadLocation").show();
    		    },
				success :function(data) {
					if(data.emptyLongitude) {Globals.loadAjaxFlashMessage(data.emptyLongitude, false, false ,true); $(".loadLocation").hide();}
					if(data.longitudeOnlyNumbers) {Globals.loadAjaxFlashMessage(data.longitudeOnlyNumbers, false, false ,true); $(".loadLocation").hide();}
					if(data.emptyLatitude) {Globals.loadAjaxFlashMessage(data.emptyLatitude, false, false ,true); $(".loadLocation").hide();}
					if(data.latitudeOnlyNumbers) {Globals.loadAjaxFlashMessage(data.latitudeOnlyNumbers, false, false ,true); $(".loadLocation").hide();}
					if(data.emptyName) {Globals.loadAjaxFlashMessage(data.emptyName, false, false ,true); $(".loadLocation").hide();}
					
					if(data.emptyLocationPhoto) {Globals.loadAjaxFlashMessage(data.emptyLocationPhoto, false, false ,true); $(".loadLocation").hide();}
					if(data.uploadImageProblem) {Globals.loadAjaxFlashMessage(data.uploadImageProblem, false, false ,true); $(".loadLocation").hide();}
					if(data.extendsFileSize) {Globals.loadAjaxFlashMessage(data.extendsFileSize, false, false ,true); $(".loadLocation").hide();}
					if(data.emptyVideo) {Globals.loadAjaxFlashMessage(data.emptyVideo, false, false ,true); $(".loadLocation").hide();}

					
					
					
					//if(data.redirect) {
					//	Globals.loadAjaxFlashMessage(data.redirect, false, false ,true);
					//	$(".loadLocation").hide();
					//}
					if(data.redirectLink) {
						window.location = data.redirectLink;
					}
					
					if(data.success) {
						Globals.loadAjaxFlashMessage(data.success, false, false ,true);
						$("#locationHeader").html("Longitude: " + data.newLongitude + " / Latitude: " + data.newLatitude); 
					}
					
					
				},
				cache: false,
		        contentType: false,
		        processData: false
			});					
		});
	}

	function companyAddress() {
		$("#CompanyAddress").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingAddress").show();
    		    },
				success :function(data) {
					if(data.companyName) {
						Globals.loadAjaxFlashMessage(data.companyName, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.companyAddress) {
						Globals.loadAjaxFlashMessage(data.companyAddress, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$$(".loadingAddress").hide();
					}
					if(data.companyCity) {
						Globals.loadAjaxFlashMessage(data.companyCity, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.companyState) {
						Globals.loadAjaxFlashMessage(data.companyState, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.companyZip) {
						Globals.loadAjaxFlashMessage(data.companyZip, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
						$(".loadingAddress").hide();
					}
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.areaCodeMissing) {
						Globals.loadAjaxFlashMessage(data.areaCodeMissing, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.emptyPhonePart1) {
						Globals.loadAjaxFlashMessage(data.emptyPhonePart1, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					if(data.emptyPhonePart2) {
						Globals.loadAjaxFlashMessage(data.emptyPhonePart2, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".loadingAddress").hide();
					}
					
					
					
					
				}
			});					
		});
	}
	
	function declineWarranty() {
		$("#declineWarranty").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".warrantyLoad").show();
    		    },
				success :function(data) {
					if(data.emptyDeclineNotes) {
						Globals.loadAjaxFlashMessage(data.emptyDeclineNotes, false, false ,true);
						//$(".message").delay(1500).fadeOut(3500);
						$(".warrantyLoad").hide();
					}
					if(data.redirect) {
						window.location = data.redirect;
					}
				}
			});					
		});
	}
	
	function updateColorQuantity() {
		$(".inventoryForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			var formID = formURL.split("/");

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $("#"+ formID[formID.length - 1] + ".ajaxReloader").show();
    		    },
				success :function(data) {
					if(data.emptyQuantity) {
						Globals.loadAjaxFlashMessage(data.emptyQuantity, false, false ,true);
						$("#"+ formID[formID.length - 1] + ".ajaxReloader").hide();
					}
					if(data.onlyNumbers) {
						Globals.loadAjaxFlashMessage(data.onlyNumbers, false, false ,true);
						$("#"+ formID[formID.length - 1] + ".ajaxReloader").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
						$("#"+ formID[formID.length - 1] + ".ajaxReloader").hide();
					}
					if(data.quantityUpdated) {
						Globals.loadAjaxFlashMessage(data.quantityUpdated, false, false ,true);
						$("#"+ formID[formID.length - 1] + ".ajaxReloader").hide();
						if(data.quantityNumber > 2) {
							$(".OutOfStockMessage .text#" + data.colorID).fadeOut();
						} else {
							$(".OutOfStockMessage .text#" + data.colorID).fadeIn();
						}
						//console.log(formID[formID.length - 1]);
					}
				}
			});					
		});
	}
	
	function teamMemberForm() {
		$("#teamMemberForm").submit(function(e){
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			var formURL = $(this).attr("action");
			//var postData = $(this).serialize();

			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: formData,
		        async: true,
		        dataType: 'json',
		        beforeSend: function(){
		            $(".loadingTeam").show();
    		    },
		        success: function (data) {
		        	if(data.emptyPhoto) {
		        		Globals.loadAjaxFlashMessage(data.emptyPhoto, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.extendsFileSize) {
			        	Globals.loadAjaxFlashMessage(data.extendsFileSize, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.emptyFirstName) {
		        		Globals.loadAjaxFlashMessage(data.emptyFirstName, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.emptyLastName) {
		        		Globals.loadAjaxFlashMessage(data.emptyLastName, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.emptyJobTitle) {
		        		Globals.loadAjaxFlashMessage(data.emptyJobTitle, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.emptyDescription) {
		        		Globals.loadAjaxFlashMessage(data.emptyDescription, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.redirect) {
		        		window.location = data.redirect;
		        	}
		        	if(data.MySqlError) {
		        		Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
		        		$(".loadingTeam").hide();
		        	}
		        	if(data.savedEdits) {
		        		window.location = data.savedEdits;
		        	}
		        },
		        cache: false,
		        contentType: false,
		        processData: false
			});					
		});
	}
	
	function reorderCategoryList() {
		$("#categoryListForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingCategoryList").show();
    		    },
				success :function(data) {
					if(data.categoryListUpdated) {
						Globals.loadAjaxFlashMessage(data.categoryListUpdated, false, false ,true);
						$(".loadingCategoryList").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
						$(".loadingCategoryList").hide();
					}
				}
			});					
		});
	}
	
	function reorderTeamList() {
		$("#teamListForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingTeamList").show();
    		    },
				success :function(data) {
					if(data.teamListUpdated) {
						Globals.loadAjaxFlashMessage(data.teamListUpdated, false, false ,true);
						$(".loadingTeamList").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
						$(".loadingTeamList").hide();
					}
				}
			});					
		});
	}
	
	//http://snippetrepo.com/snippets/change-url-parameter-without-reloading-page
	function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
    }

	function changeUrlParam (param, value) {
        var currentURL = window.location.href+'&';
        var change = new RegExp('('+param+')=(.*)&', 'g');
        var newURL = currentURL.replace(change, '$1='+value+'&');

        if (getURLParameter(param) !== null){
            try {
                window.history.replaceState('', '', newURL.slice(0, - 1) );
            } catch (e) {
                console.log(e);
            }
        } else {
            var currURL = window.location.href;
            if (currURL.indexOf("?") !== -1){
                window.history.replaceState('', '', currentURL.slice(0, - 1) + '&' + param + '=' + value);
            } else {
                window.history.replaceState('', '', currentURL.slice(0, - 1) + '?' + param + '=' + value);
            }
        }
    }
    function colorProductPicker() {
    	$('#colorpicker').farbtastic('#color');
    }
    
    function paymentSettings() {
		$("#paymentNotificationsForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingPayment").show();
    		    },
				success :function(data) {
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false ,true);
						$(".loadingPayment").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false ,true);
						$(".loadingPayment").hide();
					}
				}
			});					
		});
	}
    
    
    function validateFileUpload(ImageUpload) {
    	//http://stackoverflow.com/questions/8903854/check-image-width-and-height-on-upload-with-javascript
			var _URL = window.URL || window.webkitURL;
		
			var errorMessage = "<div class='message error'>" +
									"Image needs to be at least 600px wide." +
								"</div>";
			var errorMessage2 = "<div class='message error'>" +
									"Image needs to be at least 600px high." +
								"</div>";
								
			var errorMessage3 = "<div class='message error'>" +
									"Maximum width allowed is 1000px wide." +
								"</div>";
			var errorMessage4 = "<div class='message error'>" +
									"Maximum height allowed is 1000px wide." +
								"</div>";
							
			var errorMessageFileExt = "<div class='message error'>" +
									  	  "jpg, jpeg, png, gif file formats are allowed." +
									  "</div>";
		
			$(ImageUpload).change(function(e) {
				//validating image width
			    var file, img;
			    if ((file = this.files[0])) {
			        img = new Image();
					img.onload = function () {
						if(this.width < 600) {
							Globals.loadAjaxFlashMessage(errorMessage, ImageUpload, false, true);
						} 
						if(this.width > 1000) {
							Globals.loadAjaxFlashMessage(errorMessage3, ImageUpload, false, true);
						} 
						
						if(this.height < 600) {
							Globals.loadAjaxFlashMessage(errorMessage2, ImageUpload, false, true);
						}
						if(this.height > 1000) {
							Globals.loadAjaxFlashMessage(errorMessage4, ImageUpload, false, true);
						}
						
					}	
					img.src = _URL.createObjectURL(file);
		
			    }
				//validate file extension
				var ext = $(this).val().split('.').pop().toLowerCase();
				if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					Globals.loadAjaxFlashMessage(errorMessageFileExt, ImageUpload, false, true);
				} 
				
				if($("input[type=file]").val() != "") {
					$("a.clearFileInput").show();
				} 
			});
    }

	var _slideShowArray;
	var i;
	var Photos = [];

	return {
		
		openNadaDropDown: function() {
			$("#purchaseDropDown").hide();
			$("#nadaOrdinaryDropDown").toggle();
			$("#contactDropDown").hide();
			$("#homePageDropDown").hide();
			$("#emailDropDown").hide();
		},
		openPurchaseDropDown: function() {
			$("#purchaseDropDown").toggle();
			$("#nadaOrdinaryDropDown").hide();
			$("#contactDropDown").hide();
			$("#homePageDropDown").hide();
			$("#emailDropDown").hide();
		},
		openContactDropDown: function() {
			$("#contactDropDown").toggle();
			$("#nadaOrdinaryDropDown").hide();
			$("#purchaseDropDown").hide();
			$("#homePageDropDown").hide();
			$("#emailDropDown").hide();
		},
		openEmailDropDown: function() {
			$("#emailDropDown").toggle();
			$("#contactDropDown").hide();
			$("#nadaOrdinaryDropDown").hide();
			$("#purchaseDropDown").hide();
			$("#homePageDropDown").hide();
		},
		PartnerSingle: function() {
			FileUploadPartners();
			validateEditPartnerForm()
		},
		Settings: function() {
			emailSettings();
			toggleLocateLife();
			companyAddress();
			TrackingEmailText();
			paymentSettings();
			ShippingCompany();
			shippingBoxes();
			purchaseSettings();
			//editShippingBoxes();
		},
		
		validateNewProductForm: function() {
			validateNewProductForm();
			validateFileUpload("#image-upload");
		},
		validateProductForm: function() {
			validateProductForm();
			validateFileUpload("#image-upload");
		},
		colorList: function() {
			colorProductPicker();
			AddColorForm();	
			validateFileUpload("#color-upload");		
		},
		validateLocateLifeForm: function() {
			locateLifeForm();
		},
		openEditShippingForm: function() {
			$("#editShippingForm").show();
			$("#currentShipping, .chngShipping, .shipButtonContainer").hide();
		},
		closeEditShippingForm: function() {
			$("#editShippingForm").hide();
			$("#currentShipping, .chngShipping, .shipButtonContainer").show();
		},
		singleOrder: function() {
			newShippingAddress();
		},
		openMobileTopMenu: function() {
			$(".desktopLink").toggle();
			$(".desktopSubMenu").hide();
		},
		openSubMobileMenu: function() {
			$(".desktopSubMenu").toggle();
			$(".desktopLink").hide();
		},
		openPopup: function(url) {
			popupWindow = window.open(url,'receipt','height=600,width=1000,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
		},
		expandEmailSettings: function() {
			$("#locateLifeInfo, #TrackingEmailContent, #pickupShippingAddress, #paymentNotifications, #purchaseSettings").hide();
			$("#emailSettingsContent").toggle();
			$(".toggleIndicatorLogin, .togglePurchase, .toggleIndicatorLocateLife, .toggleIndicatorTrackingEmail, .togglePaymentNotifications").html('+');
			$(".toggleIndicatorEmailSettings").toggleText('-', '+');
		},
		expandLocateLife: function() {
			$("#emailSettingsContent, #TrackingEmailContent, #pickupShippingAddress, #paymentNotifications, #purchaseSettings").hide();
			$(".toggleIndicatorLogin, .togglePurchase, .toggleIndicatorTrackingEmail, .toggleIndicatorEmailSettings, .togglePaymentNotifications").html('+');
			$("#locateLifeInfo").toggle();
			
			$(".toggleIndicatorLocateLife").toggleText('-', '+');
		},
		expandTrackingEmailText: function() {
			$("#emailSettingsContent, #locateLifeInfo, #pickupShippingAddress, #paymentNotifications, #purchaseSettings").hide();
			$("#TrackingEmailContent").toggle();
			
			$(".toggleIndicatorLogin, .togglePurchase, .toggleIndicatorLocateLife, .toggleIndicatorShippingAddress, .togglePaymentNotifications").html('+');
			$(".toggleIndicatorTrackingEmail").toggleText('-', '+');
		},
		expandShippingAddress: function() {
			$("#shippingCompany, #shippingBoxesContent").hide();
			$("#pickupShippingAddress").toggle();
			
			$(".toggleShippingCompany, .toggleShippingBoxes").html('+');
			$(".toggleIndicatorShippingAddress").toggleText('-', '+');
		},
		expandPaymentNotifications: function() {
			$("#emailSettingsContent, #locateLifeInfo, #TrackingEmailContent, #pickupShippingAddress, #purchaseSettings").hide();
			$(".toggleIndicatorLogin, .toggleIndicatorLocateLife, .toggleIndicatorShippingAddress, .toggleIndicatorEmailSettings, .togglePurchase").html('+');
			
			
			$("#paymentNotifications").toggle();
			$(".togglePaymentNotifications").toggleText('-', '+');
			
		},
		expandShippingCompany: function() {
			$("#pickupShippingAddress, #shippingBoxesContent").hide();
			$(".toggleIndicatorShippingAddress, .toggleShippingBoxes").html('+');
			$(".toggleShippingCompany").toggleText('-', '+');
			$("#shippingCompany").toggle();
		},
		expandShippingBoxes: function() {
			$("#pickupShippingAddress, #shippingCompany").hide();
			$(".toggleIndicatorShippingAddress, .toggleIndicatorShippingAddress, .toggleShippingCompany").html('+');
			$(".toggleShippingBoxes").toggleText('-', '+');
			$("#shippingBoxesContent").toggle();
		},
		expandPurchase: function() {
			$("#emailSettingsContent, #locateLifeInfo, #TrackingEmailContent, #paymentNotifications").hide();
			$("#purchaseSettings").toggle();
			$(".togglePurchase").toggleText('-', '+');
			
			$(".toggleIndicatorLogin, .toggleIndicatorLocateLife, .toggleIndicatorTrackingEmail, .toggleIndicatorEmailSettings, .togglePaymentNotifications").html('+');
		},
		
		userSinglePage: function() {
			validateUserSinglePage();
			passwordChange();
		},
		declineWarranty: function() {
			$(".backgroundOverlay, .warrantyDeclinePopup").fadeIn("fast");
		},
		closePopup: function(element) {
			$(element).fadeOut("fast");
		},
		warrantySinglePage: function() {
			declineWarranty();
		},		
		inventory: function() {
			updateColorQuantity();
		},
		Categories: function() {
			categoryForm();
		},	
		teamList: function() {
			reorderTeamList();
		},	
		CategoriesList: function() {
			reorderCategoryList();
		},
		teamSinglePage: function() {
			teamMemberForm();
		},
		warrantyPhotoPage: function() {
			
			var currentIndex = 0,
  			items = $('.photo'),
  			itemAmt = items.length;

			function cycleItems() {
			  var item = $('.photo').eq(currentIndex);
			  items.hide();
			  item.css('display','block');
			}
	
			var autoSlide = setInterval(function() {
			  currentIndex += 1;
			  if (currentIndex > itemAmt - 1) {
			    currentIndex = 0;
			  }
			  cycleItems();
			}, 3000);
	
			$('.next').click(function() {
			  clearInterval(autoSlide);
			  currentIndex += 1;
			  if (currentIndex > itemAmt - 1) {
			    currentIndex = 0;
			  }
			  cycleItems();
			});
	
			$('.prev').click(function() {
			  clearInterval(autoSlide);
			  currentIndex -= 1;
			  if (currentIndex < 0) {
			    currentIndex = itemAmt - 1;
			  }
			  cycleItems();
			});
		},
		toggleShippingMethods: function(element) {
			var methodID = $(element).data('methodid');
			//alert(methodID);
			
			if($(element).prev().val() == 1 + ", " + methodID) {
				$(element).prev().val(0 + ", " + methodID)
			} else if($(element).prev().val() == 0 + ", " + methodID) {
				$(element).prev().val(1 + ", " + methodID)
			}
		},
		togglePreSale: function(element) {
			if($(element).prev().val() == 1) {
				$(element).prev().val(0)
			} else if($(element).prev().val() == 0) {
				$(element).prev().val(1)
			}
		},
		editShippingBox: function(id, element) {
			
			$("#" + id + ".dimensionEdit").show();
			$("#" + id + ".specificationEdit").show();
			$("#" + id + ".dimensionedSave").hide();
			$("#" + id + ".specificationSave").hide();
			 
			$("#" + id + ".button").show(); 
			$("#" + id + ".whiteButton").html("Cancel");
			$(element).attr('onclick', 'CMSController.cancelShippingBoxEdit(' + id + ', this)');
		},
		cancelShippingBoxEdit: function(id, element) {
			$("#" + id + ".dimensionEdit").hide();
			$("#" + id + ".specificationEdit").hide();
			$("#" + id + ".dimensionedSave").show();
			$("#" + id + ".specificationSave").show();
			$("#" + id + ".button").hide(); 
			
			$("#" + id + ".whiteButton").html("Edit");
			$(element).attr('onclick', 'CMSController.editShippingBox(' + id + ', this)');
		},
		openShippingLabelDropDown: function() {
			$(".printLabelsSection").toggle();
			$(".trackPackagesSection").hide();
			$(".blueButton.shipmentButton").toggleClass('labelsExpanded');
			$(".whiteButton.shipmentButton").removeClass('trackingExpanded');
		},
		openTrackingPackages: function() {
			$(".printLabelsSection").hide();
			$(".trackPackagesSection").toggle();
			$(".blueButton.shipmentButton").removeClass('labelsExpanded');
			$(".whiteButton.shipmentButton").toggleClass('trackingExpanded');
		}
	}
	
}();