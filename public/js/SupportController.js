var SupportController = function() {
	
	function hideRemovePhotoLink(ImageUpload) {
		$(ImageUpload).val('');
		$(".removePhoto").hide();
	}
	
	function validateFileUpload(ImageUpload) {
    	//http://stackoverflow.com/questions/8903854/check-image-width-and-height-on-upload-with-javascript
			var _URL = window.URL || window.webkitURL;
		
			var errorMessage = "<div class='message error'>" +
									"Minimum image width allowed is 1000px wide." +
								"</div>"
			var errorMessage2 = "<div class='message error'>" +
									"Minimum image height allowed is 1000px high." +
								"</div>"
							
			var errorMessageFileExt = "<div class='message error'>" +
									  	  "jpg, jpeg, png, gif file formats are allowed." +
									  "</div>"
		
			$(ImageUpload).change(function(e) {
				//validating image width
			    var file, img;
			    if ((file = this.files[0])) {
			        img = new Image();
					img.onload = function () {
						if(this.width > 1000) {
							Globals.loadAjaxFlashMessage(errorMessage, false, false, true, "#warrantyPhotoError");
							hideRemovePhotoLink(ImageUpload);
						} 
						
						if(this.height > 1000) {
							Globals.loadAjaxFlashMessage(errorMessage2, false, false, true, "#warrantyPhotoError");
							hideRemovePhotoLink(ImageUpload);
						}
						
					}	
					img.src = _URL.createObjectURL(file);
		
			    }
				//validate file extension
				var ext = $(this).val().split('.').pop().toLowerCase();
				if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					Globals.loadAjaxFlashMessage(errorMessageFileExt, false, false, true, "#warrantyPhotoError");
					hideRemovePhotoLink(ImageUpload);
				} 
				
				if($("input[type=file]").val() != "") {
					$("a.clearFileInput").show();
				} 
			});
    }
	
	
	
	function ContactFormValidation() {
		$(".contactFormElement").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $("#ajaxImage").show();
    		    },
				success :function(data) {
					if(data.firstName) {
						Globals.loadAjaxFlashMessage(data.firstName, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "36px");
					}
					if(data.lastName) {
						Globals.loadAjaxFlashMessage(data.lastName, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "36px");
					}
					if(data.emptyEmail) {
						Globals.loadAjaxFlashMessage(data.emptyEmail, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "36px");
					}
					if(data.verifyEmail) {
						Globals.loadAjaxFlashMessage(data.verifyEmail, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "36px");
					}
					if(data.emptyComments) {
						Globals.loadAjaxFlashMessage(data.emptyComments, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "36px");
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "auto");
					}
					
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						$("input:text, textarea").val('');
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "auto");
					}
				}
			});					
		});
	}
	
	function scrollToTop(element) {
		$('html, body').animate({
	        scrollTop: $(element).offset().top - 200
	    },1000);
	    $(".ajaxLoadingIndicator").hide();
	}
	function WarrantyForm() {
		$("#WarrantyForm").submit(function(e){
			e.preventDefault();
			
			var formData = new FormData($(this)[0]);
			
			var formURL = $(this).attr("action");
			//var postData = $(this).serialize();

			$.ajax({
				url: formURL,
		        type: 'POST',
		        data: formData,
		        async: true,
		        dataType: 'json',
		        beforeSend: function(){
		            $(".ajaxLoadingIndicator").show();
		            $(".warrantyBtn").attr("disabled", "disabled");
    		    },
		        success: function (data) {
		        	if(data.emptyPhotos) {
		        		Globals.loadAjaxFlashMessage(data.emptyPhotos, false, false, true, "#warrantyPhotoError");
		        		scrollToTop("#warrantyPhotoError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.photoUploadError) {
		        		Globals.loadAjaxFlashMessage(data.photoUploadError, false, false, true, "#warrantyPhotoError");
		        		scrollToTop("#warrantyPhotoError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.warrantyDescription) {
		        		Globals.loadAjaxFlashMessage(data.warrantyDescription, false, false, true, "#descriptionError");
		        		scrollToTop("#descriptionError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.orderNumber) {
		        		Globals.loadAjaxFlashMessage(data.orderNumber, false, false, true, "#OrderIDError");
		        		scrollToTop("#OrderIDError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.orderNumberVerification) {
		        		Globals.loadAjaxFlashMessage(data.orderNumberVerification, false, false, true, "#OrderIDError");
		        		scrollToTop("#OrderIDError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.verificationNumberNotMatching) {
		        		Globals.loadAjaxFlashMessage(data.verificationNumberNotMatching, false, false, true, "#OrderIDError");
		        		scrollToTop("#OrderIDError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}		     
		        	if(data.orderNumberNotExist) {
		        		Globals.loadAjaxFlashMessage(data.orderNumberNotExist, false, false, true, "#OrderIDError");
		        		scrollToTop("#OrderIDError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.firstName) {
		        		Globals.loadAjaxFlashMessage(data.firstName, false, false, true, "#contactInfo");
		        		scrollToTop("#contactInfo");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.lastName) {
		        		Globals.loadAjaxFlashMessage(data.lastName, false, false, true, "#contactInfo");
		        		scrollToTop("#contactInfo");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.Email) {
		        		Globals.loadAjaxFlashMessage(data.Email, false, false, true, "#contactInfo");
		        		scrollToTop("#contactInfo");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.correctEmailFormat) {
		        		Globals.loadAjaxFlashMessage(data.correctEmailFormat, false, false, true, "#contactInfo");
		        		scrollToTop("#contactInfo");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.emptyPhone) {
		        		Globals.loadAjaxFlashMessage(data.emptyPhone, false, false, true, "#contactInfo");
		        		scrollToTop("#contactInfo");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.emptyAddress) {
		        		Globals.loadAjaxFlashMessage(data.emptyAddress, false, false, true, "#ShippingError");
		        		scrollToTop("#ShippingError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.emptyCity) {
		        		Globals.loadAjaxFlashMessage(data.emptyCity, false, false, true, "#ShippingError");
		        		scrollToTop("#ShippingError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.emptyState) {
		        		Globals.loadAjaxFlashMessage(data.emptyState, false, false, true, "#ShippingError");
		        		scrollToTop("#ShippingError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.emptyZip) {
		        		Globals.loadAjaxFlashMessage(data.emptyZip, false, false, true, "#ShippingError");
		        		scrollToTop("#ShippingError");
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.MySqlError) {
		        		$(".mysqlerrorPopup").hide().fadeIn('fast').delay(2000).fadeOut('slow');
		        		$(".ajaxLoadingIndicator").hide();
		        		$(".warrantyBtn").removeAttr("disabled");
		        	}
		        	if(data.redirect) {
		        		$(".ajaxLoadingIndicator").hide();
		        		window.location = data.redirect;
		        	}
		        	//
		        	
		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown) {
				     console.log(XMLHttpRequest);
				     console.log(textStatus);
				     console.log(errorThrown);
				},
		        cache: false,
		        contentType: false,
		        processData: false
			});					
		});
		
		

	}
	
	return {
		startContactPage: function() {
			ContactFormValidation();
		},
		startWarrantyPage: function() {
			validateFileUpload("#warrantyUpload1");
			validateFileUpload("#warrantyUpload2");
			validateFileUpload("#warrantyUpload3");
			WarrantyForm();
		},
		showRemoveButton: function(element) {
			$(element).parent().next().find('.removePhoto').show();
		},
		removeSelectedPhoto: function(element) {
			$(element).prev().find('input').val('');
			$(element).find('.removePhoto').hide()
		}
	}
}();