var CartController = function() {

    function validateCardNumber(){
        var retVal = false;
		   	
		// Feel free too apply classes or something to make this look how you like
		if(!Stripe.card.validateCardNumber($('.creditCardNumber').val())){
        	if(document.getElementById("paymentCreditCardNumberInvalid") == null) {
				$("<div class='errorMessageText' id='paymentCreditCardNumberInvalid'>Credit Card Number is invalid</div>").insertBefore(".creditCardNumber");
			}
            $('.creditCardNumber').css('border-color', '#c30000');
        }else{
            $('.creditCardNumber').css('border-color', 'green');
            $("#paymentCreditCardNumberInvalid").remove();
            retVal = true;
        }
		
        // Get Card Type From Number
        var cardType = Stripe.card.cardType($('.creditCardNumber').val());
        if(cardType !== 'Unknown'){
            // ADAM: Here you can do a few things. My suggestion is to make a class with every card type
            // I'm just going to set the text to "Visa" or "Mastercard"
            // If you used a class, and had an image tied to that, it would look like
            // $('.creditCardImage').class(cardType) and the class of Visa would be added
            $("#cardTypeIconHolder").addClass(cardType.replace(/\s/g, ''));
            $('#cardType').text(cardType);
        }else{
            // No card type known. Just remove the text or style
            $("#cardTypeIconHolder").removeClass(cardType);
            $('#cardType').text('');
        }
        return retVal;
    }

    function validateCVC(){
        if(!Stripe.card.validateCVC($('.CVC').val())){
            $('.CVC').css('border-color', '#c30000');
            return false;
        }else{
            $('.CVC').css('border-color', 'green');
            return true;
        }
    }

    function validateExpiration(){
        var month = $('.expirationMonth').val();
        var year = $('.expirationYear').val();
        if (month === ''){
            $('.expirationMonth').css('border-color', '#c30000');
            return false;
        }else {
            if (year === '') {
                $('.expirationYear').css('border-color', '#c30000');
                return false;
            } else if (year === new Date().getFullYear().toString()) {
                if (parseInt(month) < new Date().getMonth() + 1){ // We do the +1 here because months are zero-indexed
                    // ADAM: Here you should change this to show a message inline probably
                    // I just added this for your testing purposes. If I were good at design I'd do it
                    // But I'm not
                    alert('Please Enter a Future Month');
                    return false;
                }
            }
            else{
                $('.expirationMonth').css('border-color', 'green');
                $('.expirationYear').css('border-color', 'green');
                return true;
            }
        }
    }
	
	function stripeResponseHandler(status, response) {
	  var $form = $('#billingForm');


		
		
	  if (response.error) {
	  	alert(response.error.message);

	    // Show the errors on the form
	    //$form.find('.payment-errors').text(response.error.message);
	    console.log(response.error.message);
	    $form.find('input[type="submit"]').prop('disabled', false);
	  } else {
	  	$(".processingOverlay, .ProcessingPopup").show();
	    // response contains id and card, which contains additional card details
	    var token = response.id;
	    // Insert the token into the form so it gets submitted to the server
	    var stripeHiddenTokenInput = $('<input type="hidden" name="stripeToken" />').val(token)
	    
	    
	    
	    $form.append(stripeHiddenTokenInput);
	    
	    // and submit
	   $form.get(0).submit();
	  
	  }
	}
	
	function FillValidateAddressForm(shippingElement, ValidateElement) {
		$(shippingElement).change(function(){
        	$(ValidateElement).val($(this).val());
        });
	}
	
	function BillingPage() {
        // Setup Card Number validation on text change
        $('.creditCardNumber').keyup(function(){
             validateCardNumber();
        });

        $('.CVC').keyup(function(){
           validateCVC();
        });

        $('.expirationMonth, .expirationYear').change(function(){
            validateExpiration()
        });
        FillBillingAddressInfo ('#shippingAddress', "#billingAddress");
        FillBillingAddressInfo ('#shippingCity', "#billingCity");
        FillBillingAddressInfo ('#shippingState', "#billingState");
        FillBillingAddressInfo ('#shippingZip', "#billingZip");
		
		FillBillingAddressInfo ('#shippingCountry', "#billingCountry");
		//this is for auto loading shipping rates
		FillBillingAddressInfo ('#shippingState', "#shippingStateHidden");

	}
	
	function FillBillingAddressInfo (shipping, billing, country = false) {
		$(shipping).change(function(){
			
        	$(billing).val($(this).val());
        });
	}

	function ShippingForm() {
		$(".postalCode").on("change", function(){
			$("#hiddenBillingZip").val($(this).val());
		});
		$("#shippingMethod").on("change", function() {
			$("#hiddenShippingService").val($(this).val());
			$("#hiddenShippingMethodValue").val($(this).find("option:selected").html());
		});
		
		$("#shippingCountry").on("change", function() {
			$("#hiddenCountryName").val($(this).find("option:selected").html());
		});
		
		$("#shippingState").on("change", function() {
			$("#hiddenStateName").val($(this).find("option:selected").html());
		});
		
		
		
	}

	function removeValidationError (inputElement, message) {
		$(inputElement).on("change", function() {
			$(message).remove();
			$(inputElement).css('border-color', '');
		})
		
	}
	
	function ValidateInput(element, id, ErrorMessage, InputElement, placement) {
		if(element === "") {
		   	if(document.getElementById(id) == null) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
		   	$(InputElement).css('border-color', "#c30000");
            return false;
		}
		else {
			$("#" + id).remove();
			$(InputElement).css('border-color', "");
            return true;
		}
	}
	
	function ValidateDropDown(element, id, ErrorMessage, InputElement, placement) {
		if(element === "" || element === null) {
		   	if(document.getElementById(id) == null || document.getElementById(id) == -1) {
		   		$("<div class='errorMessageText' id='" + id + "'>" + ErrorMessage + "</div>").insertAfter(placement);
		   	}
		   	$(InputElement).css('border-color', "#c30000");
            return false;
		}
		else {
			$("#" + id).remove();
			$(InputElement).css('border-color', "");
            return true;
		}
	}
	

	
	function ValidateEmail() {
		var ShippingEmail = $("#shippingEmail").val();
		var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!emailFilter.test(ShippingEmail)) {
			if(document.getElementById("emailNotValid") == null) {
				$("<div class='errorMessageText' id='emailNotValid'>Email is not valid</div>").insertAfter("#shippingEmail");
			}
			$('#shippingEmail').css('border-color', "#c30000");
			return false
		}
		else {
			$("#emailNotValid").remove();
			$('#shippingEmail').css('border-color', "");
			return true;
		}
		
	}
	
	//http://stackoverflow.com/questions/5680050/po-box-regular-expression-validation
	function ValidateAddress() {
		var POBoxCheck = /\bP(ost|ostal)?([ \.]*(O|0)(ffice)?)?([ \.]*Box)?\b/i;
		if ($("#shippingAddress").val().match(POBoxCheck)) {
			if(document.getElementById("noPOBoxAddress") == null) {
				$("<div class='errorMessageText' id='noPOBoxAddress'>We are unable to ship to a Post Office Box.\nPlease provide a different shipping address</div>").insertAfter("#shippingAddress");
			}
			$('#shippingAddress').css('border-color', "#c30000");
			return false
      } else {
      		$("#noPOBoxAddress").remove();
			$('#sshippingAddress').css('border-color', "");
			return true;
      }
		
	}
	
	function ValidateForm() {
		var isValid = true;
		//billing method validation
		isValid = ValidateInput($('.CVC').val(), "paymentCVCRequired", "CVC is required", ".CVC", ".CVC") && isValid;
        isValid = ValidateDropDown($(".expirationYear").val(), "paymentExpirationYearRequired", "Expiration Year is required", ".expirationYear", ".expirationYear") && isValid;
        isValid = ValidateDropDown($(".expirationMonth").val(), "paymentExpirationMonthRequired", "Expiration Month is required", ".expirationMonth", ".expirationMonth") && isValid;
        isValid = ValidateInput($(".creditCardNumber").val(), "paymentCreditCardNumberRequired", "Credit Card Number is required", ".creditCardNumber", ".creditCardNumber") && isValid;
        isValid = ValidateInput($(".creditCardName").val(), "paymentCardHolderNameRequired", "Credit Card Name is required", ".creditCardName", ".creditCardName") && isValid;
        isValid = ValidateInput($("#billingZip").val(), "paymentZipRequired", "Zip is required", "#billingZip", "#billingZip") && isValid;
        isValid = ValidateDropDown($("#billingState").val(), "paymentStateRequired", "State is required", "#billingState", "#billingState") && isValid;
        isValid = ValidateInput($("#billingCity").val(), "paymentCityRequired", "City is required", "#billingCity", "#billingCity") && isValid;
        isValid = ValidateInput($("#billingAddress").val(), "paymentAddressRequired", "Address is required", "#billingAddress", "#billingAddress") && isValid;
		isValid = ValidateDropDown($("#billingCountry").val(), "paymentCountryRequired", "Country is required", "#billingCountry", "#billingCountry") && isValid;
		
		 //shipping method validation
        isValid = ValidateDropDown($("#shippingMethod").val(), "shippingMethodRequired", "Shipping Method is required", "#shippingMethod", "#shippingMethod") && isValid;
        isValid = ValidateInput($("#shippingZip").val(), "zipRequired", "Zip is required", "#shippingZip", "#shippingZip") && isValid;
        isValid = ValidateDropDown($("#shippingState").val(), "stateRequired", "State is required", "#shippingState", "#shippingState") && isValid;
        isValid = ValidateDropDown($("#shippingCountry").val(), "countryRequired", "Country is required", "#shippingCountry", "#shippingCountry") && isValid;
        isValid = ValidateInput($("#shippingCity").val(), "cityRequired", "City is required", "#shippingCity", "#shippingCity") && isValid;
        isValid = ValidateInput($("#shippingAddress").val(), "addressRequired", "Address is required", "#shippingAddress", "#shippingAddress") && isValid;
        isValid = ValidateInput($("#shippingEmail").val(), "addressRequired", "Email is required", "#shippingEmail", "#shippingEmail") && isValid;
        isValid = ValidateEmail() && isValid;
        isValid = ValidateAddress() && isValid;
        isValid = ValidateInput($("#telephoneNumber").val(), "telelphoneNumberRequired", "Contact Number is required", "#telephoneNumber", "#telephoneNumber") && isValid;
        isValid = ValidateInput($("#shippingLastName").val(), "lastNameRequired", "Last name is required", "#shippingLastName", "#shippingLastName") && isValid;
        isValid = ValidateInput($("#shippingFirstName").val(), "firstNameRequired", "First name is required", "#shippingFirstName", "#shippingFirstName") && isValid;
        return isValid;
		
	}

	function StripeToken() {
		  $('#billingForm').submit(function(event) {
    			var $form = $(this);

				
    			// Disable the submit button to prevent repeated clicks
    			$form.find('button').prop('disabled', true);
    			
    			//stripe value checking
				var cardHolderName = $('.creditCardName').val();
				var address = $('#billingAddress').val();
				var city = $('#billingCity').val();
				var providence = $('#billingState').val();
				var postalCode = $('#billingZip').val();

				var creditCardNumber = $('.creditCardNumber').val();
				var creditCardCVC = $('.CVC').val();
				var creditCardExpirationMonth = $('.expirationMonth').val();
				var creditCardExpirationYear = $('.expirationYear').val();
				var shippingCountry = $('#billingCountry').find("option:selected").html();
    			    			
    			if (ValidateForm() && validateCardNumber() && validateCVC() && validateExpiration()) {
							
                	Stripe.card.createToken({
                    	name: cardHolderName,
                        address_line1: address,
                        address_city: city,
                        address_state: providence,
                        address_zip: postalCode,
                        number: creditCardNumber,
                        cvc: creditCardCVC,
                        exp_year: creditCardExpirationYear,
                        exp_month: creditCardExpirationMonth,
                        address_country: shippingCountry
                   	}, stripeResponseHandler);
                } else {
                	// You can change this too
                    alert('Please fix the issues displayed on the screen');
                }
    			// Prevent the form from submitting with the default action
    			return false;
  			});
		
		
						
	}	
	
	function RemoveValidationErrorMessages() {
	
		removeValidationError('#billingAddress', '#paymentAddressRequired');
		removeValidationError('#billingCity', '#paymentCityRequired');
		removeValidationError('#billingState', '#paymentStateRequired');
		removeValidationError('#billingZip', '#paymentZipRequired');
		removeValidationError('.creditCardName', '#paymentCardHolderNameRequired');
		removeValidationError('.creditCardNumber', '#paymentCreditCardNumberRequired');
		removeValidationError('.expirationMonth', '#paymentExpirationMonthRequired');
		removeValidationError('.expirationYear', '#paymentExpirationYearRequired');
		removeValidationError('.CVC', '#paymentCVCRequired');
		removeValidationError('#shippingMethod', '#shippingMethodRequired');
		removeValidationError('#shippingCountry', '#countryRequired');
		
		removeValidationError('#billingCountry', '#paymentCountryRequired');
		removeValidationError('#shippingFirstName', '#firstNameRequired');
		removeValidationError('#shippingLastName', '#lastNameRequired');
		removeValidationError('#shippingEmail', '#emailRequired');
		removeValidationError('#shippingAddress', '#addressRequired');
		removeValidationError('#shippingCity', '#cityRequired');
		removeValidationError('#shippingState', '#stateRequired');
		removeValidationError('#shippingZip', '#zipRequired');
		removeValidationError('#telephoneNumber', '#telelphoneNumberRequired');
	}

	function isInputNotEmpty(input) {
		if (input.val() == "") {
			return false;
		}
	
	}




	return {
		StartDocument: function() {

		},	
		PaymentPage: function() {
  			StripeToken();
  			$(".CardError").delay(1500).fadeOut(3500)
  			ShippingForm();
  			BillingPage();
  			RemoveValidationErrorMessages();
  			
  			
  			
  			
  			//combination of these two links 
  			//http://jsfiddle.net/7huEr/14/ - http://stackoverflow.com/questions/4220126/run-javascript-function-when-user-finishes-typing-instead-of-on-key-up
  			
  			var typingTimer;                //timer identifier
			var doneTypingInterval = 500;  
			var inputNames = "#shippingFirstName, #shippingLastName, #shippingAddress, #shippingCity, #shippingStateHidden, #shippingZip"
	
			
			$(inputNames).bind('keyup change', function() {
				var $emptyFields = $(inputNames).filter(function() {
			    	return $.trim(this.value) === "";
			    });

                $hasEmptyFields = $emptyFields.length > 0;
			    if (!$hasEmptyFields) {
			    	$(".shippingRateButton").removeAttr("disabled");
			        
			        $(inputNames).bind('keyup change', function () {							
						clearTimeout(typingTimer);
						typingTimer = setTimeout(doneTyping, doneTypingInterval);
					});
						
						//on keydown, clear the countdown 
					$(inputNames).bind('keydown change', function () {
						clearTimeout(typingTimer);
					});
				}
			});
	
			//user is "finished typing," do something
			function doneTyping() {
                if($('#shippingZip').val().length == 5) {
                    CartController.LoadShippingOptions();
                }
			}
  			
		},
		Processing: function() {
			$(".processingOverlay, .ProcessingPopup").show();	
		},

        LoadShippingOptions: function(){
        	var inputNames = "#shippingFirstName, #shippingLastName, #shippingAddress, #shippingCity, #shippingStateHidden, #shippingZip"
        	var $emptyFields = $(inputNames).filter(function() {
			   	return $.trim(this.value) === "";
			});

            if($('#shippingZip').val().length < 5){
                alert('Zipcode must be 5 digits');
            }else if ($emptyFields.length) {
			    alert("Please fil in your shipping information in order to generate shipping rates")	
			} else {
        	
        	$.ajax({
                type: 'POST',
                url: $("#shippingMethod").data('path') + 'cart/getShippingRates',
                data: JSON.stringify({
                    firstName: $('#shippingFirstName').val(),
                    lastName: $('#shippingLastName').val(),
                    address: $('#shippingAddress').val(),
                    city: $('#shippingCity').val(),
                    state: $('#shippingState').val(),
                    zip: $('#shippingZip').val(),
                    phone: $('#telephoneNumber').val()
                }),
                dataType: 'json',
                cache: false,
                beforeSend: function(){
                	$('#shippingMethod').removeAttr('style');
                	$("option", "#shippingMethod").each(function() {
	                        $(this).remove();
	                    });
                	$('#shippingMethod').append($('<option>', {
	                        value: "",
	                        text: "Loading Shipping Rates"
	                    }));
				$(".loadingShippingRates img").show();
				$(".shippingRateButton").hide();
				$('#shippingState').attr('onchange', "CartController.LoadShippingOptions();");
				var cartTotal  = $("#cartTotalElement").html(); 
        		var grandTotalText = (+cartTotal);
        		$('#shippingRate').hide().html(Globals.MoneyFormat(+'0.00')).fadeIn('fast');
        		
        	
        		
        		$('#grandTotalValue').hide().html(Globals.MoneyFormat(grandTotalText)).fadeIn('fast');		
					
					
                },
                success :function(data) {
                	$(".shippingRateButton").show();
                	$(".loadingShippingRates img").hide();
                	if(data.ShippingRateError) {
                		$("option", "#shippingMethod").each(function() {
	                        $(this).remove();
	                    });
	
						$('#shippingMethod').css({'background':'#BE5151', "color":"white"});
	                    $('#shippingMethod').append($('<option>', {
	                        value: "",
	                        text: data.ShippingRateError
	                    }));
                		
                    } else {
                    	$('#shippingMethod').removeAttr('style');
                    	//$(".loadingShippingRates").hide();
	                    $("option", "#shippingMethod").each(function() {
	                        $(this).remove();
	                    });
	
	                    $('#shippingMethod').append($('<option>', {
	                        value: "",
	                        text: "Shipping Method"
	                    }));
	
	                    data.forEach(function(d){
	                        $('#shippingMethod').append($('<option>', {
	                            value: d.code,
	                            text: d.description + ' - $'+ d.price
	                        }));
	                    });
	                    
	                   $('#shippingMethod').attr('onchange', "CartController.updateShippingRateTotal(this);") 
                    }
                    
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
				     console.log(XMLHttpRequest);
				     console.log(textStatus);
				     console.log(errorThrown);
				},
            });
            
            }
        },
        
        updateShippingRateTotal: function(element) {
        	var selectedText = $(element).find('option:selected').text();
        	var cartTotal  = $("#cartTotalElement").html(); 
        	var Money = selectedText.split("$").pop();
        	var grandTotalText = (+Money + +cartTotal);
        	$('#shippingRate').hide().html(Globals.MoneyFormat(+Money)).fadeIn('fast');
        	
        	$('#grandTotalValue').hide().html(Globals.MoneyFormat(grandTotalText)).fadeIn('fast');
        }
		
	}
	
}();