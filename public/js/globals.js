var Globals = function() {
	
	function EmailUpdateValidation() {
		$("#emailUpdateSignup").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingCategory").show();
    		    },
				success :function(data) {
					if(data.emptyEmail) {						
						$('#emailValidationMessage').hide().html(data.emptyEmail).fadeIn('fast');							
					}
					if(data.verifyEmail) {
						$('#emailValidationMessage').hide().html(data.verifyEmail).fadeIn('fast');
					}
					if(data.duplicateEmail) {
						$('#emailValidationMessage').hide().html(data.duplicateEmail).fadeIn('fast');
					}
					if(data.MySqlError) {
						$('#emailValidationMessage').hide().html(data.MySqlError).fadeIn('fast');
					}
					if(data.complete) {
						$('#emailValidationMessage').hide().html(data.complete).fadeIn('fast').delay(1000).fadeOut('slow');
					}
					
				}
			});					
		});
	}
	

	
	return {
		openNadaOrdinaryBottleSubMenu: function() {
			$("#secondaryMenu .NadaOrdinaryBottleLinks").slideToggle();
			$("#secondaryMenu .purchaseLinks").slideUp();
			$("#secondaryMenu .SupportLink").slideUp();
		},
		openSupportSubMenu: function() {
			$("#secondaryMenu .SupportLink").slideToggle();
			$("#secondaryMenu .purchaseLinks").slideUp();
			$("#secondaryMenu .NadaOrdinaryBottleLinks").slideUp();
		},
		Navigation: function() {
			
			$(window).bind("scroll resize",function(){
				var width = $(window).width();
				
				if (width > 810) {
					$(".responsiveMenu").removeAttr("style")
				}
				if(width < 810) {
					$(".NadaOrdinaryBottleLinks, .purchaseLinks").removeAttr("style")
				}
				
				var scroll_top = $(window).scrollTop();
				var urlPath = $("header").attr("data-urlPath");
				if (width > 810) {
					if (scroll_top > 15) {
						//figure out jquery animations
						$(".mainMenuLink").css("padding", "30px 0px");
						$("img#logoImage").attr("src", urlPath + 'nadaBottleLogoIcon.png');
						$("img#logoImage").css("width", "59px");
						$("li#logo").css("margin", "10px 0 5px 0");
						$("header").addClass("shadow");
					
					
					}
					else {
						$(".mainMenuLink").css("padding", "");
						$("img#logoImage").css("width", "");
						$("li#logo").css("margin", "");
						$("img#logoImage").attr("src", urlPath + 'nadaBottleLogo.png');
						$("header").removeClass("shadow")
					}
				}
				
					
			});	
				
		 

		},
		loadAjaxFlashMessage: function(data, clearInput, focusInput, fadeOutMessage, errorElement) {
			//http://stackoverflow.com/questions/183638/making-my-ajax-updated-div-fade-in
			if(fadeOutMessage) {
				if(errorElement) {
					$(errorElement).hide().html(data).fadeIn('fast');	
				}
				else {
					$('.message-holder').hide().html(data).fadeIn('fast');		
				}
			}
			
			if(fadeOutMessage) {
				if(errorElement) {
					$(errorElement).delay(1500).fadeOut(3500);
				} else {
					$(".message").delay(1500).fadeOut(3500);	
				}
			}
			
			if (clearInput) {
				$(clearInput).val('');
			}
			
			if(focusInput) {
				$(focusInput).focus();
			}
			
		},
		closeFlashMessage: function () {
			$(".message").fadeOut("fast");
		},
	
		responsiveMenu: function() {
			$(".responsiveMenu").slideToggle();
		},
		CKEditorLoad: function(Element, path) {
			//CKEDITOR.config.extraPlugins = 'uploadimage';
			
			CKEDITOR.replace(Element, {
 				filebrowserUploadUrl: path + "cms/uploadImage",
 				filebrowserImageWindowWidth: '640',
    			filebrowserImageWindowHeight: '480',
    			baseUrl: '/view/'
    			//extraPlugin = 'uploadimage'
			});
			//for ( instance in CKEDITOR.instances )
        		//CKEDITOR.instances[Element].updateElement();
			//}
			
			

		},
		EmailUpdateValidation: function() {
			EmailUpdateValidation();
		},
		textAreaAdjust: function (o) {
			
			if (!$(o).prop('scrollTop')) {
		        do {
		            var b = $(o).prop('scrollHeight');
		            var h = $(o).height();
		            $(o).height(h - 5);
		        }
		        while (b && (b != $(o).prop('scrollHeight')));
		    };
		    $(o).height($(o).prop('scrollHeight') + 20);
		   
		},
		MoneyFormat: function(NumberValue) {
			return '$' + NumberValue.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		}		
		
	}	
	
}();
