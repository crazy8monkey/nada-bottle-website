var ErrorPage = function() {
	
	
	function errorFormValidation() {
		$("#error-form").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();
			
			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				success: function(data) {
					if(data.firstName) {
						Globals.loadAjaxFlashMessage(data.firstName, false, false, true);
						$("#messageContainer").css("height", "30px");
					}
					if(data.firstNameOnlyLetters) {
						Globals.loadAjaxFlashMessage(data.firstNameOnlyLetters, false, false, true);
						$("#messageContainer").css("height", "30px");
					}
					if(data.emptyEmail) {
						Globals.loadAjaxFlashMessage(data.emptyEmail, false, false, true);
						$("#messageContainer").css("height", "30px");
					}
					if(data.wrongEmailFormat) {
						Globals.loadAjaxFlashMessage(data.wrongEmailFormat, false, false, true);
						$("#messageContainer").css("height", "30px");
					}
					if(data.errorDescription) {
						Globals.loadAjaxFlashMessage(data.errorDescription, false, false, true);
						$("#messageContainer").css("height", "30px");
					}
					if(data.finishedForm) {
						Globals.loadAjaxFlashMessage(data.finishedForm, false, false, true);	
						setTimeout(function () {
       						window.location.href = data.redirect;
   	 					}, 1500);					
					}
				}
			})
		});
	}
	
	
	
	return  {
		StartDocument: function() {
			errorFormValidation();
		}
	}
}();
