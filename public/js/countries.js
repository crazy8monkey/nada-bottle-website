/*
	*	Original script by: Shafiul Azam
	*	Version 4.0
	*	Modified by: Luigi Balzano

	*	Description:
	*	Inserts Countries and/or States as Dropdown List
	*	How to Use:

		In Head section:
		----------------
		<script type= "text/javascript" src = "countries.js"></script>
		
		In Body Section:
		----------------
		Select Country (with states):   <select id="country" name ="country"></select>
			
		Select State: <select name ="state" id ="state"></select>

        Select Country (without states):   <select id="country2" name ="country2"></select>
			
		<script language="javascript">
			populateCountries("country", "state");
			populateCountries("country2");
		</script>

	*
	*	License: Free to copy, distribute, modify, whatever you want to.
	*	Aurthor's Website: http://bdhacker.wordpress.com
	*
*/

// Countries
var country_arr = new Array("Afghanistan|AF", 
							"Aland Islands|AX",
							"Albania|AL", 
							"Algeria|DZ", 
							"American Samoa|AS",
							"Andorra|AD", 
							"Angola|AO", 
							"Anguilla|AI", 
							"Antartica|AQ", 
							"Antigua and Barbuda|AG", 
							"Argentina|AR", 
							"Armenia|AM", 
							"Aruba|AW", 
							"Australia|AU", 
							"Austria|AT", 
							"Azerbaijan|AZ", 
							"Bahamas|BS", 
							"Bahrain|BH", 
							"Bangladesh|BD", 
							"Barbados|BB", 
							"Belarus|BY", 
							"Belgium|BE", 
							"Belize|BZ", 
							"Benin|BJ", 
							"Bermuda|BM", 
							"Bhutan|BT", 
							"Bolivia|BO", 
							"Bosnia and Herzegovina|BA", 
							"Botswana|BW", 
							"Bouvet Island|BV",
							"Brazil|BR", 
							"British Indian Ocean Territory|IO",
							"British Virgin Islands|VG", 
							"Brunei|BN", 
							"Bulgaria|BG", 
							"Burkina Faso|BF", 
							"Burundi|BI", 
							"Cambodia|KH", 
							"Cameroon|CM", 
							"Canada|CA", 
							"Cape Verde|CV", 
							"Caribbean Netherlands|BQ",
							"Cayman Islands|KY", 
							"Central African Republic|CF", 
							"Chad|TD", 
							"Chile|CL", 
							"China|CN", 
							"Christmas Island|CX", 
							"Cocos (Keeling) Islands|CC", 
							"Colombia|CO", 
							"Comoros|KM", 
							"Congo [DRC]|CD", 
							"Congo [Republic]|CG", 
							"Cook Islands|CK", 
							"Costa Rica|CR", 
							"Cote d'Ivoire|CI", 
							"Croatia|HR", 
							"Cuba|CU", 
							"Curacao|CW",
							"Cyprus|CY", 
							"Czeck Republic|CZ", 
							"Denmark|DK", 
							"Djibouti|DJ", 
							"Dominica|DM", 
							"Dominican Republic|DO", 
							"Ecuador|EC", 
							"Egypt|EG", 
							"El Salvador|SV", 
							"Equatorial Guinea|GQ", 
							"Eritrea|ER", 
							"Estonia|EE", 
							"Ethiopia|ET", 
							"Falkland Islands (Islas Malvinas)|FK", 
							"Faroe Islands|FO", 
							"Fiji|FJ", 
							"Finland|FI", 
							"France|FR", 
							"French Guiana|GF", 
							"French Polynesia|PF", 
							"French Southern Territories|TF", 
							"Gabon|GA", 
							"Gambia|GM", 
							"Gaza Strip|GZ", 
							"Georgia|GE", 
							"Germany|DE", 
							"Ghana|GH", 
							"Gibraltar|GI", 
							"Greece|GR", 
							"Greenland|GL", 
							"Grenada|GD", 
							"Guadeloupe|GP", 
							"Guam|GU", 
							"Guatemala|GT", 
							"Guernsey|GG", 
							"Guinea|GN", 
							"Guinea-Bissau|GW", 
							"Guyana|GY", 
							"Haiti|HT", 
							"Heard Island and McDonald Islands|HM", 
							"Honduras|HN", 
							"Hong Kong|HK", 
							"Hungary|HU", 
							"Iceland|IS", 
							"India|IN", 
							"Indonesia|ID", 
							"Iran|IR", 
							"Iraq|IQ", 
							"Ireland|IE", 
							"Isle of Man|IM", 
							"Israel|IL", 
							"Italy|IT", 
							"Jamaica|JM", 
							"Japan|JP", 
							"Jersey|JE", 
							"Jordan|JO", 
							"Kazakhstan|KZ", 
							"Kenya|KE", 
							"Kiribati|KI", 
							"Kosovo|XK",
							"Kuwait|KW", 
							"Kyrgyzstan|KG", 
							"Laos|LA", 
							"Latvia|LV", 
							"Lebanon|LB", 
							"Lesotho|LS", 
							"Liberia|LR", 
							"Libya|LY", 
							"Liechtenstein|LI", 
							"Lithuania|LT", 
							"Luxembourg|LU", 
							"Macau|MO", 
							"Macedonia, Former Yugoslav Republic of|MK", 
							"Madagascar|MG", 
							"Malawi|MW", 
							"Malaysia|MY", 
							"Maldives|MV", 
							"Mali|ML", 
							"Malta|MT", 
							"Marshall Islands|MH", 
							"Martinique|MQ", 
							"Mauritania|MR", 
							"Mauritius|MU", 
							"Mayotte|YT", 
							"Mexico|MX", 
							"Micronesia|FM", 
							"Moldova|MD", 
							"Monaco|MC", 
							"Mongolia|MN",
							"Montenegro|ME", 
							"Montserrat|MS", 
							"Morocco|MA", 
							"Mozambique|MZ", 
							"Myanmar [Burma]|MM",
							"Namibia|NA", 
							"Nauru|NR", 
							"Nepal|NP", 
							"Netherlands|NL", 
							"Netherlands Antilles|AN", 
							"New Caledonia|NC", 
							"New Zealand|NZ", 
							"Nicaragua|NI", 
							"Niger|NE", 
							"Nigeria|NG", 
							"Niue|NU", 
							"Norfolk Island|NF", 
							"Northern Mariana Islands|MP", 
							"North Korea|KP", 
							"Norway|NO", 
							"Oman|OM", 
							"Pakistan|PK", 
							"Palau|PW", 
							"Palestinian Territories|PS",
							"Panama|PA", 
							"Papua New Guinea|PG", 
							"Paraguay|PY", 
							"Peru|PE", 
							"Philippines|PH", 
							"Pitcaim Islands|PN", 
							"Poland|PL", 
							"Portugal|PT", 
							"Puerto Rico|PR", 
							"Qatar|QA", 
							"Reunion|RE", 
							"Romainia|RO", 
							"Russia|RU", 
							"Rwanda|RW", 
							"Saint Bartelemey|BL",
							"Saint Helena|SH", 
							"Saint Kitts and Nevis|KN", 
							"Saint Lucia|LC", 
							"Saint Martin|MF",
							"Saint Pierre and Miquelon|PM", 
							"Saint Vincent and the Grenadines|VC", 
							"Samoa|WS", 
							"San Marino|SM", 
							"São Tomé and Príncipe|ST", 
							"Saudi Arabia|SA",  
							"Senegal|SN", 
							"Serbia|RS",
							"Seychelles|SC", 
							"Sierra Leone|SL", 
							"Singapore|SG", 
							"Sint Maarten|SX",
							"Slovakia|SK", 
							"Slovenia|SI", 
							"Solomon Islands|SB", 
							"Somalia|SO", 
							"South Africa|ZA", 
							"South Georgia and South Sandwich Islands|GS", 
							"South Korea|KR", 
							"South Sudan|SS",
							"Spain|ES", 
							"Sri Lanka|LK", 
							"Sudan|SD", 
							"Suriname|SR", 
							"Svalbard and Jan Mayan|SJ", 
							"Swaziland|SZ", 
							"Sweden|SE", 
							"Switzerland|CH", 
							"Syria|SY", 
							"Taiwan|TW", 
							"Tajikistan|TJ", 
							"Tanzania|TZ", 
							"Thailand|TH",
							"Timor-Leste|TL", 
							"Togo|TG", 
							"Tokelau|TK", 
							"Tonga|TO", 
							"Trinidad and Tobago|TT", 
							"Tunisia|TN", 
							"Turkey|TR", 
							"Turkmenistan|TM", 
							"Turks and Caico Islands|TC",
							"Tuvalu|TV", 
							"Uganda|UG", 
							"Ukraine|UA", 
							"United Arab Emirates|AE", 
							"United Kingdom|GB", 
							"United States|US", 
							"United States Minor Outlying Islands|UM",
							"Uruguay|UY", 
							"U.S. Virgin Islands|VI", 
							"Uzbekistan|UZ", 
							"Vanuatu|VU", 
							"Vatican City|VA",
							"Venezuela|VE", 
							"Vietnam|VN", 
							"Wallis and Futuna|WF", 
							"Western Sahara|EH", 
							"Yemen|YE", 
							"Zambia|ZM", 
							"Zimbabwe|ZW");

// States
var s_a = new Array();
s_a[0]="";

s_a["Afghanistan"]="Herat-HER|" + 
				   "Kabul-KAB|" + 
				   "Kandahar-KAN|" +
				   "Nangarhar-NAN|" +
				   "Parwan-PAR|" +
				   "Wardak-WAR|" +
				   "Zabul-ZAB";

s_a["Aland Islands"]="Aland Islands-AX"

s_a["Albania"]="Qarku i Beratit-01|"+
		"Qarku i Dibres-09|"+
		"Qarku i Durresit-02|"+
		"Qarku i Elbasanit-03|"+
		"Qarku i Fierit-04|"+
		"Qarku i Gjirokastres-05|"+
		"Qarku i Korces-06|"+
		"Qarku i Lezhes-08|"+
		"Qarku i Shkodres-10|"+
		"Qarku i Tiranes-11|"+
		"Qarku i Vlores-12";

s_a["Algeria"]="Adrar-01|" + 
	   "Annaba-23|" + 
	   "El Oued-39|" + 
	   "El Tarf-36|" + 
	   "Illizi-33|" + 
	   "Oran-31|" + 
	   "Oum el Bouaghi-04|" + 
	   "Wilaya d' Alger-16|" + 
	   "Wilaya de Ain Defla-44|" + 
	   "Wilaya de Ain Temouchent-46|" + 
	   "Wilaya de Batna-05|" + 
	   "Wilaya de Bechar-08|" + 
	   "Wilaya de Bejaia-06|" + 
	   "Wilaya de Biskra-07|" + 
	   "Wilaya de Blida-09|" + 
	   "Wilaya de Bordj Bou Arreridj-34|" + 
	   "Wilaya de Bouira-10|" + 
	   "Wilaya de Boumerdes-35|" + 
	   "Wilaya de Chlef-02|" + 
	   "Wilaya de Constantine-25|" + 
	   "Wilaya de Djelfa-17|" + 
	   "Wilaya de Ghardaia-47|" + 
	   "Wilaya de Guelma-24|" + 
	   "Wilaya de Jijel-18|" + 
	   "Wilaya de Khenchela-40|" + 
	   "Wilaya de Laghouat-03" + 
	   "Wilaya de Mascara-29|" + 
	   "Wilaya de Medea-26|" + 
	   "Wilaya de Mila-43|" + 
	   "Wilaya de Mostaganem-27|" + 
	   "Wilaya de M'Sila-28|" + 
	   "Wilaya de Naama-45|" + 
	   "Wilaya de Ouargla-30|" + 
	   "Wilaya de Relizane-48|" + 
	   "Wilaya de Saida-20|" + 
	   "Wilaya de Setif-19|" + 
	   "Wilaya de Sidi Bel Abbes-22|" + 
	   "Wilaya de Skikda-21|" + 
	   "Wilaya de Souk Ahras-41|" + 
	   "Wilaya de Tamanrasset-11|" + 
	   "Wilaya de Tebessa-12|" + 
	   "Wilaya de Tiaret-14|" + 
	   "Wilaya de Tipaza-42|" + 
	   "Wilaya de Tissemsilt-38|" + 
	   "Wilaya de Tizi Ouzou-15|" + 
	   "Wilaya de Tlemcen-13";

s_a["American Samoa"]="Eastern District-E";

s_a["Andorra"]="Andorra la Vella-07|"+
		"Canillo-02|"+
		"Encamp-03|"+
		"Escaldes Engordany-08|"+
		"La Massana-04|"+
		"Ordino-05|"+
		"Sant Julià de Loria-06";

s_a["Angola"]="Bengo Province-BGO|"+
		"Benguela-BGU|"+
		"Cabinda-CAB|"+
		"Cuanza Norte Province-CNO|"+
		"Cunene Province-CNN|"+
		"Huambo-HUA|"+
		"Huila Province-HUI|"+
		"Kuando Kubango-CCU|"+
		"Kwanza Sul-CUS|"+
		"Luanda Province-LUA|"+
		"Lunda Norte Province-LNO|"+
		"Lunda Sul-LSU|"+
		"Malanje Province-MAL|"+
		"Moxico-MOX|"+
		"Namibe Province-NAM|"+
		"Provincia do Bie-BIE|"+
		"Provincia do Uige-UIG";

s_a["Anguilla"]="Anguilla-AI";

s_a["Antartica"]="Antartica-AQ";

s_a["Antigua and Barbuda"]="Parish of Saint George-03|"+
						   "Parish of Saint John-04|"+
						   "Parish of Saint Mary-05|"+
						   "Parish of Saint Paul-06|"+
						   "Parish of Saint Peter-07";

s_a["Argentina"]="Buenos Aires-B|" + 
	   "Buenos Aires F.D.-C|"+ 
	   "Chaco-H|"+
	   "Chubut-U|"+
	   "Cordoba-X|" + 
	   "Corrientes-W|" +
	   "Entre Rios-E|" +
	   "Formosa-P|" +
	   "La Pampa-L|" + 
	   "Mendoza-M|" +
	   "Neuquen-Q|" +
	   "Provincia de Catamarca-K|" +
	   "Provincia de Jujuy-Y|" +
	   "Provincia de La Rioja-F|" +
	   "Provincia de Misiones-N|" +
	   "Provincia de San Juan-J|" +
	   "Provincia de San Luis-D|" +
	   "Provincia de Tucuman-T|" +
	   "Rio Negro-R|" + 
	   "Salta-A|" +
	   "Santa Cruz-Z|" +
	   "Santa Fe-S|" +
	   "Santiago del Estero-G|" +
	   "Tierra del Fuego-V";

s_a["Armenia"]="Aragatsotni Marz-AG|" +
        "Ararati Marz-AR|"+ 
        "Armaviri Marz-AV|" +
        "Geghark'unik'i Marz-GR|"+
        "Kotayk'i Marz-KT|"+
        "Lorru Marz-LO|"+
        "Shiraki Marz-SH|"+
        "Syunik'i Marz-SU|"+
        "Tavushi Marz-TV|"+
        "Vayots' Dzori Marz-VD|"+
        "Yerevan-ER";

s_a["Aruba"]="Aruba-AW";

s_a["Australia"]="Australian Capital Territory-ACT|"+
		"New South Wales-NSW|"+
		"Northern Territory-NT|"+
		"Queensland-QLD|"+
		"South Australia-SA|"+
		"Tasmania-TAS|"+
		"Victoria-VIC|"+
		"Western Australia-WA";


s_a["Austria"]="Burgenland-1|"+
		"Carinthia-2|"+
		"Lower Austria-3|"+
		"Salzburg-5|"+
		"Styria-6|"+
		"Tyrol-7|"+
		"Upper Austria-4|"+
		"Vienna-9|"+
		"Vorarlberg-8";

s_a["Azerbaijan"]="Absheron Rayon-ABS|"+
		"Baku City-BA|"+
		"Ganja City-GA|"+
		"Goygol Rayon-XAN|"+
		"Lankaran Rayon-LA|"+
		"Mingacevir City-MI|"+
		"Nakhichevan-NX|"+
		"Oghuz Rayon-OGU|"+
		"Quba Rayon-QBA|"+
		"Qusar Rayon-QUS|"+
		"Shaki City-SAK|"+
		"Sumqayit City-SM|"+
		"Yevlax City-YE";

s_a["Bahamas"]="Bimini-BI|"+
		"Central Abaco District-CO|"+
		"City of Freeport District-FP|"+
		"Harbour Island-HI|"+
		"New Providence District-NP|"+
		"North Andros District-NS";

s_a["Bahrain"]="Central Governorate-16|"+
		"Manama-13|"+
		"Muharraq-15|"+
		"Northern-17|"+
		"Southern Governorate-14";

s_a["Bangladesh"]="Chittagong-B|"+
		"Dhaka Division-C|"+
		"Khulna Division-D|"+
		"Rajshahi Division-E|"+
		"Rangpur Division-F|"+
		"Sylhet Division-G";

s_a["Barbados"]="Christ Church-01|"+
		"Saint Andrew-02|"+
		"Saint George-03|"+
		"Saint James-04|"+
		"Saint John-05|"+
		"Saint Lucy-07|"+
		"Saint Michael-08|"+
		"Saint Peter-09|"+
		"Saint Philip-10|"+
		"Saint Thomas-11";

s_a["Belarus"]="Brest-BR|"+
		"Gomel-HO|"+
		"Grodnenskaya-HR|"+
		"Minsk-HM|"+
		"Minsk-MI|"+
		"Mogilev-MA|"+
		"Vitebsk-VI";

s_a["Belgium"]="Antwerp Province-VAN|"+
		"Brussels Capital-BRU|"+
		"Flanders-VLG|"+
		"Hainaut Province-WHT|"+
		"Limburg Province-VLI|"+
		"Wallonia-WAL|"+
		"Walloon Brabant Province-WBR|"+
		"West Flanders Province-VWV";

s_a["Belize"]="Belize District-BZ|"+
		"Cayo District-CY|"+
		"Corozal District-CZL|"+
		"Orange Walk District-OW|"+
		"Stann Creek District-SC";

s_a["Benin"]="Atlantique Department-AQ|"+
		"Departement de l'Oueme-OU|"+
		"Littoral-LI|"+
		"Zou Department-ZO";

s_a["Bermuda"]="Hamilton city-HC|"+
		"Saint George-GC|"+
		"Sandys Parish-SA";

s_a["Bhutan"]="Chhukha Dzongkhag-12|"+
		"Paro-11|"+
		"Thimphu Dzongkhag-15";

s_a["Bolivia"]="Departamento de Chuquisaca-H|"+
		"Departamento de Cochabamba-C|"+
		"Departamento de La Paz-L|"+
		"Departamento de Pando-N|"+
		"Departamento de Potosi-P|"+
		"Departamento de Santa Cruz-S|"+
		"Departamento de Tarija-T|"+
		"El Beni-B|"+
		"Oruro-O";

s_a["Bosnia and Herzegovina"]="Brčko-BRC|"+
		"Federation of Bosnia and Herzegovina-BIH|"+
		"Republika Srpska-SRP";

s_a["Botswana"]="Central District-CE|"+
		"North East District-NE|"+
		"North West District-NW|"+
		"South East District-SE";

s_a["Bouvet Island"]="Bouvet Island-BV";

s_a["Brazil"]="Acre-AC|"+
		"Alagoas-AL|"+
		"Amapa-AP|"+
		"Amazonas-AM|"+
		"Bahia-BA|"+
		"Ceara-CE|"+
		"Espirito Santo-ES|"+
		"Federal District-DF|"+
		"Goias-GO|"+
		"Maranhao-MA|"+
		"Mato Grosso-MT|"+
		"Mato Grosso do Sul-MS|"+
		"Minas Gerais-MG|"+
		"Para-PA|"+
		"Paraiba-PB|"+
		"Parana-PR|"+
		"Pernambuco-PE|"+
		"Piaui-PI|"+
		"Rio de Janeiro-RJ|"+
		"Rio Grande do Norte-RN|"+
		"Rio Grande do Sul-RS|"+
		"Rondonia-RO|"+
		"Roraima-RR|"+
		"Santa Catarina-SC|"+
		"Sao Paulo-SP|"+
		"Sergipe-SE|"+
		"Tocantins-TO";

s_a["British Indian Ocean Territory"]="British Indian Ocean Territory-IO";

s_a["British Virgin Islands"]="British Virgin Islands-VG";

s_a["Brunei"]="Belait District-BE|"+
		"Brunei and Muara District-BM|"+
		"Temburong District-TE|"+
		"Tutong District-TU";

s_a["Bulgaria"]="Blagoevgrad-01|"+
		"Burgas-02|"+
		"Gabrovo-07|"+
		"Lovech-11|"+
		"Oblast Dobrich-08|"+
		"Oblast Khaskovo-26|"+
		"Oblast Kurdzhali-09|"+
		"Oblast Kyustendil-10|"+
		"Oblast Montana-12|"+
		"Oblast Pleven-15|"+
		"Oblast Razgrad-17|"+
		"Oblast Ruse-18|"+
		"Oblast Shumen-27|"+
		"Oblast Silistra-19|"+
		"Oblast Sliven-20|"+
		"Oblast Smolyan-21|"+
		"Oblast Stara Zagora-24|"+
		"Oblast Turgovishte-25|"+
		"Oblast Veliko Turnovo-04|"+
		"Oblast Vidin-05|"+
		"Oblast Vratsa-06|"+
		"Oblast Yambol-28|"+
		"Pazardzhik-13|"+
		"Pernik-14|"+
		"Plovdiv-16|"+
		"Sofia Capital-22|"+
		"Sofiya-23|"+
		"Varna-03";

s_a["Burkina Faso"]="Centre-03|"+
		"High Basins Region-09|"+
		"Southwest Region-13";
		
s_a["Burundi"]="Bujumbura Mairie Province-BM";

s_a["Cambodia"]="Banteay Meanchey-1|"+
		"Battambang-2|"+
		"Kampong Speu-5|"+
		"Kampong Thom-6|"+
		"Kandal-8|"+
		"Pailin-24|"+
		"Phnom Penh-12|"+
		"Preah Sihanouk-18";

s_a["Cameroon"]="Adamaoua-AD|"+
		"Centre-CE|"+
		"Littoral-LT|"+
		"North Region-NO|"+
		"North West Region-NW|"+
		"South Region-SU|"+
		"South West Region-SW|"+
		"West-OU";

s_a["Canada"]="Alberta-AB|"+
		"British Columbia-BC|"+
		"Manitoba-MB|"+
		"New Brunswick-NB|"+
		"Newfoundland and Labrador-NL|"+
		"Northwest Territories-NT|"+
		"Nova Scotia-NS|"+
		"Nunavut-NU|"+
		"Ontario-ON|"+
		"Prince Edward Island-PE|"+
		"Quebec-QC|"+
		"Saskatchewan-SK|"+
		"Yukon-YT";

s_a["Cape Verde"]="Concelho de Sao Domingos-SD|"+
		"Concelho de Sao Vicente-SV";

s_a["Caribbean Netherlands"]= "Bonaire-BO|"+
		 "Saba-SA";

s_a["Cayman Islands"]="Cayman Islands-KY";

s_a["Central African Republic"]="Commune de Bangui-BGF";

s_a["Chad"]="Chari-Baguirmi Region-CB|"+
		"Hadjer Lamis-HL|"+
		"Logone Occidental Region-LO|"+
		"Ouaddai Region-OD";

s_a["Chile"]="Antofagasta-AN|"+
		"Atacama-AT|"+
		"Aysen-AI|"+
		"Coquimbo-CO|"+
		"Los Lagos-LL|"+
		"Maule-ML|"+
		"Region de Arica y Parinacota-AP|"+
		"Region de la Araucania-AR|"+
		"Region del Biobio-BI|"+
		"Region del Libertador General Bernardo O'Higgins-LI|"+
		"Region de Los Rios-LR|"+
		"Region de Magallanes y de la Antartica Chilena-MA|"+
		"Region de Valparaiso-VS|"+
		"Santiago Metropolitan-RM|"+
		"Tarapacá-TA";

s_a["China"]="Anhui Sheng-34|"+
		"Beijing Shi-11|"+
		"Chongqing Shi-50|"+
		"Fujian-35|"+
		"Gansu Sheng-62|"+
		"Guangdong-44|"+
		"Guangxi Zhuangzu Zizhiqu-45|"+
		"Guizhou Sheng-52|"+
		"Hainan-46|"+
		"Hebei-13|"+
		"Heilongjiang Sheng-23|"+
		"Henan Sheng-41|"+
		"Hubei-42|"+
		"Hunan-43|"+
		"Inner Mongolia-15|"+
		"Jiangsu Sheng-32|"+
		"Jiangxi Sheng-36|"+
		"Jilin Sheng-22|"+
		"Liaoning-21|"+
		"Ningxia Huizu Zizhiqu-64|"+
		"Qinghai Sheng-63|"+
		"Shaanxi-61|"+
		"Shandong Sheng-37|"+
		"Shanghai Shi-31|"+
		"Shanxi Sheng-14|"+
		"Sichuan Sheng-51|"+
		"Tianjin Shi-12|"+
		"Tibet Autonomous Region-54|"+
		"Xinjiang Uygur Zizhiqu-65|"+
		"Yunnan-53|"+
		"Zhejiang Sheng-33";

s_a["Christmas Island"]="Christmas Island-CX";

s_a["Cocos (Keeling) Islands"]="Cocos (Keeling) Islands-CC";

s_a["Colombia"]="Antioquia-ANT|"+
		"Atlántico-ATL|"+
		"Bogota D.C.-DC|"+
		"Cundinamarca-CUN|"+
		"Departamento de Bolivar-BOL|"+
		"Departamento de Boyaca-BOY|"+
		"Departamento de Caldas-CAL|"+
		"Departamento de Casanare-CAS|"+
		"Departamento de Cordoba-COR|"+
		"Departamento de La Guajira-LAG|"+
		"Departamento del Amazonas-AMA|"+
		"Departamento del Caqueta-CAQ|"+
		"Departamento del Cauca-CAU|"+
		"Departamento del Cesar-CES|"+
		"Departamento del Guainia-GUA|"+
		"Departamento del Guaviare-GUV|"+
		"Departamento del Huila-HUI|"+
		"Departamento del Magdalena-MAG|"+
		"Departamento del Meta-MET|"+
		"Departamento del Putumayo-PUT|"+
		"Departamento del Valle del Cauca-VAC|"+
		"Departamento del Vichada-VID|"+
		"Departamento de Narino-NAR|"+
		"Departamento de Norte de Santander-NSA|"+
		"Departamento de Risaralda-RIS|"+
		"Departamento de Santander-SAN|"+
		"Departamento de Sucre-SUC|"+
		"Departamento de Tolima-TOL|"+
		"Providencia y Santa Catalina, Departamento de Archipielago de San Andres-SAP|"+
		"Quindio Department-QUI";

s_a["Comoros"]="Grande Comore-G|"+
		"Ndzuwani-A"

s_a["Congo [DRC]"]="Katanga Province-KA|"+
		"Kinshasa City-KN|"+
		"Nord Kivu-NK|"+
		"Province du Bas Congo-BC";

s_a["Congo [Republic]"]="Commune de Brazzaville-BZV|"+
		"Pointe-Noire-16|"+
		"Sangha-13";

s_a["Cook Islands"]="Cook Islands-CK";

s_a["Costa Rica"]="Provincia de Alajuela-A|"+
		"Provincia de Cartago-C|"+
		"Provincia de Guanacaste-G|"+
		"Provincia de Heredia-H|"+
		"Provincia de Limon-L|"+
		"Provincia de Puntarenas-P|"+
		"Provincia de San Jose-SJ";

s_a["Cote d'Ivoire"]="Haut Sassandra-02|"+
		"Lagunes-01";

s_a["Croatia"]="Bjelovarsko Bilogorska Zupanija-07|"+
		"Brodsko Posavska Zupanija-12|"+
		"Dubrovacko Neretvanska Zupanija-19|"+
		"Grad Zagreb-21|"+
		"Istarska Zupanija-18|"+
		"Karlovacka Zupanija-04|"+
		"Koprivnicko Krizevacka Zupanija-06|"+
		"Krapinsko Zagorska Zupanija-02|"+
		"Licko-Senjska Zupanija-09|"+
		"Medimurska Zupanija-20|"+
		"Osjecko Baranjska Zupanija-14|"+
		"Pozesko Slavonska Zupanija-11|"+
		"Primorsko Goranska Zupanija-08|"+
		"Sibensko Kninska Zupanija-15|"+
		"Sisacko Moslavacka Zupanija-03|"+
		"Splitsko Dalmatinska Zupanija-17|"+
		"Varazdinska Zupanija-05|"+
		"Viroviticko Podravska Zupanija-10|"+
		"Vukovarsko Srijemska Zupanija-16|"+
		"Zadarska Zupanija-13|"+
		"Zagreb County-01";

s_a["Cuba"]="La Habana-03|"+
		"Las Tunas-10|"+
		"Provincia de Camaguey-09|"+
		"Provincia de Ciego de Avila-08|"+
		"Provincia de Cienfuegos-06|"+
		"Provincia de Guantanamo-14|"+
		"Provincia de Holguin-11|"+
		"Provincia de Matanzas-04|"+
		"Provincia de Pinar del Rio-01|"+
		"Provincia de Sancti Spiritus-07|"+
		"Provincia de Santiago de Cuba-13|"+
		"Provincia Granma-12";

s_a["Curacao"]="Curacao-CW";

s_a["Cyprus"]="Ammochostos-04|"+
		"Keryneia-06|"+
		"Larnaka-03|"+
		"Lefkosia-01|"+
		"Limassol-02|"+
		"Pafos-05";

s_a["Czeck Republic"]="Central Bohemia-ST|"+
		"Hlavni mesto Praha-PR|"+
		"Jihocesky kraj-JC|"+
		"Karlovarsky kraj-KA|"+
		"Kraj Vysocina-VY|"+
		"Kralovehradecky kraj-KR|"+
		"Liberecky kraj-LI|"+
		"Moravskoslezsky kraj-MO|"+
		"Olomoucky kraj-OL|"+
		"Pardubicky kraj-PA|"+
		"Plzensky kraj-PL|"+
		"South Moravian-JM|"+
		"Ustecky kraj-US|"+
		"Zlín-ZL";

s_a["Denmark"]="Capital Region-84|"+
		"Central Jutland-82|"+
		"North Denmark-81|"+
		"South Denmark-83|"+
		"Zealand-85";

s_a["Djibouti"]="Djibouti-DJ";

s_a["Dominica"]="Saint Andrew-02|"+
		"Saint George-04|"+
		"Saint John-05|"+
		"Saint Patrick-09";

s_a["Dominican Republic"]="Nacional-01|"+
		"Provincia de Azua-02|"+
		"Provincia de Barahona-04|"+
		"Provincia de El Seibo-08|"+
		"Provincia de Hato Mayor-30|"+
		"Provincia de Independencia-10|"+
		"Provincia de La Altagracia-11|"+
		"Provincia de La Romana-12|"+
		"Provincia de La Vega-13|"+
		"Provincia de Monsenor Nouel-28|"+
		"Provincia de Monte Cristi-15|"+
		"Provincia de Monte Plata-29|"+
		"Provincia de Pedernales-16|"+
		"Provincia de Peravia-17|"+
		"Provincia de San Cristobal-21|"+
		"Provincia de San Juan-22|"+
		"Provincia de San Pedro de Macoris-23|"+
		"Provincia de Santiago-25|"+
		"Provincia de Santiago Rodriguez-26|"+
		"Provincia de Valverde-27|"+
		"Provincia Duarte-06|"+
		"Provincia Espaillat-09|"+
		"Provincia Maria Trinidad Sanchez-14|"+
		"Provincia Sanchez Ramirez-24|"+
		"Puerto Plata-18|"+
		"Samaná-20";

s_a["Ecuador"]="Provincia de Cotopaxi-X|"+
		"Provincia de El Oro-O|"+
		"Provincia de Esmeraldas-E|"+
		"Provincia de Galapagos-W|"+
		"Provincia de Imbabura-I|"+
		"Provincia del Azuay-A|"+
		"Provincia del Canar-F|"+
		"Provincia del Carchi-C|"+
		"Provincia del Chimborazo-H|"+
		"Provincia del Guayas-G|"+
		"Provincia de Loja-L|"+
		"Provincia de Los Rios-R|"+
		"Provincia del Pastaza-Y|"+
		"Provincia del Tungurahua-T|"+
		"Provincia de Manabi-M|"+
		"Provincia de Morona-Santiago-S|"+
		"Provincia de Napo-N|"+
		"Provincia de Pichincha-P|"+
		"Provincia de Santa Elena-SE|"+
		"Provincia de Santo Domingo de los Tsachilas-SD|"+
		"Provincia de Sucumbios-U|"+
		"Provincia de Zamora Chinchipe-Z";

s_a["Egypt"]="Alexandria-ALX|"+
		"As Suways-SUZ|"+
		"Beheira Governorate-BH|"+
		"Eastern Province-SHR|"+
		"Ismailia Governorate-IS|"+
		"Kafr ash Shaykh-KFS|"+
		"Luxor-LX|"+
		"Muhafazat ad Daqahliyah-DK|"+
		"Muhafazat al Fayyum-FYM|"+
		"Muhafazat al Gharbiyah-GH|"+
		"Muhafazat al Jizah-GZ|"+
		"Muhafazat al Minufiyah-MNF|"+
		"Muhafazat al Minya-MN|"+
		"Muhafazat al Qahirah-C|"+
		"Muhafazat al Qalyubiyah-KB|"+
		"Muhafazat Aswan-ASN|"+
		"Muhafazat Asyut-AST|"+
		"Muhafazat Bani Suwayf-BNS|"+
		"Muhafazat Bur Sa`id-PTS|"+
		"Muhafazat Dumyat-DT|"+
		"Muhafazat Matruh-MT|"+
		"Muhafazat Qina-KN|"+
		"Muhafazat Shamal Sina'-SIN|"+
		"Muhafazat Suhaj-SHG|"+
		"Red Sea-BA|"+
		"South Sinai-JS";

s_a["El Salvador"]="Departamento de Ahuachapan-AH|"+
		"Departamento de Cabanas-CA|"+
		"Departamento de Chalatenango-CH|"+
		"Departamento de Cuscatlan-CU|"+
		"Departamento de La Libertad-LI|"+
		"Departamento de La Paz-PA|"+
		"Departamento de La Union-UN|"+
		"Departamento de Morazan-MO|"+
		"Departamento de San Miguel-SM|"+
		"Departamento de San Salvador-SS|"+
		"Departamento de Santa Ana-SA|"+
		"Departamento de San Vicente-SV|"+
		"Departamento de Sonsonate-SO|"+
		"Departamento de Usulutan-US";

s_a["Equatorial Guinea"]="Provincia de Bioko Norte-BN";

s_a["Eritrea"]="Maekel Region-MA";

s_a["Estonia"]="Harju-37|"+
		"Hiiumaa-39|"+
		"Ida Virumaa-44|"+
		"Järvamaa-51|"+
		"Jõgevamaa-49|"+
		"Lääne-57|"+
		"Lääne Virumaa-59|"+
		"Pärnumaa-67|"+
		"Põlvamaa-65|"+
		"Raplamaa-70|"+
		"Saare-74|"+
		"Tartu-78|"+
		"Valgamaa-82|"+
		"Viljandimaa-84|"+
		"Võrumaa-86";

s_a["Ethiopia"]="Adis Abeba Astedader-AA|"+
		"Afar Region-AF|"+
		"Amhara-AM|"+
		"Bīnshangul Gumuz-BE|"+
		"Dire Dawa-DD|"+
		"Gambela-GA|"+
		"Harari Region-HA|"+
		"Oromiya-OR|"+
		"Somali-SO|"+
		"Southern Nations, Nationalities, and People's Region-SN|"+
		"Tigray-TI";

s_a["Falkland Islands (Islas Malvinas)"]="Falkland Islands (Islas Malvinas)-FK"

s_a["Faroe Islands"]="Faroe Islands-FO";

s_a["Fiji"]="Central-C|"+
		"Western-W";

s_a["Finland"]="Central Ostrobothnia-07|"+
		"Haeme-06|"+
		"Kainuu-05|"+
		"Kymenlaakso-09|"+
		"Lapland-10|"+
		"Northern Ostrobothnia-14|"+
		"Northern Savo-15|"+
		"North Karelia-13|"+
		"Päijänne Tavastia-16|"+
		"Pirkanmaa-11|"+
		"Satakunta-17|"+
		"Southern Finland-ES|"+
		"Southern Ostrobothnia-03|"+
		"Southern Savonia-04|"+
		"South Karelia-02|"+
		"Uusimaa-18|"+
		"Varsinais-Suomi-19|"+
		"Western Finland-LS";

s_a["France"]="Alsace-A|"+
		"Aquitaine-B|"+
		"Auvergne-C|"+
		"Bourgogne-D|"+
		"Brittany-E|"+
		"Centre-F|"+
		"Champagne Ardenne-G|"+
		"Corsica-H|"+
		"Franche Comté-I|"+
		"Haute Normandie-Q|"+
		"Île de France-J|"+
		"Languedoc Roussillon-K|"+
		"Limousin-L|"+
		"Lorraine-M|"+
		"Lower Normandy-P|"+
		"Midi Pyrénées-N|"+
		"Nord Pas de Calais-O|"+
		"Pays de la Loire-R|"+
		"Picardie-S|"+
		"Poitou-Charentes-T|"+
		"Provence Alpes Côte d'Azur-U|"+
		"Rhône Alpes-V";

s_a["French Guiana"]="French Guiana-GF";

s_a["French Polynesia"]="Iles du Vent-V|"+
		"Iles Tuamotu Gambier-T";

s_a["French Southern Territories"]="French Southern Territories-TF";

s_a["Gabon"]="Estuaire-1|"+
		"Province de la Ngounie-4|"+
		"Province de l'Ogooue-Maritime-8|"+
		"Province du Moyen-Ogooue-3";

s_a["Gambia"]="City of Banjul-B|"+
		"Western Division-W";

s_a["Gaza Strip"]="Gaza Strip-GZ";

s_a["Georgia"]="Abkhazia-AB|"+
		"Ajaria-AJ|"+
		"Guria-GU|"+
		"Imereti-IM|"+
		"Kakheti-KA|"+
		"K'alak'i T'bilisi-TB|"+
		"Kvemo Kartli-KK|"+
		"Mtskheta Mtianeti-MM|"+
		"Racha-Lechkhumi and Kvemo Svaneti-RL|"+
		"Samegrelo and Zemo Svaneti-SZ|"+
		"Samtskhe Javakheti-SJ|"+
		"Shida Kartli-SK";

s_a["Germany"]="Baden-Württemberg Region-BW|"+
		"Bavaria-BY|"+
		"Brandenburg-BB|"+
		"Bremen-HB|"+
		"Hamburg-HH|"+
		"Hesse-HE|"+
		"Land Berlin-BE|"+
		"Lower Saxony-NI|"+
		"Mecklenburg-Vorpommern-MV|"+
		"North Rhine-Westphalia-NW|"+
		"Rheinland-Pfalz-RP|"+
		"Saarland-SL|"+
		"Saxony-SN|"+
		"Saxony Anhalt-ST|"+
		"Schleswig Holstein-SH|"+
		"Thuringia-TH";

s_a["Ghana"]="Ashanti Region-AH|"+
		"Brong-Ahafo-BA|"+
		"Central Region-CP|"+
		"Greater Accra Region-AA|"+
		"Upper East Region-UE|"+
		"Upper West Region-UW|"+
		"Volta Region-TV|"+
		"Western Region-WP";

s_a["Gibraltar"]="Gibraltar-GI";

s_a["Greece"]="Attica-I|"+
		"Central Greece-H|"+
		"Central Macedonia-B|"+
		"Crete-M|"+
		"East Macedonia and Thrace-A|"+
		"Epirus-D|"+
		"Ionian Islands-F|"+
		"North Aegean-K|"+
		"Peloponnese-J|"+
		"South Aegean-L|"+
		"Thessaly-E|"+
		"West Greece-G|"+
		"West Macedonia-C";

s_a["Greenland"]="Kujalleq-KU|"+
		"Qaasuitsup-QA|"+
		"Qeqqata-QE|"+
		"Sermersooq-SM";

s_a["Grenada"]="Saint Andrew-01|"+
		"Saint David-02|"+
		"Saint George-03|"+
		"Saint John-04|"+
		"Saint Patrick-06";

s_a["Guadeloupe"]="Guadeloupe-GP";

s_a["Guam"]="Guam-GU";

s_a["Guatemala"]="Departamento de Alta Verapaz-AV|"+
		"Departamento de Baja Verapaz-BV|"+
		"Departamento de Chimaltenango-CM|"+
		"Departamento de Chiquimula-CQ|"+
		"Departamento de El Progreso-PR|"+
		"Departamento de Escuintla-ES|"+
		"Departamento de Guatemala-GU|"+
		"Departamento de Huehuetenango-HU|"+
		"Departamento de Izabal-IZ|"+
		"Departamento de Jutiapa-JU|"+
		"Departamento del Peten-PE|"+
		"Departamento de Quetzaltenango-QZ|"+
		"Departamento de Retalhuleu-RE|"+
		"Departamento de Sacatepequez-SA|"+
		"Departamento de San Marcos-SM|"+
		"Departamento de Santa Rosa-SR|"+
		"Departamento de Solola-SO|"+
		"Departamento de Totonicapan-TO|"+
		"Departamento de Zacapa-ZA|"+
		"Suchitepeque-SU";

s_a["Guernsey"]="Guernsey-GG";

s_a["Guinea"]="Boke Region-B|"+
		"Conakry Region-C|"+
		"Faranah-F|"+
		"Kankan Region-K|"+
		"Kindia-D|"+
		"Labe Region-L|"+
		"Mamou Region-M|"+
		"Nzerekore Region-N";

s_a["Guinea-Bissau"]="Bissau-BS|"+
		"Cacheu Region-CA";

s_a["Guyana"]="Demerara Mahaica Region-DE|"+
		"East Berbice Corentyne Region-EB|"+
		"Essequibo Islands West Demerara Region-ES|"+
		"Upper Demerara Berbice Region-UD";

s_a["Haiti"]="Centre-CE|"+
		"Departement de l'Ouest-OU|"+
		"Departement du Nord Est-NE|"+
		"Nord-ND|"+
		"Sud-SD|"+
		"Sud Est-SE";


//Heard Island and McDonald Island
s_a["Heard Island and McDonald Islands"]="Heard Island and McDonald Islands-HM";

s_a["Vatican City"]="Vatican City-VA";

s_a["Honduras"]="Bay Islands-IB|"+
		 "Departamento de Atlantida-AT|"+
		 "Departamento de Choluteca-CH|"+
		 "Departamento de Colon-CL|"+
		 "Departamento de Comayagua-CM|"+
		 "Departamento de Copan-CP|"+
		 "Departamento de Cortes-CR|"+
		 "Departamento de El Paraiso-EP|"+
		 "Departamento de Francisco Morazan-FM|"+
		 "Departamento de Gracias a Dios-GD|"+
		 "Departamento de Intibuca-IN|"+
		 "Departamento de La Paz-LP|"+
		 "Departamento de Lempira-LE|"+
		 "Departamento de Ocotepeque-OC|"+
		 "Departamento de Olancho-OL|"+
		 "Departamento de Santa Barbara-SB|"+
		 "Departamento de Valle-VA|"+
		 "Departamento de Yoro-YO";

s_a["Hong Kong"]="Central and Western District-HCW|"+
		 "Eastern-HEA|"+
		 "Islands-NT|"+
		 "Kowloon City-KKC|"+
		 "Kwai Tsing-NKT|"+
		 "North-NT|"+
		 "Sai Kung-NT|"+
		 "Sham Shui Po-KSS|"+
		 "Sha Tin-NST|"+
		 "Southern-HSO|"+
		 "Tai Po-NT|"+
		 "Wanchai-HWC|"+
		 "Wong Tai Sin-KWT|"+
		 "Yau Tsim Mong-KYT|"+
		 "Yuen Long District-NYL";

s_a["Hungary"]="Bács Kiskun-BK|"+
		 "Baranya-BA|"+
		 "Bekes-BE|"+
		 "Borsod Abaúj Zemplén-BZ|"+
		 "Budapest fovaros-BU|"+
		 "Csongrad megye-CS|"+
		 "Fejér-FE|"+
		 "Győr Moson Sopron-GS|"+
		 "Hajdú Bihar-HB|"+
		 "Heves megye-HE|"+
		 "Jász Nagykun Szolnok-JN|"+
		 "Komárom Esztergom-KE|"+
		 "Nograd megye-NO|"+
		 "Pest megye-PE|"+
		 "Somogy megye-SO|"+
		 "Szabolcs Szatmár Bereg-SZ|"+
		 "Tolna megye-TO|"+
		 "Vas-VA|"+
		 "Veszprem megye-VE|"+
		 "Zala-ZA";

s_a["Iceland"]="Capital Region-1|"+
		 "East-7|"+
		 "Northeast-6|"+
		 "Northwest-5|"+
		 "South-8|"+
		 "Southern Peninsula-2|"+
		 "West-3|"+
		 "Westfjords-4";

s_a["India"]="Andhra Pradesh-AP|"+
		 "Arunachal Pradesh-AR|"+
		 "Assam-AS|"+
		 "Bihar-BR|"+
		 "Chandigarh-CH|"+
		 "Dadra and Nagar Haveli-DN|"+
		 "Daman and Diu-DD|"+
		 "Goa-GA|"+
		 "Gujarat-GJ|"+
		 "Haryana-HR|"+
		 "Karnataka-KA|"+
		 "Kashmir-JK|"+
		 "Kerala-KL|"+
		 "Laccadives-LD|"+
		 "Madhya Pradesh-MP|"+
		 "Maharashtra-MH|"+
		 "Manipur-MN|"+
		 "Meghalaya-ML|"+
		 "Mizoram-MZ|"+
		 "Nagaland-NL|"+
		 "National Capital Territory of Delhi-DL|"+
		 "Odisha-OR|"+
		 "Rajasthan-RJ|"+
		 "Sikkim-SK|"+
		 "State of Chhattisgarh-CT|"+
		 "State of Himachal Pradesh-HP|"+
		 "State of Jharkhand-JH|"+
		 "State of Punjab-PB|"+
		 "Tamil Nadu-TN|"+
		 "Tripura-TR|"+
		 "Union Territory of Andaman and Nicobar Islands-AN|"+
		 "Union Territory of Puducherry-PY|"+
		 "Uttarakhand-UL|"+
		 "Uttar Pradesh-UP|"+
		 "West Bengal-WB";

s_a["Indonesia"]="Bali-BA|"+
		 "Bangka–Belitung Islands-BB|"+
		 "Banten-BT|"+
		 "Central Java-JT|"+
		 "Central Kalimantan-KT|"+
		 "Central Sulawesi-ST|"+
		 "Daerah Istimewa Yogyakarta-YO|"+
		 "Daerah Khusus Ibukota Jakarta-JK|"+
		 "East Java-JI|"+
		 "East Kalimantan-KI|"+
		 "East Nusa Tenggara-NT|"+
		 "Nanggroe Aceh Darussalam Province-AC|"+
		 "North Kalimantan-KI|"+
		 "North Sulawesi-SA|"+
		 "North Sumatra-SU|"+
		 "Propinsi Bengkulu-BE|"+
		 "Provinsi Gorontalo-GO|"+
		 "Provinsi Jambi-JA|"+
		 "Provinsi Lampung-LA|"+
		 "Provinsi Maluku-MA|"+
		 "Provinsi Maluku Utara-MU|"+
		 "Provinsi Papua-PA|"+
		 "Provinsi Riau-RI|"+
		 "Provinsi Sulawesi Barat-SR|"+
		 "Riau Islands-KR|"+
		 "South Kalimantan-KS|"+
		 "South Sulawesi-SN|"+
		 "South Sumatra-SS|"+
		 "Sulawesi Tenggara-SG|"+
		 "West Java-JB|"+
		 "West Kalimantan-KB|"+
		 "West Nusa Tenggara-NB|"+
		 "West Papua-PB|"+
		 "West Sumatra-SB";

s_a["Iran"]="Bushehr-06|"+
		 "East Azerbaijan-01|"+
		 "Fars-14|"+
		 "Hormozgan-23|"+
		 "Isfahan-04|"+
		 "Kerman-15|"+
		 "Khuzestan-10|"+
		 "Markazi-22|"+
		 "Māzandarān-21|"+
		 "Ostan e Ardabil-03|"+
		 "Ostan e Azarbayjan e Gharbi-02|"+
		 "Ostan e Chahar Mahal va Bakhtiari-08|"+
		 "Ostan e Gilan-19|"+
		 "Ostan e Golestan-27|"+
		 "Ostan e Hamadan-24|"+
		 "Ostan e Ilam-05|"+
		 "Ostan e Kermanshah-17|"+
		 "Ostan e Khorasan e Jonubi-29|"+
		 "Ostan e Khorasan e Shomali-31|"+
		 "Ostan e Kohgiluyeh va Bowyer Ahmad-18|"+
		 "Ostan e Kordestan-16|"+
		 "Ostan e Lorestan-20|"+
		 "Ostan e Qazvin-28|"+
		 "Ostan e Tehran-07|"+
		 "Qom-26|"+
		 "Razavi Khorasan-30|"+
		 "Semnān-12|"+
		 "Sistan and Baluchestan-13|"+
		 "Yazd-25|"+
		 "Zanjan-11";

s_a["Iraq"]="Anbar-AN|"+
		 "An Najaf-NA|"+
		 "Dhi Qar-DQ|"+
		 "Dihok-DA|"+
		 "Diyālá-DI|"+
		 "Mayorality of Baghdad-BG|"+
		 "Maysan-MA|"+
		 "Muhafazat al Basrah-BA|"+
		 "Muhafazat al Muthanna-MU|"+
		 "Muhafazat Arbil-AR|"+
		 "Muhafazat as Sulaymaniyah-SU|"+
		 "Muhafazat Babil-BB|"+
		 "Muhafazat Karbala'-KA|"+
		 "Muhafazat Kirkuk-TS|"+
		 "Muhafazat Ninawa-NI|"+
		 "Muhafazat Wasit-WA";

s_a["Ireland"]="Co Clare-CE|"+
		 "Connaught-C|"+
		 "County Mayo-MO|"+
		 "Leinster-L|"+
		 "Munster-M|"+
		 "Ulster-U";

s_a["Isle of Man"]="Isle of Man-IM";

s_a["Israel"]="Central District-M|"+
		 "Haifa-HA|"+
		 "Jerusalem-JM|"+
		 "Northern District-Z|"+
		 "Southern District-D|"+
		 "Tel Aviv-TA";

s_a["Italy"]="Abruzzo-65|"+
		 "Aosta Valley-23|"+
		 "Apulia-75|"+
		 "asilicate-77|"+
		 "Calabria-78|"+
		 "Campania-72|"+
		 "Emilia Romagna-45|"+
		 "Friuli Venezia Giulia-36|"+
		 "Latium-62|"+
		 "Liguria-42|"+
		 "Lombardy-25|"+
		 "Molise-67|"+
		 "Piedmont-21|"+
		 "Sardinia-88|"+
		 "Sicily-82|"+
		 "The Marches-57|"+
		 "Trentino Alto Adige-32|"+
		 "Tuscany-52|"+
		 "Umbria-55|"+
		 "Veneto-34";

s_a["Jamaica"]="Kingston-01|"+
		 "Parish of Clarendon-13|"+
		 "Parish of Manchester-12|"+
		 "Parish of Saint Andrew-02|"+
		 "Parish of Saint Ann-06|"+
		 "Parish of Saint Catherine-14|"+
		 "Parish of Saint James-08|"+
		 "Parish of Saint Thomas-03|"+
		 "Parish of Westmoreland-10|"+
		 "St. Elizabeth-11";

s_a["Japan"]="Aichi-23|"+
		 "Akita-05|"+
		 "Aomori-02|"+
		 "Chiba-12|"+
		 "Ehime-38|"+
		 "Fukui-18|"+
		 "Fukuoka-40|"+
		 "Fukushima ken-07|"+
		 "Gifu-21|"+
		 "Gunma-10|"+
		 "Hiroshima-34|"+
		 "Hokkaidō-01|"+
		 "Hyōgo-28|"+
		 "Ibaraki-08|"+
		 "Ishikawa-17|"+
		 "Iwate-03|"+
		 "Kagawa-37|"+
		 "Kagoshima-46|"+
		 "Kanagawa-14|"+
		 "Kōchi-39|"+
		 "Kumamoto-43|"+
		 "Kyōto-26|"+
		 "Mie-24|"+
		 "Miyagi-04|"+
		 "Miyazaki-45|"+
		 "Nagano-20|"+
		 "Nagasaki-42|"+
		 "Nara-29|"+
		 "Niigata-15|"+
		 "Ōita-44|"+
		 "Okayama-33|"+
		 "Okinawa-47|"+
		 "Ōsaka-27|"+
		 "Saga Prefecture-41|"+
		 "Saitama-11|"+
		 "Shiga Prefecture-25|"+
		 "Shimane-32|"+
		 "Shizuoka-22|"+
		 "Tochigi-09|"+
		 "Tokushima-36|"+
		 "Tōkyō-13|"+
		 "Tottori-31|"+
		 "Toyama-16|"+
		 "Wakayama-30|"+
		 "Yamagata-06|"+
		 "Yamaguchi-35|"+
		 "Yamanashi-19";

s_a["Jersey"]="Jersey-JE";

s_a["Jordan"]="Amman-AM|"+
		 "Balqa-BA|"+
		 "Irbid-IR|"+
		 "Jerash-JA|"+
		 "Madaba-MD|"+
		 "Mafraq-MA|"+
		 "Zarqa-AZ";

s_a["Kazakhstan"]="Aktyubinskaya Oblast'-AKT|"+
		 "Almaty Oblysy-ALM|"+
		 "Almaty Qalasy-ALA|"+
		 "Aqmola Oblysy-AKM|"+
		 "Astana Qalasy-AST|"+
		 "Atyrau Oblysy-ATY|"+
		 "Batys Qazaqstan Oblysy-ZAP|"+
		 "East Kazakhstan-VOS|"+
		 "Mangistauskaya Oblast'-MAN|"+
		 "Pavlodar Oblysy-PAV|"+
		 "Qaraghandy Oblysy-KAR|"+
		 "Qostanay Oblysy-KUS|"+
		 "Qyzylorda Oblysy-KZY|"+
		 "Severo Kazakhstanskaya Oblast'-SEV|"+
		 "Yuzhno Kazakhstanskaya Oblast'-YUZ|"+
		 "Zhambyl Oblysy-ZHA";

s_a["Kenya"]="Nairobi Province-110";

s_a["Kiribati"]="Gilbert Islands-G";

s_a["Kosovo"]="Kosovo-XK";

s_a["North Korea"]="Pyongyang-01"

s_a["South Korea"]="Busan-26|"+
		 "ChungcheongbukDo-43|"+
		 "ChungcheongnamDo-44|"+
		 "Daegu-27|"+
		 "Daejeon-30|"+
		 "GangwonDo-42|"+
		 "Gwangju-29|"+
		 "GyeonggiDo-41|"+
		 "GyeongsangbukDo-47|"+
		 "GyeongsangnamDo-48|"+
		 "Incheon-28|"+
		 "JejuDo-49|"+
		 "JeollabukDo-45|"+
		 "JeollanamDo-46|"+
		 "Seoul-11|"+
		 "Ulsan-31";

s_a["Kuwait"]="Al Aḩmadī-AH|"+
		 "Al Asimah-KU|"+
		 "Al Farwaniyah-FA|"+
		 "Muhafazat Hawalli-HA";

s_a["Kyrgyzstan"]="Chuyskaya Oblast'-C|"+
		 "Osh Oblasty-O";

s_a["Laos"]="Vientiane-VT";

s_a["Latvia"]="Ādaži-011|"+
	 	 "Aglona-001|"+
	 	 "Aizkraukles Rajons-002|"+
	 	 "Aizpute-003|"+
	 	 "Aloja-005|"+
	 	 "Alsunga-006|"+
	 	 "Aluksnes Rajons-007|"+
	 	 "Auces Novads-010|"+
	 	 "Babīte-012|"+
	 	 "Baldone-013|"+
	 	 "Balvu Rajons-015|"+
	 	 "Bauskas Rajons-016|"+
	 	 "Brocēni-018|"+
	 	 "Carnikava-020|"+
	 	 "Cesu Rajons-022|"+
	 	 "Cibla-023|"+
	 	 "Daugavpils-025|"+
	 	 "Dobeles Rajons-026|"+
	 	 "Engure-029|"+
	 	 "Grobiņa-032|"+
	 	 "Gulbenes Rajons-033|"+
	 	 "Ikšķile-035|"+
	 	 "Inčukalns-037|"+
	 	 "Jekabpils Municipality-042|"+
	 	 "Jelgava-JEL|"+
	 	 "Jelgavas Rajons-041|"+
	 	 "Jurmala-JUR|"+
	 	 "Kandava-043|"+
	 	 "Ķekava-052|"+
	 	 "Kocēni-045|"+
	 	 "Kraslavas Rajons-047|"+
	 	 "Krimulda-048|"+
	 	 "Kuldigas Rajons-050|"+
	 	 "Lecava-034|"+
	 	 "Lielvārde-053|"+
	 	 "Liepaja-LPX|"+
	 	 "Limbazu Rajons-054|"+
	 	 "Līvāni-056|"+
	 	 "Lubāna-057|"+
	 	 "Ludzas Rajons-058|"+
	 	 "Madona Municipality-059|"+
	 	 "Mālpils-061|"+
	 	 "Mārupe-062|"+
	 	 "Naukšēni-064|"+
	 	 "Nereta-065|"+
	 	 "Ogre-067|"+
	 	 "Olaine-068|"+
	 	 "Ozolnieku Novads-069|"+
	 	 "Preili Municipality-073|"+
	 	 "Rezekne-REZ|"+
	 	 "Riga-RIX|"+
	 	 "Rojas Novads-079|"+
	 	 "Ropazu Novads-080|"+
	 	 "Rugaju Novads-082|"+
	 	 "Rujienas Novads-084|"+
	 	 "Salacgrivas Novads-086|"+
	 	 "Salaspils Novads-087|"+
	 	 "Saldus Municipality-088|"+
	 	 "Siguldas Novads-091|"+
	 	 "Skriveru Novads-092|"+
	 	 "Smiltenes Novads-094|"+
	 	 "Stopinu Novads095|"+
	 	 "Talsi Municipality-097|"+
	 	 "Tukuma Rajons-099|"+
	 	 "Valka Municipality-101|"+
	 	 "Valmiera District-VMR|"+
	 	 "Varaklanu Novads-102|"+
	 	 "Vecumnieku Novads-105|"+
	 	 "Ventspils Municipality-106|"+
	 	 "Viesites Novads-107|"+
	 	 "Vilakas Novads-108|"+
	 	 "Vilanu Novads-109|"+
	 	 "Zilupes Novads-110";

s_a["Lebanon"]="Beyrouth-BA|"+
		 "Mohafazat Aakkar-AK|"+
		 "Mohafazat Baalbek Hermel-BH|"+
		 "Mohafazat Beqaa-BI|"+
		 "Mohafazat Liban Nord-AS|"+
		 "Mohafazat Liban Sud-JA|"+
		 "Mohafazat Mont Liban-JL|"+
		 "Mohafazat Nabatiye-NA";

s_a["Lesotho"]="Maseru-A";

s_a["Liberia"]="Montserrado County-MO";

s_a["Libya"]="Sha`biyat az Zawiyah-ZA|"+
		 "Sha`biyat Banghazi-BA|"+
		 "Sha`biyat Misratah-MI|"+
		 "Sha`biyat Nalut-NL|"+
		 "Sha`biyat Sabha-SB|"+
		 "Tripoli-TB";

s_a["Liechtenstein"]="Balzers-01|"+
		 "Eschen-02|"+
		 "Gemeinde Gamprin-03|"+
		 "Mauren-04|"+
		 "Planken-05|"+
		 "Ruggell-06|"+
		 "Schaan-07|"+
		 "Schellenberg-08|"+
		 "Triesen-09|"+
		 "Triesenberg-10|"+
		 "Vaduz-11";

s_a["Lithuania"]="Alytaus apskritis-AL|"+
		 "Kauno apskritis-KU|"+
		 "Klaipedos apskritis-KL|"+
		 "Marijampoles apskritis-MR|"+
		 "Panevėžys-PN|"+
		 "Siauliu apskritis-SA|"+
		 "Taurages apskritis-TA|"+
		 "Telsiu apskritis-TE|"+
		 "Utenos apskritis-UT|"+
		 "Vilnius County-VL";

s_a["Luxembourg"]="District de Diekirch-D|"+
		 "District de Grevenmacher-G|"+
		 "District de Luxembourg-L";

s_a["Macau"]="Macau-MO";

s_a["Macedonia, Former Yugoslav Republic of"]="Berovo-03|"+
		 "Bitola-04|"+
		 "Bogdanci-05|"+
		 "Bogovinje-06|"+
		 "Čair-79|"+
		 "Debar-21|"+
		 "Demir Hisar-25|"+
		 "Gevgelija-18|"+
		 "Gostivar-19|"+
		 "Kavadarci-36|"+
		 "Kratovo-43|"+
		 "Kriva Palanka-44|"+
		 "Kumanovo-47|"+
		 "Makedonska Kamenica-51|"+
		 "Makedonski Brod-52|"+
		 "Negotino-54|"+
		 "Novo Selo-56|"+
		 "Ohrid-58|"+
		 "Opstina Cucer Sandevo-82|"+
		 "Opstina Delcevo-23|"+
		 "Opstina Dojran-26|"+
		 "Opstina Karpos-38|"+
		 "Opstina Kicevo-40|"+
		 "Opstina Kocani-42|"+
		 "Opstina Krusevo-46|"+
		 "Opstina Pehcevo-60|"+
		 "Opstina Probistip-63|"+
		 "Opstina Radovis-64|"+
		 "Opstina Stip-70|"+
		 "Prilep-62|"+
		 "Resen-66|"+
		 "Struga-71|"+
		 "Strumica-72|"+
		 "Sveti Nikole-69|"+
		 "Tetovo-76|"+
		 "Valandovo-10|"+
		 "Veles-13|"+
		 "Vinica-14";

s_a["Madagascar"]="Madagascar-MG";

s_a["Malawi"]="Central Region-C|"+
		 "Northern Region-N|"+
		 "Southern Region-S";

s_a["Malaysia"]="Johor-01|"+
		 "Kedah-02|"+
		 "Kelantan-03|"+
		 "Kuala Lumpur-14|"+
		 "Labuan-15|"+
		 "Melaka-04|"+
		 "Negeri Sembilan-05|"+
		 "Pahang-06|"+
		 "Penang-07|"+
		 "Perak-08|"+
		 "Perlis-09|"+
		 "Putrajaya-16|"+
		 "Sabah-12|"+
		 "Sarawak-13|"+
		 "Selangor-10|"+
		 "Terengganu-11";

s_a["Maldives"]="Maale-MLE";

//mali
s_a["Mali"]="Bamako Region-BKO|"+
		 "Kayes-1|"+
		 "Sikasso-3";

s_a["Malta"]="Attard-01|"+
		 "Balzan-02|"+
		 "Birkirkara-04|"+
		 "Birzebbuga-05|"+
		 "Bormla-06|"+
		 "Dingli-07|"+
		 "Ghajnsielem-13|"+
		 "Hal Gharghur-15|"+
		 "Hal Ghaxaq-17|"+
		 "Haz Zabbar-64|"+
		 "Haz Zebbug-66|"+
		 "Il Belt Valletta-60|"+
		 "Il Birgu-03|"+
		 "Il Fgura-08|"+
		 "Il Furjana-09|"+
		 "Il Gudja-11|"+
		 "Il Gzira-12|"+
		 "Il Hamrun-18|"+
		 "Il Kalkara-21|"+
		 "Il Marsa-26|"+
		 "Il Mellieha-30|"+
		 "Il Mosta-32|"+
		 "Il Munxar-36|"+
		 "Il Qala-42|"+
		 "Il Qrendi-44|"+
		 "In Nadur-37|"+
		 "In Naxxar-38|"+
		 "Ir Rabat-46|"+
		 "Is Siggiewi-55|"+
		 "Is Swieqi-57|"+
		 "Ix Xaghra-61|"+
		 "Ix Xewkija-62|"+
		 "Ix Xghajra-63|"+
		 "Iz Zebbug-65|"+
		 "Iz Zejtun-67|"+
		 "Iz Zurrieq-68|"+
		 "Kirkop-23|"+
		 "L Gharb-14|"+
		 "L Ghasri-16|"+
		 "Lija-24|"+
		 "L Iklin-19|"+
		 "L Imdina-29|"+
		 "L Imgarr-31|"+
		 "L Imqabba-33|"+
		 "L Imsida-34|"+
		 "L Imtarfa-35|"+
		 "L Isla-20|"+
		 "Luqa-25|"+
		 "Marsaskala-27|"+
		 "Marsaxlokk-28|"+
		 "Paola-39|"+
		 "Qormi-43|"+
		 "Safi-47|"+
		 "Saint John-49|"+
		 "Saint Julian-48|"+
		 "Saint Lawrence-50|"+
		 "Saint Lucia-53|"+
		 "Saint Paul’s Bay-51|"+
		 "Saint Venera-54|"+
		 "Sannat-52|"+
		 "Ta' Kercem-22|"+
		 "Tal Pieta-41|"+
		 "Tarxien-58|"+
		 "Tas Sliema-56|"+
		 "Ta' Xbiex-59|"+
		 "Victoria-45";

s_a["Marshall Islands"]="Majuro Atoll-MAJ"

s_a["Martinique"]="Martinique-MQ";

s_a["Mauritania"]="District de Nouakchott-NKC|"+
		 "Wilaya du Trarza-06";

s_a["Mauritius"]="Black River District-BL|"+
		 "Flacq District-FL|"+
		 "Moka District-MO|"+
		 "Pamplemousses District-PA|"+
		 "Plaines Wilhems District-PW|"+
		 "Port Louis District-PL|"+
		 "Rodrigues-RO|"+
		 "Savanne District-SA";

s_a["Mayotte"]="Mayotte-YT";

s_a["Mexico"]="Aguascalientes-AGU|"+
		 "Baja California Sur-BCS|"+
		 "Campeche-CAM|"+
		 "Chiapas-CHP|"+
		 "Chihuahua-CHH|"+
		 "Coahuila-COA|"+
		 "Colima-COL|"+
		 "Durango-DUR|"+
		 "Estado de Baja California-BCN|"+
		 "Estado de Mexico-MEX|"+
		 "Guanajuato-GUA|"+
		 "Guerrero-GRO|"+
		 "Hidalgo-HID|"+
		 "Jalisco-JAL|"+
		 "Mexico City-DIF|"+
		 "Michoacán-MIC|"+
		 "Morelos-MOR|"+
		 "Nayarit-NAY|"+
		 "Nuevo León-NLE|"+
		 "Oaxaca-OAX|"+
		 "Puebla-PUE|"+
		 "Querétaro-QUE|"+
		 "Quintana Roo-ROO|"+
		 "San Luis Potosí-SLP|"+
		 "Sinaloa-SIN|"+
		 "Sonora-SON|"+
		 "Tabasco-TAB|"+
		 "Tamaulipas-TAM|"+
		 "Tlaxcala-TLA|"+
		 "Veracruz-VER|"+
		 "Yucatán-YUC|"+
		 "Zacatecas-ZAC";

s_a["Micronesia"]="State of Kosrae-KSA|"+
		 "State of Pohnpei-PNI|"+
		 "State of Yap-YAP";

s_a["Moldova"]="Anenii Noi-AN|"+
		 "Basarabeasca-BS|"+
		 "Cahul-CA|"+
		 "Cantemir-CT|"+
		 "Cimişlia-CM|"+
		 "Criuleni-CR|"+
		 "Donduşeni-DO|"+
		 "Drochia-DR|"+
		 "Făleşti-FA|"+
		 "Floreşti-FL|"+
		 "Gagauzia-GA|"+
		 "Hînceşti-HI|"+
		 "Laloveni-IA|"+
		 "Leova-LE|"+
		 "Municipiul Balti-BA|"+
		 "Municipiul Bender-BD|"+
		 "Municipiul Chisinau-CU|"+
		 "Nisporeni-NI|"+
		 "Orhei-OR|"+
		 "Raionul Calarasi-CL|"+
		 "Raionul Causeni-CS|"+
		 "Raionul Dubasari-DU|"+
		 "Raionul Edineţ-ED|"+
		 "Raionul Ocniţa-OC|"+
		 "Raionul Soroca-SO|"+
		 "Raionul Stefan Voda-SV|"+
		 "Rezina-RE|"+
		 "Sîngerei-SI|"+
		 "Şoldăneşti-SD|"+
		 "Strășeni-ST|"+
		 "Taraclia-TA|"+
		 "Teleneşti-TE|"+
		 "Ungheni-UN|"+
		 "Unitatea Teritoriala din Stinga Nistrului-SN";

s_a["Monaco"]="Monaco-MC";

s_a["Mongolia"]="Arhangay Aymag-073|"+
		 "Bayanhongor Aymag-069|"+
		 "Bayan-Olgiy Aymag-071|"+
		 "Bulgan-067|"+
		 "Central Aimak-047|"+
		 "Darhan Uul Aymag-037|"+
		 "Dzavhan Aymag-057|"+
		 "East Aimak-061|"+
		 "East Gobi Aymag-063|"+
		 "Govi Altay Aymag-065|"+
		 "Govi Sumber-064|"+
		 "Hentiy Aymag-039|"+
		 "Hovd-043|"+
		 "Hovsgol Aymag-041|"+
		 "Middle Govĭ-059|"+
		 "Ömnögovĭ-053|"+
		 "Orhon Aymag-035|"+
		 "Övörhangay-055|"+
		 "Selenge Aymag-049|"+
		 "Suhbaatar Aymag-051|"+
		 "Ulaanbaatar Hot-1";

s_a["Montenegro"]="Bijelo Polje-04|"+
		 "Budva-05|"+
		 "Cetinje-06|"+
		 "Herceg Novi-08|"+
		 "Kotor-10|"+
		 "Opstina Niksic-12|"+
		 "Pljevlja-14|"+
		 "Podgorica-16|"+
		 "Tivat-19|"+
		 "Ulcinj-20";

s_a["Montserrat"]="Montserrat-ME";

s_a["Morocco"]="Chaouia Ouardigha-09|"+
		 "Doukkala Abda-10|"+
		 "Gharb Chrarda-Beni Hssen-02|"+
		 "Guelmim Es Semara-14|"+
		 "Laayoune Boujdour Sakia El Hamra-15|"+
		 "Marrakech Tensift Al Haouz-11|"+
		 "Oriental-04|"+
		 "Region de Fes Boulemane-05|"+
		 "Region de Meknes Tafilalet-06|"+
		 "Region de Rabat Sale Zemmour Zaer-07|"+
		 "Region de Souss Massa Draa-13|"+
		 "Region de Tanger Tetouan-01|"+
		 "Region du Grand Casablanca-08|"+
		 "Tadla Azilal-12|"+
		 "Taza Al Hoceima Taounate-03";

s_a["Mozambique"]="Cabo Delgado Province-P|"+
		 "Cidade de Maputo-MPM|"+
		 "Manica Province-B|"+
		 "Maputo Province-L|"+
		 "Nampula-N|"+
		 "Niassa Province-A|"+
		 "Sofala Province-S|"+
		 "Tete-T";

s_a["Myanmar [Burma]"]="Kayin State-13|"+
		 "Magway Region-03|"+
		 "Mandalay Region-04|"+
		 "Shan State-17|"+
		 "Yangon Region-06";

s_a["Namibia"]="Erongo-ER|"+
		 "Hardap-HA|"+
		 "Karas-KA|"+
		 "Khomas-KH|"+
		 "Kunene-KU|"+
		 "Omaheke-OH|"+
		 "Omusati-OS|"+
		 "Oshana-ON|"+
		 "Oshikoto-OT|"+
		 "Otjozondjupa-OD|"+
		 "Zambezi Region-CA";

s_a["Nauru"]="Anabar-02";

s_a["Nepal"]="Central Region-1|"+
		 "Eastern Region-4|"+
		 "Far Western-5|"+
		 "Western Region-3";

s_a["Netherlands"]="Friesland-FR|"+
		 "Groningen-GR|"+
		 "Limburg-LI|"+
		 "North Brabant-NB|"+
		 "North Holland-NH|"+
		 "Provincie Drenthe-DR|"+
		 "Provincie Flevoland-FL|"+
		 "Provincie Gelderland-GE|"+
		 "Provincie Overijssel-OV|"+
		 "Provincie Utrecht-UT|"+
		 "Provincie Zeeland-ZE|"+
		 "South Holland-ZH";

s_a["Netherlands Antilles"]="Netherlands Antilles-AN";

s_a["New Caledonia"]="North Province-N|"+
		 "South Province-S";

s_a["New Zealand"]="Auckland-AUK|"+
		 "Bay of Plenty-BOP|"+
		 "Canterbury-CAN|"+
		 "Chatham Islands-CIT|"+
		 "Gisborne-GIS|"+
		 "Hawke's Bay-HKB|"+
		 "Manawatu Wanganui-MWT|"+
		 "Marlborough-MBH|"+
		 "Nelson-NSN|"+
		 "Northland-NTL|"+
		 "Otago-OTA|"+
		 "Southland-STL|"+
		 "Taranaki-TKI|"+
		 "Tasman-TAS|"+
		 "Waikato-WKO|"+
		 "Wellington-WGN|"+
		 "West Coast-WTC";

s_a["Nicaragua"]="Departamento de Boaco-BO|"+
		 "Departamento de Carazo-CA|"+
		 "Departamento de Chinandega-CI|"+
		 "Departamento de Chontales-CO|"+
		 "Departamento de Esteli-ES|"+
		 "Departamento de Granada-GR|"+
		 "Departamento de Jinotega-JI|"+
		 "Departamento de Leon-LE|"+
		 "Departamento de Madriz-MD|"+
		 "Departamento de Managua-MN|"+
		 "Departamento de Masaya-MS|"+
		 "Departamento de Matagalpa-MT|"+
		 "Departamento de Nueva Segovia-NS|"+
		 "Departamento de Rio San Juan-SJ|"+
		 "Departamento de Rivas-RI|"+
		 "Region Autonoma Atlantico Sur-AS";

s_a["Niger"]="Agadez-1|"+
		 "Niamey-8";

s_a["Nigeria"]="Abia State-AB|"+
		 "Adamawa-AD|"+
		 "Akwa Ibom State-AK|"+
		 "Anambra-AN|"+
		 "Bauchi-BA|"+
		 "Bayelsa State-BY|"+
		 "Benue State-BE|"+
		 "Cross River State-CR|"+
		 "Delta-DE|"+
		 "Ebonyi State-EB|"+
		 "Edo-ED|"+
		 "Ekiti State-EK|"+
		 "Enugu State-EN|"+
		 "Federal Capital Territory-FC|"+
		 "Gombe State-GO|"+
		 "Imo State-IM|"+
		 "Kaduna State-KD|"+
		 "Kano State-KN|"+
		 "Katsina State-KT|"+
		 "Kebbi State-KE|"+
		 "Kogi State-KO|"+
		 "Kwara State-KW|"+
		 "Lagos-LA|"+
		 "Niger State-NI|"+
		 "Ogun State-OG|"+
		 "Ondo State-ON|"+
		 "Osun State-OS|"+
		 "Oyo State-OY|"+
		 "Plateau State-PL|"+
		 "Rivers State-RI|"+
		 "Sokoto State-SO|"+
		 "Taraba State-TA|"+
		 "Yobe State-YO|"+
		 "Zamfara State-ZA";

s_a["Niue"]="Niue-NU";

s_a["Norfolk Island"]="Norfolk Island-NF";

s_a["Northern Mariana Islands"]="Rota-R|"+
		 "Saipan-S|"+
		 "Tinian-T";

s_a["Norway"]="Akershus-02|"+
		 "Aust Agder-09|"+
		 "Buskerud-06|"+
		 "Finnmark Fylke-20|"+
		 "Hedmark-04|"+
		 "Hordaland Fylke-12|"+
		 "More og Romsdal fylke-15|"+
		 "Nordland Fylke-18|"+
		 "Nord Trondelag Fylke-17|"+
		 "Oppland-05|"+
		 "Oslo County-03|"+
		 "Østfold-01|"+
		 "Rogaland Fylke-11|"+
		 "Sogn og Fjordane Fylke-14|"+
		 "Sor-Trondelag Fylke-16|"+
		 "Telemark-08|"+
		 "Troms Fylke-19|"+
		 "Vest-Agder Fylke-10|"+
		 "Vestfold-07";

s_a["Oman"]="Muhafazat ad Dakhiliyah-DA|"+
		 "Muhafazat Masqat-MA|"+
		 "Muhafazat Zufar-ZU";

s_a["Pakistan"]="BAzad Kashmir-JK|"+
		 "Balochistan-BA|"+
		 "Federally Administered Tribal Areas-TA|"+
		 "Gilgit-Baltistan-NA|"+
		 "Islamabad Capital Territory-IS|"+
		 "Khyber Pakhtunkhwa-NW|"+
		 "Punjab-PB|"+
		 "Sindh-SD";

s_a["Palau"]="Palau-PW";

s_a["Palestinian Territories"]="Palestinian Territories-PS"

s_a["Panama"]="Embera Wounaan-EM|"+
		 "Guna Yala-KY|"+
		 "Ngoebe-Bugle-NB|"+
		 "Provincia de Bocas del Toro-1|"+
		 "Provincia de Chiriqui-4|"+
		 "Provincia de Cocle-2|"+
		 "Provincia de Colon-3|"+
		 "Provincia de Herrera-6|"+
		 "Provincia del Darien-5|"+
		 "Provincia de Los Santos-7|"+
		 "Provincia de Panama-8|"+
		 "Provincia de Veraguas-9";

s_a["Papua New Guinea"]="Bougainville-NSA|"+
		 "Central Province-CPM|"+
		 "Chimbu Province-CPK|"+
		 "Eastern Highlands Province-EHG|"+
		 "East New Britain Province-EBR|"+
		 "East Sepik Province-ESW|"+
		 "Enga Province-EPW|"+
		 "Gulf Province-GPK|"+
		 "Madang Province-MPM|"+
		 "Manus Province-MRL|"+
		 "Milne Bay Province-MBA|"+
		 "Morobe Province-MPL|"+
		 "National Capital-NCD|"+
		 "New Ireland-NIK|"+
		 "Northern Province-NPP|"+
		 "Southern Highlands Province-SHM|"+
		 "Western Highlands Province-WHM|"+
		 "Western Province-WPD|"+
		 "West New Britain Province-WBK|"+
		 "West Sepik Province-SAN";

s_a["Paraguay"]="Asuncion-ASU|"+
		 "Departamento Central-11|"+
		 "Departamento de Alto Paraguay-16|"+
		 "Departamento de Boqueron-19|"+
		 "Departamento de Caaguazu-5|"+
		 "Departamento de Caazapa-6|"+
		 "Departamento de Canindeyu-14|"+
		 "Departamento de Itapua-7|"+
		 "Departamento de la Cordillera-3|"+
		 "Departamento del Alto Parana-10|"+
		 "Departamento del Amambay-13|"+
		 "Departamento del Guaira-4|"+
		 "Departamento de Misiones-8|"+
		 "Departamento de Paraguari-9";

s_a["Peru"]="Amazonas-AMA|"+
		 "Ancash-ANC|"+
		 "Apurimac-APU|"+
		 "Arequipa-ARE|"+
		 "Ayacucho-AYA|"+
		 "Cajamarca-CAJ|"+
		 "Callao-CAL|"+
		 "Cusco-CUS|"+
		 "Departamento de Moquegua-MOQ|"+
		 "Huancavelica-HUV|"+
		 "Ica-ICA|"+
		 "Junin-JUN|"+
		 "La Libertad-LAL|"+
		 "Lambayeque-LAM|"+
		 "Lima-LIM|"+
		 "Loreto-LOR|"+
		 "Pasco-PAS|"+
		 "Piura-PIU|"+
		 "Provincia de Lima-LMA|"+
		 "Puno-PUN|"+
		 "Region de Huanuco-HUC|"+
		 "Region de San Martin-SAM|"+
		 "Tacna-TAC|"+
		 "Tumbes-TUM|"+
		 "Ucayali-UCA";

s_a["Philippines"]="Autonomous Region in Muslim Mindanao-14|"+
		 "Bicol-05|"+
		 "Cagayan Valley-02|"+
		 "Calabarzon-40|"+
		 "Caraga-13|"+
		 "Central Luzon-03|"+
		 "Central Visayas-07|"+
		 "Cordillera-15|"+
		 "Davao-11|"+
		 "Eastern Visayas-08|"+
		 "Ilocos-01|"+
		 "Mimaropa-41|"+
		 "National Capital Region-00|"+
		 "Northern Mindanao-10|"+
		 "Province of Cavite-CAV|"+
		 "Province of Ilocos Sur-ILS|"+
		 "Province of Laguna-LAG|"+
		 "Province of La Union-LUN|"+
		 "Province of Negros Oriental-NER|"+
		 "Province of Nueva Ecija-NUE|"+
		 "Province of Pampanga-PAM|"+
		 "Province of Pangasinan-PAN|"+
		 "Province of Rizal-RIZ|"+
		 "Province of South Cotabato-SCO|"+
		 "Province of Tarlac-TAR|"+
		 "Soccsksargen-12|"+
		 "Western Visayas-06|"+
		 "Zamboanga Peninsula-09";

s_a["Pitcaim Islands"]="Pitcaim Islands-PN";

s_a["Poland"]="Greater Poland Voivodeship-WP|"+
		 "Kujawsko Pomorskie-KP|"+
		 "Lesser Poland Voivodeship-MA|"+
		 "Łódź Voivodeship-LD|"+
		 "Lower Silesian Voivodeship-DS|"+
		 "Lublin Voivodeship-LU|"+
		 "Lubusz-LB|"+
		 "Masovian Voivodeship-MZ|"+
		 "Opole Voivodeship-OP|"+
		 "Podlasie-PD|"+
		 "Pomeranian Voivodeship-PM|"+
		 "Silesian Voivodeship-SL|"+
		 "Subcarpathian Voivodeship-PK|"+
		 "Świętokrzyskie-SK|"+
		 "Warmian Masurian Voivodeship-WN|"+
		 "West Pomeranian Voivodeship-ZP";

s_a["Portugal"]="Aveiro-01|"+
		 "Azores-20|"+
		 "Bragança-04|"+
		 "Distrito da Guarda-09|"+
		 "Distrito de Beja-02|"+
		 "Distrito de Braga-03|"+
		 "Distrito de Castelo Branco-05|"+
		 "Distrito de Coimbra-06|"+
		 "Distrito de Evora-07|"+
		 "Distrito de Faro-08|"+
		 "Distrito de Leiria-10|"+
		 "Distrito de Portalegre-12|"+
		 "Distrito de Santarem-14|"+
		 "Distrito de Viana do Castelo-16|"+
		 "Distrito de Vila Real-17|"+
		 "Distrito de Viseu-18|"+
		 "Distrito do Porto-13|"+
		 "Lisbon-11|"+
		 "Madeira-30|"+
		 "Setúbal-15";

s_a["Puerto Rico"]="Puerto Rico-PR";

s_a["Qatar"]="Al Wakrah-WA|"+
		 "Baladiyat ad Dawhah-DA|"+
		 "Baladiyat al Khawr wa adh Dhakhirah-KH|"+
		 "Baladiyat ar Rayyan-RA|"+
		 "Baladiyat ash Shamal-MS|"+
		 "Baladiyat az Za`ayin-ZA|"+
		 "Baladiyat Umm Salal-US";

s_a["Reunion"]="Reunion-RE";

s_a["Romainia"]="Arad-AR|"+
		 "Bihor-BH|"+
		 "Bucuresti-B|"+
		 "Constanta-CT|"+
		 "Covasna-CV|"+
		 "Dolj-DJ|"+
		 "Giurgiu-GR|"+
		 "Gorj-GJ|"+
		 "Harghita-HR|"+
		 "Hunedoara-HD|"+
		 "Ilfov-IF|"+
		 "Judetul Alba-AB|"+
		 "Judetul Arges-AG|"+
		 "Judetul Bacau-BC|"+
		 "Judetul Bistrita Nasaud-BN|"+
		 "Judetul Botosani-BT|"+
		 "Judetul Braila-BR|"+
		 "Judetul Brasov-BV|"+
		 "Judetul Buzau-BZ|"+
		 "Judetul Calarasi-CL|"+
		 "Judetul Caras Severin-CS|"+
		 "Judetul Cluj-CJ|"+
		 "Judetul Dambovita-DB|"+
		 "Judetul Galati-GL|"+
		 "Judetul Ialomita-IL|"+
		 "Judetul Iasi-IS|"+
		 "Judetul Mehedinti-MH|"+
		 "Judetul Mures-MS|"+
		 "Judetul Neamt-NT|"+
		 "Judetul Salaj-SJ|"+
		 "Judetul Sibiu-SB|"+
		 "Judetul Timis-TM|"+
		 "Judetul Valcea-VL|"+
		 "Maramureş-MM|"+
		 "Olt-OT|"+
		 "Prahova-PH|"+
		 "Satu Mare-SM|"+
		 "Suceava-SV|"+
		 "Teleorman-TR|"+
		 "Tulcea-TL|"+
		 "Vaslui-VS|"+
		 "Vrancea-VN";

s_a["Russia"]="Altai Krai-ALT|"+
		 "Amurskaya Oblast'-AMU|"+
		 "Arkhangelskaya-ARK|"+
		 "Astrakhanskaya Oblast'-AST|"+
		 "Bashkortostan-BA|"+
		 "Belgorodskaya Oblast'-BEL|"+
		 "Bryanskaya Oblast'-BRY|"+
		 "Chechnya-CE|"+
		 "Chelyabinsk-CHE|"+
		 "Chukotskiy Avtonomnyy Okrug-CHU|"+
		 "Chuvashia-CU|"+
		 "Dagestan-DA|"+
		 "Irkutskaya Oblast'-IRK|"+
		 "Ivanovskaya Oblast'-IVA|"+
		 "Jewish Autonomous Oblast-YEV|"+
		 "Kabardino-Balkarskaya Respublika-KB|"+
		 "Kaliningradskaya Oblast'-KGD|"+
		 "Kalmykiya-KL|"+
		 "Kaluzhskaya Oblast'-KLU|"+
		 "Kamtchatski Kray-KAM|"+
		 "Karachayevo Cherkesiya-KC|"+
		 "Kemerovskaya Oblast'-KEM|"+
		 "Khabarovsk Krai-KHA|"+
		 "Khanty Mansiyskiy Avtonomnyy Okrug-Yugra-KHM|"+
		 "Kirovskaya Oblast'-KIR|"+
		 "Komi Republic-KO|"+
		 "Kostromskaya Oblast'-KOS|"+
		 "Krasnodarskiy Kray-KDA|"+
		 "Krasnoyarskiy Kray-KYA|"+
		 "Kurganskaya Oblast'-KGN|"+
		 "Kurskaya Oblast'-KRS|"+
		 "Leningrad-LEN|"+
		 "Lipetskaya Oblast'-LIP|"+
		 "Magadanskaya Oblast'-MAG|"+
		 "Moscow-MOW|"+
		 "Moskovskaya-MOS|"+
		 "Murmansk-MUR|"+
		 "Nenetskiy Avtonomnyy Okrug-NEN|"+
		 "Nizhegorodskaya Oblast'-NIZ|"+
		 "North Ossetia-SE|"+
		 "Novgorodskaya Oblast'-NGR|"+
		 "Novosibirskaya Oblast'-NVS|"+
		 "Omskaya Oblast'-OMS|"+
		 "Orenburgskaya Oblast'-ORE|"+
		 "Orlovskaya Oblast'-ORL|"+
		 "Penzenskaya Oblast'-PNZ|"+
		 "Perm Krai-PER|"+
		 "Primorskiy Kray-PRI|"+
		 "Pskovskaya Oblast'-PSK|"+
		 "Republic of Karelia-KR|"+
		 "Respublika Adygeya-AD|"+
		 "Respublika Altay-AL|"+
		 "Respublika Buryatiya-BU|"+
		 "Respublika Ingushetiya-IN|"+
		 "Respublika Khakasiya-KK|"+
		 "Respublika Mariy-El-ME|"+
		 "Respublika Mordoviya-MO|"+
		 "Respublika Sakha (Yakutiya)-SA|"+
		 "Respublika Tyva-TY|"+
		 "Rostov-ROS|"+
		 "Ryazanskaya Oblast'-RYA|"+
		 "Sakhalinskaya Oblast'-SAK|"+
		 "Samarskaya Oblast'-SAM|"+
		 "Saratovskaya Oblast'-SAR|"+
		 "Smolenskaya Oblast'-SMO|"+
		 "Stavropol'skiy Kray-STA|"+
		 "St. Petersburg-SPE|"+
		 "Sverdlovskaya Oblast'-SVE|"+
		 "Tambovskaya Oblast'-TAM|"+
		 "Tatarstan-TA|"+
		 "Tomskaya Oblast'-TOM|"+
		 "Transbaikal Territory-ZAB|"+
		 "Tul'skaya Oblast'-TUL|"+
		 "Tverskaya Oblast'-TVE|"+
		 "Tyumenskaya Oblast'-TYU|"+
		 "Udmurtskaya Respublika-UD|"+
		 "Ulyanovsk Oblast-ULY|"+
		 "Vladimirskaya Oblast'-VLA|"+
		 "Volgogradskaya Oblast'-VGG|"+
		 "Vologodskaya Oblast'-VLG|"+
		 "Voronezhskaya Oblast'-VOR|"+
		 "Yamalo Nenetskiy Avtonomnyy Okrug-YAN|"+
		 "Yaroslavskaya Oblast'-YAR";

s_a["Rwanda"]="Kigali-01";

s_a["Saint Bartelemey"]="Saint Bartelemey-BL";

s_a["Saint Helena"]="Saint Helena-HL";

s_a["Saint Kitts and Nevis"]="CSaint George Basseterre-03|"+
		 "Saint Mary Cayon-08|"+
		 "Saint Paul Charlestown-10";

s_a["Saint Lucia"]="Anse la Raye-AR|"+
		 "Castries Quarter-CA|"+
		 "Choiseul Quarter-CH|"+
		 "Gros Islet-GI|"+
		 "Soufriere-SO|"+
		 "Vieux Fort-VF";

s_a["Saint Martin"]="Saint Martin-MF";

s_a["Saint Pierre and Miquelon"]="Commune de Miquelon Langlade-M|"+
		 "Commune de Saint Pierre-P";

s_a["Saint Vincent and the Grenadines"]="Grenadines-06|"+
		 "Parish of Charlotte-01|"+
		 "Parish of Saint George-04";

s_a["Samoa"]="Tuamasaga-TU";

s_a["San Marino"]="Castello di Acquaviva-01|"+
		 "Castello di Borgo Maggiore-06|"+
		 "Castello di Domagnano-03|"+
		 "Castello di San Marino Citta-07|"+
		 "Serravalle-09";

s_a["São Tomé and Príncipe"]="São Tomé Island-S";

s_a["Saudi Arabia"]="Al Jawf-12|"+
		 "Al Madinah al Munawwarah-03|"+
		 "Al-Qassim-05|"+
		 "Ar Riyāḑ-01|"+
		 "Eastern Province-04|"+
		 "Jizan-09|"+
		 "Makkah Province-02|"+
		 "Mintaqat al Bahah-11|"+
		 "Mintaqat `Asir-14|"+
		 "Mintaqat Ha'il-06|"+
		 "Mintaqat Najran-10|"+
		 "Mintaqat Tabuk-07|"+
		 "Northern Borders-08";

s_a["Senegal"]="Dakar-DK|"+
		 "Fatick-FK|"+
		 "Kaolack-KL|"+
		 "Kolda-KD|"+
		 "Louga-LG|"+
		 "Region de Kaffrine-KA|"+
		 "Region de Kedougou-KE|"+
		 "Region de Sedhiou-SE|"+
		 "Saint Louis-SL|"+
		 "Tambacounda-TC|"+
		 "Ziguinchor-ZG";

s_a["Serbia"]="Autonomna Pokrajina Vojvodina-VO"

s_a["Seychelles"]="Beau Vallon-08|"+
		 "English River-16|"+
		 "Takamaka-23";

s_a["Sierra Leone"]="Western Area-W";

s_a["Singapore"]="Singapore-SG";

s_a["Sint Maarten"]="Sint Maarten-SX";

s_a["Slovakia"]="Banskobystricky kraj-BC|"+
		 "Bratislavsky kraj-BL|"+
		 "Kosicky kraj-KI|"+
		 "Nitriansky kraj-NI|"+
		 "Presovsky kraj-PV|"+
		 "Trenciansky kraj-TC|"+
		 "Trnavsky kraj-TA|"+
		 "Zilinsky kraj-ZI";


s_a["Slovenia"]="Beltinci-002|"+
		 "Benedikt-148|"+
		 "Bloke-150|"+
		 "Bohinj-004|"+
		 "Borovnica-005|"+
		 "Brda-007|"+
		 "Brezovica-008|"+
		 "Celje-011|"+
		 "Cerknica-013|"+
		 "Cerkvenjak-153|"+
		 "Destrnik-018|"+
		 "Dobrova-Polhov Gradec-021|"+
		 "Dobrovnik-156|"+
		 "Dolenjske Toplice-157|"+
		 "Dol pri Ljubljani-022|"+
		 "Dornava-024|"+
		 "Dravograd-025|"+
		 "Duplek-026|"+
		 "Gorenja Vas-Poljane-027|"+
		 "Gorje-207|"+
		 "Gornja Radgona-029|"+
		 "Gornji Grad-030|"+
		 "Gornji Petrovci-031|"+
		 "Grosuplje-032|"+
		 "Hajdina-159|"+
		 "Horjul-162|"+
		 "Hrastnik-034|"+
		 "Idrija-036|"+
		 "Ig-037|"+
		 "Ilirska Bistrica-038|"+
		 "Izola-040|"+
		 "Jesenice-041|"+
		 "Kamnik-043|"+
		 "Kanal-044|"+
		 "Kobilje-047|"+
		 "Komen-049|"+
		 "Komenda-164|"+
		 "Koper-050|"+
		 "Kostanjevica na Krki-197|"+
		 "Kozje-051|"+
		 "Kranj-052|"+
		 "Kranjska Gora-053|"+
		 "Kungota-055|"+
		 "Lenart-058|"+
		 "Lendava-059|"+
		 "Litija-060|"+
		 "Ljubljana-061|"+
		 "Ljubno-062|"+
		 "Ljutomer-063|"+
		 "Logatec-064|"+
		 "Log–Dragomer-208|"+
		 "Lovrenc na Pohorju-167|"+
		 "Lukovica-068|"+
		 "Makole-198|"+
		 "Maribor-070|"+
		 "Markovci-168|"+
		 "Medvode-071|"+
		 "Mestna Obcina Novo mesto-085|"+
		 "Miren (Kostanjevica)-075|"+
		 "Mislinja-076"+"Mokronog (Trebelno)-199|"+
		 "Moravske Toplice-078|"+
		 "Mozirje-079|"+
		 "Murska Sobota-080|"+
		 "Muta-081|"+
		 "Naklo-082|"+
		 "Nazarje-083|"+
		 "Nova Gorica-084|"+
		 "Obcina Ajdovscina-001|"+
		 "Obcina Bled-003|"+
		 "Obcina Bovec-006|"+
		 "Obcina Braslovce-151|"+
		 "Obcina Brezice-009|"+
		 "Obcina Crensovci-015|"+
		 "Obcina Crna na Koroskem-016|"+
		 "Obcina Crnomelj-017|"+
		 "Obcina Divaca-019|"+
		 "Obcina Domzale-023|"+
		 "Obcina Gorisnica-028|"+
		 "Obcina Hoce-Slivnica-160|"+
		 "Obcina Ivancna Gorica-039|"+
		 "Obcina Kidricevo-045|"+
		 "Obcina Kocevje-048|"+
		 "Obcina Krizevci-166|"+
		 "Obcina Krsko-054|"+
		 "Obcina Lasko-057|"+
		 "Obcina Loska Dolina-065|"+
		 "Obcina Majsperk-069|"+
		 "Obcina Menges-072|"+
		 "Obcina Mezica-074|"+
		 "Obcina Miklavz na Dravskem Polju-169|"+
		 "Obcina Mirna Pec-170|"+
		 "Obcina Moravce-077|"+
		 "Obcina Ormoz-087|"+
		 "Obcina Poljcane-200|"+
		 "Obcina Race-Fram-098|"+
		 "Obcina Radece-099|"+
		 "Obcina Ravne na Koroskem-103|"+
		 "Obcina Recica ob Savinji-209|"+
		 "Obcina Rence-Vogrsko-201|"+
		 "Obcina Rogaska Slatina-106|"+
		 "Obcina Rogasovci-105|"+
		 "Obcina Ruse-108|"+
		 "Obcina Semic-109|"+
		 "Obcina Sempeter-Vrtojba-183|"+
		 "Obcina Sencur-117|"+
		 "Obcina Sentilj-118|"+
		 "Obcina Sentjernej-119|"+
		 "Obcina Sentjur-120|"+
		 "Obcina Sezana-111|"+
		 "Obcina Skocjan-121|"+
		 "Obcina Skofljica-123|"+
		 "Obcina Smarje pri Jelsah-124|"+
		 "Obcina Smarjeske Toplice-206|"+
		 "Obcina Smartno ob Paki-125|"+
		 "Obcina Smartno pri Litiji-194|"+
		 "Obcina Sodrazica-179|"+
		 "Obcina Sostanj-126|"+
		 "Obcina Sredisce ob Dravi-202|"+
		 "Obcina Starse-215|"+
		 "Obcina Store-127|"+
		 "Obcina Straza-203|"+
		 "Obcina Sveti Jurij ob Scavnici-116|"+
		 "Obcina Tisina-010|"+
		 "Obcina Tolmin-128|"+
		 "Obcina Trzic-131|"+
		 "Obcina Turnisce-132|"+
		 "Obcina Velike Lasce-134|"+
		 "Obcina Verzej-188|"+
		 "Obcina Zalec-190|"+
		 "Obcina Zavrc-143|"+
		 "Obcina Zelezniki-146|"+
		 "Obcina Ziri-147|"+
		 "Obcina Zirovnica-192|"+
		 "Obcina Zrece-144|"+
		 "Odranci-086|"+
		 "Oplotnica-171|"+
		 "Pesnica-089|"+
		 "Piran-090|"+
		 "Podlehnik-172|"+
		 "Podvelka-093|"+
		 "Polzela-173|"+
		 "Postojna-094|"+
		 "Prebold-174|"+
		 "Preddvor-095|"+
		 "Prevalje-175|"+
		 "Ptuj-096|"+
		 "Puconci-097|"+
		 "Radenci-100|"+
		 "Radlje ob Dravi-101|"+
		 "Radovljica-102|"+
		 "Ribnica-104|"+
		 "Selnica ob Dravi-178|"+
		 "Sevnica-110|"+
		 "Škofja Loka-122|"+
		 "Slovenj Gradec-112|"+
		 "Slovenska Bistrica-113|"+
		 "Slovenske Konjice-114|"+
		 "Sveta Ana-181|"+
		 "Sveta Trojica v Slovenskih Goricah-204|"+
		 "Sveti Jurij v Slovenskih Goricah-210|"+
		 "Trbovlje-129|"+"Trebnje-130|"+
		 "Trzin-186|"+
		 "Velenje-133|"+
		 "Videm-135|"+
		 "Vipava-136|"+
		 "Vodice-138|"+
		 "Vojnik-139|"+
		 "Vrhnika-140|"+
		 "Vuzenica-141|"+
		 "Zagorje ob Savi-142";

s_a["Solomon Islands"]="Guadalcanal Province-GU";

s_a["Somalia"]="Banaadir-BN|"+
		 "Woqooyi Galbeed-WO";

s_a["South Africa"]="Eastern Cape-EC|"+
		 "Gauteng-GP|"+
		 "KwaZulu-Natal-ZN|"+
		 "Limpopo-LP|"+
		 "Mpumalanga-MP|"+
		 "Northern Cape-NC|"+
		 "Orange Free State-FS|"+
		 "Province of North West-NW|"+
		 "Province of the Western Cape-WC";


s_a["South Georgia and South Sandwich Islands"]="South Georgia and South Sandwich Islands-GS";

s_a["South Sudan"]= "Central Equatoria-EC";

s_a["Spain"]="Andalusia-AN|"+
		 "Aragon-AR|"+
		 "Balearic Islands-IB|"+
		 "Basque Country-PV|"+
		 "Canary Islands-CN|"+
		 "Cantabria-CB|"+
		 "Castille and León-CL|"+
		 "Castille-La Mancha-CM|"+
		 "Catalonia-CT|"+
		 "Ceuta-CE|"+
		 "Extremadura-EX|"+
		 "Galicia-GA|"+
		 "La Rioja-RI|"+
		 "Madrid-MD|"+
		 "Melilla-ML|"+
		 "Murcia-MC|"+
		 "Navarre-NC|"+
		 "Principality of Asturias-AS|"+
		 "Valencia-VC";

s_a["Sri Lanka"]="Central Province-2|"+
		 "Eastern Province-5|"+
		 "North Central Province-7|"+
		 "Province of Sabaragamuwa-9|"+
		 "Southern Province-3|"+
		 "Western Province-1";

s_a["Sudan"]="Al Jazīrah-GZ|"+
		 "Blue Nile-NB|"+
		 "Kassala-KA|"+
		 "Khartoum-KH|"+
		 "Northern Darfur-DN|"+
		 "River Nile-NR|"+
		 "Southern Darfur-DS|"+
		 "Southern Kordofan-KS";

s_a["Suriname"]="Distrikt Brokopondo-BR|"+
		 "Distrikt Commewijne-CM|"+
		 "Distrikt Coronie-CR|"+
		 "Distrikt Marowijne-MA|"+
		 "Distrikt Nickerie-NI|"+
		 "Distrikt Para-PR|"+
		 "Distrikt Paramaribo-PM|"+
		 "Distrikt Saramacca-SA|"+
		 "Distrikt Sipaliwini-SI";

s_a["Svalbard and Jan Mayan"]="Svalbard-21";

s_a["Swaziland"]="Hhohho District-HH|"+
		 "Manzini District-MA";

s_a["Sweden"]="Blekinge-K|"+
		 "Dalarna-W|"+
		 "Gävleborg-X|"+
		 "Gotland-I|"+
		 "Halland-N|"+
		 "Jämtland-Z|"+
		 "Jönköping-F|"+
		 "Kalmar-H|"+
		 "Kronoberg-G|"+
		 "Norrbotten-BD|"+
		 "Örebro-T|"+
		 "Östergötland-E|"+
		 "Skåne-M|"+
		 "Södermanland-D|"+
		 "Stockholm-AB|"+
		 "Uppsala-C|"+
		 "Värmland-S|"+
		 "Västerbotten-AC|"+
		 "Västernorrland-Y|"+
		 "Västmanland-U|"+
		 "Västra Götaland-O";

s_a["Switzerland"]="Aargau-AG|"+
		 "Appenzell Ausserrhoden-AR|"+
		 "Appenzell Innerrhoden-AI|"+
		 "Basel-City-BS|"+
		 "Basel-Landschaft-BL|"+
		 "Bern-BE|"+
		 "Fribourg-FR|"+
		 "Geneva-GE|"+
		 "Glarus-GL|"+
		 "Grisons-GR|"+
		 "Jura-JU|"+
		 "Lucerne-LU|"+
		 "Neuchâtel-NE|"+
		 "Nidwalden-NW|"+
		 "Obwalden-OW|"+
		 "Saint Gallen-SG|"+
		 "Schaffhausen-SH|"+
		 "Schwyz-SZ|"+
		 "Solothurn-SO|"+
		 "Thurgau-TG|"+
		 "Ticino-TI|"+
		 "Uri-UR|"+
		 "Valais-VS|"+
		 "Vaud-VD|"+
		 "Zug-ZG|"+
		 "Zurich-ZH";

s_a["Syria"]="Aleppo Governorate-HL|"+
		 "AsSuwayda Governorate-SU|"+
		 "Damascus Governorate-DI|"+
		 "Hama Governorate-HM|"+
		 "Latakia Governorate-LA|"+
		 "Quneitra Governorate-QU|"+
		 "Tartus Governorate-TA";

s_a["Taiwan"]="Taiwan-TW";

s_a["Tajikistan"]="Gorno-Badakhshan-GB|"+
		 "Viloyati Sughd-SU";

s_a["Tanzania"]="Arusha-01|"+
		 "Dar es Salaam Region-02|"+
		 "Dodoma-03|"+
		 "Kagera-05|"+
		 "Kigoma-08|"+
		 "Kilimanjaro-09|"+
		 "Mbeya-14|"+
		 "Morogoro-16|"+
		 "Mtwara-17|"+
		 "Mwanza-18|"+
		 "Tanga-25<|"+
		 "Zanzibar Urban/West-15";

s_a["Thailand"]="Bangkok-10|"+
		 "Changwat Amnat Charoen-37|"+
		 "Changwat Buriram-31|"+
		 "Changwat Chachoengsao-24|"+
		 "Changwat Chai Nat-18|"+
		 "Changwat Chaiyaphum-36|"+
		 "Changwat Chanthaburi-22|"+
		 "Changwat Chiang Rai-57|"+
		 "Changwat Chon Buri-20|"+
		 "Changwat Chumphon-86|"+
		 "Changwat Kalasin-46|"+
		 "Changwat Kamphaeng Phet-62|"+
		 "Changwat Kanchanaburi-71|"+
		 "Changwat Khon Kaen-40|"+
		 "Changwat Krabi-81|"+
		 "Changwat Lampang-52|"+
		 "Changwat Lamphun-51|"+
		 "Changwat Loei-42|"+
		 "Changwat Lop Buri-16|"+
		 "Changwat Mae Hong Son-58|"+
		 "Changwat Maha Sarakham-44|"+
		 "Changwat Mukdahan-49|"+
		 "Changwat Nakhon Nayok-26|"+
		 "Changwat Nakhon Pathom-73|"+
		 "Changwat Nakhon Ratchasima-30|"+
		 "Changwat Nakhon Sawan-60|"+
		 "Changwat Nakhon Si Thammarat-80|"+
		 "Changwat Nan-55|"+
		 "Changwat Narathiwat-96|"+
		 "Changwat Nong Bua Lamphu-39|"+
		 "Changwat Nong Khai-43|"+
		 "Changwat Nonthaburi-12|"+
		 "Changwat Pathum Thani-13|"+
		 "Changwat Pattani-94|"+
		 "Changwat Phangnga-82|"+
		 "Changwat Phatthalung-93|"+
		 "Changwat Phayao-56|"+
		 "Changwat Phetchabun-67|"+
		 "Changwat Phetchaburi-76|"+
		 "Changwat Phichit-66|"+
		 "Changwat Phitsanulok-65|"+
		 "Changwat Phrae-54|"+
		 "Changwat Phra Nakhon Si Ayutthaya-14|"+
		 "Changwat Prachin Buri-25|"+
		 "Changwat Prachuap Khiri Khan-77|"+
		 "Changwat Ranong-85|"+
		 "Changwat Ratchaburi-70|"+
		 "Changwat Rayong-21|"+
		 "Changwat Roi Et-45|"+
		 "Changwat Sa Kaeo-27|"+
		 "Changwat Sakon Nakhon-47|"+
		 "Changwat Samut Prakan-11|"+
		 "Changwat Samut Sakhon-74|"+
		 "Changwat Samut Songkhram-75|"+
		 "Changwat Sara Buri-19|"+
		 "Changwat Satun-91|"+
		 "Changwat Sing Buri-17|"+
		 "Changwat Sisaket-33|"+
		 "Changwat Songkhla-90|"+
		 "Changwat Sukhothai-64|"+
		 "Changwat Suphan Buri-72|"+
		 "Changwat Surat Thani-84|"+
		 "Changwat Surin-32|"+
		 "Changwat Tak-63|"+
		 "Changwat Trang-92|"+
		 "Changwat Trat-23|"+
		 "Changwat Ubon Ratchathani-34|"+
		 "Changwat Udon Thani-41|"+
		 "Changwat Uthai Thani-61|"+
		 "Changwat Uttaradit-53|"+
		 "Changwat Yala-95|"+
		 "Changwat Yasothon-35|"+
		 "Chiang Mai Province-50|"+
		 "Phuket-83";

s_a["Timor-Leste"]="Dili-DI";

//togo
s_a["Togo"]="Maritime-M|"+
		 "Savanes-S";

s_a["Tokelau"]="Nukunonu-N";

s_a["Tonga"]="Tongatapu-04|"+
		 "Vava`u-05";

s_a["Trinidad and Tobago"]="Borough of Arima-ARI|"+
		 "Chaguanas-CHA|"+
		 "City of Port of Spain-POS|"+
		 "City of San Fernando-SFO|"+
		 "Couva-Tabaquite-Talparo-CTT|"+
		 "Diego Martin-DMN|"+
		 "Eastern Tobago-ETO|"+
		 "Mayaro-RCM|"+
		 "Penal/Debe-PED|"+
		 "Point Fortin-PTF|"+
		 "Princes Town-PRT|"+
		 "Sangre Grande-SGE|"+
		 "San Juan/Laventille-SJL|"+
		 "Siparia-SIP|"+
		 "Tobago-WTO|"+
		 "Tunapuna/Piarco-TUP";

s_a["Tunisia"]="Gafsa-71|"+
		 "Gouvernorat de Beja-31|"+
		 "Gouvernorat de Ben Arous-13|"+
		 "Gouvernorat de Bizerte-23|"+
		 "Gouvernorat de Gabes-81|"+
		 "Gouvernorat de Jendouba-32|"+
		 "Gouvernorat de Kairouan-41|"+
		 "Gouvernorat de Kasserine-42|"+
		 "Gouvernorat de Kebili-73|"+
		 "Gouvernorat de Kef-33|"+
		 "Gouvernorat de l'Ariana-12|"+
		 "Gouvernorat de Mahdia-53|"+
		 "Gouvernorat de Monastir-52|"+
		 "Gouvernorat de Nabeul-21|"+
		 "Gouvernorat de Sfax-61|"+
		 "Gouvernorat de Sidi Bouzid-43|"+
		 "Gouvernorat de Siliana-34|"+
		 "Gouvernorat de Sousse-51|"+
		 "Gouvernorat de Tozeur-72|"+
		 "Gouvernorat de Tunis-11|"+
		 "Gouvernorat de Zaghouan-22|"+
		 "Manouba-14|"+
		 "Tataouine-83";

s_a["Turkey"]="Adana-01|"+
		 "Adiyaman-02|"+
		 "Afyonkarahisar-03|"+
		 "Ağrı-04|"+
		 "Aksaray-68|"+
		 "Amasya-05|"+
		 "Ankara-06|"+
		 "Antalya-07|"+
		 "Ardahan-75|"+
		 "Artvin-08|"+
		 "Aydın-09|"+
		 "Balıkesir-10|"+
		 "Bartın-74|"+
		 "Batman-72|"+
		 "Bayburt-69|"+
		 "Bilecik-11|"+
		 "Bingöl-12|"+
		 "Bitlis-13|"+
		 "Bolu-14|"+
		 "Burdur-15|"+
		 "Bursa-16|"+
		 "Çanakkale-17|"+
		 "Çankırı-18|"+
		 "Çorum-19|"+
		 "Denizli-20|"+
		 "Diyarbakir-21|"+
		 "Duezce-81|"+
		 "Edirne-22|"+
		 "Elazığ-23|"+
		 "Erzincan-24|"+
		 "Erzurum-25|"+
		 "Eskişehir-26|"+
		 "Gaziantep-27|"+
		 "Giresun-28|"+
		 "Guemueshane-29|"+
		 "Hakkâri-30|"+
		 "Hatay-31|"+
		 "Iğdır-76|"+
		 "Isparta-32|"+
		 "Istanbul-34|"+
		 "Izmir-35|"+
		 "Kahramanmaraş-46|"+
		 "Karabuek-78|"+
		 "Karaman-70|"+
		 "Kars-36|"+
		 "Kastamonu-37|"+
		 "Kayseri-38|"+
		 "Kilis-79|"+
		 "Kırıkkale-71|"+
		 "Kırklareli-39|"+
		 "Kırşehir-40|"+
		 "Kocaeli-41|"+
		 "Konya-42|"+
		 "Kütahya-43|"+
		 "Malatya-44|"+
		 "Manisa-45|"+
		 "Mardin-47|"+
		 "Mersin-33|"+
		 "Muğla-48|"+
		 "Muş-49|"+
		 "Nevsehir-50|"+
		 "Nigde-51|"+
		 "Ordu-52|"+
		 "Osmaniye-80|"+
		 "Rize-53|"+
		 "Sakarya-54|"+
		 "Samsun-55|"+
		 "Şanlıurfa-63|"+
		 "Siirt-56|"+
		 "Sinop-57|"+
		 "Şırnak-73|"+
		 "Sivas-58|"+
		 "Tekirdağ-59|"+
		 "Tokat-60|"+
		 "Trabzon-61|"+
		 "Tunceli-62|"+
		 "Uşak-64|"+
		 "Van-65|"+
		 "Yalova-77|"+
		 "Yozgat-66|"+
		 "Zonguldak-67";

s_a["Turkmenistan"]="Ahal-A";

s_a["Turks and Caico Islands"]="Turks and Caico Islands-TC"

s_a["Tuvalu"]="Funafuti-FUN";

s_a["Uganda"]="Central Region-C|"+
		 "Western Region-W";

s_a["Ukraine"]="Cherkas'ka Oblast'-71|"+
     	 "Chernihiv-74|"+
     	 "Chernivtsi-77|"+
     	 "Crimea-43|"+
     	 "Dnipropetrovska Oblast'-12|"+
     	 "Donets'ka Oblast'-14|"+
     	 "Ivano-Frankivs'ka Oblast'-26|"+
     	 "Kharkivs'ka Oblast'-63|"+
     	 "Khersons'ka Oblast'-65|"+
     	 "Khmel'nyts'ka Oblast'-68|"+
     	 "Kirovohrads'ka Oblast'-35|"+
     	 "Kyiv City-30|"+"Kyiv Oblast-32|"+
     	 "Luhans'ka Oblast'-09|"+
     	 "L'vivs'ka Oblast'-46|"+
     	 "Misto Sevastopol'-40|"+
     	 "Mykolayivs'ka Oblast'-48|"+
     	 "Odessa-51|"+
     	 "Poltavs'ka Oblast'-53|"+
     	 "Rivnens'ka Oblast'-56|"+
     	 "Sums'ka Oblast'-59|"+
     	 "Ternopil's'ka Oblast'-61|"+
     	 "Vinnyts'ka Oblast'-05|"+
     	 "Volyns'ka Oblast'-07|"+
     	 "Zakarpattia Oblast-21|"+
     	 "Zaporizhia-23|"+
     	 "Zhytomyrs'ka Oblast'-18";

s_a["United Arab Emirates"]="Abu Dhabi-AZ|"+
		 "Ajman-AJ|"+
		 "Al Fujayrah-FU|"+
		 "Ash Shariqah-SH|"+
		 "Dubai-DU|"+
		 "Ra's al Khaymah-RK|"+
		 "Umm al Quwain-UQ";

s_a["United Kingdom"]="Cambridgeshire-CAM|"+
		 "City of London-LND|"+
		 "Cornwall-CON|"+
		 "England-ENG|"+
		 "Gloucestershire-GLS|"+
		 "Hampshire-HAM|"+
		 "Kent-KEN|"+
		 "Northern Ireland-NIR|"+
		 "Oxfordshire-OXF|"+
		 "Richmond upon Thames-RIC|"+
		 "Scotland-SCT|"+
		 "Somerset-SOM|"+
		 "Surrey-SRY|"+
		 "Wales-WLS|"+
		 "West Sussex-WSX"


s_a["United States Minor Outlying Islands"]="United States Minor Outlying Islands-UM"

s_a["Uruguay"]="Canelones-CA|"+
		 "Cerro Largo-CL|"+
		 "Colonia-CO|"+
		 "Departamento de Flores-FS|"+
		 "Departamento de Montevideo-MO|"+
		 "Departamento de Paysandu-PA|"+
		 "Departamento de Rio Negro-RN|"+
		 "Departamento de Rocha-RO|"+
		 "Departamento de Salto-SA|"+
		 "Departamento de Tacuarembo-TA|"+
		 "lorida-FD|"+
		 "Lavalleja-LA|"+
		 "Maldonado-MA|"+
		 "Soriano-SO";

//united states
s_a["United States"]="Alabama-AL|" +
         "Alaska-AK|" +
         "Arizona-AR|" +
         "Arkansas-AZ|"+ 
         "California-CA|" +
         "Colorado-CO|" + 
         "Connecticut-CT|"+
         "Delaware-DE|" +
         "District of Columbia-DC|"+ 
         "Florida-FL|" +
         "Georgia-GA|"+
         "Hawaii-HI|"+
         "Idaho-IA|" +
         "Illinois-ID|" +
         "Indiana-IL|" +
         "Iowa-IN|" +
         "Kansas-KS|" +
         "Kentucky-KY|" +
         "Louisiana-LA|" +
         "Maine-ME|" +
         "Maryland-MD|" +
         "Massachusetts-MA|" +
         "Michigan-MI|" +
         "Minnesota-MN|" +
         "Mississippi-MO|" +
         "Missouri-MS|" +
         "Montana-MT|" +
         "Nebraska-NE|" +
         "Nevada-NM|" +
         "New Hampshire-NH|" +
         "New Jersey-NJ|" +
         "New Mexico-NM|" +
         "New York-NY|" +
         "North Carolina-NC|" +
         "North Dakota-ND|" +
         "Ohio-OH|" +
         "Oklahoma-OK|" +
         "Oregon-OR|" +
         "Pennsylvania-PA|" +
         "Rhode Island-RI|" +
         "South Carolina-SC|" +
         "South Dakota-SD|" +
         "Tennessee-TN|" +
         "Texas-TX|" +
         "Utah-UT|" +
         "Vermont-VT|" +
         "Virginia-VA|" +
         "Washington-WA|" +
         "West Virginia-WV|" +
         "Wisconsin-WI|" +
         "Wyoming-WY";

s_a["Uzbekistan"]="Bukhara-BU|"+
		 "Fergana-FA|"+
		 "Navoiy Province-NW|"+
		 "Qashqadaryo-QA|"+
		 "Samarqand Viloyati-SA|"+
		 "Surxondaryo Viloyati-SU|"+
		 "Toshkent Shahri-TK|"+
		 "Toshkent Viloyati-TO"

s_a["Vanuatu"]="Penama Province-PAM|"+
		 "Shefa Province-SEE";

s_a["Venezuela"]="Anzoátegui-B|"+
	     "Apure-C|"+
	     "Aragua-D|"+
	     "Barinas-E|"+
	     "Bolívar-F|"+
	     "Capital-A|"+
	     "Carabobo-G|"+
	     "Cojedes-H|"+
	     "Delta Amacuro-Y|"+
	     "Dependencias Federales-W|"+
	     "Estado Trujillo-T|"+
	     "Falcón-I|"+
	     "Guárico-J|"+
	     "Lara-K|"+
	     "Mérida-L|"+
	     "Miranda-M|"+
	     "Monagas-N|"+
	     "Nueva Esparta-O|"+
	     "Portuguesa-P|"+
	     "Sucre-R|"+
	     "Táchira-S|"+
	     "Vargas-X|"+
	     "Yaracuy-U|"+
	     "Zulia-V"

s_a["Vietnam"]= "An Giang-44|"+
		  "Dak Nong-72|"+
		  "Gia Lai-30|"+
		  "Hau Giang-73|"+
		  "Ho Chi Minh City-65|"+
		  "Huyen Dien Bien-71|"+
		  "Kon Tum-28|"+
		  "Long An-41|"+
		  "Thanh Pho Can Tho-48|"+
		  "Thanh Pho Da Nang-60|"+
		  "Thanh Pho Hai Phong-62|"+
		  "Thanh Pho Ha Noi-64|"+
		  "Tinh Bac Giang-54|"+
		  "Tinh Bac Kan-53|"+
		  "Tinh Bac Lieu-55|"+
		  "Tinh Bac Ninh-56|"+
		  "Tinh Ba Ria-Vung Tau-43|"+
		  "Tinh Ben Tre-50|"+
		  "Tinh Binh Dinh-31|"+
		  "Tinh Binh Duong-57|"+
		  "Tinh Binh Phuoc-58|"+
		  "Tinh Binh Thuan-40|"+
		  "Tinh Ca Mau-59|"+
		  "Tinh Cao Bang-04|"+
		  "Tinh Dak Lak-33|"+
		  "Tinh Dong Nai-39|"+
		  "Tinh Dong Thap-45|"+
		  "Tinh Ha Giang-03|"+
		  "Tinh Hai Duong-61|"+
		  "Tinh Ha Nam-63|"+
		  "Tinh Ha Tinh-23|"+
		  "Tinh Hoa Binh-14|"+
		  "Tinh Hung Yen-66|"+
		  "Tinh Khanh Hoa-34|"+
		  "Tinh Kien Giang-47|"+
		  "Tinh Lai Chau-01|"+
		  "Tinh Lam Dong-35|"+
		  "Tinh Lang Son-09|"+
		  "Tinh Lao Cai-02|"+
		  "Tinh Nam Dinh-67|"+
		  "Tinh Nghe An-22|"+
		  "Tinh Ninh Binh-18|"+
		  "Tinh Ninh Thuan-36|"+
		  "Tinh Phu Tho-68|"+
		  "Tinh Phu Yen-32|"+
		  "Tinh Quang Binh-24|"+
		  "Tinh Quang Nam-27|"+
		  "Tinh Quang Ngai-29|"+
		  "Tinh Quang Ninh-13|"+
		  "Tinh Quang Tri-25|"+
		  "Tinh Soc Trang-52|"+
		  "Tinh Son La-05|"+
		  "Tinh Tay Ninh-37|"+
		  "Tinh Thai Binh-20|"+
		  "Tinh Thai Nguyen-69|"+
		  "Tinh Thanh Hoa-21|"+
		  "Tinh Thua Thien-Hue-26|"+
		  "Tinh Tien Giang-46|"+
		  "Tinh Tra Vinh-51|"+
		  "Tinh Tuyen Quang-07|"+
		  "Tinh Vinh Long-49|"+
		  "Tinh Vinh Phuc-70|"+
		  "Tinh Yen Bai-06"

s_a["U.S. Virgin Islands"]="Saint Croix Island-C|"+
		 "Saint John Island-J|"+
		 "Saint Thomas Island-T";


s_a["Wallis and Futuna"]="Circonscription d'Uvea-W";

s_a["Western Sahara"]="Western Sahara-EH";

s_a["Yemen"]="Aden-AD|"+
		 "Muhafazat Hadramawt-HD|"+
		 "Sanaa-SN";

s_a["Zambia"]="Central Province-02|"+
		 "Copperbelt-08|"+
		 "Eastern Province-03|"+
		 "Luapula Province-04|"+
		 "Lusaka Province-09|"+
		 "Northern Province-05|"+
		 "North-Western Province-06|"+
		 "Southern Province-07|"+
		 "Western Province-01";

s_a["Zimbabwe"]="Bulawayo-BU|" + 
		 "Harare-HA|" + 
		 "Manicaland-MA|" + 
		 "Mashonaland West-MW|" + 
		 "Masvingo Province-MV|" + 
		 "Matabeleland North-MN|" + 
		 "Matabeleland South Province-MS|" + 
		 "Midlands Province-MI";


function populateStates( countryElementId, stateElementId ){
	
	var selectedCountryIndex = document.getElementById( countryElementId ).selectedIndex;

	var selectedCountryText = document.getElementById(countryElementId).options

	var stateElement = document.getElementById( stateElementId );
	
	stateElement.length=0;	// Fixed by Julian Woods
	stateElement.options[0] = new Option('Select State','');
	stateElement.selectedIndex = 0;
	
	var state_arr = s_a[selectedCountryText[selectedCountryIndex].text].split("|");	
	
	
	for (var i=0; i<state_arr.length; i++) {
		stateElement.options[stateElement.length] = new Option(state_arr[i].split("-")[0], state_arr[i].split("-")[1]);
	}
}

function populateCountries(countryElementId, stateElementId){
	// given the id of the <select> tag as function argument, it inserts <option> tags
	var countryElement = document.getElementById(countryElementId);
	countryElement.length=0;
	countryElement.options[0] = new Option('Select Country','');
	countryElement.selectedIndex = 0;
	for (var i=0; i<country_arr.length; i++) {
		
		var country = country_arr[i].split("|");
	
		countryElement.options[countryElement.length] = new Option(country[0], country[1]);
		
		
	}

	// Assigned all countries. Now assign event listener for the states.

	if( stateElementId ){
		countryElement.onchange = function(){
			populateStates( countryElementId, stateElementId );
		};
	}
}