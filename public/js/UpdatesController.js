var UpdatesController = function() {
	
	function startDocument() {
		$("#emailForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $("#ajaxImage").show();
    		    },
				success :function(data) {
					
					
					if(data.emptyEmail) {
						Globals.loadAjaxFlashMessage(data.emptyEmail, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "44px");
					}
					
					if(data.verifyEmail) {
						Globals.loadAjaxFlashMessage(data.verifyEmail, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "44px");
					}
					
					if(data.emptyFeedback) {
						Globals.loadAjaxFlashMessage(data.emptyFeedback, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "44px");
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "auto");
					}
				
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						$("#ajaxImage").hide();
						$("#messageContainer").css("height", "auto");
						$("input:text, textarea").val('');
						setTimeout(function () {
       						window.location.href = data.redirect; //will redirect to your blog page (an ex: blog.html)
    					}, 1500);
						
						
					}
				}
			});					
		});
		
		
	}
	
	return {
		StartDocument: function() {
			startDocument()
		},
	}
}();










