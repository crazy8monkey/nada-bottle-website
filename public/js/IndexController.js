var IndexController = function() {
	
	return {
		rightArrowPartnerSlideShowClick: function() {
			
				var $active = $('ul.sponsorsList li.active');

    			if ( $active.length == 0 ) {
    				$active = $('ul.sponsorsList li:last');
    			} 

			    var $next =  $active.next().length ? $active.next()
    		    : $('ul.sponsorsList li:first');

			    $active.addClass('last-active').fadeOut();
        
   				 $next.css({opacity: 0.0})
    			    .addClass('active').fadeIn()
        			.animate({opacity: 1.0}, 1000, function() {
        		    $active.removeClass('active last-active');
        		});
		},
		leftArrowPartnerSlideShowClick: function() {
			var $active = $('ul.sponsorsList li.active');

    		if ( $active.length == 0 ) {
    			$active = $('ul.sponsorsList li:first');
    		} 

			var $next =  $active.next().length ? $active.next() : $('ul.sponsorsList li:last');

			    $active.addClass('last-active').fadeOut();
        
   				 $next.css({opacity: 0.0})
    			    .addClass('active').fadeIn()
        			.animate({opacity: 1.0}, 1000, function() {
        		    $active.removeClass('active last-active');
        		});
			
		},
		openAboutUsVideo: function() {
			$("a#AboutUsLink").fancybox({
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	false
			});		
		}
		
	}
}();
