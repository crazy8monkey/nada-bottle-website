var LoginController = function() {
	
	function ValidateCredentials() {
		$("#adminLogin").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingForm").show();
    		    },
				success :function(data) {
					if(data.loginErrorMessage) {
						Globals.loadAjaxFlashMessage(data.loginErrorMessage, false, false, true);
						$(".loadingForm").hide();
					}
					if (data.redirect) {
						setTimeout(function () {
       						window.location.href = data.redirect; //will redirect to your blog page (an ex: blog.html)
    					}, 1500);						
					}
					
				}
			});					
		});
	}
	
	
	function ValidateEmailForm() {
		$("#resetPassword").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingForgotPasswordForm").show();
    		    },
				success :function(data) {
					if(data.emptyEmail) {
						Globals.loadAjaxFlashMessage(data.emptyEmail, false, false, true);
						$(".loadingForgotPasswordForm").hide();
					}
					if(data.verifyEmail) {
						Globals.loadAjaxFlashMessage(data.verifyEmail, false, false, true);
						$(".loadingForgotPasswordForm").hide();
					}
					if(data.errorEmail) {
						Globals.loadAjaxFlashMessage(data.errorEmail, false, false, true);
						$(".loadingForgotPasswordForm").hide();
					}
					if (data.success) {
						Globals.loadAjaxFlashMessage(data.success, false, false, true);
						setTimeout(function () {
       						window.location.href = data.redirect; //will redirect to your blog page (an ex: blog.html)
    					}, 1500);
						$("#forgotEmail").val('');
						$(".loadingForgotPasswordForm").hide();
					}
					
				}
			});					
		});
	}
	
	
	function passwordChange() {
		$("#ChangePassword").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $(".loadingForm").show();
    		    },
				success :function(data) {
					if(data.emptyPassword) {
						Globals.loadAjaxFlashMessage(data.emptyPassword, false, false, true);
						$(".loadingForm").hide();
					}
					if(data.passwordLenth) {
						Globals.loadAjaxFlashMessage(data.passwordLenth, false, false, true);
						$(".loadingForm").hide();
					}
					if(data.userNewPassword) {
						Globals.loadAjaxFlashMessage(data.userNewPassword, false, false, true);
						$(".loadingForm").hide();
					}
					if(data.userConfirmPasswordNotMatch) {
						Globals.loadAjaxFlashMessage(data.userConfirmPasswordNotMatch, false, false, true);
						$(".loadingForm").hide();
					}
					
					if(data.complete) {
						Globals.loadAjaxFlashMessage(data.complete, false, false, true);
						$(".message, #ajaxImage").delay(1500).fadeOut(3500);
						$(".password").val("");
						$(".loadingForm").hide();
					}
				}
			});					
		});
	}
	

	return {
		ValidateCredentials: function() {
			ValidateCredentials();
		},
		validateEmailForgotPassword: function() {
			ValidateEmailForm();
		},
		validateResetPasswordForm: function() {
			passwordChange()
		}
		
	
	}
	
}();
