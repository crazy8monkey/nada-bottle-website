String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

jQuery.fn.extend({
    toggleText: function (a, b){
        var that = this;
            if (that.text() != a && that.text() != b){
                that.text(a);
            }
            else
            if (that.text() == a){
                that.text(b);
            }
            else
            if (that.text() == b){
                that.text(a);
            }
        return this;
    }
});

var PurchaseController = function() {
	var quantity = 0;
	function SingleProduct() {
		
		
		$("#quantityChange").keyup(function(){
			
			var PriceChange = $("#currentPriceContainer").html() * $("#quantityChange").val()	
			
			$("#priceByQuantityChange").html(PriceChange + ".00")
			//$("#hiddenTotalPriceValue").val(PriceChange);
			
			if($("#quantityChange").val() === "0") {
				$("#quantityChange").val("1");
				$("#priceByQuantityChange").html($("#currentPriceContainer").html());
			}
			
		})
		//
	}
	function mouseOverProducts() {
		$(".productCard").each(function(){
			$(this).mouseenter(function(){
				$(this).find(".mouseOverWhite").fadeIn("fast");
				$(this).find(".priceInfo").fadeIn("fast");
				
				//, 
			});
			$(this).mouseleave(function(){
				$(this).find(".mouseOverWhite").fadeOut("fast");
				$(this).find(".priceInfo").fadeOut("fast");
				
			})	
		});
		
		
	}
	
	function validateSingleProduct() {
		$("#AddToCart").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $("#ajaxImage").show();
		            $(".message.error.OutOfStockMessage").remove();
    		    },
				success :function(data) {
					if(data.productColor) {
						Globals.loadAjaxFlashMessage(data.productColor, false, false, true);
						$("#messageContainer").css("height", "30px");
						$("#ajaxImage").hide();
					}
					if(data.emptyQuantity) {
						Globals.loadAjaxFlashMessage(data.emptyQuantity, false, false, true);
						$("#messageContainer").css("height", "30px");
						$("#ajaxImage").hide();
					}
					if(data.productEntered) {
						Globals.loadAjaxFlashMessage(data.productEntered, false, false, true);
						$("#messageContainer").css("height", "30px");
						$("#ajaxImage").hide();
					}
					
					if(data.OutOfStock) {
						Globals.loadAjaxFlashMessage(data.OutOfStock, false, false, true);
						$("#messageContainer").css("height", "30px");
						$("#ajaxImage").hide();
					}	
					if(data.quantityExtendsStock) {
						Globals.loadAjaxFlashMessage(data.quantityExtendsStock, false, false, true);
						$("#messageContainer").css("height", "30px");
						$("#ajaxImage").hide();
					}
					if(data.exceedsMaxWeight) {
						Globals.loadAjaxFlashMessage(data.exceedsMaxWeight, false, false, true);
						$("#ajaxImage").hide();
					}
					
					if(data.redirect) {
						window.location = data.redirect;
					}
				}
		
			});					
		});
	}
	
	function getQueryVariable(variable)
	{
	       var query = window.location.search.substring(1);
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){return pair[1];}
	       }
	       return(false);
	}
	
	return  {
     
	SinglePage: function() {
			SingleProduct();
			validateSingleProduct();
		},
		ProductCategory: function() {
			mouseOverProducts();
		},
		expandSpecs: function() {
			$("#productDimentionInfo").slideToggle();
			//$("#productDimentionInfo .toggleIndicator");
			$("#productDescriptionInfo").slideUp();
			$(".spec span.toggleIndicator").toggleText('-', '+');
			$(".description span.toggleIndicator").html('+');
			
		},
		expandDescription: function() {
			$("#productDescriptionInfo").slideToggle();
			$("#productDimentionInfo").slideUp();
			
			$(".spec span.toggleIndicator").html('+');
			$(".description span.toggleIndicator").toggleText('-', '+');
		},
		SelectProductColor: function(colorElement) {
			
		
			$(colorElement).each(function() {
				
				
				if (!$(this).find(".colorBoxHover").hasClass("selected")) {
    				$(".colorBoxHover.selected").removeClass("selected");
    				
    				var imagePath = $(this).find('.colorBox').data('image');
    				var colorID = $(this).find('.colorBox').data('colorid');
    				var colorName = $(this).find('.colorBox').data('colorname');
    				var colorHex = $(this).find('.colorBox').data('hex');
    				
            		if(!imagePath || imagePath.endsWith('/')){
                		imagePath = $('#hidProductImage').val();
            		}
                	$('#imgProduct').attr('src', imagePath);
    				$("#colorChoiceSelected").val(colorID);
    				$("#colorSelectedText").html("<strong>Color Selected: </strong>" + colorName);
    				$("#colorSelectedText").show();
    				$("#colorSelectedText").css("border", "1px solid" + colorHex);
    				
    				$(this).find(".colorBoxHover").addClass("selected");
                    quantity =$(this).find('.colorBox').data('remaining');
                    if(quantity <= 2){
                        /// ADAM: Please make this look good. Add a nice validation message for this. It makes they can't order more than is available;
                       $(".OutOfStockSection").show();
                    }
                    else {
                    	$(".OutOfStockSection").hide();
                    }

  				}							
			})
		}
	}
}();