var LocateLifeController = function() {
	
	function locateLifeForm() {
		$("#locateLifeForm").submit(function(e){
			e.preventDefault();
			
			var formURL = $(this).attr("action");
			var postData = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: formURL,
				data: postData,
				dataType: 'json',
				cache: false,
				beforeSend: function(){
		            $("#ajaxImage").show();
		             $("#ajaxImage").css("height", "25px");
    		    },
				success :function(data) {
					if(data.longitutde) {
						Globals.loadAjaxFlashMessage(data.longitutde, false, false, true);
						$("#ajaxImage img").hide();
					}
					if(data.latitude) {
						Globals.loadAjaxFlashMessage(data.latitude, false, false, true);
						$("#ajaxImage img").hide();
					}
					if(data.redirect) {
						window.location = data.redirect;
						$("#ajaxImage img").hide();
					}
					if(data.noLocation) {
						Globals.loadAjaxFlashMessage(data.noLocation, false, false, true);
						$("#ajaxImage img").hide();
					}
					if(data.MySqlError) {
						Globals.loadAjaxFlashMessage(data.MySqlError, false, false, true);
						$("#ajaxImage img").hide();
					}
				}
			});					
		});
	}
	
	return {
		StartDocument: function() {
			locateLifeForm();
		}
	}
}();
