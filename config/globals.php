<?php


// email header types
define ('MYSQL_ERROR_TYPE', 'MYSQL ERROR');
define ('SHIPPING_ERROR_TYPE', 'CART SHIPPING RATE ERROR');
define ('STRIPE_CHECKOUT_ERROR_TYPE', 'STRIPE CHECKOUT ERROR');
define ('TRACK_ERROR_TYPE', 'TRACKING ERROR');
define ('TAXJAR_API_TYPE', 'TAXJAR API ERROR');


//message types
define ('MYSQL_INSERT_LABEL', 'MySql Insert Error: ');
define ('MYSQL_SELECT_LABEL', 'MySql Select Error: ');
define ('MYSQL_UPDATE_LABEL', 'MySql Update Error: ');
define ('MYSQL_DELETE_LABEL', 'MySql Delete Error: ');
define ('MYSQL_GENERAL_LABEL', 'MySql General Error: ');
define ('SHIPPING_RATE_LABEL', 'Shipping Rate Error: ');
define ('TAXJAR_API_LABEL', 'TaxJar API Error: ');

define ('STRIPE_CARD_EXCEPTION_LABEL', 'Stripe Card Exception: ');
define ('STRIPE_INVALID_REQUEST_EXCEPTION_LABEL', 'Stripe Invalid Request Exception: ');
define ('STRIPE_AUTHENTICATION_EXCEPTION_LABEL', 'Stripe Authentication Exception: ');
define ('STRIPE_BASE_EXCEPTION_LABEL', 'Stripe Base Exception: ');
define ('STRIPE_API_EXCEPTION_LABEL', 'Stripe API Connection Exception: ');
define ('STRIPE_RATELIMIT_EXCEPTION_LABEL', 'Stripe API Connection Exception: ');

define ('NORMAL_CHECKOUT_EXCEPTION', 'Non Stripe Checkout Exception:');
define ('TRACKING_EMAIL_EXCEPTION', 'Tracking Email Exception: ');

//system default error message
define ('SYSTEM_ERROR_MESSAGE', 'Something went wrong with our system. Please try again');


