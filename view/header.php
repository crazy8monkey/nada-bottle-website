<!DOCTYPE html>
<!--[if IE 7]>     <html class="ie7" xml:lang="en"> <![endif]-->
<!--[if IE 8]>     <html class="ie8" xml:lang="en"> <![endif]-->
<!--[if IE 9]>     <html class="ie9" xml:lang="en"> <![endif]-->
<!--[if gt IE 10]> <html xml:lang="en"> <![endif]-->
<!--[if !IE]><!--> <html xml:lang="en"> <!--<![endif]-->
	<head>
		<title><?php echo isset($this->title) ? $this->title : 'Nada Bottle'; ?></title>
		
		
		<link rel="shortcut icon" href="<?php echo PATH ?>public/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo PATH ?>public/images/favicon.ico" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

		<meta name="description" content="Nada Bottle" />
		<meta name="keywords" content="Clean water for every purchase, Helping the need, clean water, water">

		
		<?php  if (isset($this -> mainWebsite)) : ?>	
			<link rel="stylesheet" href="<?php echo PATH ?>public/font/style.css" type="text/css" />
			<link rel="stylesheet" href="<?php echo PATH ?>public/css/style.css" type="text/css" />
		<?php endif; ?>
		<?php  if (isset($this -> thankyou)) : ?>
			<link rel="stylesheet" href="<?php echo PATH ?>public/font/style.css" type="text/css" />
		<?php endif; ?>
		
		
		
		<?php if(isset($this->css))  {
			foreach ($this -> css as $css)  {	?>
			<link rel="stylesheet" href="<?php echo PATH  ?>public/css/<?php echo $css; ?>" /> 
		<?php }
		}
		?>
		
		<?php  if (isset($this -> adminWebsite)) : ?>
			<link rel="stylesheet" href="<?php echo PATH ?>public/css/admin/style.css" type="text/css" />	
		<?php endif; ?>
		
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
		<?php  if (isset($this -> mainWebsite)) : ?>
			<script src="<?php echo PATH ?>public/js/globals.js" type="text/javascript"></script>
		<?php endif; ?>
		<!--
		<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>-->
		
		<!--[if lt IE 9]>
			<script src="html5shiv.js" type="text/javascript"></script>
		<![endif]-->
		<?php if(isset($this->js))  {
			foreach ($this -> js as $js) {	?>
				<script type="text/javascript" src="<?php echo $js; ?>"></script>
			<?php }
		}
		?>
		
		<script type="text/javascript"> 
			document.createElement("header");
			document.createElement("footer");
			$(document).ready(function(){
			<?php if(isset($this->startJsFunction)) {
				foreach ($this -> startJsFunction as $startJsFunction) { ?>	
					<?php echo $startJsFunction; ?>	
			<?php	}
			}
			?>
			
			<?php  if (isset($this -> mainWebsite)) : ?>
				Globals.Navigation();	
				Globals.EmailUpdateValidation();
			<?php endif; ?>
			<?php  if (isset($this -> adminWebsite)) : ?>
				$(".message").delay(1500).fadeOut(3500);
			<?php endif; ?>
			});
		</script>
		
	</head>
<body>


<?php  if (isset($this -> mainWebsite)) : ?>

<header data-urlPath = "<?php echo PATH ?>public/images/">
	<div class='wrapper' style="position:relative;">
		<a href="<?php echo PATH ?>cart">
			<div class="eCommerceUserLogin">
				<div class="ShoppingCartLink">
					<?php echo (isset($this->cart)) ? $this->cart : 'Your Cart'; ?>
				</div>
			</div>
		</a>
		<ul class="mainMenu">
			<li class="mainMenuLink">
				<a href="<?php echo PATH ?>purchase">SHOP</a>
			</li>
			<li class="mainMenuLink">
				<a href="<?php echo PATH ?>locateLife">LOCATE LIFE</a>
			</li>
			<li id="logo">
				<a href="<?php echo PATH ?>">
					<img src="<?php echo PATH ?>public/images/nadaBottleLogo.png" id="logoImage"  />
				</a>
			</li>
			<li class="mainMenuLink">
				<a href="javascript:void(0);" onclick="Globals.openNadaOrdinaryBottleSubMenu();">NADA ORDINARY BOTTLE</a>
			</li>
			<li class="mainMenuLink">
				<a href="javascript:void(0);" onclick="Globals.openSupportSubMenu();">SUPPORT</a>
			</li>
		</ul>
		<div style="clear:both"></div>
	</div>
	<div id="secondaryMenu">
		<div class='wrapper'>
			<div class="NadaOrdinaryBottleLinks">
				<div style="display:table; margin:0 auto">
					<a href="<?php echo PATH ?>about/story">
						<div class="link">
							<div class="icon" id="story"></div>
							Story
						</div>
					</a>
					<a href="<?php echo PATH ?>about/cleanWater">
						<div class="link">
							<div class="icon" id="cleanWater"></div>
							Clean Water
						</div>					
					</a>
					
					<a href="<?php echo PATH ?>about/thankyou">
						<div class="link">
							<div class="icon" id="thankyou"></div>
							Thank You
						</div>
					</a>
					<a href="<?php echo PATH ?>about/ourteam">
						<div class="link">
							<div class="icon" id="OurTeam"></div>
							Our Team
						</div>
					</a>
					
				</div>
				<div style="clear:both"></div>
			</div>
			<div class="SupportLink">
				<div style="display:table; margin:0 auto">
					<a href="<?php echo PATH ?>support/contact">
						<div class="link">
							<div class="icon" id="Contact"></div>
							Contact
						</div>
					</a>
					<a href="<?php echo PATH ?>support/warranty">
						<div class="link">
							<div class="icon" id="Warranty"></div>
							Warranty
						</div>
					</a>
					<a href="<?php echo PATH ?>support/faq">
						<div class="link">
							<div class="icon" id="FAQ"></div>
							FAQ
						</div>
					</a>
				</div>
				<div style="clear:both"></div>
			</div>
		</div>
	</div>
	<a href="javascript:void(0);" style="text-decoration:none;" onclick="Globals.responsiveMenu()">
		<div id="responsiveMenu">
			<div style="position:absolute; left:10px;">
				<div style="position:absolute; background:white; height:3px; top: -2px; width: 30px;"></div>
				<div style="position:absolute; background:white; height:3px; top: 8px; width: 30px;"></div>
				<div style="position:absolute; background:white; height:3px; top: 18px; width: 30px;"></div>
			</div>
			&nbsp;
		</div>	
	</a>
	<div class="responsiveMenu">
		<a style="text-decoration:none;" href="<?php echo PATH ?>purchase"><div>Shop</div></a>
		<a style="text-decoration:none;" href="<?php echo PATH ?>locateLife"><div>Locate Life</div></a>
		<div style="position:relative">Nada Ordinary Bottle<div class="arrow"></div></div>
		<div id="responsiveNadaBottle" class="subMenu">
			<a href="<?php echo PATH ?>about/story"><div class='link'>Story</div></a>
			<a href="<?php echo PATH ?>about/cleanWater"><div class='link'>Clean Water</div></a>
			<a href="<?php echo PATH ?>about/thankyou"><div class='link'>Thank You</div></a>
			<a href="<?php echo PATH ?>about/ourteam"><div class='link'>Our Team</div></a>
		</div>
		<div style="position:relative">Support<div class="arrow"></div></div>
		<div id="responsiveSupport" class="subMenu">
			<a href="<?php echo PATH ?>support/contact"><div class='link'>Contact</div></a>
			<a href="<?php echo PATH ?>support/warranty"><div class='link'>Warranty</div></a>
			<a href="<?php echo PATH ?>support/faq"><div class='link'>FAQ</div></a>
		</div>
	</div>
	
</header>

<div class="push"></div>
<?php endif; ?>
<?php  if (isset($this -> adminWebsiteHeader)) : ?>
<header>
	<div class="container" style="float:left">
		Nada Bottle CMS
	</div>
	<div style="float:right">
		<div class="desktopLink">
			<a href="<?php echo PATH?>cms/dashboard">
				<div class="linkContaner container">
					Dashboard
				</div>
			</a>
			<a href="<?php echo PATH?>cms/profile?userid=<?php echo $_SESSION["user"]->_userId ?>&firstName=<?php echo $_SESSION["user"]->FirstName ?>&lastName=<?php echo $_SESSION["user"]->LastName ?>">
				<div class="linkContaner container">
					Your Profile
				</div>
			</a>
			<a href="<?php echo PATH?>cms/settings/shipping">
				<div class="linkContaner container">
					Settings
				</div>
			</a>
					
			<a href="<?php echo PATH?>cms/logout">
				<div class="linkContaner container">
					Logout
				</div>
			</a>
		</div>
		<a href="javascript:void(0);" onclick="CMSController.openMobileTopMenu()"><div class="DropDownArrow"></div></a>
	</div>
	<div style="clear:both"></div>
</header>
<?php endif; ?>
<?php if (isset($this -> subMenu)) : ?>
<div class="subMenu">
	<a href="javascript:void(0);" onclick="CMSController.openSubMobileMenu()">
		<div class="mobileMenuLink" style="position:relative">
			<div style="position: absolute; top: 10px; height: 3px; background: black; width: 100%;;"></div>
			<div style="position: absolute; top: 20px; height: 3px; background: black; width: 100%;"></div>
			<div style="position: absolute; top: 30px; height: 3px; background: black; width: 100%;"></div>
		</div>
	</a>
	<div class="desktopSubMenu">
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('pages')) : ?>
			<a style="text-decoration:none;" href="<?php echo PATH?>cms/story">
				<div class="link" <?php if(isset($this -> mainPagesSection)) {echo "style='font-weight:bold'"; } ?>>Pages</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($this -> eCommerceLinkTab)) : ?>
		<a style="text-decoration:none;" href="<?php echo $this -> eCommcerLink ?>">
			<div class="link" <?php if(isset($this -> eCommerceSection)) {echo "style='font-weight:bold'"; } ?>>eCommerce</div>
		</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('ContactInquiries')) : ?>
			<a style="text-decoration:none;" href="<?php echo PATH?>cms/contact?page=1">
				<div class="link" <?php if(isset($this -> contactInquiriesLink)) {echo "style='font-weight:bold'"; } ?>>Contact Inquires</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Partners')) : ?>
			<a style="text-decoration:none;" href="<?php echo PATH?>cms/partners">
				<div class="link" <?php if(isset($this -> partnersLink)) {echo "style='font-weight:bold'"; } ?>>Partners</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('LocateLife')) : ?>
			<a style="text-decoration:none;" href="<?php echo PATH ?>cms/locatelife">
				<div class="link" <?php if(isset($this -> locateLife)) {echo "style='font-weight:bold'"; } ?>>Locate Life</div>
			</a>
		<?php endif ?>
        
        <?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Users') || $_SESSION["user"]->HasPermission('teamEditPermissions')) : ?>
			<a style="text-decoration:none;" href="<?php echo PATH ?>cms/users">
				<div class="link" <?php if(isset($this -> usersSection)) {echo "style='font-weight:bold'"; } ?>>Users</div>
			</a>
        <?php endif ?>
        
        <?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('FeedBack')) : ?>
      		<!--<a style="text-decoration:none;" href="<?php echo PATH ?>cms/feedback">
				<div class="link" <?php if(isset($this -> feedBackLink)) {echo "style='font-weight:bold'"; } ?>>Feed Back</div>
			</a>-->
        <?php endif ?>
        
        <?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('newsLetterPermissions')) : ?>
        	<a style="text-decoration:none;" href="<?php echo PATH ?>cms/emailList">
				<div class="link" <?php if(isset($this -> newsLetterLink)) {echo "style='font-weight:bold'"; } ?>>Newsletter</div>
			</a>
        <?php endif ?>
		<div style="clear:both"></div>
	</div>
</div>
<?php if(isset($this -> eCommerceSection)) : ?>
	<div class="sectionMenu" id="eCommerce">
		<strong>eCommerce</strong>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceOrders')) : ?>
			<a href="<?php echo PATH ?>cms/orders?page=1">
				<div class="link">
					Orders	
				</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceCategories')) : ?>
			<a href="<?php echo PATH ?>cms/categories">
				<div class="link">
					Categories	
				</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceProducts')) : ?>
			<a href="<?php echo PATH ?>cms/products">
				<div class="link">
					Products	
				</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceTaxes')) : ?>
			<a href="<?php echo PATH ?>cms/taxReport">
				<div class="link">
					Taxes	
				</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceWarranty')) : ?>
			<a href="<?php echo PATH ?>cms/warranties">
				<div class="link">
					Warranties	
				</div>
			</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceInventory')) : ?>
			<a href="<?php echo PATH ?>cms/inventory">
				<div class="link">
					Inventory	
				</div>
			</a>
		<?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) : ?>
			<a href="<?php echo PATH ?>cms/paymentLogs">
				<div class="link">
					Payment Logs	
				</div>
			</a>
		<?php endif ?>
		
	</div>
<?php endif; ?>
<?php if(isset($this -> mainPagesSection)) : ?>
	<div class="sectionMenu" id="mainPagesSection">
		<div style="margin-bottom:20px;">
			<a href="<?php echo PATH ?>cms/locatelifeContent">
				<div class="link">
					Locate Life	
				</div>
			</a>
		</div>
		<div style="margin-bottom:20px;">
			<strong>Nada Ordinary Bottle</strong>
			<a href="<?php echo PATH ?>cms/story">
				<div class="link">
					Story	
				</div>
			</a>
			<a href="<?php echo PATH?>cms/cleanWater">
				<div class="link">
					Clean Drinking Water	
				</div>
			</a>
			<a href="<?php echo PATH?>cms/thankyou">
				<div class="link">
					Thank You
				</div>
			</a>
			<a href="<?php echo PATH?>cms/team">
				<div class="link">
					Our Team
				</div>
			</a>
		</div>	
		<strong>Support</strong>
		<a href="<?php echo PATH?>cms/contactContent">
			<div class="link">
				Contact	
			</div>
		</a>
		<a href="<?php echo PATH?>cms/warranty">
			<div class="link">
				Warranty	
			</div>
		</a>
		<a href="<?php echo PATH?>cms/faq">
			<div class="link">
				FAQ
			</div>
		</a>
	</div>
<?php endif; ?>

<?php endif; ?>
<noscript>
 <div class="enableJavascriptOverlay"></div>
 <div class="enableJavascriptPopup">
     For full functionality of this site it is necessary to enable JavaScript.
	 Here are the <a href="http://www.enable-javascript.com/" target="_blank">
	 instructions how to enable JavaScript in your web browser</a>.	
 </div>
 
</noscript>	