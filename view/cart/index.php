<?php
$cartTotal = "";
$salesTax = "";
$salesTaxCalculation = "";
$cartTotalSalesTax = "";
$selected = "";
				
if (isset($_SESSION["cart_array"])) {				



foreach($this->productCart as $key => $value) {
        $quantity = $value['quantity'];
        $colorChoice = $value['color'];
        $arrayIndex = $value['index'];

        setlocale(LC_MONETARY, "en_US");
        $Totalprice = $value['price'] * $quantity;

        //$Totalprice =  $Totalprice);

        $cartTotal =  $Totalprice + $cartTotal;
       
    }

	
}	

?>
<div class="pageName">
	<div class="wrapper">
		<div class="entryText">
			<?php if(isset($this -> HTMLCART)) :?>
			Your Cart
			<?php endif; ?>
			<?php if(isset($this -> payment)) :?>
				Checkout
			<?php endif; ?>
			<?php if(isset($this -> confirm)) :?>
				Confirm
			<?php endif; ?>
		</div>		
	</div>	
</div>
<div class="pageContent">
	<div class="wrapper">
		<div class="contentContainer">
			<div class="contentWrapper">
				<?php if(isset($this -> HTMLCART)) :?>
					
						<?php if (!isset($_SESSION['cart_array']) || count($_SESSION['cart_array']) < 1) { ?>
							<h2>Your shopping cart is empty</h2>
						<?php } else {?>
						<div class="userCart">
						<div class='cartInfo'>
							<div class="Header">
						 		<table style='width:100%; border-collapse: collapse;' id='cartInfoTitle'>
						 			<tr>
						 				<?php if (isset($_SESSION["cart_array"])) { ?>
											<td class="clearButton">
												<div class="clearCartButton">
													<a class="clearCart" href="<?php echo PATH ?>cart/clearCart">
														Clear Cart
													</a>	
												</div>	
											</td>
										<?php } ?>
						 				
						 				<td class='tdImageHeader'></td>
						 				<td class='product'></td>
						 				
						 				<td align='right' style='text-align:center' class='qnty'>Qnty</td>
						 				<td class="totalPrice">Total</td>
						 				<td class='delete'></td>
						 			</tr>
						 		</table>
					 		</div>
					 		<table class='tableCartItem'>
					 			<?php foreach($this->productCart as $key => $value) : ?>
					 				<?php 
					 					$quantity = $value['quantity'];
										$colorChoice = $value['color'];
										$arrayIndex = $value['index'];
					 					setlocale(LC_MONETARY, "en_US");
								 		$Totalprice = $value['price'] * $quantity;
					 				?>
					 				<tr>
										<td style='padding:0px;'>
											<form method='post' action='<?php echo PATH ?>cart/updateItem/<?php echo $arrayIndex ?>' >
											</form>
												<input type='hidden' name="hiddenProductID" value = "<?php echo $value['product_id'] ?>" />
												<table style='width: 100%; border-collapse: collapse;' class='cartTable'>
													<tr>
									 			 		<td class='tdImage' style="padding-bottom: 10px;">
									 			 			<div class='image'>
									 			 				<img src='<?php echo PATH ?>view/purchase/product-images/<?php echo $value['product_image'] ?>' width='100%' />
									 			 			</div>
									 			 		</td>
									 			 		<td class="product">
									 			 			<div class="productName"><?php echo $value['product_name'] ?></div>
									 			 			<div style='clear:both; margin-bottom:5px;'>Price: <?php echo money_format("%10.2n", $value['price']) ?></div>
									 			 			<input type="hidden" value="<?php echo $colorChoice->ProductColorID  ?>" name="colorChoices" />
					                                        <div class="colorBox" style="background: <?php echo $colorChoice->ColorHexacode ?>;">&nbsp;</div>
					                                        <div class="colorName"><?php echo $colorChoice->Color ?></div>
									 			 		</td>
									 			 																<td align='right' style='text-align:center' class='qnty'>
															<!--<input type='text' name='productQuantity' class='quantityText' value='<?php echo $quantity ?>' style='width:50px;' />
															<input type='submit' class='quantityText' value="edit" />-->
															<div class='mobileQuantity'>
																<?php echo $quantity ?>
															</div>
														</td>
														<td align='right' class="totalPrice">
															<?php echo money_format("%10.2n", $Totalprice) ?>
														</td>
														<td align='right' class='delete'>
															<a href='<?php echo PATH ?>cart/deleteItem/<?php echo $arrayIndex ?>'>
																<div class='deleteItem'></div>
															</a>
														</td>
													</tr>
												</table>
											
										</td>
									</tr>
					 			<?php endforeach; ?>
					 		</table>
			 			</div>
					 	<div style='padding: 20px 15px;'>
					        <a class="emailUpdates" style="color:white;" href="<?php echo PATH ?>cart/payment">
					            Payment Information
					        </a>
					    </div>
					</div>
 					<?php } ?>
 				
			<?php endif; ?>
		
<?php if(isset($this -> payment)) :?>
	<div style="width:100%; clear:both">
		<div class="shippingFormContent">
			<form method="post" id="billingForm" action="<?php echo PATH ?>cart/Confirm">
				<?php $i = 1; ?>
				<?php foreach($this->productCart as $key => $value) : ?>
				 	<input type='hidden' name='hiddenItem<?php echo $i ?>Name' value='<?php echo $value['product_name'] ?>' />
					<input type='hidden' name='hiddenItem<?php echo $i ?>Qnty' value='<?php echo $value['quantity'] ?>' />
		            <input type='hidden' name='hiddenItem<?php echo $i ?>ProductID' value='<?php echo $value['product_id'] ?>' />
		            <input type='hidden' name='hiddenItem<?php echo $i ?>ProductColorID' value='<?php echo $value['color']->ProductColorID ?>' />
		            <input type='hidden' name='hiddenItem<?php echo $i ?>Price' value='<?php echo $value['price'] ?>' />
		            <input type='hidden' name='hiddenItem<?php echo $i ?>ProductImage' value='<?php echo $value['product_image'] ?>' />
				<?php $i ++ ?>				
			 	<?php endforeach; ?>
			 	
			 		<?php 
			 		
			 		
			 		Session::init();
					
					
						
						
					if(isset($_GET['data_id'])) {
						$data_key = 'POSTDATA_'.$_GET['data_id'];
					}
			 		
					$name = "";
					$post = array();
					
			 		if ( !empty ( $_GET['data_id'] ) && !empty( $_SESSION[$data_key] ))
					{ 
					    
					    $post = $_SESSION[$data_key];
						$firstName = "value='" .$post['firstName'] ."'";
						$lastName = "value='" .$post['lastName'] ."'";
						$email = "value='" .$post['email'] ."'";
						$phone = "value='" .$post['phone'] ."'";
						$address = "value='" .$post['address'] ."'";
						$state = $post['state'];
						$city = "value='" .$post['City'] ."'";
						$zip = "value='" .$post['postalCode'] ."'";
						
					    unset ( $_SESSION[$data_key] );
					} else {
						$firstName = "";
						$lastName = "";
						$email = "";
						$phone = "";
						$address = "";
						$state = "";
						$city = "";
						$state = "";
						$zip = "";
					}
			 		?>
					
					<div class="whiteEmailContainerCart userCart" style="margin-bottom: 20px;">
						<div class="cartHeader" id="cartInfoHeader">
							Shipping Information
						</div>
						<div style="color:#c30000; font-style:italic; font-size:14px; line-height: 14px;">
							*PO Boxes cannot be shipped to. Please use a physical address 
						</div>
						<div class="billingErrors"></div>
						<div class='checkoutSection'>
					 		<div id="ShippingRateInputs">
						 		<div class="content">
							 		<div class="labelContent halfInput">
							 			<div class="label">First Name</div>
									 	<input type='text' class='firstName' id="shippingFirstName" name='firstName' <?php echo $firstName ?> />
							 		</div>
							 		<div class="labelContent halfInput" style="margin-right:0px; padding-right:0px;">
								 		<div class="label">Last Name</div>
										<input type='text' class='lastName addressInput' id="shippingLastName" name='lastName' <?php echo $lastName ?> />
								 	</div>
								 	<div style="clear:both"></div>
							 	</div>
								<div class="labelContent halfInput">
									<div class="label">Email</div>
									<input type='text' class="addressInput" id="shippingEmail" name='email' <?php echo $email ?>  />
								</div>
								<div class="labelContent halfInput" style="margin-right:0px; padding-right:0px;">
									<div class="label">Phone Number</div>
									<input type='text' class="addressInput" id="telephoneNumber" name='phone' <?php echo $phone ?> />
								</div>
								<div class="labelContent fullWidth" style="float:none;">
									<div class="label">Address</div>
									<input type='text' class="addressInput" id="shippingAddress" name='address' <?php echo $address ?> />
								</div>
							 	<div style="clear:both; margin-bottom: 6px;">
							 		<div class="labelContent thirdInput">
								 		<div class="label">City</div>
									 	<input type='text'class='city' id="shippingCity" name='City' <?php echo $city ?> />
								 	</div>
								 	<div class="labelContent thirdInput">
								 		<div class="label">State/Province</div>
										<select class='providence' id="shippingState" name="state">
											<option value=""></option>
										</select>
							 	 	</div>
							 	 	<div class="labelContent thirdInput zipInput">
							 	 		<div class="label">Zip</div>
								 		<input type='text' class='postalCode' maxlength="5" id="shippingZip" name='postalCode' <?php echo $zip ?>  />
							 	 	</div>
							 	</div>
							 	<div class="labelContent fullWidth" style="float:none;">
							 		<div class="label">Country</div>
							 		<select class='countryDropDown' id="shippingCountry" name="country">
							 			<option value=""></option>
							 		</select>
							 	</div>
							 	
						 	</div>
						 	
						 	<div class="labelContent fullWidth shipping">
						 		<div class="label">Shipping</div>
								<select class="shipping" id="shippingMethod" name='shippingService' data-path="<?php echo PATH ?>">
							 		<option value="">Fill in Shipping information to see shipping rate options</option>
							 	</select>
							 	<input type="hidden" id="shippingStateHidden" value="" />
							 	<div class="shippingError" style="color:#c30000"></div>
							</div>
							<div class="loadingShippingRates" style="text-align:center; clear:both">
								<img src="<?php echo PATH ?>public/images/ajax-loader.gif" style="display:none; padding:9px 0px 8px 0px" />
								<input type="button" class="shippingRateButton" title="Generate Rates" onclick="CartController.LoadShippingOptions()" />
							</div>
						 	<div style="clear:both"></div>	 		
					 	</div>
					</div>
					<script type="text/javascript">
						populateCountries("shippingCountry", "shippingState");
					</script>
					
				
					<div class="whiteEmailContainerCart userCart" style=" border-bottom:none;">
						<div class="cartHeader" id="cartInfoHeader">
							Payment & Billing Information
						</div>
						<div class="payment-errors"></div>
						<div class='checkoutSection'>
							<div class="content">
							<div style="font-size:16px; margin-bottom:10px;" id="cartInfoHeader">
								Billing info 
							</div>
							<div style="border-bottom: 1px solid #f4f4f4; padding-bottom: 15px;">
								<div class="labelContent fullWidth" style="float:none;">
							 		<div class="label">Address</div>
									<input type='text' class="addressInput" id="billingAddress" <?php echo $address ?> />
								</div>
								<div style="clear:both; margin-bottom: 14px;">
									<div class="labelContent thirdInput">
							 			<div class="label">City</div>
										<input type='text'class='city' name='City' id="billingCity" <?php echo $city ?> />
							 		</div>
							 		<div class="labelContent thirdInput">
							 			<div class="label">State/Province</div>
											<select class='providence' id="billingState">
												<option value=""></option>
											</select>
						 	 			</div>
									
									<div class="labelContent thirdInput zipInput">
						 	 			<div class="label">Zip</div>
							 	 		<input type='text' class='postalCode' maxlength="5" id="billingZip" <?php echo $zip ?> />
						 	 		</div>
								</div>
								<div class="labelContent fullWidth" style="float:none;">
							 		<div class="label">Country</div>
							 		<select class='countryDropDown' id="billingCountry" name="country">
							 			<option value=""></option>
							 		</select>
							 	</div>
								
								<div style="clear:both"></div>
							</div>
							<script type="text/javascript">
								populateCountries("billingCountry", "billingState");
							</script>
								
							<div style="font-size:16px; margin-bottom:10px; margin-top:10px;" id="cartInfoHeader">
								<div style="float:left">
									Card Info	
								</div>
								<div id="cardTypeContainer" style="float:right">
									<div id="cardTypeIconHolder"></div>
									<div id="cardType" style="float: left; margin-left: 5px; margin-top: -2px;"></div>	
								</div>
								<div style="clear:both"></div>
							</div>
							<div class="labelContent fullWidth">
						 		<div class="label">Card Holder Name</div>
			                    
							 	<input class="creditCardName" type='text' />
						 	</div>
							<div class="labelContent fullWidth">
						 		<div class="label">Credit Card Number</div>
			                    <span id="cardType"></span>
							 	<input class="creditCardNumber" type='text' />
						 	</div>
							<div style="margin-bottom:15px;">
								<div class="labelContent thirdInput">
						 	 		<div class="label">Expiration Month</div>
							 	 	<select class="expirationMonth">
								 		<option value=""></option>
								 	 	<?php foreach($this->months as $key => $value) { ?>
											<option value='<?php echo $value ?>'><?php echo $key; ?></option>
										<?php } ?>
								 	</select>
						 	 	</div>
								<div class="labelContent thirdInput">
						 	 		<div class="label">Expiration Year</div>
							 	 	<select class="expirationYear">
								 		<option value=""></option>
								 	 	<?php foreach($this-> year as $key => $value) { ?>
											<option value='<?php echo $value ?>'><?php echo $value ?></option>
										<?php } ?>
								 	</select>
						 	 	</div>
								<div class="labelContent thirdInput" style="margin-right:0px; padding-right:0px;">
						 	 		<div class="label">CVC</div>
							 		<input class="CVC" type='text' maxlength="3" />
						 	 	</div>
								<div style="clear:both"></div>
							</div>
							<div style="padding: 0px 5px 5px 0px; clear: both;">
								<input type="submit" value="Submit" style="width:auto;"/>
							</div>
						</div>
					</div>
				</div>
				
				
				<input type="hidden" name="hiddenBillingZip" id="hiddenBillingZip" />
				<input type="hidden" name="hiddenShippingService" id="hiddenShippingService" />
				<input type="hidden" name="hiddenShippingMethodValue" id="hiddenShippingMethodValue" />
				
				<!-- for each loop through products!-->
				<?php $cartVolume = ""; 
					  $cartWeight = "";
				 
					foreach($this->productCart as $key => $value)  {
						$cartVolume += ($value['height'] * $value['length'] * $value['width']) * $value['quantity'];
						$cartWeight += $value['weight'] * $value['quantity'];
					} 
					 
				?>				
				<input type="hidden" name="hiddenCartVolumne" value="<?php echo $cartVolume ?>" />
				<input type="hidden" name="hiddenCartWeight" value="<?php echo $cartWeight ?>" />
			</form>
		</div>
		<div class="CartTotalSection">
		<div id="cartTotalElement" style='display:none'>
			<?php echo number_format($cartTotal, 2, '.', '') ?>
		</div>
		
			<div class="header">
				Your Cart
			</div>
			<table>
	            <tr>
	                <td>
	                    Cart Total:
	                </td>
	                <td>
	                    <?php echo  money_format("%10.2n", $cartTotal) ?> USD
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    Shipping:
	                </td>
	                <td>
	                    <span id="shippingRate">$0.00</span> USD
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    Taxes:
	                </td>
	                <td>
	                    $0.00 USD
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    Your Total:
	                </td>
	                <td>
	                    <span id="grandTotalValue"><?php echo money_format("%10.2n", $cartTotal) ?></span> USD
	                </td>
	            </tr>
	      	</table>
		</div>
		<div style="clear:both"></div>
	</div>
	<?php require 'view/message-section.php' ?>
<?php endif; ?>

<?php if(isset($this -> confirm)) :?>
<form method="post" id="billingForm"  action="<?php echo PATH ?>cart/payCart">
	<div style="width:100%; clear:both">
		<div class="shippingFormContent">
			<div class="whiteEmailContainerCart userCart" style='border-bottom:none'>
			<div class="cartHeader">
				Your Cart
			</div>
			<table class="confirmCartTable">
			<tr class='header'>
			<td></td>
			<td>Product</td>
			<td style='text-align:center'>Qty</td>
			<td style='text-align:right'>Total</td>
			</tr>
				<?php foreach($this->productCart as $key => $value) : ?>
					 				<?php 
					 					$quantity = $value['quantity'];
										$colorChoice = $value['color'];
										$arrayIndex = $value['index'];
					 					setlocale(LC_MONETARY, "en_US");
								 		$Totalprice = $value['price'] * $quantity;
					 				?>
					 	<tr>
						 	<td style='width:100px; padding-left:0px'>
						 		<img src='<?php echo PATH ?>view/purchase/product-images/<?php echo $value['product_image'] ?>' width='100%' style='display: inherit;' />
						 	</td>
					 		<td>
								<div class="productName" style='margin-bottom: 0px;
'><?php echo $value['product_name'] ?></div>
<div style='clear:both'>Price: <?php echo money_format("%10.2n", $value['price']) ?></div>
								
					                	<div class="colorBox" style="background: <?php echo $colorChoice->ColorHexacode ?>;">&nbsp;</div>
					                        <div class="colorName" style="margin-top:0px;"><?php echo $colorChoice->Color ?></div>
					                        
							</td>
														<td style='text-align:center'>
																<?php echo $quantity ?>
															
														</td>
														<td style='text-align:right'>
															<?php echo money_format("%10.2n", $Totalprice) ?>
														</td>
														
										
									</tr>
					 				
					 				
				<?php endforeach ?>
			</table>
			<div class='confirmButton'>
        		 	   <input type="submit" value="Confirm" style="width:100%;" onclick="CartController.Processing()"/>
        		</div>
			</div>
		</div>
		<div class="CartTotalSection">
		<div class="header">
				Your Total
			</div>
			<table>
	            <tr>
                <td>
                    Cart Total:
                </td>
                <td>
                    <?php echo money_format("%10.2n", $cartTotal) ?> USD
                </td>
            </tr>
            <tr>
                <td>
                    Shipping
                </td>
                <td style="text-align:right">
                    <?php echo money_format("%10.2n", $this -> shippingRates) ?> USD
                </td>
            </tr>
            <tr>
                <td>
                    Taxes:
                </td>
                <td style="text-align:right">
                    <?php echo money_format("%10.2n", $this -> salesTax) ?> USD
                </td>
            </tr>
            
            <tr>
                <td>
                    Your Total:
                </td>
                <td style="text-align:right">
                    <?php echo money_format("%10.2n", $cartTotal + $this -> salesTax + $this -> shippingRates) ?> USD
                </td>
            </tr>
	      	</table>
		</div>
		<div class="CartTotalSection" style="margin-top:15px;">
		<div class="header">
				Ship. Address
			</div>
			<div style="padding:10px;">
			<div style="margin-bottom:5px; color:black"><?php echo $_SESSION['checkout']['first-name'].' '.$_SESSION['checkout']['last-name'];?></div>
			<div style="margin-bottom:5px; color:black"><?php echo $_SESSION['checkout']['address'];?></div>
			<div style="color:black"><?php echo $_SESSION['checkout']['city'].', '. $_SESSION['checkout']['state'] . " " . $_SESSION['checkout']['zip'];?></div>

            
			</div>
		</div>
		<div class="CartTotalSection" style="margin-top:15px;">
		<div class="header">
				Contact Info.
			</div>
			<?php $areaCode = substr(preg_replace('/[^0-9]/','',$_SESSION['checkout']['phone']), 0, 3);
					  $RestOfPhone1 = substr(preg_replace('/[^0-9]/','',$_SESSION['checkout']['phone']), 3, 3);
					  $RestOfPhone2 = substr(preg_replace('/[^0-9]/','',$_SESSION['checkout']['phone']), 6, 7); 
					$telephone = "(" .$areaCode . ") ". $RestOfPhone1 . " - " . $RestOfPhone2;
					
				?>
			
			
			<div style="padding:10px;">
			<div style="margin-bottom:5px; color:black"><?php echo $_SESSION['checkout']['email']; ?></div>
			<div style="color:black"><?php echo $telephone;?></div>

            
			</div>
		</div>
		<div style="clear:both"></div>
		<div class='confirmButtonMobile'>
         	   <input type="submit" value="Confirm" style="width:100%;" onclick="CartController.Processing()"/>
        	</div>
	</div>

    
 
    
    
    
</form>
<?php endif; ?>
	
	</div>
	<div class="contentPush"></div>
	</div>
	</div>
</div>
<div class="processingOverlay"></div>
<div class="ProcessingPopup" style="text-align:center">
	<div class="header">Processing</div>
	<img src="<?php echo PATH?>public/images/processingLoader.gif" />
</div>