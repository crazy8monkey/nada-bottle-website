<div class="pageContent" style="background: #c1e8fb; padding-top: 15px;">
	<div class="wrapper">
		<div class="contentContainer">
			<div class="contentWrapper">
				<?php if ($this -> locateLifePermissions['locateLifeSettings'] == 1) : ?>
	
	<div class="locateLifeForm">
		<div class="locateLifeText">
			
				<div style="position: relative;">
					<?php require 'view/message-section.php' ?>	
				</div>
				<div class="locateLifeGeneratorSection">
					<div style="position:relative;">
						<div class="text">
							Locate Life Generator
							
						</div>	
						<div class="divider" style="position: absolute; top: 50%; width: 100%; z-index: 0;"></div>
					</div>
					
				</div>
				<div class="formInputs">
					<form method="post" id="locateLifeForm" action="<?php echo PATH ?>locateLife/getlocation">
						<input type='text' placeholder="longitude" name="longitude" />	
						<input type='text' placeholder="latitude" name="latitude" />
						<input type="submit" value="Locate Life" />
					
					</form>	
				</div>	
				<div class="divider"></div>
			
		</div>	
	</div>
	<div class="Image">
		<img src="<?php echo PATH ?>public/images/LocateLife-Image.png" alt="Locate Life">				
	</div>
	<div style="clear:both"></div>
	<?php endif; ?>
	<?php if ($this -> locateLifePermissions['locateLifeSettings'] == 0) : ?>
		<div class="locateLifeForm" style="  float: none; background: transparent; width: auto; position: static;">
			<h2>Locate Life</h1>
			<div style="margin-top: 15px; border-bottom: 1px solid #f4f4f4; padding-bottom: 10px; margin-bottom: 20px;">
				Locate the lives you’ve saved. Give more, see results, save lives.<br />
				Our Mission to “Locate Life” and provide clean drinking water with every purchase
			</div>
			<div style="margin-top:15px">
			<h2 style="margin-bottom:10px;">Coming Soon!</h1>
			Nada Bottle Customers can enter their authentic link between the product they purchased and the recipient of 
			the safe water. Each customer will receive a handwritten sticker sharing the exact coordinates of the school 
			or health clinic that received the LifeStraw® Community purifier as a result of their purchase.<br /><br />

			By purchasing a Nada Bottle the LifeStraw® products by Vestergaard will become more available for South Africa, 
			Namibia, Botswana, Swaziland, Lesotho, Transkei and Zimbabwe. <br /><br />

			Nada Bottle is committed to providing safe drinking water by supplying LifeStraw® filters, by Vestergaard Frandsen(VF), 
			to protect the worlds most vulnerable people of basic diseases, malaria and other vector-borne diseases.
			</div>
		</div>
		
	<?php endif; ?>
			</div>
		</div>
		<div class="contentPush"></div>
	</div>
</div>

