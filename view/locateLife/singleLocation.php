<script type="text/javascript">
	$(document).ready(function() {
		$("a#AboutUsLink").fancybox({
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	false
			});
	})
</script>

<div class="pageContent singleLocationInformation" style="background:url(<?php echo PATH ?>view/cms/locatelife/location/<?php echo $this -> singleLocation -> locationID . "/" . $this -> singleLocation -> currentImage ?>) no-repeat; padding-top:10px;">
	<div class="locateLifeForm" style="position:relative">
		
	</div>
	<div class="locationInformation">
		<a class="fancybox fancybox.iframe" href="<?php echo $this -> singleLocation -> locationVideo?>" id="AboutUsLink">					
			<div class="playIcon"></div>
		</a>
		
		<div class="header">
			<?php echo $this -> singleLocation -> name?>	
		</div>
		<?php echo $this -> singleLocation -> description?>	
	</div>
	<div class="wrapper">
		<div class="contentContainer">
			<div class="contentWrapper">
				
				<div style="clear:both"></div>
			</div>
		</div>
		<div class="contentPush"></div>
	</div>
	
</div>

<div style="clear:both"></div>
