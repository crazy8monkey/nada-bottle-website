<ul class="warrantyBulletPoints">
	<li>
		You are responsible for all costs associated with returning the Product to use for replacement under our warranty. If we deem the Product returned
by you to us qualifies for replacement under our warranty, we will pay the shipping and handling associated with returning a replacement Product
to you.
	</li>
	<li>
		Nada Bottle LLC is not responsible for any Products sent by you to us in connection with a warranty claim which are lost in the mail. We advise you
to insure your package sent to use and tracking information.
	</li>
	<li>
		At Nada Bottle, we’re a tiny team and our goal is to keep your satisfied. To do so, please allow us 7-10 business days to process a return from the day
that we receive your product and the warranty form at our warehouse.
	</li>	
</ul>