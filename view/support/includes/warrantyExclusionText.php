<div style="font-family:arial; color:black; font-size:11px;">
	<strong>Warranty Exclusions</strong><br /><br />

	Our Product warranty does not cover damage resulting from any of the following: normal wear, alterations, abuse, 
	taking apart of the Product, abrasion, improper care, accidents, prolonged UV exposure to inner PET liner, and/or natural 
	breakdown of materials over a prolonged period.<br /><br />

	EXCLUDING THE ONE (1) YEAR WARRANTY PROVIDED ABOVE, NADA BOTTLE LLC, ITS AFFILIATES AND THEIR SUPPLIERS, TO THE MAXIMUM 
	EXTENT PERMITTED BY LAW, MAKE NO WARRANTIES, EXPRESSED OR IMPLIED, AND DISCLAIM ALL WARRANTIES, DUTIES AND CONDITIONS, WHETHER EXPRESSED, 
	IMPLIED OR STATUTORY, WITH RESPECT TO THE PRODUCT, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, AGAINST 
	LATENT DEFECTS, OR FITNESS FOR A PARTICULAR PURPOSE.<br /><br />

	<strong>Limitation of Liability</strong><br /><br />

	Nada Bottle LLC, its affiliates and their suppliers' maximum liability hereunder is limited to incidental damages not to exceed the 
	original purchase price of the Product. NADA BOTTLE LLC, ITS AFFILIATES AND THEIR SUPPLIERS HEREBY DISCLAIM AND EXCLUDE ANY LIABILITY 
	FOR ANY CONSEQUENTIAL OR OTHER DAMAGES RESULTING FROM ANY REASON WHATSOEVER. THIS LIABILITY EXCLUSION AND LIMITATION APPLIES TO ALL LEGAL 
	THEORIES UNDER WHICH DAMAGES MAY BE SOUGHT. 

</div>
