<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".warrantDescription");
	});
</script>

<div id="backgroundImage" class="coloradoFalls">
	<div id="backgroundImageOverlay"></div>
</div>
<div class="pageName">
	<div class="wrapper">
		<div class="entryText">
			<?php echo $this -> content -> contentHeader ?>
		</div>		
	</div>		
</div>


<div class="pageContent">
	<?php //echo Hash::createRandomString("warranty"); ?>
	<div class="wrapper">
		<div class="contentContainer">
			<?php if(isset($this -> warrantySection)) :?>
			<div class="contentWrapper">
				<?php echo $this -> content -> content ?>
			</div>
			<div class="warrantySubmitForm">
				<form action="<?php echo PATH ?>support/newWarranty" id="WarrantyForm" method="post" enctype="multipart/form-data">
					<div class="steps">
						<h1>1. Defect Details</h1>
						<div id="descriptionError"></div>
						<div class="descriptionText">
							<?php echo $this -> content -> warrantyQuestion2 ?>
						</div>	
						<textarea name="warrantDescription" onkeyup="Globals.textAreaAdjust(this)" class="warrantDescription"></textarea>
					</div>
					<div class="steps">
						<h1>2. Your Order Number</h1>
						<div class="userInfo">
							<div id="OrderIDError"></div>
							<div class="descriptionText" style="margin-bottom:10px;">
								To verify that you are a Nada Bottle Customer, please type in your Order # related to your purchase, and your Order verification ID that is shown in your receipt
							</div>
							<div class="line">
								<div class="left">
									<div class="label">Your Order Number</div>
									<input type="text" name="OrderNumber" />	
								</div>
								<div class="right">
									<div class="label">Order Verification ID</div>
									<input type="text" name="OrderVerificationNumber" />
								</div>
								<div style="clear:both"></div>
							</div>
						</div>
					</div>
					<div class="steps">
						<h1>3. Pics of the Problem</h1>
						<div id="warrantyPhotoError"></div>
						<div class="descriptionText">
							<?php echo $this -> content -> warrantyQuestion1 ?>
						</div>
						<div class="inputFileLine" style="margin-top:5px;">
							<div class="fileInput">
								<input type="file" id="warrantyUpload1" class="warrantyPhoto" onchange="SupportController.showRemoveButton(this);" name="warrantyPhoto[]" />		
							</div>
							<a href="javascript:void(0);" onclick="SupportController.removeSelectedPhoto(this);">
								<div class="removePhoto">Remove</div>	
							</a>
							
							<div style="clear:both"></div>
						</div>	
						<div class="inputFileLine">
							<div class="fileInput">
								<input type="file" id="warrantyUpload2" class="warrantyPhoto" onchange="SupportController.showRemoveButton(this);" name="warrantyPhoto[]" />
							</div>
							<a href="javascript:void(0);" onclick="SupportController.removeSelectedPhoto(this);">
								<div class="removePhoto">Remove</div>
							</a>
							
							<div style="clear:both"></div>
						</div>
						<div class="inputFileLine">
							<div class="fileInput">
								<input type="file" id="warrantyUpload3" class="warrantyPhoto" onchange="SupportController.showRemoveButton(this);" name="warrantyPhoto[]" />
							</div>
							<a href="javascript:void(0);" onclick="SupportController.removeSelectedPhoto(this);">
								<div class="removePhoto">Remove</div>
							</a>
							<div style="clear:both"></div>
						</div>
						
						<div id="warrantyErrorPhoto"></div>
					</div>
					
					<div class="steps" style="margin-bottom:10px;">
						<h1>4. Your Information</h1>
						<div id="contactInfo"></div>
						<div class="userInfo">
							<div class="userInfoSection">Contact Information</div>
							<div class="line">
								<div class="label">Full Name</div>
								<div class="left">
									<input type="text" placeholder="First Name" name="firstName" />
								</div>	
								<div class="right">
									<input type="text" placeholder="Last Name" name="lastName" />
								</div>
								<div style="clear:both"></div>
							</div>
							<div class="line">
								<div class="left">
									<div class="label">Email</div>
									<input type="text" name="userEmail" />
								</div>	
								<div class="right">
									<div class="label">Phone Number</div>
									<input type="text" name="userPhoneNumber" />
								</div>
								<div style="clear:both"></div>
							</div>
							<div class="userInfoSection" style="margin-top:15px;">Shipping</div>
							<div id="ShippingError"></div>
							<div class="line">
								<div class="label">Address</div>
								<input type="text" name="shipAddress" />
							</div>
							<div class="line">
								<div class="third marginRight">
									<div class="label">City</div>
									<input type="text" name="shipCity" />	
								</div>
								<div class="third marginRight">
									<div class="label">State</div>
									<select name="shipState">
										<option value="0"></option>
										<?php foreach($this->states as $key => $value) : ?>
											<option value='<?php echo $key ?>'><?php echo $value ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="third">
									<div class="label">Zip</div>
									<input type="text" name="shipZip" maxlength="5" />	
								</div>
								<div style="clear:both"></div>
							</div>
						</div>
					</div>
					<div class="steps">
						<h1>5. Print Your Warranty</h1>
						<div class="descriptionText">
							<?php echo $this -> content -> warrantyFormDescription ?>
						</div>
					</div>
					<div class="steps" style="margin-bottom:50px; margin-top:0px;">
						<input type="submit" class="warrantyBtn" value="Submit Warranty" style="width:auto;">
					</div>
				</form>
				<div style="clear:both"></div>
			</div>
			<?php endif; ?>
			<?php if(isset($this -> warrantySent)) :?>
			<div class="warrantySubmitForm" style="border-top:none; padding-top:0px;">

				<div class="steps" style="margin-bottom:15px; border-bottom:1px solid #e9e9e9">
					<h1 style="margin-bottom:10px;">Warranty Request Sent</h1>
					<p>Thank you for submitting your warranty request to Nada Bottle LLC.. We will take a look what you submitted, and we will 
						inform you the next step you should take. For your records, please check your email to receive/print a copy of your 
						warranty you have submitted to us.<br /><br />

						When it comes to our warranty here are a few things to consider.</p>
						<?php require 'view/support/includes/warrantyNotResponsibleText.php' ?>
					
					<h1 style="margin: 50px 0px 10px 0px;">Thank you for being a Nada Bottle customer!</h1>
					
				</div>
				<?php require 'view/support/includes/warrantyExclusionText.php' ?>
				<div style="margin:25px 0px 45px 0px;">
					<a href="<?php echo PATH ?>support/warranty"class="warrantyBtn">Back to warranty</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
		
		<div class="contentPush"></div>
	</div>
</div>
<div class="ajaxLoadingIndicator">
	<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
</div>
<div class="mysqlerrorPopup">
<?php echo SYSTEM_ERROR_MESSAGE ?>

</div>