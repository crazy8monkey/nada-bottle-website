<div class="warranty">
<?php if(isset($_GET['token'])) {
	if($_GET['token'] == isset($this -> warrantyPrint -> securityTokenCheck)) {?>


	<div class="warrantyPrintHeader">
		YOUR WARRANTY INFO
	</div>
	<div class="warrantySection">
		Warranty: #<?php echo $this -> warrantyPrint -> _warrantyID ?> - <?php echo $this -> recordedTime -> formatDate($this -> warrantyPrint -> submittedDate) . ' / ' . $this -> warrantyPrint -> submittedTime ?>
	</div>
	<div class="warrantySection">
		<div class="secondaryHeader">
			Your Information
		</div>
		<?php echo $this -> warrantyPrint -> firstName . ' ' . $this -> warrantyPrint -> lastName ?><br />
		<?php echo $this -> warrantyPrint -> email ?>
	</div>
	<div class="warrantySection">
		<div class="secondaryHeader">
			Shipping Information
		</div>
		<?php echo $this -> warrantyPrint -> address  ?><br />
		<?php echo $this -> warrantyPrint -> city . ', ' . $this -> warrantyPrint -> state . ' ' . $this -> warrantyPrint -> zip ?>
	</div>	
	<div class="warrantySection">
		<div class="secondaryHeader">
			Order #
		</div>
		<?php echo $this -> warrantyPrint -> orderNumber  ?>
	</div>				
	<div class="warrantySection" style="border-top:1px solid #cecece; border-bottom:1px solid #cecece; padding:15px 0px">
		<div class="secondaryHeader">
			Defect Details
		</div>
		<?php echo $this -> warrantyPrint -> description  ?>
	</div>
	<div class="warrantySection" style="border-bottom:1px solid #cecece;">
		<?php require 'view/support/includes/warrantyNotResponsibleText.php' ?>
	</div>
	<div class="warrantySection" style="padding:0px 0px 15px 0px">
		<?php require 'view/support/includes/warrantyExclusionText.php' ?>
	</div>
</div>
<div class="pageWrapper">
	<a href="javascript:void(0);" onclick="window.print();">
		<div class="warrantyPrintButton">
			<img src="<?php echo PATH ?>public/images/warrantyPrintButton.png" />
		</div>	
	</a>

	<?php } else { ?>
		<div class="padding">
			This page is called Incorrectly
		</div>
	<?php } ?>
<?php } else { ?>
	<div class="padding">
		This page is called Incorrectly
	</div>
<?php } ?>
</div>
