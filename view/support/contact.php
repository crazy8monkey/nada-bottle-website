<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".comments");
	});
</script>

<div id="backgroundImage" class="coloradoFalls">
	<div id="backgroundImageOverlay"></div>
</div>
<div class="pageName">
	<div class="wrapper">
		<div class="entryText">
			<?php echo $this -> content -> contentHeader ?>
		</div>		
	</div>	
</div>
<div class="pageContent" style="padding-top: 20px;">
	<div class="wrapper">
		<div class="contentWrapper">
		<div style="margin-bottom:10px;"><h2>We're here to help</h2></div>
		<div class="contactFormContainer">
			<div style="position: relative;" id="messageContainer">
				<?php require 'view/message-section.php' ?>	
			</div>
			<form class="contactFormElement" method="post" action="<?php echo PATH ?>support/contactInquirySubmit">
				<div class="contactFormLine">
					<div class="contactLabel">Full Name</div>
					<input type="text" class="fullName" name="firstName" placeholder="First Name" />
					<input type="text" class="fullName lastName" name="lastName" placeholder="Last Name" />	
				</div>
				<div class="contactFormLine">
					<div class="contactLabel">Email</div>
					<input type="text" class="email" name="email" />	
				</div>
				<div class="contactFormLine">
					<div class="contactLabel">Comments/Concerns</div>
					<textarea name="comments" class="comments" onkeyup="Globals.textAreaAdjust(this)"></textarea>
				</div>
				<div class="contactFormLine">
					<input type="submit" value="Submit" />
				</div>
			</form>
		</div>
		
		<div class="contactText">
			<?php echo $this -> content -> content ?>
		</div>
		</div>

	</div>
	<div style="clear:both"></div>
</div>
