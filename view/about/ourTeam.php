<div id="backgroundImage" class="creseteButte">
	<div id="backgroundImageOverlay"></div>
</div>
<div class="pageName">
	<div class="wrapper">
		<div class="entryText">
			Our Team
		</div>		
	</div>	
</div>
<div class="pageContent">
	<div class="wrapper">
		<div class="contentContainer" >
			<div class="contentWrapper" id="teamMemberPage">
				<div style="margin:0 auto;">
				<?php foreach ($this -> teamMembers as $key => $value) : ?>
					
					<?php if($value['teamLink'] != NULL) : ?>
						<a href="http://www.<?php echo $value['teamLink']?>" target="_blank">
							<div class="teamMemberSingle">
								<div style="background:url(<?php echo PATH. "view/cms/users/team-images/" . $value['photo'] ?>) no-repeat" class="profilePic"></div>
								<div class="teamName">
									<?php echo $value['firstName'] . ' ' . $value['lastName'] ?>
								</div>
								<div class="jobTitle">
									<?php echo $value['jobTitle'] ?>
								</div>
								<div class="text">
									<?php echo $value['description'] ?>
								</div>
								<div class="teamLink">
									<img src="<?php echo PATH ?>public/images/teamLink.png" />
								</div>
								
							</div>
							
						</a>
					<?php endif; ?>
					<?php if($value['teamLink'] == NULL) : ?>
					<div class="teamMemberSingle">
						
						<div style="background:url(<?php echo PATH. "view/cms/users/team-images/" . $value['photo'] ?>) no-repeat" class="profilePic"></div>
						<div class="teamName">
							<?php echo $value['firstName'] . ' ' . $value['lastName'] ?>
						</div>
						<div class="jobTitle">
							<?php echo $value['jobTitle'] ?>
						</div>
						<div class="text">
							<?php echo $value['description'] ?>
						</div>
						
					</div>
					<?php endif; ?>
				<?php endforeach; ?>
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
		<div class="contentPush"></div>
	</div>
</div>