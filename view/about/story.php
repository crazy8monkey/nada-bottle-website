<div id="backgroundImage" class="creseteButte">
	<div id="backgroundImageOverlay"></div>
</div>
<div class="pageName">
	<div class="wrapper">
		<div class="entryText">
			<?php echo $this -> content -> contentHeader ?>
		</div>		
	</div>	
</div>
<div class="pageContent">
	<div class="wrapper">
		<div class="contentContainer">
			<div class="contentWrapper">
				<?php echo $this -> content -> content ?>
			</div>
		</div>
		<div class="contentPush"></div>
	</div>
</div>