<div id="backgroundImage" class="creseteButte">
	<div id="backgroundImageOverlay"></div>
</div>
<div class="pageName">
	<div class="wrapper">
		<div class="entryText">
			<?php echo $this -> content -> contentHeader ?>
		</div>		
	</div>	
</div>

<div class="pageContent">
	<div class="wrapper">
		<div class="contentContainer">
			<div class="contentWrapper">
				<?php echo $this -> content -> content ?>
				<div style='text-align:center; line-height:25px;'>
				<?php foreach ($this -> thankyoulist as $key => $value) {
					echo "<span class='color:#9c9c9c !important'>" . $value['supporterName'] . "</span> <span style='color:#217CAB'>•</span> ";
				} ?>
				</div>
			</div>
		</div>
		<div class="contentPush"></div>
	</div>
</div>