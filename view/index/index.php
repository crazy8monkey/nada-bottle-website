<!-- http://www.slidesjs.com/-->	
	<script>
    $(function(){
      $("#slideShow").slidesjs({
      	play: {
      		active: false,
			interval: 5000,
			auto: true,
			restartDelay: 5000
		},
        width: 940,
        height: 450
      });
    });
  </script>	
	<!--<div class="entryText">
		Haiti, Las Vegas, New Dehli, Nakuru. Where will your clean water donation go?<br />
		LOCATE LIFE.
	</div>-->
	
<div class="pageContent homePageContent">
	<div class="wrapper">
		<div id="slideShow">
			<div class="slide">
					<div class="locateLifeButtonRedirect">
						<a style="color:White; text-decoration:none;" href="<?php echo PATH ?>purchase">
							<div class="locateLifeLink">
								Shop
							</div>
						</a>
					</div>
							
				<img src="<?php echo PATH ?>public/images/shop.jpg" />
			</div>
			<div class="slide">
				<div class="locateLifeSlideContainer">
					<div class="locateLifeButtonRedirect">
							<a style="color:White; text-decoration:none;" href="<?php echo PATH ?>locateLife">
								<div class="locateLifeLink">
									Locate Life
								</div>
							</a>
						</div>
					<div style="clear:both"></div>
				</div>
				
				<img src="<?php echo PATH ?>public/images/LocateLife-Image.png" alt="Locate Life">
			</div>
			<div class="slide">
				<div class="locateLifeSlideContainer">
					<div class="locateLifeButtonRedirect">
							<a style="color:White; text-decoration:none;" href="<?php echo PATH ?>about/cleanWater">
								<div class="locateLifeLink">
									Clean Water
								</div>
							</a>
						</div>
					<div style="clear:both"></div>
				</div>
				<img src="<?php echo PATH ?>public/images/Boy.jpg" alt="Locate Life">
			</div>
			<a href="#" style='text-decoration:none;' class="slidesjs-previous slidesjs-navigation"><i class="icon-chevron-left icon-large"></i></a>
      		<a href="#" style='text-decoration:none;' class="slidesjs-next slidesjs-navigation"><i class="icon-chevron-right icon-large"></i></a>
      		
		</div>
	</div>
	<div class="wrapper">
	<div class="OurStorySection" style="position:relative; border-top: 1px solid #f4f4f4; margin-top: 20px;">
		<div class="sloganSection">
			<img src="<?php echo PATH ?>public/images/HomeMapImage.png" />
			<div class="slogalText">
				
					For every Nada Bottle sold, one person in an impoverished community will be provided with safe drinking water.
				</div>
				
			</div>
		</div>
		
		
	</div>
	

	
	
	
	<?php if(!empty($this->partners)) : ?>
	<div class="ourPartners">
		<div class="wrapper">
			<div style="margin-bottom: 50px;"><h1>Our Partners</h1></div>
			<div class="sponsorsSlideShow">
				<ul class="sponsorsList">
					<?php 
					$i = 0;
					echo "<li class='active'>";
					foreach ($this->partners as $key => $value) { ?>
						<?php if($value['publishState'] == 1) : ?>
						<a href="http://www.<?php echo $value['partnerLink'] ?>" target="_blank" style="text-decoration:none;">
							<img src="<?php echo PATH ?>view/index/partnersImages/<?php echo $value['partnerImage'] ?>" />	
						</a>
						<?php endif; ?>
						<?php $i++; 
						if($i % 11 === 0) { ?>
							</li><li style='display:none;'>
						<?php } 
					 } 
					echo "</li>";
					 ?>
					
				</ul>
				<?php if($i > 11) { ?>
					<a href="javascript:void(0);" onclick="IndexController.leftArrowPartnerSlideShowClick()">
						<div id="leftSlideShowArrow"></div>	
					</a>
					<a href="javascript:void(0);" onclick="IndexController.rightArrowPartnerSlideShowClick()">
						<div id="rightSlideShowArrow"></div>
					</a>
				<?php } ?>
					
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
	<?php endif; ?>	
	<div class="contentPush"></div>
</div>


