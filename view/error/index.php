<link rel="stylesheet" href="<?php echo PATH ?>public/font/style.css" type="text/css" />
<script src="<?php echo PATH ?>public/js/globals.js" type="text/javascript"></script>
<div class="wrapper" style="max-width: 450px; padding:0px 10px;">
	<div class="errorHeader">
		<img src="<?php echo PATH ?>public/images/nadaBottleLogoIcon.png" />
		<h1>Page Not Found</h1>
	</div>
	<div class="errorText">
		Looks like this page does not exist.  Maybe this is 
		a broken link? something broke while you are on our site? <br /><br />

		Please fill out this form and give us a step by step 
		description on how the issue was caused so we can look 
		into and/or fix the issue.
	</div>
	<div id="messageContainer" style="position: relative;">
		<?php require 'view/message-section.php' ?>	
	</div>
	<div class="formContainer">
		<form method="post" id="error-form" action="<?php echo PATH ?>error/insertError">
			<div class="inputLine">
				<div class="label">
					Full Name	
				</div>
				<input name="errorName" type="text">
			</div>
			<div class="inputLine">
				<div class="label">
					Email
				</div>
				<input name="errorEmail" type="text">
			</div>
			<div class="inputLine">
				<div class="label">
					Description
				</div>
				<textarea name="errorDescription" class='error-input'></textarea>
			</div>
			<input type="submit" value="Submit" />	
		</form>
	</div>
</div>
	
