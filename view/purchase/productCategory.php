<div id="backgroundImage" class="coloradoFalls2">
	<div id="backgroundImageOverlay"></div>
</div>
<div class="wrapper">
	<div class="entryText" style="border-bottom:none; padding: 0px;"></div>		
</div>	

<div class="pageContent">
	<div class="wrapper">
		<div class="contentWrapper">
			<?php if(isset($_GET['productType'])) : ?>
			
			<?php $category = Category::categoryName($_GET['productType']); ?>
				<?php if(!empty($category -> categoryImage)) : ?>
				<div class="categoryImage">
					<img src="view/purchase/category-images/<?php echo $category -> categoryImage; ?>" />
				</div>
				<?php endif; ?>
			<?php endif; ?>
			<div class="mobileProductCategoryElement">
				<div style="margin-bottom:5px;">
					<strong>Categories</strong>
				</div>
				<select class="mobileProductCategory" onchange="if (this.value) window.location.href=this.value">
					<option value="<?php echo PATH ?>purchase">All Products</option>
					<?php foreach ($this -> categories as $key => $value) : ?>
						<option value="<?php echo PATH ?>purchase?productType=<?php echo $value['ProductCategory'] ?>" <?php echo isset($_GET['productType']) && $_GET['productType'] == $value['ProductCategory'] ? 'selected' : '' ?>>
							<?php echo str_replace("_", " ", $value['ProductCategory']) ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
			
			<div class="CategoryList">
				<a href="<?php echo PATH ?>purchase">
					<?php
						$allSelected = "";
						if (!isset($_GET['productType'])) {
							$allSelected = " selected";
						}
					?>
					<div class="link<?php echo $allSelected ?>">
						All Products		
					</div>
				</a>
				<?php foreach ($this -> categories as $key => $value) : ?>
					<a href="<?php echo PATH ?>purchase?productType=<?php echo $value['ProductCategory'] ?>">
						<?php 
							$selectedClass = "";
							if (isset($_GET['productType'])) {
								if($value['ProductCategory'] == $_GET['productType']) { 
									$selectedClass = " selected";
								} else {
									$selectedClass = "";
								}	
							}
							
						?>
						
						<div class="link<?php echo $selectedClass ?>">
							<?php echo str_replace("_", " ", $value['ProductCategory']) ?>		
						</div>
					</a>
				<?php endforeach; ?>
				<div class="purchaseIcon"></div>
			</div>
			<div class="productList">
			
				<?php if(!empty($this -> products)) : ?>
				<ul class="productULContainer">
					
				<?php foreach ($this -> products as $key => $value) : ?>
					<?php if($value['publishState'] == 1) :?>
						<?php $productID = $this -> security -> encode($value['product_id']);
							$productCategory = $this -> security -> encode($value['product_category']);
						?>
					<li>
					<a href="<?php echo PATH ?>purchase/product?product=<?php echo urlencode($productID) ?>&productType=<?php echo urlencode($productCategory) ?>">
						<div class="productCard">
							<img src="<?php echo PATH ?>view/purchase/product-images/<?php echo $value['product_image'];?>" />
							<div class="priceInfo">
								<div class="header"><?php echo $value['product_name'] ?></div>
								$<?php echo $value['price'] ?>
							</div>
							<div class="mouseOverWhite"></div>
						</div>
					</a>
					</li>
					<?php endif; ?>
				<?php endforeach ?>
				</ul>
				<?php endif; ?>
				<?php if(empty($this -> products)) : ?>
					<div class="noProducts">	
						There are no products in this category.		
					</div>
				<?php endif; ?>
			</div>
			<div style="clear:both"></div>
		</div>
		<div class="contentPush"></div>
	</div>
</div>