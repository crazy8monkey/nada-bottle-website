<?php setlocale(LC_MONETARY,"en_US"); ?>

<div class="wrapper">
	<div class="entryText" style="border-bottom:none; padding: 0px;"></div>	
</div>	

<div class="pageContent" style='padding-top:0px;'>
	<div class="productBreadCrumbs" style='background:#f4f4f4'>
		<div class="wrapper">
			<a href="<?php echo PATH . "purchase?productType=" . $this -> productSingle['product_category']?>"><?php echo str_replace("_", " ", $this -> productSingle['product_category']) ?></a><span>&#8250;</span><?php echo $this -> productSingle['product_name'] ?>
		</div>	
	</div>
	<div class="wrapper">
			
			<?php if($this -> preSaleVisible == 1) : ?>			
				<div style="color:white; font-size:16px; background:#c30000; text-align:center; padding:10px; margin:20px 0px;;">
					<?php echo $this -> preSaleText ?>
				</div>
			<?php endif; ?>
			
			<div class="contentWrapper">
				<?php $category = Category::categoryName($this -> security -> decode($_GET['productType'])); ?>
				<?php if(!empty($category -> categoryImage)) : ?>
					<div class="categoryImage">
						<img src="<?php echo PATH ?>view/purchase/category-images/<?php echo $category -> categoryImage; ?>" />
					</div>
				<?php endif; ?>
				
				
			<div style="clear:both">
				<div class="productImage">
					<div class="OutOfStockSection">Out of Stock</div>
		            <input type="hidden" id="hidProductImage" value="<?php echo PATH; ?>view/purchase/product-images/<?php echo $this -> productSingle['product_image'] ?>"/>
					<img id="imgProduct" src="<?php echo PATH; ?>view/purchase/product-images/<?php echo $this -> productSingle['product_image'] ?>" width="100%" />
					<div id="colorSelectedText"></div>
				</div>
				<form method="post" id="AddToCart" action="<?php echo PATH ?>purchase/sendtocart">
				<div class="productContent">
					<h2><?php echo $this -> productSingle['product_name'] ?></h2>
					<div style="position: relative;" id="messageContainer">
						<?php require 'view/message-section.php' ?>	
					</div>
		
					<div class="productPricing">
						<div class="quantity">
							<input type="text" value="1" name="quantity" class="quantityTextInput" id="quantityChange" />	
						</div>
						<div class="price">
							$<span id="priceByQuantityChange"><?php echo $this -> productSingle['price'] ?></span>
							<div style="display:none" id="currentPriceContainer"><?php echo $this -> productSingle['price'] ?></div>
						</div>
						
						<div style="clear:both"></div>
					</div>
					
					<div style="margin-bottom:15px;">
						<div class="productColorSection">
							<?php foreach ($this -> colors as $key => $value) { ?>
								<a href="javascript:void(0);" onclick="PurchaseController.SelectProductColor(this)">
									<div class="colorBox" data-hex="<?php echo $value['hexacode'] ?>" data-colorid="<?php echo $value['productColor_id']; ?>" data-colorname="<?php echo $value['Color']; ?>" data-image="<?php echo PATH . "view/purchase/product-images/" . $value['productColorImage'] ?>" data-remaining="<?php echo $value['quantityRemaining']; ?>" style="background:<?php echo $value['hexacode'] ?>">
										<div class="colorBoxHover"></div>
									</div>
								</a>
							<?php }?>
							<input type="hidden" name="colorChoices" id="colorChoiceSelected"/>
						</div>
					</div>
					
					<div class='submitButton'>
						<input type="submit" class="addCartBtn" value="ADD TO CART" />
					</div>
					
					<div class="productDescription sectionLine">
						<div class="info">
							<a style="text-decoration:none;" href="javascript:void(0);" onclick="PurchaseController.expandDescription();"> 
								<div class="linkElement">
									<strong>
										<div class="plus description">
											<span class="toggleIndicator">-</span>
										</div> 
										Description
									</strong>
								</div>
							</a>
						</div>
						<div id="productDescriptionInfo">
							<?php echo $this -> productSingle['product_description'] ?>	
						</div>
						
					</div>
					<div class="productDescription sectionLine">
						<div class="info">
							<a style="text-decoration:none;" href="javascript:void(0);" onclick="PurchaseController.expandSpecs();"> 
								<div class="linkElement">
									<strong>
										<div class="plus spec">
											<span class="toggleIndicator">+</span>
										</div> 
										Specifications
									</strong>
								</div>
							</a>
							<div id="productDimentionInfo" style="display:none; margin-left:20px;">
								<div class="line">
									<div class="label">Outer:</div>
									<?php echo $this -> productSingle['spec_outer'] ?>
								</div>
								<div class="line">
									<div class="label">Inner:</div>
									<?php echo $this -> productSingle['spec_inner'] ?>
								</div>
								<div class="line">
									<div class="label">Strap:</div>
									<?php echo $this -> productSingle['spec_strap'] ?>
								</div>
								<div class="line">
									<div class="label">Cap:</div>
									<?php echo $this -> productSingle['spec_cap'] ?>
								</div>
								<div class="line">
									<div class="label">Grommet:</div>
									<?php echo $this -> productSingle['spec_grommet'] ?>
								</div>
								<div class="line">
									<div class="label">Width:</div>
									<?php echo $this -> productSingle['width'] ?> in
								</div>
								<div class="line">
									<div class="label">Height:</div>
									<?php echo $this -> productSingle['height'] ?> in
								</div>
								<div class="line">
									<div class="label">Depth:</div>
									<?php echo $this -> productSingle['length'] ?> in
								</div>
								<div class="line">
									<div class="label">Weight:</div>
									<?php echo $this -> productSingle['weight'] ?> lb
								</div>
								<div class="line">
									<div class="label">Volume:</div>
									<?php echo $this -> productSingle['spec_volumne'] ?>
								</div>
							</div>
						
						</div>
					</div>
					
					<input type="hidden" name="productName" value="<?php echo $this -> productSingle['product_name'] ?>" />
					<input type="hidden" id="hiddenProductID" name="productID" value="<?php echo $this -> productSingle['product_id'] ?>" />
					<input type="hidden" id="hiddenTotalPriceValue" name="productQuantityPrice" value="<?php echo $this -> productSingle['price'] ?>"/>
					<input type="hidden" id="hidImage" name="productImage" value="<?php echo $this -> productSingle['product_image'] ?>" />
					
				</div>
				<div style="clear:both"></div>
			</div>
			
			</form>
			
				
				
			<div style="clear:both"></div>
			</div>
			<div class="contentPush"></div>
		
	</div>
</div>