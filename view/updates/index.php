<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".feedBackNotes");
	});
</script>
<div class="wrapper">
	<div class="entryText">
		Feed Back	
	</div>
	<div class="whiteEmailContainer">
		
		<div style="position:relative; margin-bottom: 10px;" id="messageContainer">
			<?php require 'view/message-section.php' ?>
		</div>
		<form method="post" id="emailForm" action="<?php echo PATH ?>updates/enterFeedback">
			<div class="label">Email</div>
			<input type="text" name="email" />
			<div class="label">Feed back notes</div>
			<textarea class="feedBackNotes" onkeyup="Globals.textAreaAdjust(this)" name="feedback"></textarea>
			<input type="submit" value="Submit" />	
		</form>	
	</div>
</div>

