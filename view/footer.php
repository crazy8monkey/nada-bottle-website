<?php  if (isset($this -> adminWebsite)) : ?>
	<?php require 'view/message-section.php' ?>
	<div class='form-overlay'></div>
<?php endif; ?>	
<?php  if (isset($this -> mainWebsite)) : ?>
<?php if (isset($this -> showFooter)) : ?>
<footer>
	<div style="border-bottom:1px solid #f4f4f4"></div>
	<div class="wrapper">
		<div class="footerContent">
			<div class="feedBack">
				<!--<div class="header">Feed back Welcome!</div>
				<div class="text">
					We want to hear from you on how to make Nada Bottle a better shopping experience!	
				</div>
				<a class="emailUpdates" href="<?php echo PATH ?>updates">Feed Back</a>-->
				<div class="link emailLink">
					
					<div class="header">Join our mailing list</div>
					<div class="text" style="margin-bottom:0px;">
						For the latest Nada Bottle happenings & updates join our mailing list!	
					</div>
					<div id="emailValidationMessage"></div>
					<form method="post" id="emailUpdateSignup" action="<?php echo PATH . $this -> emailSignup; ?>/emailSignup">
						<input type="text" name="emailUpdates" PlaceHolder="enter your email" />
						<input type="submit" value="Sign up" style="margin-top:10px;" />
					</form>
				</div>
			</div>
			<div class="socialLinks">
				<div style="clear:both; width:100%">
					<a href="https://www.pinterest.com/jordanamercer/water-bottle/" target="_blank"><img src="<?php echo PATH ?>public/images/pinterest.png" width="32" /></a>
					<a href="https://instagram.com/nadabottle/" target="_blank"><img src="<?php echo PATH ?>public/images/instagram.png" width="32" /></a>
					<a href="https://vimeo.com/121263613" target="_blank"><img src="<?php echo PATH ?>public/images/vimeo.png" width="32" /></a>
					<!--<img src="<?php echo PATH ?>public/images/youTube.png" width="32" />-->
					<a href="https://twitter.com/NadaBottle" target="_blank"><img src="<?php echo PATH ?>public/images/twitter.png" width="32" /></a>
					<a href="https://www.facebook.com/nadabottle?fref=ts" target="_blank"><img src="<?php echo PATH ?>public/images/facebook.png" width="32" /></a>	
				</div>
				
				<div class="link">
						<a href="<?php echo PATH ?>support/warranty">RETURNS/WARRANTIES</a>
					</div>
					<div class="link">
						<a href="<?php echo PATH ?>support/contact">CONTACT</a>
					</div>
				
			</div>
			<div style="clear:both"></div>
		</div>
		<div style="clear:both"></div>
	</div>
	

	
<div class="credits">
	<div class="wrapper">
		<div class="creditYear">
			<?php echo date("Y") ?> &copy; Nada Bottle, LLC. All Rights Reserved.	
		</div>
		<div class="creditDeveloped">
			Site by <a style="text-decoration:underline;" href="http://www.adamistheschmidt.com">Adam Schmidt</a> 
		</div>
		<div style="clear:both"></div>
	</div>
</div>
</footer>
<?php endif; ?>
<?php endif; ?>
</body>	
</html>