

<div class="adminContent">
	<div class="header" style="border-bottom: 1px solid #cecece; margin-bottom:10px;">
		<div class="text" style="font-size:16px !important;">
			Mailing List: Emails 	
		</div>
		
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		<?php if(!empty($this -> emailList)) : ?>
			<?php $i=0; ?>
			<?php foreach ($this -> emailList as $key => $value) : ?>
				<?php 
					$style="";
					if ($i % 2 == 0){
						$style="background:#f1f1f1";
					}
				?>
				
				<div style="padding:7px; <?php echo $style ?>">
					
					<?php echo $value['registeredEmail'] ?>
					
				</div>
				<?php $i++; ?>
			<?php endforeach ?>
			
		<?php endif; ?>
		<?php if(empty($this -> emailList)) : ?>
			<div style="font-size: 16px; padding: 10px 0px; border-top: 1px solid #cecece;">There are no team registered emails in your mailing list</div>
		<?php endif; ?>
	</div>	
</div>


