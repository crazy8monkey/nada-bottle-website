<?php
function generateBreadcrumbString($name) {	
	$userBreadCrumb = $name;
	if($_SESSION["user"] -> FullName() == $name) {
		$userBreadCrumb .= " (You)";
	}
	return $userBreadCrumb;
}

function GenerateUsersPage($name) {
	$usersNamePageString;
	if($_SESSION["user"]-> FullName() !== $name) {
		$usersNamePageString = $name;
	} else {
		$usersNamePageString = "Your Profile";
	}
	return $usersNamePageString;
}

function showDeleteButton($name) {
	$hideDeleteButton = false;
	
	if($_SESSION["user"]-> FullName() !== $name) {
		$hideDeleteButton = true;
	}
	
	return $hideDeleteButton;
}

function getFullName($name) {
	return $name;
}


$userBreadCrumbString;
$NewUserHeader;
$showDeleteButtonVariable;
$firstName;
$lastName;
$userName;
$email;
$userFullName;

$pagePermission;
$ecommerceOrders;
$ecommerceCategories;
$ecommerceProducts;
$ecommerceTaxes;
$ecommerceWarranty;
$ecommerceInventory;
$ecommercePaymentLogs;
$contactInquriesPermissions;
$PartnersPermissions;
$LocateLifePermissions;
$UsersPermissions;
$FeedBackPermissions;
$SettingsEmail;
$SettingsLocateLife;
$SettingsTrackingEmailText;
$SettingsShippingAddress;
$generalEditPermissions;
$newsLetter;

$userFormURL;
$showNewPasswordSection = false;

if(isset($this -> user -> _userId)) {
	$userFormURL = PATH . "cms/saveUser/" . $this -> user -> _userId;
	$userBreadCrumbString = generateBreadcrumbString($this -> user -> FullName());
	$NewUserHeader = GenerateUsersPage($this -> user -> FullName());
	$showDeleteButtonVariable = showDeleteButton($this -> user -> FullName());
	$firstName = $this -> user-> FirstName;
	$lastName = $this -> user-> LastName;
	$userName = $this -> user -> Username;
	$email = $this -> user -> Email;
	$userFullName = getFullName($this -> user -> FullName());
	
	$pagePermission = $this -> user -> HasPermission('pages');
	$ecommerceOrders = $this -> user -> HasPermission('eCommerceOrders');
	$ecommerceCategories = $this -> user -> HasPermission('eCommerceCategories');
	$ecommerceProducts = $this -> user -> HasPermission('eCommerceProducts');
	$ecommerceTaxes = $this -> user -> HasPermission('eCommerceTaxes');
	$ecommerceWarranty = $this -> user -> HasPermission('eCommerceWarranty');
	$ecommerceInventory = $this -> user -> HasPermission('eCommerceInventory');
	$ecommercePaymentLogs = $this -> user -> HasPermission('eCommercePaymentLogs');
	$contactInquriesPermissions = $this -> user -> HasPermission('ContactInquiries');
	$PartnersPermissions = $this -> user -> HasPermission('Partners');
	$LocateLifePermissions = $this -> user -> HasPermission('LocateLife');
	$UsersPermissions = $this -> user -> HasPermission('Users');
	$FeedBackPermissions = $this -> user -> HasPermission('FeedBack');
	$SettingsEmail = $this -> user -> HasPermission('SettingsEmail');
	$SettingsLocateLife = $this -> user -> HasPermission('SettingsLocateLife');
	$SettingsTrackingEmailText = $this -> user -> HasPermission('SettingsTrackingEmailText');
	$SettingsShippingAddress = $this -> user -> HasPermission('SettingsShippingPickupAddress');
	$generalEditPermissions = $this -> user -> HasPermission('generalEditPermissions');
	$newsLetter = $this -> user -> HasPermission('newsLetterPermissions');
} else {
	$userFormURL = PATH . "cms/saveUser";
	$showNewPasswordSection = true;
	$userBreadCrumbString = "New User";
	$NewUserHeader = "New User";
	$showDeleteButtonVariable = false;
	$firstName = "";
	$lastName = "";
	$userName = "";
	$email = "";
	$userFullName = "";
	$pagePermission = "";
	$ecommerceOrders = "";
	$ecommerceCategories = "";
	$ecommerceProducts = "";
	$ecommerceTaxes = "";
	$ecommerceWarranty = "";
	$ecommerceInventory = "";
	$ecommercePaymentLogs = "";
	$contactInquriesPermissions = "";
	$PartnersPermissions = "";
	$LocateLifePermissions = "";
	$UsersPermissions = "";
	$FeedBackPermissions = "";
	$SettingsEmail = "";
	$SettingsLocateLife = "";
	$SettingsTrackingEmailText = "";
	$SettingsShippingAddress = "";
	$generalEditPermissions = "";
	$newsLetter = "";
}

?>


<div class="adminContent">
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Users')) : ?>
		<div class="transactionBreadCrumb">
			<a href="<?php echo PATH ?>cms/users">Users</a><span>&#8250;</span><span id="currentUser" style="font-size:12px;  margin: 0px;"><?php echo $userBreadCrumbString; ?></span>
		</div>
	<?php endif; ?>
	<div class="header">
		<div class="text">
			<?php echo $NewUserHeader; ?>
		</div>
		<?php if($showDeleteButtonVariable == true) :?>
		<div class="deleteContainer">
			<a href="<?php echo PATH ?>cms/delete/<?php echo $this -> user->GetUserId() ?>/users" style="text-decoration:none;">
				<div class="redButton deleteUser">
					Delete User
				</div>
			</a>
		</div>
		<?php endif; ?>
		<div style="clear:both"></div>
	</div>
	<form method="post" id="updateUser" action="<?php echo $userFormURL ?>">
		<div class="section">
			<div class="sectionHeader">User Information</div>
			<div class="inputLine">
				<div class="label">
					First Name
				</div>
				<input type="text" name="userFirstName" value="<?php echo $firstName ?>" />
				<div style="clear:both"></div>	
			</div>
			<div class="inputLine">
				<div class="label">
					Last Name
				</div>
				<input type="text" name="userLastName" value="<?php echo $lastName ?>" />
				<div style="clear:both"></div>	
			</div>
		</div>
		
		<div class="section">
			<div class="sectionHeader">Credentials</div>
			<div class="inputLine">
				<div class="label">
					Username
				</div>
				<input type="text" name="userUserName" value="<?php echo $userName ?>" />
				<div style="clear:both"></div>	
			</div>
			<div class="inputLine">
				<div class="label">
					Email
				</div>
				<input type="text" name="userEmail" value="<?php echo $email ?>" />
				<div style="clear:both"></div>	
			</div>
		</div>
		<?php if($showNewPasswordSection == true) : ?>
		<div class="section">
			<?php $newPassword = Hash::createRandomString("password"); ?>
			<div class="sectionHeader">Password</div>
			<div><strong>Users New Password after save:</strong> <?php echo $newPassword; ?></div>
			<div style="margin-top:5px;">
				*New user will be emailed of their new credentials
			</div>
			<input type="hidden" name="newUserPassword" value="<?php echo $newPassword ?>" />
		</div>
		<?php endif; ?>
		<?php if($_SESSION["user"]-> FullName() !== $userFullName) :?>
			<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('generalEditPermissions')) : ?>
		
			<div class="section" id="permissionsSection">
				<div class="sectionHeader">Permissions</div>
				<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('pages')) : ?>
					<div class="inputLine">				
						<div class="radioInput">
							<div class="contentLabel">Pages</div>
							<div class="labels">
								<input type="radio" name="chkPages" value="1" <?php echo $pagePermission ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkPages" value="0" <?php echo $pagePermission ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					</div>
				<?php endif ?>
				
				<div class="inputLine">
					<div class="permissionsLable" style="float:none; margin-bottom:5px;">
						eCommerce
					</div>
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceOrders')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Orders</div>
							<div class="labels">
								<input type="radio" name="chkOrders" value="1" <?php echo $ecommerceOrders ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkOrders" value="0" <?php echo $ecommerceOrders ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceCategories')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Categories</div>
							<div class="labels">
								<input type="radio" name="chkCategories" value="1" <?php echo $ecommerceCategories ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkCategories" value="0" <?php echo $ecommerceCategories ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceProducts')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Products</div>
							<div class="labels">
								<input type="radio" name="chkProducts" value="1" <?php echo $ecommerceProducts ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkProducts" value="0" <?php echo $ecommerceProducts ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceTaxes')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Taxes</div>
							<div class="labels">
								<input type="radio" name="chkTaxes" value="1" <?php echo $ecommerceTaxes ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkTaxes" value="0" <?php echo $ecommerceTaxes ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceWarranty')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Warranties</div>
							<div class="labels">
								<input type="radio" name="chkWarranties" value="1" <?php echo $ecommerceWarranty ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkWarranties" value="0" <?php echo $ecommerceWarranty ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceInventory')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Inventory</div>
							<div class="labels">
								<input type="radio" name="chkInventory" value="1" <?php echo $ecommerceInventory ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkInventory" value="0" <?php echo $ecommerceInventory ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Payment Logs</div>
							<div class="labels">
								<input type="radio" name="chkPaymentLogs" value="1" <?php echo $ecommercePaymentLogs ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkPaymentLogs" value="0" <?php echo $ecommercePaymentLogs ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
				</div>
	            <div class="inputLine">
	            	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('ContactInquiries')) : ?>
		            	<div class="radioInput">
		            		<div class="contentLabel">Contact Inquiries</div>
							<div class="labels">
								<input type="radio" name="chkContactInquiries" value="1" <?php echo $contactInquriesPermissions ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkContactInquiries" value="0" <?php echo $contactInquriesPermissions ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Partners')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Partners</div>
							<div class="labels">
								<input type="radio" name="chkPartners" value="1" <?php echo $PartnersPermissions ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkPartners" value="0" <?php echo $PartnersPermissions ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('LocateLife')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Locate Life</div>
							<div class="labels">
								<input type="radio" name="chkLocateLife" value="1" <?php echo $LocateLifePermissions ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkLocateLife" value="0" <?php echo $LocateLifePermissions ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Users')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Users</div>
							<div class="labels">
								<input type="radio" name="chkUsers" value="1" <?php echo $UsersPermissions ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkUsers" value="0" <?php echo $UsersPermissions ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('FeedBack')) : ?>
						<!--<div class="radioInput">
							<div class="contentLabel">Feed Back</div>
							<div class="labels">
								<input type="radio" name="chkFeedBack" value="1" <?php echo $FeedBackPermissions ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkFeedBack" value="0" <?php echo $FeedBackPermissions ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>-->
					<?php endif ?>	
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('newsLetterPermissions')) : ?>
						<div class="radioInput">
							<div class="contentLabel">NewsLetter</div>
							<div class="labels">
								<input type="radio" name="chkNewsLetter" value="1" <?php echo $newsLetter ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkNewsLetter" value="0" <?php echo $newsLetter ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
								
	            </div>
				<div class="inputLine">
					<div class="permissionsLable" style="float:none; margin-bottom:5px;">
						Settings
					</div>
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsEmail')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Email</div>
							<div class="labels">
								<input type="radio" name="chkSettingsEmail" value="1" <?php echo $SettingsEmail ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkSettingsEmail" value="0" <?php echo $SettingsEmail ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>					
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsLocateLife')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Locate Life</div>
							<div class="labels">
								<input type="radio" name="chkSettingsLocateLife" value="1" <?php echo $SettingsLocateLife ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkSettingsLocateLife" value="0" <?php echo $SettingsLocateLife ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>						
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsTrackingEmailText')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Tracking Email Text</div>
							<div class="labels">
								<input type="radio" name="chkSettingsTrackingEmailText" value="1" <?php echo $SettingsTrackingEmailText ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkSettingsTrackingEmailText" value="0" <?php echo $SettingsTrackingEmailText ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
					
					<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsShippingPickupAddress')) : ?>
						<div class="radioInput">
							<div class="contentLabel">Shipping Pickup Address</div>
							<div class="labels">
								<input type="radio" name="chkShippingPickupAddress" value="1" <?php echo $SettingsShippingAddress ? 'checked' : '' ?> />Yes
								<input type="radio" name="chkShippingPickupAddress" value="0" <?php echo $SettingsShippingAddress ? '' : 'checked' ?> />No	
							</div>
							<div style="clear:both"></div>
						</div>
					<?php endif ?>
				</div>
				<div class="inputLine">
					<div class="permissionsLable" style="float:none; margin-bottom:5px;">
						General
					</div>
					<div class="radioInput">
						<div class="contentLabel">Edit user permissions</div>
						<div class="labels">
							<input type="radio" name="chkEditPermissions" value="1" <?php echo $generalEditPermissions ? 'checked' : '' ?> />Yes
							<input type="radio" name="chkEditPermissions" value="0" <?php echo $generalEditPermissions ? '' : 'checked' ?> />No	
						</div>
						<div style="clear:both"></div>
					</div>
				</div>
				
			</div>
			<?php endif; ?>
		<?php endif; ?>
		<div style="clear:both; width:100%">
			<div class="submitButton">
				<input type="submit" class="blueButton" value="Save Information" />	
			</div>
			<div class="loadingUserSettings">
				<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
			</div>
			<div style="clear:both"></div>
		</div>	
	
	</form>
	<?php if($showNewPasswordSection != true) : ?>
	<form method="post" id="ChangePassword" action="<?php echo PATH ?>cms/updatePassword/<?php echo $this -> user -> GetUserId()?>">
		<div class="section" style="margin-top:20px; ">
			<div class="sectionHeader">New Password</div>
			<div style="margin-top:-13px; margin-bottom:15px; color:#c30000">Leave this section empty if you don't want to change your password</div>
			<div class="inputLine">
				<div class="label">
					New Password
				</div>
				<input type="password" id="newPassword" name="userNewPassword" />
				<div style="clear:both"></div>	
			</div>
			<div class="inputLine">
				<div class="label">
					Confirm Password
				</div>
				<input type="password" id="newPasswordConfirm" name="userConfirmPassword" />
				<div style="clear:both"></div>	
				<input type="hidden" id="userEmail" name="userEmail" value="<?php echo $this -> user -> Email ?>" />
			</div>
			
		</div>
		<div style="clear:both; width:100%; margin-bottom:20px;">
			<div class="submitButton">
				<input type="submit" class="blueButton" value="Save New Password" />	
			</div>
			<div class="loadingPasswordForm">
				<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
			</div>
			<div style="clear:both"></div>
		</div>
	</form>
	<?php endif; ?>
</div>


