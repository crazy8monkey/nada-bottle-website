

<div class="adminContent">
	<div class="header">
		<div class="text">
			Users List	
		</div>
		<div class="newUserSection">
			<a style="text-decoration:none;" href="<?php echo PATH ?>cms/addNewUser">
				<div class="greenButton newUserButton">
					New User
				</div>
			</a>	
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		<table class="usersList">
			<tr class="header">
				<td style="padding-right:10px;">
					User
				</td>
				<td align="right">
					Email
				</td>
			</tr>
			<?php foreach ($this -> usersList as $key => $value) { ?>
				<tr>
					<td style="padding-right:10px;">
						<a href="<?php echo PATH ?>cms/profile?userid=<?php echo $value['userID'] ?>&firstName=<?php echo $value['firstName'] ?>&lastName=<?php echo $value['lastName'] ?>">
							<?php echo $value['firstName'] . " " . $value['lastName'] ?>
						</a>
					</td>
					<td align="right">
						<a href="mailto:<?php echo $value['email'] ?>"><?php echo $value['email'] ?></a>
					</td>
				</tr>
			<?php } ?>
		</table>
	</div>	
</div>


