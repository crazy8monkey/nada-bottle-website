<?php





$breadCrumbString;
$NewTeamMemberHeader;
$showDeleteButtonVariable;
$firstName;
$lastName;
$userName;
$email;
$userFullName;
$description;
$teamMemberLink;
$jobTitle;

$userFormURL;
$showDeleteButtonVariable = true;

if(isset($this -> teamMemberSingle -> teamID)) {
	$userFormURL = PATH . "cms/saveTeamMember/" . $this -> teamMemberSingle -> teamID;
	$breadCrumbString = $this -> teamMemberSingle -> FullName();
	$NewTeamMemberHeader = "Edit Team Member";
	$firstName = $this -> teamMemberSingle -> firstName;
	$lastName = $this -> teamMemberSingle -> lastName;
	$description = $this -> teamMemberSingle -> description;
	
	if($this -> teamMemberSingle -> teamLink != NULL) {
		$teamMemberLink = $this -> teamMemberSingle -> teamLink;
	} else {
		$teamMemberLink = "";	
	}
	$jobTitle = $this -> teamMemberSingle -> jobTitle;
	
	
} else {
	$showDeleteButtonVariable = false;
	$userFormURL = PATH . "cms/saveTeamMember";
	$breadCrumbString = "New Team Member";
	$NewTeamMemberHeader = "New Team Member";
	$firstName = "";
	$lastName = "";
	$description = "";
	$teamMemberLink = "";
	$jobTitle = "";
}

?>
<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".jobDescription");
	});
</script>

<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-pages.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/team">Team</a><span>&#8250;</span><span id="currentUser" style="font-size:12px;  margin: 0px;"><?php echo $breadCrumbString; ?></span>
	</div>
	<div class="header">
		<div class="text">
			<?php echo $NewTeamMemberHeader; ?>
		</div>
		<?php if($showDeleteButtonVariable == true) :?>
		<div class="deleteContainer">
			<a href="<?php echo PATH ?>cms/delete/<?php echo $this -> teamMemberSingle -> teamID ?>/team" style="text-decoration:none;">
				<div class="redButton deleteUser">
					Delete Team Memeber
				</div>
			</a>
		</div>
		<?php endif; ?>
		<div style="clear:both"></div>
	</div>
	<form method="post" id="teamMemberForm" action="<?php echo $userFormURL ?>">
		<div class="section">
			
			<div class="sectionHeader">Profile Picture</div>
			<?php if($showDeleteButtonVariable == true) :?>
				<div class="profilePic" style="background:url(<?php echo PATH. "view/cms/users/team-images/" . $this -> teamMemberSingle -> photo?>) no-repeat"></div>
			<?php endif; ?>			
			<div id="uploadLogoDesktop">
				Please Upload an image on your desktop
			</div>
			<div id="desktopFileUpload">
				<input type="file" name="profilePic" />
			</div>

		</div>
		<div class="section">
			<div class="sectionHeader">Team Information</div>
			<div class="inputLine">
				<div class="label">
					First Name
				</div>
				<input type="text" name="teamFirstName" value="<?php echo $firstName ?>" />
				<div style="clear:both"></div>	
			</div>
			<div class="inputLine">
				<div class="label">
					Last Name
				</div>
				<input type="text" name="teamLastName" value="<?php echo $lastName ?>" />
				<div style="clear:both"></div>	
			</div>
			<div class="inputLine">
				<div class="label">
					Job Title
				</div>
				<input type="text" name="teamJobTitle" value="<?php echo $jobTitle ?>" />
				<div style="clear:both"></div>
			</div>
			<div class="inputLine">
				<div style="font-weight:bold; margin-bottom:5px;">Job Description</div>
				<textarea onkeyup="Globals.textAreaAdjust(this)" class="jobDescription" name="teamDescription"><?php echo $description?></textarea>
			</div>
		</div>
		<div class="section">
			<div style="clear:both">
				<div style="font-weight:bold; margin-bottom:4px;">Team Link</div>
			</div>	
			<div id="TeamLink" style="margin-top:10px">
				<div class="hyperlinkContainer">http://www.</div>
				<input type="text" value="<?php echo $teamMemberLink ?>" name="teamMemberLink" />
			</div>
		</div>
		
		<div style="width:100%; clear:both">
			<div class="submitButton">
				<input type="submit" class="blueButton" value="Save New Team Member" />	
			</div>
			<div class="loadingTeam">
				<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
			</div>
			<div style="clear:both"></div>
		</div>
		
		
			
		</div>
	
	</form>
</div>


