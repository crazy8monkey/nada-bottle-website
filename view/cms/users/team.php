

<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-pages.php' ?>
	<div class="header" style="border-bottom: 1px solid #cecece; margin-bottom:10px;">
		<div class="text">
			Team	
		</div>
		<div class="newUserSection" id="newTeamMember">
			<a style="text-decoration:none;" href="<?php echo PATH ?>cms/newTeamMember">
				<div class="greenButton newUserButton" style="font-size:12px;">
					New Team Member
				</div>
			</a>	
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		<?php if(!empty($this -> teamMembers)) : ?>
			<form id="teamListForm" method="post" action="<?php echo PATH ?>cms/reOrderList/team">
				<div style="font-size:16px; padding: 10px 0px; ">
					<span id="desktopClick">Click</span><span id="mobileTouch">Touch</span> and drag the team member line and re-order to your liking, and click "save order" to save your changes
				</div>
				<div style="clear:both; width:100%; margin-bottom: 15px;">
					<div class='submitButton'>
						<input type="submit" class="blueButton" value="Save Order" />
					</div>
					<div class='loadingTeamList'>
						<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
					</div>
					<div style="clear:both"></div>
				</div>
				
				<div id="teamList" style="background: #E7E7E7; border-top: 1px solid #EDEDED;">
					
				<?php foreach ($this -> teamMembers as $key => $value) : ?>
					<div class='TeamMemberLine'>
						<input type="hidden" value="<?php echo $value['id'] ?>" name="teamID[]" />
						<div class="dragIcon"></div>
						<div class="name">
							<a href="<?php echo PATH ?>cms/teamMember/<?php echo $value['id'] ?>"><?php echo $value['firstName'] . " " . $value['lastName'] ?></a>
							<strong> (<?php echo $value['jobTitle'] ?>)</strong>	
						</div>
						<div style="clear:both"></div>
					</div>
				<?php endforeach ?>
				</div>
			</form>
		<?php endif; ?>
		<?php if(empty($this -> teamMembers)) : ?>
			<div style="font-size: 16px; padding: 10px 0px; border-top: 1px solid #cecece;">There are no team members entered</div>
		<?php endif; ?>
	</div>	
</div>
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#teamList" ).sortable().disableSelection();
	})
</script>

