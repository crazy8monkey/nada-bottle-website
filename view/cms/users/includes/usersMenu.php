<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Users') && $_SESSION["user"]->HasPermission('teamEditPermissions')) : ?>
<div class="filterSection">
	
	<div class="linkContainer">
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('Users') ) : ?>
		<a href="<?php echo PATH ?>cms/users">
			<div class="link <?php if(isset($this -> usersSelected)) {echo "selected"; } ?>">
				Users
			</div>
		</a>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('teamEditPermissions')) : ?>
		<a href="<?php echo PATH ?>cms/team">
			<div class="link <?php if(isset($this -> teamSelected)) {echo "selected"; } ?>">
				Team
			</div>
		</a>
		
		<?php endif ?>
	</div>	
	<div style="clear:both"></div>
</div>	
<?php endif ?>