<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="header">
		Inventory
	</div>
	<div class="contentWrapper">
		
		<div class="adminContentWrapper" id="InventorySection">
			<?php if(!empty($this -> product)) : ?>
			<?php foreach ($this -> product as $key => $value) : ?>
				<div class="section">
					<div class="sectionHeader"><?php echo $value['product_name']; ?></div>
						
							<div class="inventoryList">
								<div class="header" style="border-bottom:1px solid #e9e9e9">
								<div class="colorColumn columnHeader">Colors</div>
								<div class="stockColumn columnHeader" style="text-align:left;"># of Stock</div>
								<div style="clear:both"></div>
							</div>
						
						<?php $productID = $value['product_id']; ?>
						<?php foreach ($this -> colors as $key => $value) : ?>
							<?php if($productID == $value['productId']) : ?>
							<div class="colorLine">						
								<div class="colorColumn colorElements">
									<div class="hideMobile">
										<div class="colorBox" style="background:<?php echo $value['hexacode'] ?>"></div>
										<div class="colorName"><?php echo $value['Color'] ?></div>		
									</div>
									
									<div style="clear:both"></div>
									<div class="mobileColor" style="background:<?php echo $value['hexacode'] ?>">
										<?php echo $value['Color'] ?>
									</div>
								</div>
								<form action="<?php echo PATH ?>cms/updateInventory/<?php echo $value['productColor_id'] ?>" method="post" class="inventoryForm">
									<div class="stockFormElement">
										<div class="mobileHeader"># of Stock</div>
										<div class="stockColumnElement">
											<input type="text" name="quantity" value="<?php echo $value['quantityRemaining'] ?>" />
										</div>
										<div class="OutOfStockMessage">
											<div class='text' id="<?php echo $value['productColor_id']?>" <?php echo $value['quantityRemaining'] > 2 ? 'style="display:none;"' : '' ?>>*Out of Stock</div>										
										</div>
										<div class="ajaxButtonContainer">
											<div class="ajaxReloader" id="<?php echo $value['productColor_id']?>">
												<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
											</div>
											<div class="updateButton">
												<input type="submit" class="whiteButton" value="Update" />
											</div>	
										</div>
										
									</div>
								</form>
								<div style="clear:both"></div>
							</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
			<?php endif; ?>
			<?php if(empty($this -> product)) : ?>
				<div style="font-size: 16px; padding: 0px 0px 15px 0px;">There are no products entered or live/published products to your customers</div>
			<?php endif; ?>
		</div>
	</div>
</div>
