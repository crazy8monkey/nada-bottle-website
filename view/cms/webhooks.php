<div class="adminContent">
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/settings/misc">Settings</a><span>›</span>Stripe Webhooks	
	</div>	
	
	<div class="header" style="font-size:16px;">
		Stripe Webhooks
	</div>
	
	<div class="adminContentWrapper">
		<div class="LoggingContentContainer">
			<?php foreach($this -> webhooks as $key => $value) : ?>
				<div class="webHookElement">
					<pre><?php echo $value['FormPostValue']; ?></pre>
				</div>
				
			<?php endforeach; ?>
		</div>
	</div>
</div>
