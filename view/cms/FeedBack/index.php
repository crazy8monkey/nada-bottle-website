<script>
	$(document).ready(function(){
		
		$('#checkAll').click (function () {
		    var checkedStatus = this.checked;
		    $('.contactChecked').each(function () {
		        $(this).prop('checked', checkedStatus);
		     });
		});
		
		
	});
	function checkCheckedCheckboxes() {
		if($('.contactChecked').is(':checked')) {
	        return true
    	}
    	else {
 	        alert('You must check at least one box to delete');
    		return false
    	}
	}
</script>

<div class="adminContent">
	<div class="header" style="border-bottom: 0px; font-size:16px;">
		Feed Back
	</div>
	<div class="adminContentWrapper">
		<form method="post" action="<?php echo PATH ?>cms/deleteMultiple/feedback">
			<div class="contactLine" style="background: #f0f0f0; height: 30px;">
				<div style="float:left; padding: 7px 0px 5px 0px;">
					<input type="checkbox" id="checkAll" onclick="CMSController.toggleContactCheckBoxes() "/> Select All
				</div>
				<div style="float:right;">
					<input type="submit" class="redButton" value="Delete Checked" onclick="return checkCheckedCheckboxes();"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="overflow-x:hidden">
				<?php foreach ($this->feedBackList as $key => $value) { ?>
					
						<div class="contactLine" style="border-bottom:1px solid #cecece">
							<div style="float:left; padding-right:10px;">
								<input type="checkbox" class="contactChecked" name="feedback[]" value="<?php echo $value['id']?>" />
							</div>
							<div style="float:left; text-decoration:underline">
								<a href="<?php echo PATH . "cms/feedbackSingle/" . $value['id'] ?>" style="color:#2a2a2a;">
									Feed back Entry #<?php echo $value['id'] ?>
								</a>
							</div>	
							<div class="contactContent">
								<?php echo $this -> recordedTime -> formatDate($value['date']); ?> / <?php echo $value['time']; ?>
							</div>	
							<div style="clear:both"></div>	
						</div>
					
				<?php } ?>
			</div>
			<?php if(empty($this->feedBackList)) : ?> 
				<div style="font-size: 16px; padding: 15px 0px;">There are no feed back at this time</div>
			<?php endif; ?>
		</form>
		<div style="clear:both"></div>
	</div>
</div>
