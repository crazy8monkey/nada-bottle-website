
<div class="adminContent">
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/feedback">Feed Back</a><span>&#8250;</span><span style="font-size:12px;  margin: 0px;">Feed Back Entry #<?php echo $this -> feedback -> feedBackID; ?></span>
	</div>
	<div class="line" style="clear:both; width:100%">
		<div class="header">
			Feed Back Entry #<?php echo $this -> feedback -> feedBackID ?>
		</div>	
		<div class="button">
			<form method="post" action="<?php echo PATH . 'cms/delete/' . $this -> feedback -> feedBackID; ?>/feedback">
				<input type="submit" class="redButton" value="Delete Feed Back" />
			</form>
		</div>
		<div style="clear:both"></div>
	</div>
	

	<div class="adminContentWrapper" style="font-size:12px; ">
		<div style="padding:10px 0px;">
			<div style="width:100px; float:left;"><strong>Submitted:</strong></div> <?php echo $this -> recordedTime -> formatDate($this -> feedback -> feedBackDate); ?> / <?php echo $this -> feedback -> feedBackTime ?>
			<div style="margin-bottom:20px;">
				<div style="width:100px; float:left;"><strong>Email: </strong></div><a style="color:#2a2a2a" href="mailto:<?php echo $this -> feedback -> feedBackEmail ?>"><?php echo $this -> feedback -> feedBackEmail ?></a> 	
			</div>
			<div style="margin-bottom:20px;">
				<strong>Comments</strong><br />
				<?php echo $this -> feedback -> feedBackDescription ?>	
			</div>
		</div>
	</div>
</div>
