<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".warrantyDeclineNotes");
	});
</script>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/warranties">Warranties</a><span>&#8250;</span><span style="font-size:12px;  margin: 0px;"><?php echo $this -> warrantySingle -> getFullName() ?>'s Warranty</span>
	</div>
	<div class="line" style="clear:both; width:100%; border-bottom: 1px solid #cecece;">
		<div class="header">
			Warranty
		</div>	
		<div class="button">
			<form method="post" action="<?php echo PATH . 'cms/delete/' . $this -> warrantySingle -> _warrantyID ?>/warranty">
				<input type="submit" class="redButton" value="Delete Warranty" />
			</form>
		</div>
		<div style="clear:both"></div>
	</div>
	

	<div class="adminContentWrapper" style="font-size:12px; ">
		<div class="warrantyContent">
			<?php if ($this -> warrantySingle -> warrantyStatus == 1) :?>
				<div class="ApprovedWarranty">
					Warranty Approved
				</div>
			<?php endif; ?>
			<?php if ($this -> warrantySingle -> warrantyStatus == 2) :?>
				<div class="declineWarranty">
					Warranty Declined
				</div>
				<div class="section" style="border-top:none;    padding-top: 0px;">
				<div class="sectionHeader">
					Decline Notes
				</div>
				<div style="padding-bottom:10px; border-bottom:1px solid #cecece; margin-bottom: 10px;">
					<?php echo $this -> warrantySingle -> declineNotes; ?>
				</div>
			<?php endif; ?>
			<div style="margin-bottom:10px;">
				<div style="width:100px; float:left;"><strong>Submitted:</strong></div> 
				<?php echo $this -> recordedTime -> formatDate($this -> warrantySingle -> submittedDate) ?> / <?php echo $this -> warrantySingle -> submittedTime ?>	
			</div>
			
			<div class="section">
				<div class="sectionHeader">
					User Information
				</div>
				<div class="line">
					<div class="label"><strong>Full Name: </strong></div>
					<?php echo $this -> warrantySingle -> getFullName() ?>	
				</div>
				<div class="line">
					<div class="label"><strong>Email: </strong></div>
					<a style="color:#2a2a2a" href="mailto:<?php echo $this -> warrantySingle -> email ?>"><?php echo $this -> warrantySingle -> email ?></a>	
				</div>
				<div class="line">
					<?php $areaCode = substr($this -> warrantySingle -> phoneNumber, 0, 3);
					  $RestOfPhone1 = substr($this -> warrantySingle -> phoneNumber, 3, 3);
					  $RestOfPhone2 = substr($this -> warrantySingle -> phoneNumber, 6, 7); 
					  $telephone = "(" .$areaCode . ") ". $RestOfPhone1 . " - " . $RestOfPhone2;
					
					?>
					<div class="label"><strong>Phone Number </strong></div><?php echo $telephone ?>
				</div>
				<div class="line">
					<div class="label"><strong>Shipment Address:</strong></div>
					<div class="shipmentAddress">
						<?php echo $this -> warrantySingle -> address ?><br />
						<?php echo $this -> warrantySingle -> city ?>, 
						<?php echo $this -> warrantySingle -> state ?>
						<?php echo $this -> warrantySingle -> zip ?>						
					</div>

				</div>
				<div class="line" style="margin-top:10px;">
					<div class="label"><strong>Order #:</strong></div>
					<div class="shipmentAddress">
						<a style="color:#2a2a2a" href="javascript:void(0);" onclick="CMSController.openPopup('<?php echo PATH ?>cms/reviewOrder/<?php echo $this -> warrantySingle -> orderNumber ?>')">
							<?php echo $this -> warrantySingle -> orderNumber ?> / View Order
						</a>
					</div>

				</div>
				
				
				<div class="line">
					<a href="javascript:void(0);" onclick="CMSController.openPopup('<?php echo PATH ?>cms/warrantyPhoto/<?php echo $this -> warrantySingle -> _warrantyID?>')" style="text-decoration:none;">
						<div class="blueButton">
							View Photo(s)	
						</div>
					</a>
					<div style="clear:both"></div>
				</div>
				
			</div>
			<div class="section">
				<div class="sectionHeader">
					Warrant Description
				</div>
				<?php echo $this -> warrantySingle -> description ?>
				
				<div class="buttonHolder">
					<?php if ($this -> warrantySingle -> warrantyStatus == 0) :?>
					<a href="<?php echo PATH ?>cms/approveWarranty/<?php echo $this -> warrantySingle -> _warrantyID ?>" style="text-decoration:none;">
						<div class="greenButton button">
							Approve Warranty
						</div>
					</a>
					<a onclick="CMSController.declineWarranty();" href="javascript:void(0);" style="text-decoration:none;">
						<div class="whiteButton button">
							Decline Decline
						</div>
					</a>
					<?php endif; ?>
					<?php if ($this -> warrantySingle -> warrantyStatus != 0) :?>
						<a href="<?php echo PATH ?>cms/resetWarrantyStatus/<?php echo $this -> warrantySingle -> _warrantyID ?>" style="text-decoration:none;">
						<div class="whiteButton button">
							Reset Warranty Status
						</div>
					</a>
					<?php endif; ?>
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
	</div>
	<a href="javascript:void(0);" onclick="CMSController.closePopup('.warrantyDeclinePopup, .backgroundOverlay')">
		<div class="backgroundOverlay"></div>
	</a>
	<div class="warrantyDeclinePopup">
		<form id="declineWarranty" action="<?php echo PATH ?>cms/declineWarranty/<?php echo $this -> warrantySingle -> _warrantyID ?>" method="post">
			<div class="header">
				Decline Warranty
				<a href="javascript:void(0);" onclick="CMSController.closePopup('.warrantyDeclinePopup, .backgroundOverlay')"><div class="close"></div></a>
			</div>
			<div class="paddingContent">
				<div style="margin-bottom:10px;">
					Notes why this warranty is declined <br />
					<span style="color:#c30000">* These notes are going to be sent to the user</span>
				</div>
				<textarea onkeyup="CMSController.textAreaAdjust(this)" class="warrantyDeclineNotes" name="warrantyDeclineNotes"></textarea>
			</div>
			<div style="border-top:1px solid #e9e9e9; padding:10px;">
				<div class="SubmitButtonContainer">
					<input type="submit" value="Decline Warranty" class="whiteButton">	
				</div>
				<div class="warrantyLoad">
					<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
				</div>
				<div style="clear:both"></div>
			</div>
		</form>
	</div>
	
</div>
