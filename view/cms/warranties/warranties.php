<script>
	$(document).ready(function(){
		
		$('#checkAll').click (function () {
		    var checkedStatus = this.checked;
		    $('.warrantyChecked').each(function () {
		        $(this).prop('checked', checkedStatus);
		     });
		});
		
		
	});
	function checkCheckedCheckboxes() {
		if($('.warrantyChecked').is(':checked')) {
	        return true
    	}
    	else {
 	        alert('You must check at least one box to delete');
    		return false
    	}
	}
</script>

<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="header" style="border-bottom: 0px; font-size:16px;">
		Warranties
	</div>
	<div class="adminContentWrapper">
		<form method="post" action="<?php echo PATH ?>cms/deleteMultiple/warranty">
			<div class="contactLine" style="background: #f0f0f0; height: 30px;">
				<div style="float:left; padding: 7px 0px 5px 0px;">
					<input type="checkbox" id="checkAll" onclick="CMSController.toggleContactCheckBoxes() "/> Select All
				</div>
				<div style="float:right;">
					<input type="submit" class="redButton" value="Delete Checked" onclick="return checkCheckedCheckboxes();"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<div style="overflow-x:hidden">
				<?php 
				$count = null;
        		$prevOrder = null;
				
				foreach ($this->warranties as $key => $value) { 
					$count ++;
					$showDateHeader = false;
					if($key == 0) {
						$showDateHeader = true;
					} else {
		                if($this -> recordedTime -> formatDate($prevOrder['date']) != $this -> recordedTime -> formatDate($value['date'])) {
		                    $showDateHeader = true;
		                }
					}
		            $prevOrder = $value;
					
					?>
					
						<?php if ($showDateHeader == true) : ?>
						<div class='dateHeader'>
							<div class='line'></div>
							<div class='text'> 
								<?php echo $this -> recordedTime -> formatDate($value['date']) ?> 
							</div>
						</div>
						<?php endif; ?>
						<div class="contactLine">
							<div class="indicator"></div>
							<div class="checkboxContainer">
								<input type="checkbox" class="warrantyChecked" name="warranties[]" value="<?php echo $value['warrantyID']?>" />
							</div>
							<div class="contactName" style="float:left; text-decoration:underline">
								<a href="<?php echo PATH . "cms/warrantySingle/" . $value['warrantyID'] ?>" style="color:#2a2a2a;">
									Warranty #: <?php echo $value['warrantyID'] ?>
								</a>
							</div>	
							<div class="contactContent">
								<?php echo $value['time']; ?>
							</div>	
							<div style="clear:both"></div>	
						</div>
					
				<?php } ?>
			</div>
			<?php if(empty($this->warranties)) : ?> 
				<div style="font-size: 16px; padding: 15px 0px;">There are no warranties at this time</div>
			<?php endif; ?>
		</form>
		<div style="clear:both"></div>
	</div>
</div>
