<html>
	<head>
		<script>
			$(document).ready(function(){
				CMSController.warrantyPhotoPage();	
			});
		</script>
		<link rel="stylesheet" href="<?php echo PATH ?>public/css/admin/warrantyPhoto.css" /> 
		<title><?php echo "Warranty #: " . $this -> warrantyPhoto-> _warrantyID . " photo / ". $this  -> warrantyPhoto -> getFullName(); ?></title>
	</head>
	<body>
		<div id="container_2">
				
			<?php $i = 0;?>
			<?php foreach ($this->photos as $key => $value) : ?>
				<?php $i ++ ?>
				<div class="photo" style="background-image:url(<?php echo PATH . "view/cms/warranties/images/" . $this -> warrantyPhoto-> _warrantyID . '/' . $value['warrantyPhoto'] ?>)" id="photo<?php echo $i ?>"></div>
			<?php endforeach; ?>
			
			
		</div>	
		
		<?php if($i > 1) : ?>
			<a href="javascript:void(0);" class="prev">
				<div class="left slideShowArrow"></div>
			</a>
			<a href="javascript:void(0);" class="next">
				<div class="right slideShowArrow"></div>
			</a>
		<?php endif; ?>
	
	</body>
</html>

