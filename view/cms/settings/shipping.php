<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".productDescription");
	});
</script>
<?php foreach ($this -> settings as $key => $value) { ?>
				
<div class="adminContent">
	<div class="header">
		Settings
	</div>
	
	<div class="adminContentWrapper">
	
		<?php require 'view/cms/settings/includes/settingsMenu.php' ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsShippingPickupAddress')) : ?>
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandShippingAddress()">
				<div class="sectionHeader"><div class="toggleIndicatorShippingAddress">+</div> Shipping Label Address</div>
				<div style="clear:both"></div>
			</a>
			
			<div class="sectionContent" id="pickupShippingAddress">
				<form method="post" id="CompanyAddress" action="<?php echo PATH ?>cms/updateCompanyAddress">
					<div style="margin-bottom:10px;">
						<input type="text" class="companyName" name="companyName" value="<?php echo $value['CompanyName']; ?>" />	
					</div>
					<div style="margin-bottom:10px;">
						
						<input type="text" class="companyAddress" name="companyAddress" value="<?php echo $value['ShippingAddress']; ?>" />	
					</div>
						
						<input type="hidden" class="companyAddress" name="companyAddress2" value="<?php echo $value['ShippingAddress2']; ?>" />	
					
					<div style="clear:both; width:100%; margin-bottom:10px;">
						
						<input type="text" name="companyCity" class="companyCity" value="<?php echo $value['ShippingCity']; ?>" />
						<input type="text" name="companyState" class="companyState" maxlength="2" value="<?php echo $value['ShippingState']; ?>" />
						<input type="text" name="companyZip" class="companyZip" maxlength="5" value="<?php echo $value['ShippingZip']; ?>" />
						<div style="clear:both"></div>
					</div>
					<div>
						<?php 
							$areaCode = substr($value['shippingPhoneNumber'], 0, 3);
							$RestOfPhone1 = substr($value['shippingPhoneNumber'], 3, 3);
						  	$RestOfPhone2 = substr($value['shippingPhoneNumber'], 6, 7); 
						?>
						<div style="font-weight:bold; margin-bottom:5px; margin-top:10px;">Company Phone Number</div>
						
						<input type="text" class="phoneNumber" name="companyPhoneNumberAreaCode" maxlength="3" value="<?php echo $areaCode; ?>" />
						<input type="text" class="phoneNumber" name="companyPhoneNumber2" maxlength="3" value="<?php echo $RestOfPhone1; ?>" />
						<input type="text" class="phoneNumber" name="companyPhoneNumber3" maxlength="4" value="<?php echo $RestOfPhone2; ?>" />
					</div>
					<div style="clear:both; width:100%; margin-top:5px;">
						<div class="submitButton">
							<input type="submit" class="blueButton" value="Save Company Address" />	
						</div>
						<div class="loadingAddress">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>
						<div style="clear:both"></div>
					</div>	
							
				</form>
			</div>
		</div>
		<?php endif ?>
		
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandShippingCompany()">
				<div class="sectionHeader"><div class="toggleShippingCompany">+</div> Shipping Company</div>
				<div style="clear:both"></div>
			</a>			
			<div class="sectionContent" id="shippingCompany">
				<div style="margin-bottom:10px">
					Shipping company settings will work on new orders ONLY, any previous orders saved/process in your system will not be affected by what you 
					have selected.
				</div>
				<form method="post" id="ShippingCompany" action="<?php echo PATH ?>cms/saveShippingCompany">
					<div style="margin-bottom:10px;">
						<input type="radio" name="shippingCompany" value="UPS" <?php echo ($value['ShippingCompany'] == "UPS") ? "checked" : ""; ?> /> UPS
					</div>
					<div style="margin-bottom:10px;">
						<input type="radio" name="shippingCompany" value="FedEx" <?php echo ($value['ShippingCompany'] == "FedEx") ? "checked" : ""; ?> /> FedEx
					</div>
					<input type="submit" value="save" class="blueButton" />
				</form>
			</div>
		</div>
		
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandShippingBoxes()">
				<div class="sectionHeader"><div class="toggleShippingBoxes"><?php echo isset($_GET['newBox']) ? '-' : '+'; ?></div> Shipping Boxes</div>
				<div style="clear:both"></div>
			</a>
			<div class="sectionContent" id="shippingBoxesContent" <?php echo isset($_GET['newBox']) ? 'style="display:block;"' : ''; ?>>
				<div class="rules">
					<div class="rulesHeader">
						Rules on creating shipment boxes
					</div>
					<ul>
						<li>
							Package value should not exceed the maxium size total constraints of 165 In. (length + girth)<br /> 
							girth = (width * 2) + (height * 2) 
						</li>
						<li>
							The maxium per package weight is 150 lbs
						</li>
						<li>
							There must be at least 1 shipping box entered in your system
						</li>
						<li>
							When submitting weight of a new package, you need to enter the weight of the package itself (without contents), and the package stuffing
						</li>
					</ul>
				</div>
				
				
				<?php if(empty($this -> boxes)) : ?>
					There are no boxes submitted
				<?php endif; ?>
				<?php if(!empty($this -> boxes)) : ?>
				<div class="BoxesHeader">
					Boxes
				</div>
				<div class="ShippingBoxesList">
					
					<?php $i = 0; ?>
					<?php 
					
					$boxCount = count($this -> boxes);
					$showButton = false;
							
					if($boxCount > 1) {
						$showButton = true;
					}
					
					 ?>
					
					
					<?php foreach ($this -> boxes as $key => $value) : ?>
						<?php
							$style="";
							if ($i % 2 == 0){
								$style="background:#f1f1f1";
							}
						?>
						<div class="boxLine" style='<?php echo $style ?>'>
							<form method="post" action="<?php echo PATH ?>cms/saveShippingBox/<?php echo $value['id'] ?>" class="editShippingBox" id="<?php echo $value['id'] ?>">
								<div style="margin-bottom:3px;font-weight:bold">Dimensions (L x W x H)</div>
								<div class="dimensionedSave" id="<?php echo $value['id'] ?>">
									<span class="length"><?php echo $value['length'] ?>''</span> x <span class="width"><?php echo $value['width'] ?>''</span> x <span class="height"><?php echo $value['height'] ?>''</span>		
								</div>
								<div class="dimensionEdit" id="<?php echo $value['id'] ?>">
									<input type="text" value="<?php echo $value['length'] ?>" name="boxLength" />'' <span class='dimentionX'>x</span> <input type="text" value="<?php echo $value['width'] ?>" name="boxWidth" />'' <span class='dimentionX'>x</span> <input type="text" value="<?php echo $value['height'] ?>" name="boxHeight" />''
								</div>
								<div style="margin-bottom:3px; margin-top:10px; font-weight:bold">Specifications</div>
								<div class="specificationSave" id="<?php echo $value['id'] ?>">
									<div style="clear:both; width:100%">
										<?php
										$item = ""; 
										 if($value['items'] > 1) {
										 	$item = "items";
										 } else {
										 	$item = "item";
										 }
										 
										?>
										<div style="width:95px; float:left">Contain:</div> <span class='itemsHold'><?php echo $value['items'] . ' ' . $item ?></span>	
										<div style="clear:both"></div>
									</div>	
									
									<div style="clear:both; width:100%">
										<div style="width:95px; float:left">Weight:</div> <span class='weight'><?php echo $value['weight'] ?></span> lbs	
										<div style="clear:both"></div>
									</div>
									<div style="clear:both; width:100%">
										<?php 
										$girth = ($value['width'] * 2) + ($value['height'] * 2) + $value['length'];
									
										?>
										<div style="width:95px; float:left">Length + Girth:</div> <span class='girth'><?php echo $girth ?>''</span>	
										<div style="clear:both"></div>
									</div>
									
																	
								</div>
								<div class="specificationEdit" id="<?php echo $value['id'] ?>">
									<div style="clear:both; width:100%">
										<?php
										
										$item = ""; 
										 if($value['items'] > 1) {
										 	$item = "items";
										 } else {
										 	$item = "item";
										 }
										 
										?>
										
										<div style="width:95px; float:left; margin-top: 5px;">Contain:</div> <input type="text" value="<?php echo $value['items'] ?>" name="boxItemsHold" /> <span class='itemsHold'><?php echo $item ?></span>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both; width:100%; margin-top: 5px;">
										<div style="width:95px; float:left; margin-top: 5px;">Weight:</div> <input type="text" value="<?php echo $value['weight'] ?>" name="boxWeight" /> lbs	
										<div style="clear:both"></div>
									</div>
								</div>

								<div id="<?php echo $value['id'] ?>" class="button" style="clear:both; width:100%">
									<div class="editBox">
										<input type="submit" class="blueButton" value="Save" />	
									</div>
									<div class="editBoxLoading" id="<?php echo $value['id'] ?>">
										<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
									</div>
									<div style="clear:both"></div>
								</div>
								
								
							</form>
							
							
							<a href="javascript:void(0)" onclick="CMSController.editShippingBox(<?php echo $value['id'] ?>, this)" style="text-decoration:none;:">
								<div class="edit whiteButton" id="<?php echo $value['id'] ?>">
									Edit
								</div>
							</a>
							
							<div style="clear:both"></div>
							<?php if($showButton == true) :?>
								<a href="<?php echo PATH ?>cms/delete/<?php echo $value['id'] ?>/boxes" style="text-decoration:none;">
									<div class="deleteBtn"></div>
									<div class="mobileDelete redButton" style=" text-align:center;">
										Delete
									</div>
								</a>
							<?php endif; ?>
						</div>
						<?php $i++; ?>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
				<div style="border-top:1px solid #F1F1F1; padding-top:10px; margin-top:10px;">
					<form id="shippingBoxes" action="<?php echo PATH ?>cms/saveShippingBox" method="post">
						<div style="clear:both; width:100%">
							<div class="Dimensions">
								<div class="Header">
									Dimensions (In Inches)
								</div>
								<div class="dimentionInput">
									<div class="label">Length</div>
									<input type="text" name="boxLength" />
									<div class="ByElement">x</div>
								</div>
								<div class="dimentionInput Dimensions">
									<div class="label">Width</div>
									<input type="text" name="boxWidth" />
									<div class="ByElement">x</div>
								</div>
								<div class="dimentionInput Dimensions">
									<div class="label">Height</div>
									<input type="text" name="boxHeight" />
								</div>
								<div style="clear:both"></div>
							</div>
							<div class="boxSpec">
							 	<div class="Header">
									Specifications
								</div>
							 	<div class="dimentionInput" style="margin-left:0px">
									<div class="label">Items Contain</div>
									<input type="text" name="boxItemsHold" />
								</div>
								<div class="dimentionInput">
									<div class="label">Weight</div>
									<input type="text" name="boxWeight" />
								</div>
							 	<div style="clear:both"></div>
							 </div>
							
							
							<div style="clear:both"></div>
						</div>
						 
						<div style="margin-top:10px; clear:both">
							<div class="NewBoxButton">
								<input type="submit" value="New Shipping Box" class="blueButton" />
							</div>
							<div class="loadingBox">
								<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
							</div>
						</div>
						
						
					</form>	
				</div>
				
				
			</div>
		</div>
		
			
<?php } ?>
	
		
		
	</div>
</div>




