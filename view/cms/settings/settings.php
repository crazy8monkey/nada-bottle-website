<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".productDescription");
	});
</script>
<?php foreach ($this -> settings as $key => $value) { ?>
				
<div class="adminContent">
	<div class="header">
		Settings
	</div>
	
	<div class="adminContentWrapper">
		<?php require 'view/cms/settings/includes/settingsMenu.php' ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsEmail')) : ?>
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandEmailSettings()">
				<div class="sectionHeader"><div class="toggleIndicatorEmailSettings">+</div> Email Settings</div>
				<div style="clear:both"></div>
			</a>
			
			<div class="sectionContent" id="emailSettingsContent">
				<form id="emailSettings" action="<?php echo PATH?>cms/emailSettings" method="post">
					<div class="InputLine">
						
						<div class="Label">Payment Notifications:</div>
						<div class="InputText">
							<input type="text" name="paymentEmail" value="<?php echo $value['PaymentsEmail']; ?>" />
						</div>
						
						<div style="clear:both"></div>
					</div>
					<div class="InputLine">
						
						<div class="Label">Contact/Feed back Inquiries:</div>
						<div class="InputText">
							<input type="text" name="contactEmail" value="<?php echo $value['contactEmail']; ?>" />
						</div>
						
						<div style="clear:both"></div>
					</div>
					<div class="InputLine">
						<div class="Label">Receipt Copies:</div>
						<div class="InputText">
							<input type="text" name="receiptEmail" value="<?php echo $value['ReceiptEmail']; ?>" />
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="InputLine">
						<div class="Label">Inventory Reminder:</div>
						<div class="InputText">
							<input type="text" name="inventoryReminderEmail" value="<?php echo $value['InventoryEmail']; ?>" />
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="InputLine">
						<div class="Label">Warranties:</div>
						<div class="InputText">
							<input type="text" name="warrantyEmail" value="<?php echo $value['WarrantiesEmail']; ?>" />
						</div>
						<div style="clear:both"></div>
					</div>
					<div style="clear:both; width:100%">
						<div class="submitEmailSettings">
							<input type="submit" class="blueButton" value="Save Email Settings" />		
						</div>
						<div class="loadingEmail">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>	
						<div style="clear:both"></div>
					</div>
					
					
					
					
				</form>
			</div>
		</div>
		<?php endif ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsLocateLife')) : ?>
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandLocateLife()">
				<div class="sectionHeader"><div class="toggleIndicatorLocateLife">+</div> Locate Life</div>
				<div style="clear:both"></div>
			</a>
			
			<div class="sectionContent" id="locateLifeInfo">
				<form method="post" id="locateLifeForm" action="<?php echo PATH ?>cms/toggleLocateLife">

					<div class="locateLifeToggle" style="margin-bottom:15px;">
						<input type="radio" name="toggleLocateLifePage" value="1" <?=($value['locateLifeSettings'] == 1) ? "checked" : ""; ?> />Show
						<input type="radio" name="toggleLocateLifePage" value="0" <?=($value['locateLifeSettings'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />Hide	
					</div>
					<div style="clear:both; width:100%">
						<div class="locateLifeButton">
							<input type="submit" class="blueButton" value="Save Locate Life Settings" />
						</div>
						<div class="loadingLocateLife">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>	
						<div style="clear:both"></div>
					</div>
				</form>
			</div>
		</div>
		<?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('SettingsTrackingEmailText')) : ?>
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandTrackingEmailText()">
				<div class="sectionHeader"><div class="toggleIndicatorTrackingEmail">+</div> Tracking Email Text</div>
				<div style="clear:both"></div>
			</a>
			
			<div class="sectionContent" id="TrackingEmailContent">
				<form method="post" id="TrackingEmailText" action="<?php echo PATH ?>cms/saveTrackingEmail">
					<div style="margin-bottom:10px;">
						<textarea onkeyup="Globals.textAreaAdjust(this)" class="productDescription" style="width:100%; min-width:100%; min-height:80px" name="trackingEmailText"><?php echo str_replace("\n", '<br />', strip_tags($value['TrackingEmailText'], '<br />')); ?></textarea>	
					</div>
					<div style="clear:both; width:100%">
						<div class="submitTrackingEmailBtn">
							<input type="submit" class="blueButton" value="Save Tracking Email Text" />				
						</div>
						<div class="loadingTracking">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>	
						<div style="clear:both"></div>
					</div>
					
					
						
				</form>
			</div>
		</div>
		<?php endif ?>
		
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) : ?>
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandPaymentNotifications()">
				<div class="sectionHeader"><div class="togglePaymentNotifications">+</div> Payment Notifications</div>
				<div style="clear:both"></div>
			</a>
			
			<div class="sectionContent" id="paymentNotifications">
				<form method="post" id="paymentNotificationsForm" action="<?php echo PATH ?>cms/updatePaymentEmailSettings">
					<div class="paymentEmailLine">
						<div class="label">
							Succeded
						</div>
						<div class="content">
							<input type="radio" name="paymentSucceded" value="1" <?=($value['paymentSucceded'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentSucceded" value="0" <?=($value['paymentSucceded'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>
					
					<div class="paymentEmailLine">
						<div class="label">
							Failed
						</div>
						<div class="content">
							<input type="radio" name="paymentFailed" value="1" <?=($value['paymentFailed'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentFailed" value="0" <?=($value['paymentFailed'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>
					
					<div class="paymentEmailLine">
						<div class="label">
							Refunded
						</div>
						<div class="content">
							<input type="radio" name="paymentRefunded" value="1" <?=($value['paymentRefunded'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentRefunded" value="0" <?=($value['paymentRefunded'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>
					
					<div class="paymentEmailLine">
						<div class="label">
							Captured
						</div>
						<div class="content">
							<input type="radio" name="paymentCaptured" value="1" <?=($value['paymentCaptured'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentCaptured" value="0" <?=($value['paymentCaptured'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>
					
					<div class="paymentEmailLine">
						<div class="label">
							Dispute: Created
						</div>
						<div class="content">
							<input type="radio" name="paymentDisputeCreated" value="1" <?=($value['paymentDisputeCreated'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentDisputeCreated" value="0" <?=($value['paymentDisputeCreated'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>
	
					<div class="paymentEmailLine">
						<div class="label">
							Dispute: Funds Withdrawn
						</div>
						<div class="content">
							<input type="radio" name="paymentDisputeFundsWidthdrawn" value="1" <?=($value['paymentDisputeFundsWithdrawn'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentDisputeFundsWidthdrawn" value="0" <?=($value['paymentDisputeFundsWithdrawn'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>	          

					<div class="paymentEmailLine">
						<div class="label">
							Dispute: Funds Reinstated
						</div>
						<div class="content">
							<input type="radio" name="paymentDisputeFundsReinstated" value="1" <?=($value['paymentDisputeFundsReinstated'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentDisputeFundsReinstated" value="0" <?=($value['paymentDisputeFundsReinstated'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>	       

					<div class="paymentEmailLine">
						<div class="label">
							Dispute: Updated
						</div>
						<div class="content">
							<input type="radio" name="paymentDisputeUpdated" value="1" <?=($value['paymentDisputeUpdated'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentDisputeUpdated" value="0" <?=($value['paymentDisputeUpdated'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>

					<div class="paymentEmailLine">
						<div class="label">
							Dispute: Closed
						</div>
						<div class="content">
							<input type="radio" name="paymentDisputeClosed" value="1" <?=($value['paymentDisputeClosed'] == 1) ? "checked" : ""; ?> />Yes
							<input type="radio" name="paymentDisputeClosed" value="0" <?=($value['paymentDisputeClosed'] == 1) ? "" : "checked"; ?> style="margin-left:5px;" />No								
						</div>
						<div style="clear:both"></div>
					</div>							    
	
					<div style="clear:both; width:100%">
						<div class="PaymentSubmitButton">
							<input type="submit" class="blueButton" value="Save Payment Settings" />	
						</div>
						<div class="loadingPayment">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>
						<div style="clear:both"></div>
					</div>	
							
				</form>
			</div>
		</div>
		<?php endif ?>
		
		<div class="section">
			<a class="toggleLink" href="javascript:void(0);" onclick="CMSController.expandPurchase()">
				<div class="sectionHeader"><div class="togglePurchase">+</div> Purchase</div>
				<div style="clear:both"></div>
			</a>
			<div class="sectionContent" id="purchaseSettings">
				<form method="post" id="purchaseSettingForm" action="<?php echo PATH ?>cms/updatePurchaseSettings">
					<div style="margin-bottom:10px;">
						<input type="hidden" name="hiddenVisiblePreSale" value="<?php echo $value['PreSaleShow'] ?>">
						<input type="checkbox" onchange="CMSController.togglePreSale(this)" <?=($value['PreSaleShow'] == 1) ? "checked" : ""; ?> />
						Show Pre-sale text						
					</div>
					<textarea name="preSaleText" style="max-width:100%; width:100%; padding:0px; min-width:100%"><?php echo str_replace("\n", '<br />', strip_tags($value['PreSaleText'], '<br />')); ?></textarea>
					<div style="clear:both; width:100%; margin-top:5px;">
						<div class="purchaseSubmitButton">
							<input type="submit" class="blueButton" value="Save Purchase Settings" />	
						</div>
						<div class="loadingPurchase">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>
						<div style="clear:both"></div>
					</div>	
				</form>
			</div>
		</div>
		
		<div class="section">
			<div class="sectionHeader">Logging/Errors</div>
			<a style="color:#575757" href="<?php echo PATH ?>cms/logging">Checkout Error tracking</a><br />
			<a style="color:#575757" href="<?php echo PATH ?>cms/webhooklist">Stripe Webhooks</a><br />
			<a style="color:#575757" href="<?php echo PATH ?>cms/phpinformation">PHP Info</a><br />
		</div>
		
		<div class="section" id="references">
			<div class="sectionHeader">Reference Pages</div>
			<div class="grouping">
				<div class="header">Sales Tax</div>
				<a style="color:#575757" href="http://blog.taxjar.com/sales-tax-nexus-definition/" target="_blank">Sales Tax Nexus Definition</a><br />
				<a style="color:#575757" href="http://www.taxjar.com/guides/intro-to-sales-tax/" target="_blank">Intro to Sales Tax</a><br />
			</div>
			<div class="grouping">
				<div class="header">Stripe</div>
				<a style="color:#575757" href="https://stripe.com/help/disputes-overview" target="_blank">Stripe Disputes Overview</a><br />
				<a style="color:#575757" href="https://dashboard.stripe.com/test/payments/overview" target="_blank">Stripe Payments Overview</a>
			</div>
			<div class="grouping">
				<div class="header">Newsletter</div>
				<a style="color:#575757" href="https://www.ftc.gov/tips-advice/business-center/guidance/can-spam-act-compliance-guide-business" target="_blank">CAN-SPAM Act: A Compliance Guide for Business</a><br />
			</div>
			
		</div>
			
<?php } ?>
	
		
		
	</div>
</div>




