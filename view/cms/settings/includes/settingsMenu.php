<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) : ?>
<div class="filterSection">
	<div class="linkContainer">
	<a href="<?php echo PATH ?>cms/settings/shipping">
		<div class="link <?php if(isset($this -> shipping)) {echo "selected"; } ?>">
			Shipping Info.
		</div>
	</a>
	<a href="<?php echo PATH ?>cms/settings/methods">
		<div class="link <?php if(isset($this -> methods)) {echo "selected"; } ?>">
			Shipping Methods
		</div>
	</a>
	<a href="<?php echo PATH ?>cms/settings/misc">
		<div class="link <?php if(isset($this -> misc)) {echo "selected"; } ?>">
			Misc
		</div>
	</a>		
	</div>

	<div style="clear:both"></div>
</div>
<?php endif ?>

