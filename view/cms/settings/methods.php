<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".productDescription");
	});
</script>
				
<div class="adminContent">
	<div class="header">
		Settings
	</div>
	
	<div class="adminContentWrapper">
		<?php require 'view/cms/settings/includes/settingsMenu.php' ?>
		<form method="post" action="<?php echo PATH ?>cms/saveShippingMethods">
			<div class="shippingMethodHeader">			
				<div class="header">
					<?php echo $this -> shippingCompanyNameString?> Shipping Methods
				</div>
				<div class="saveButton">
					<input type="submit" class="blueButton" value="Save Methods" />
				</div>
				<div style="clear:both"></div>
			</div>	
			<?php foreach ($this -> shippingMethods as $key => $value) : ?>
				<div class="methodLine">
					<input type="hidden" name="hiddenActiveShippingMethods[]" value="<?php echo $value['isActive'] ?>, <?php echo $value['id'] ?>" />
					<input type="checkbox" data-methodID="<?php echo $value['id'] ?>" onchange="CMSController.toggleShippingMethods(this)" value="<?php echo $value['id'] ?>" <?php echo($value['isActive'] == 1) ? "checked" : ""; ?> /><?php echo $value['Name'] ?>
				</div>
			<?php endforeach; ?>
			
		</form>
		
	
		
	
		
		
	</div>
</div>




