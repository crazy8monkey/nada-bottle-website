
<div class="adminContent">
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/contact">Contact Inquiries</a><span>&#8250;</span><span style="font-size:12px;  margin: 0px;"><?php echo $this -> contact -> fullName() ?></span>
	</div>
	<div class="line" style="border-bottom: 1px solid #cecece; clear:both; width:100%">
		<div class="header">
			Contact Inquiry
		</div>	
		<div class="button">
			<form method="post" action="<?php echo PATH . 'cms/delete/' . $this -> contact -> _contactID; ?>/contact">
				<input type="submit" class="redButton" value="Delete Contact Inquiry" />
			</form>
		</div>
		<div style="clear:both"></div>
	</div>
	

	<div class="adminContentWrapper" style="font-size:12px; ">
		<div style="padding:10px 0px;">
			<div style="width:100px; float:left;"><strong>Submitted:</strong></div> <?php echo $this -> recordedTime -> formatDate($this -> contact -> contactDate) ?> / <?php echo $this -> contact -> contactTime ?>
			<div style="margin-bottom:20px;">
				<div style="width:100px; float:left;"><strong>Email: </strong></div><a style="color:#2a2a2a" href="mailto:<?php echo $this -> contact -> email ?>"><?php echo $this -> contact -> email ?></a> 	
			</div>
			<div style="margin-bottom:20px;">
				<strong>Comments</strong><br />
				<?php echo $this -> contact -> comments ?>	
			</div>
		</div>
	</div>
</div>
