<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".WarrantyForm");
	});
</script>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-pages.php' ?>
	<div class="header">
		Warranty
	</div>
	<div class="adminContentWrapper">
		<form action="<?php echo PATH; ?>cms/updateContent/<?php echo $this -> warranty -> pageID; ?>/Warranty" method="Post">
			<div class="headerInput">
				<div class='label'>Page Title</div>
				<input type="text" name="PageHeader" value="<?php echo $this -> warranty -> contentHeader;?>" />	
			</div>
			<textarea id="pageContent" name="pageContent"><?php echo $this -> warranty -> content ?></textarea>
			<div class="headerInput">
				<div class='label'>1. Defect Details</div>
			</div>
			<textarea name="WarrantyQuestion2" class="WarrantyForm" onkeyup="Globals.textAreaAdjust(this)" style="width:100%; padding:0px;"><?php echo str_replace("\n", '<br />', strip_tags($this -> warranty -> warrantyQuestion2, '<br />')); ?></textarea>
			<div class="headerInput">
				<div class='label'>2. Pics of the Problem</div>
			</div>
			<textarea name="WarrantyQuestion1" class="WarrantyForm" onkeyup="Globals.textAreaAdjust(this)" style="width:100%; padding:0px;"><?php echo str_replace("\n", '<br />', strip_tags($this -> warranty -> warrantyQuestion1, '<br />')); ?></textarea>
			<div class="headerInput">
				<div class='label'>5. Print Your Warranty Form</div>
			</div>
			<textarea name="PrintWarrantyDescription" class="WarrantyForm" onkeyup="Globals.textAreaAdjust(this)" style="width:100%; padding:0px;"><?php echo str_replace("\n", '<br />', strip_tags($this -> warranty -> warrantyFormDescription, '<br />')); ?></textarea>
			
			<div style="margin-top:15px">
				<input type="submit" class="blueButton" value="Submit" />	
			</div>	
		</form>
	</div>
</div>
<script type="text/javascript">
	Globals.CKEditorLoad('pageContent', '<?php echo PATH ?>');
</script>

