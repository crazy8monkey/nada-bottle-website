<style>
	textarea {
		min-width:100%
	}
</style>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-pages.php' ?>
	<div class="header">
		Contact Page
	</div>
	<div class="adminContentWrapper">
		<form action ="<?php echo PATH; ?>cms/updateContent/<?php echo $this -> content -> pageID; ?>/ContactContent" method="post">
			<div class="headerInput">
				<div class='label'>Page Title</div>
				<input type="text" name="PageHeader" value="<?php echo $this -> content -> contentHeader;?>" />	
			</div>
			<textarea id="pageContent" name="pageContent"><?php echo $this -> content -> content ?></textarea>	
			<div style="margin-top:15px">
				<input type="submit" class="blueButton" value="Submit" />	
			</div>
		</form>		
	</div>
</div>
<script type="text/javascript">
   Globals.CKEditorLoad('pageContent', '<?php echo PATH ?>');
</script>
