<?php 
?>

<div class="adminContent">
	<div class="adminContentWrapper">
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceOrders')) : ?>
			
		<div class="WidgetLine" style="margin-top:0px;">	
			<div class="widget">
				<div class="header">
					Recent Orders (latest 10)
				</div>
				<?php if(!empty($this -> latestOrders)): ?>
				<div class="content">
				<table>
					<?php foreach ($this -> latestOrders as $key => $value) : ?>
						<tr>
							<td>
								<a href="<?php echo PATH ?>cms/transaction/<?php echo $value['orderID'] ?>">Order #:<?php echo $value['orderID'] ?></a>
							</td>
							<td align="right">
								<?php echo $this -> recordedTime -> formatDate($value['date']) . ' / ' . $value['time'] ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
				</div>
				<div class="moreButton">
					<a href="<?php echo PATH ?>cms/orders">
						<div class="whiteButton">
							More Orders
						</div>
					</a>
				</div>
				<?php endif; ?>
				<?php if(empty($this -> latestOrders)): ?>
					<div style="font-size: 16px; padding: 15px 0px;">There are no orders at this time</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			
			
			<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('ContactInquiries')) : ?>
			<div class="widget">
				<div class="header">
					Recent Inquiries (latest 10)
				</div>
				<?php if(!empty($this -> latestContacts)): ?>
				<div class="content">
				<table>
					<?php foreach ($this -> latestContacts as $key => $value) : ?>
						<tr>
							<td>
								<a href="<?php echo PATH ?>cms/singleContactInquiry/<?php echo $value['contactID'] ?>"><?php echo $value['FirstName'] . " " . $value['LastName'] ?></a>
							</td>
							<td align="right">
								<?php echo $this -> recordedTime -> formatDate($value['date']) ?> / <?php echo $value['time'] ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
				</div>
				<div class="moreButton">
					<a href="<?php echo PATH ?>cms/contact">
						<div class="whiteButton">
							More Contact Inquiries
						</div>
					</a>
				</div>
				<?php endif; ?>
				<?php if(empty($this -> latestContacts)): ?>
					<div style="font-size: 16px; padding: 15px 0px;">There are no contact inquires at this time</div>
				<?php endif; ?>
			</div>
			
			<div style="clear:both"></div>
		</div>
		<?php endif; ?>
		
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceWarranty')) : ?>
			<div class="WidgetLine">	
				<div class="widget">
					<div class="header">
						Recent Warranty Requests (latest 10)
					</div>
					<?php if(!empty($this -> latestWarranties)): ?>
					<div class="content">
					<table>
						<?php foreach ($this -> latestWarranties as $key => $value) : ?>
							<tr>
								<td>
									
									<a <?php if($value['isApproved'] == 0) {echo "style='color: #FF8D2B !important; font-weight:bold;'";} ?>href="<?php echo PATH ?>cms/warrantySingle/<?php echo $value['warrantyID'] ?>">Warranty #:<?php echo $value['warrantyID'] ?></a>
								</td>
								<td align="right">
									<?php echo $this -> recordedTime -> formatDate($value['date']) . ' / ' . $value['time'] ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
					</div>
					<div class="moreButton">
						<a href="<?php echo PATH ?>cms/warranties">
							<div class="whiteButton">
								More Requests
							</div>
						</a>
					</div>
					<?php endif; ?>
					<?php if(empty($this -> latestWarranties)): ?>
						<div style="font-size: 16px; padding: 15px 0px;">There are no orders at this time</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	
	</div>
	
</div>
