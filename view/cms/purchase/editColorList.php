<!--http://acko.net/blog/farbtastic-jquery-color-picker-plug-in -->
<script type="text/javascript">	
	$(window).resize(function(){
		CMSController.textAreaAdjust(".productDescription");
	});
</script>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/products?Category=<?php echo $_GET['productType']; ?>"><?php echo str_replace("_", " ", $_GET['productType']); ?></a><span>&#8250;</span>
		<span style="font-size:12px; margin: 0px;">
			<?php echo $this -> productSingle['product_name']; ?>
		</span>
	</div>
	<div class="header" style="font-size:16px;">
		Edit Product Colors	
	</div>
	<div class="contentWrapper">
		
		<div class="adminContentWrapper">
		
			
			<div class="section">
				<?php require 'view/cms/purchase/includes/productSIngle.php' ?>
				
			</div>
			
			<div class="section" style="border-top:none;">	
				<div class="sectionHeader">Product Colors</div>
		        <div class="inputLine colorList">
					<?php if(!empty($this -> colors)) :?>
						<?php foreach ($this -> colors as $key => $value) : ?>
							<div class="colorElement">
								<div class="colorBoxElement" style="background:<?php echo $value['hexacode'] ?>"></div>
								<div class="text">
									<a class="hideMobileLink" href="<?php echo PATH ?>cms/editcolor?ID=<?php echo $value['productColor_id'] ?>&productType=<?php echo $_GET['productType']?>&productID=<?php echo $value['productId'] ?>&productName=<?php echo $this -> productSingle['product_name']; ?>">
										<?php echo $value['Color'] ?>
									</a>
									<div class="mobileColorLInk"><?php echo $value['Color'] ?></div>
								</div>
								<div class="deleteContainer">
									<a href="<?php echo PATH ?>cms/delete/<?php echo $value['productColor_id'] ?>/colors/<?php echo $_GET['productType']?>"><div class="delete"></div></a>
								</div>
								<div style="clear:both"></div>
							</div>
									
									
								<div align="right" style='position:relative'>
									
								</div>
							<?php endforeach; ?>
						<div style="clear:both"></div>
					<?php endif; ?>
					<?php if(empty($this -> colors)) :?>
						There are no colors entered for this product
					<?php endif; ?>
				</div>
			</div>
			<div id="mobileGoBackToProductInfo">
				<div id="uploadProductColorsDesktop">Please add product colors on your desktop</div>
			</div>
			<div class="section" id="hideAddColorSection">
				<div class="sectionHeader">Add New Color</div>
				<form id="ProductColorForm" method="post" action="<?php echo PATH ?>cms/addProductColor/<?php echo $this -> productSingle['product_id'] . "/" . $this -> productSingle['product_category']; ?>">
					<div style="float:left; margin-right:20px;">
						<div class="inputLine">
							<div class="label">Color Palette</div>
							<div id="colorpicker"></div>
							<input type="text" id="color" class="required" name="color" readonly="readonly" value="#123456"/>	
						</div>
					</div>
					<div style="float:left;">
						<div class="inputLine">
							<div class="label">Color Name</div>
							<input type="text" class="required" name="colorName"/>	
						</div>
						<div class="inputLine">
							<div class="label">Color Image</div>
							<input type="file" id="color-upload" class="required" name="colorImage">	
						</div>
	                    <div class="inputLine">
	                        <div class="label">Color Quantity</div>
	                        <input type="number" name="remainingQuantity" class="required" value="<?php echo $this -> colorSingle -> RemainingQuantity; ?>" />
	                    </div>
					</div>
					<div style="clear:both; width:100%">
						<div class="submitButton">
							<input type="submit" class="blueButton" value="Add Color">	
						</div>
						<div class="loadingColor">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
						</div>
						<div style="clear:both"></div>
					</div>
					
					
					<div style="margin-top:10px; margin-bottom:10px; clear:both">
						
					</div>
					
				</form>
			</div>
		
		</div>
	</div>
</div>
