<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/products?Category=<?php echo $_GET['productType']; ?>"><?php echo str_replace("_", " ", $_GET['productType']); ?></a><span>&#8250;</span>
		<a href="<?php echo PATH ?>cms/editcolorlist?ProductID=<?php echo $_GET['productID']; ?>&productType=<?php echo $_GET['productType']; ?>"><?php echo $_GET['productName']; ?></a><span>&#8250;</span>
		<span style="font-size:12px; margin: 0px;">
			<?php echo $this -> colorSingle -> Color; ?>
		</span>
	</div>
	<div class="header" style="font-size:16px;">
		Edit Color
	</div>
	<div class="contentWrapper">
		<div class="adminContentWrapper">
			<div class="desktopVersion">
				<form id="ProductColorForm" method="post" action="<?php echo PATH ?>cms/saveColor/<?php echo $this -> colorSingle -> ProductColorID; ?>">
				<div class="section">	
					<div style="float:left; margin-right:20px;">
						<div class="inputLine">
							<div class="label">Color Name</div>
							<input type="text" class="required" name="colorName" value="<?php echo $this -> colorSingle -> Color; ?>" />					
						</div>
						<div class="inputLine">
							<div class="label">Color Image</div>
							<input type="file" id="color-upload" name="colorImage">	
						</div>
						<img src="<?php echo PATH ?>view/purchase/product-images/<?php echo $this -> colorSingle -> ProductColorImage; ?>" width="200" />
						<input type="hidden" name="hiddenOldColorImage" value="<?php echo $this -> colorSingle -> ProductColorImage; ?>" />
                    	<div class="inputLine">
                    	    <div class="label">Color Quantity</div>
                    	    <input type="number" name="remainingQuantity" class="required" value="<?php echo $this -> colorSingle -> RemainingQuantity; ?>" />
                    	</div>
					</div>
					<div style="float:left">
						<div class="inputLine">
							<div class="label">Color Palette</div>
							<div id="colorpicker"></div>
							<input type="text" id="color" class="required" name="color" value="<?php echo $this -> colorSingle -> ColorHexacode; ?>"/>
						</div>
					</div>
					<div style="clear:both"></div>				
				</div>
				
				<div style="clear:both; width:100%">
					<div class="submitButton">
						<input type="submit" class="blueButton" value="Save Changes">	
					</div>
					<div class="loadingColor">
						<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
					</div>
					<div style="clear:both"></div>
				</div>
				
				
				
			</form>
			</div>
			<div id="uploadProductColorsDesktop">Please add product colors on your desktop</div>
	</div>
	</div>
</div>
