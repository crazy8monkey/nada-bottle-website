<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/orders?page=1">Orders</a><span>&#8250;</span><span style="font-size:12px;  margin: 0px;">Order #:<?php echo $this -> singleOrder['order']['orderID']; ?> / Payments</span>
	</div>	
	<?php require 'view/cms/purchase/includes/orderSingle.php' ?>
	<div class="header" style="font-size:20px;">
		Order #: <?php echo $this -> singleOrder['order']['orderID']; ?>
	</div>
	
	
	
	<div class="contentWrapper">
		<div class="section">
			<div class="sectionHeader">Payment History</div>
			<a style="text-decoration:none;" href="<?php echo STRIPE_PAYMENT_LINK . $this -> singleOrder['order']['stripeCheckoutID'] ?>" target="_blank">
				<div class="blueButton disputeButton" style="text-align:center;">
					Stripe Link
				</div>	
			</a>
			<a style="text-decoration:none;" href="<?php echo STRIPE_CUSTOMER_LINK . $this -> singleOrder['order']['stripeCustomerID'] ?>" target="_blank">
				<div class="whiteButton disputeButton" style="text-align:center;">
					Customer
				</div>	
			</a>
			<div style="clear:both"></div>
			<?php 
			$count = null;
        	$prevOrder = null;
			?>	
			<?php foreach ($this -> paymentHistory as $key => $value) : ?>
				<?php
					$count ++;
					$showDateHeader = false;
					if($key == 0) {
						$showDateHeader = true;
					} else {
		                if($this -> recordedTime -> formatDate($prevOrder['date']) != $this -> recordedTime -> formatDate($value['date'])) {
		                    $showDateHeader = true;
		                }
					}
		            $prevOrder = $value;
		            
		            $currentPaymentLink = $this -> singleOrder['order']['stripeCheckoutID'];
				?>
				<?php if ($showDateHeader == true) : ?>
				<div class='dateHeader'>
					<div class='line'></div>
					<div class='text'> 
						<?php echo $this -> recordedTime -> formatDate($value['date']) ?> 
					</div>
				</div>
				<?php endif; ?>
				<div class="webHookEvent">
					<div class="title">
						<?php
						
						setlocale(LC_MONETARY,"en_US");
						//echo money_format("The price is %i", $number);
						
						$paymentHistoryString = '';
						if($value['paymentStatus'] == 'Partially Refunded') {
							$label = "Total Refunded: ";
						}
						if($value['paymentStatus'] == 'succeeded') {
							$label = "Charge Amount: ";
						}
						if($value['paymentStatus'] == 'dispute: funds withdrawn') {
							$label = "Dispute Fee: ";
						}
						
							//
						?>
						<div>
							<strong><?php echo $value['time'] ?></strong> - CHARGE: <?php echo $value['paymentStatus'] ?>
						</div>
						<div style='margin-left:80px; margin-top: 5px;'>
							<?php 
							if($value['paymentStatus'] != 'Fully Refunded') : ?>
								<div class='paymentLine'><?php echo ($value['amount'] == 0) ? '' : '<div class="label">' . $label.'</div>'. money_format("%10.2n", $value['amount']); ?></div>
							
							<?php endif; ?>
						
						
							
							<div class='paymentLine'><?php echo ($value['refundedAmount'] == 0) ? '' : '<div class="label">Amount Refunded: </div>'. money_format("%10.2n", $value['refundedAmount']); ?></div>
							
						
							
						<?php echo ($currentPaymentLink == $value['stripeCheckoutID']) ? '' : '<div class="paymentLine"><a href="' . STRIPE_PAYMENT_LINK. $value['stripeCheckoutID'] . '" target="_blank" style="color:#3366cc">New Payment Link</a></div>' ?>
							<div style='clear:both'></div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>