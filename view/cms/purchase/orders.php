<script type='text/javascript'>
	//puts pagination element into the corresponding section
	$(document).ready(function(){
		$(".pagination").appendTo(".pagination-section");
		$(".pagination").show();	
	});
	</script>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	
	<div class="filterSection">
		<div class="header">
			Filter Orders
		</div>
		<?php
			$finished = isset($_GET['status']) && $_GET['status'] == "finished" ? 'selected' : '';
			$unfinished = isset($_GET['status']) && $_GET['status'] == "unfinished" ? 'selected' : '';
			$invalidAddress = isset($_GET['status']) && $_GET['status'] == "invalidAddress" ? 'selected' : '';
			$disputed = isset($_GET['status']) && $_GET['status'] == "disputed" ? 'selected' : '';
			$void = isset($_GET['status']) && $_GET['status'] == "void" ? 'selected' : '';
			
			$orderLink = PATH . "cms/orders?page=1";
			$finishedLink = PATH . "cms/orders?page=1&status=finished";
			$unfinishedLink = PATH . "cms/orders?page=1&status=unfinished";
			$invalidAddressLink = PATH . "cms/orders?page=1&status=invalidAddress";
			$disputedLink = PATH . "cms/orders?page=1&status=disputed";
			$voidLink = PATH . "cms/orders?page=1&status=void";
		?>
		
		<div class="linkContainer">
			<a href="<?php echo $orderLink ?>">
				<div class="link <?php echo !isset($_GET['status'])  ? 'selected' : '' ?>">
					All
				</div>
			</a>
			<a href="<?php echo $finishedLink ?>">
				<div class="link <?php echo $finished ?>">
					Finished Orders
				</div>
			</a>
			<a href="<?php echo $unfinishedLink ?>">
				<div class="link <?php echo $unfinished ?>">
					Unfinished Orders
				</div>
			</a>
			<a href="<?php echo $invalidAddressLink ?>">
				<div class="link <?php echo $invalidAddress ?>">
					Invalid Address
				</div>
			</a>
			<a href="<?php echo $disputedLink ?>">
				<div class="link <?php echo $disputed ?>">
					Disputed Charges
				</div>
			</a>
			<a href="<?php echo $voidLink ?>">
				<div class="link <?php echo $void ?>">
					Voided Orders
				</div>
			</a>
			<div style="clear:both"></div>
		</div>
		<select onchange="if (this.value) window.location.href=this.value" name="orderStatus">
			<option value="<?php echo $orderLink ?>">All</option>
			<option value="<?php echo $finishedLink ?>" <?php echo $finished ?>>Finished Orders</option>
			<option value="<?php echo $unfinishedLink ?>" <?php echo $unfinished ?>>Unfinished Orders</option>
			<option value="<?php echo $invalidAddressLink ?>" <?php echo $invalidAddress ?>>Invalid Address</option>
			<option value="<?php echo $disputedLink ?>" <?php echo $disputed ?>>Disputed Charges</option>
			<option value="<?php echo $voidLink ?>" <?php echo $void ?>>Voided Orders</option>
		</select>	
		
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		
		<?php  
		
		
		$count = null;
        $prevOrder = null;
		foreach ($this -> orders as $key => $value) {
			$count ++;
			$showDateHeader = false;
			if($key == 0) {
				$showDateHeader = true;
			} else {
                if($this -> recordedTime -> formatDate($prevOrder['date']) != $this -> recordedTime -> formatDate($value['date'])) {
                    $showDateHeader = true;
                }
			}
            $prevOrder = $value;
			?>
			<?php if ($showDateHeader == true) : ?>
			<div class='dateHeader'>
				<div class='line'></div>
				<div class='text'> 
					<?php echo $this -> recordedTime -> formatDate($value['date']) ?> 
				</div>
			</div>
			<?php endif; ?>
			
			<?php //echo $value['date'] ?>
			<div class="OrderInformation">
				<div class="orderLink">
					<a href="<?php echo PATH ?>cms/transaction/<?php echo $value['orderID']; ?>" class="OrderLink">
						Order #: <?php echo $value['orderID']; ?>
					</a>	
				</div>
				<?php if($value['stripeStatus'] == "disputed") : ?> 
					<div class="ActionRequired">
						Action Required: Submit Evidence
					</div>
				<?php endif; ?>
				<div class="info">
					<?php if ($value['isVoided'] == 1) :?>
						<div class="voidedText" style="color:#c30000">Voided</div>
					<?php endif; ?>
					<?php if ($value['isFinished'] == 1) :?>
						<div class="voidedText" style="color:#1ca201">Order is Finished</div>
					<?php endif; ?>
					<!-- if it is not voided or finished !-->
					<?php if ($value['isVoided'] == 0 && $value['isFinished'] == 0) :?>
					<div class="statusSection">
						<div class="line">
							<div class="label">Payment Status: </div>
							<?php 
							$className = "";
							if($value['stripeStatus'] == "disputed") {
								$className = "red";	
							} else {
								$className = "green";
							}
							?>
							<div class=<?php echo $className; ?> style="float:left;"><?php echo $value['stripeStatus']; ?></div>
							<div style="clear:both"></div>
						</div>
						<div class="line">
							<div class="label">Address Status:</div>
								<?php if ($value['isValidAddress'] == 0) : ?>
									<div class="red" style="float:left;">Invalid</div>
								<?php endif ?>
								<?php if ($value['isValidAddress'] == 1) : ?>
									<div class="green" style="float:left;">Valid</div>
								<?php endif ?>
								<?php if ($value['isValidAddress'] == 2) : ?>
									<div class="red" style="float:left;">Partially</div>
								<?php endif ?>
								<?php if ($value['isValidAddress'] == 3) : ?>
									<div class="red" style="float:left;">UPS Error</div>
								<?php endif ?>
								<?php if ($value['isValidAddress'] == 4) : ?>
									<div class="green" style="float:left;">Valid <span style='font-weight:normal;'>(override)</span></div>
								<?php endif ?>
								<div style="clear:both"></div>
						</div>
					</div>
					<div class="purchaseInfo">
						<div class="line">
							<div class="label">Purchasee: </div><div style="float:left;"><?php echo $value['firstName']; ?> <?php echo $value['lastName']; ?></div>
							<div style="clear:both"></div>
						</div>
						<div class="line">
							<div class="label">Time Purchased: </div><div style="float:left;">
								<?php echo $value['time']; ?></div>
							<div style="clear:both"></div>
						</div>
					</div>
					<?php endif; ?>
					<div style="clear:both"></div>
				</div>
				
			</div>
		<?php } ?>
		<div class="pagination-section"></div>
		<?php if(empty($this -> orders)) :?>
			<div style="font-size: 16px; padding: 15px 0px; border-top:1px solid #cecece">Currently you nave no orders</div>
		<?php endif; ?>
	</div>
</div>