<div class="adminContent">
	<?php if (!isset($this -> warrantyPopup)) :?>
	
		<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
		<div class="transactionBreadCrumb">
			<a href="<?php echo PATH ?>cms/orders?page=1">Orders</a><span>&#8250;</span><span style="font-size:12px;  margin: 0px;">Order #:<?php echo $this -> singleOrder['order']['orderID']; ?></span>
		</div>	
		<?php require 'view/cms/purchase/includes/orderSingle.php' ?>
	
		<?php if ($this -> singleOrder['order']['isVoided'] == 0) :?>
			<?php if ($this -> singleOrder['order']['isValidAddress'] != 1 && $this -> singleOrder['order']['isValidAddress'] != 4) : ?>
				<div class="section" id="AddressInvalid">
					<div class="sectionHeader" style="font-size: 24px; color:#c30000">Address seems invalid</div>
					When processing this order, the shipping address the purchasee has entered seems to be invalid. 
					Here are some suggested steps to make sure that it is a valid address
					<ol>
						<li>Google map the shipping address to make sure that it is valid</li>
						<li>Call/Email the purchasee to verify that is a real purchase, and a valid address</li>
					</ol>
					<div style="clear:both; width:100%">
						<a style="text-decoration:none;" href="<?php echo PATH ?>cms/ApproveAddress/<?php echo $this -> singleOrder['order']['orderID'] ?>">
							<div class="greenButton ApproveAddressButton">
								Approve Address 
							</div>
						</a>
						<div style="clear:both"></div>	
					</div>
					
					<div style="clear:both; width:100%; margin-top:15px;">
						<div style="font-size: 20px; color:#c30000; padding-top:15px; margin-top:15px; border-top: 1px solid #cecece;">If address not valid</div> 
						<ol style="margin-top:0px;">
							<li>Log in into your stripe account</li>
							<li>Open payment Log in your stripe account -> <strong><a style="color:#2a2a2a" href="https://dashboard.stripe.com/test/payments/<?php echo $this -> singleOrder['order']['stripeCheckoutID'] ?>" target="_blank">Payment Log</a></strong></li>
							<li>Refund the payment</li>
							<li>Void / Cancel this order</li>
						</ol>
					</div>
					
					<div style="clear:both"></div>
				</div>
			<?php endif ?>
		<?php endif ?>
	
		<?php if ($this -> singleOrder['order']['isVoided'] == 0) :?>
			<?php if (preg_match('/dispute/',$this -> singleOrder['order']['stripeStatus'])) :?>
				<div class="section" id="disputed">
					<div class="sectionHeader" style="font-size: 24px; color:#c30000">Charge has been Disputed</div>
					<div style="margin-bottom:10px;">
						There is a dispute on a charge for this particular order. Action is required for you to submit evidence.<br /><br />
						Here are some referrences about handling disputes
					</div>
					<ul>
						<li><a href="https://stripe.com/help/disputes-overview" target="_blank">Disputes Overview</a></li>
						<li><a href="https://stripe.com/docs/guides/dispute-evidence" target="_blank">Submitting Evidence (Sending Evidence for your Dispute) (Using the Dashboard)</a></li>
					</ul>
						
						
					<a style="text-decoration:none;" href="<?php echo STRIPE_PAYMENT_LINK . $this -> singleOrder['order']['stripeCheckoutID'] ?>" target="_blank">
						<div class="blueButton disputeButton" style="text-align:center;">
							Submit Evidence
						</div>	
					</a>	
						
					<div style="clear:both"></div>
				</div>
			<?php endif ?>
		<?php endif ?>
	<?php endif; ?>
	<div class="header">
		<div class="orderNumber">
			Order #: <?php echo $this -> singleOrder['order']['orderID']; ?>
		</div>	
		<?php if (!isset($this -> warrantyPopup)) :?>
			<?php if ($this -> singleOrder['order']['stripeStatus'] != "refunded" && $this -> singleOrder['order']['stripeStatus'] != "refunded: lost dispute") : ?>
			<div class="voidButtonContainer">
				<?php if ($this -> singleOrder['order']['isVoided'] == 0) :?>
					<?php if ($this -> singleOrder['order']['isFinished'] == 0) :?>
						<a style="text-decoration:none;" href="<?php echo PATH ?>cms/finishOrder/<?php echo $this -> singleOrder['order']['orderID'] ?>">
							<div class="greenButton finishBtn">
								Finish order	
							</div>
						</a>
					<?php endif; ?>
					<?php if ($this -> singleOrder['order']['isFinished'] == 1) :?>
						<a style="text-decoration:none;" href="<?php echo PATH ?>cms/unFinishOrder/<?php echo $this -> singleOrder['order']['orderID'] ?>">
							<div class="whiteButton unfinishedBtn">
								Unfinish order	
							</div>
						</a>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		<?php endif; ?>
		<div style="clear:both"></div>	
	</div>
	<?php if ($this -> singleOrder['order']['isVoided'] == 1) :?>
		<div style="padding:10px 0px; border:1px solid #c30000; text-align:center; color:#c30000; font-size:16px; margin-bottom:10px;">
			VOIDED
		</div>
	<?php endif; ?>
	<?php if ($this -> singleOrder['order']['isFinished'] == 1) :?>
		<div style="padding:10px 0px; border:1px solid #1ca201; text-align:center; color:#1ca201; font-size:16px; margin-bottom:10px;">
			Order is Finished
		</div>
	<?php endif; ?>
	<div class="contentWrapper">
		
		<div class="section">
			<strong>Date Purchased: </strong><?php echo $this -> recordedTime -> formatDate($this -> singleOrder['order']['date']); ?> / <?php echo $this -> singleOrder['order']['time']; ?>
		</div>
		
		<div class="section">
			<div class="statusLine">
				<div class="label">
					Address:
				</div>
				<?php if ($this -> singleOrder['order']['isValidAddress'] == 0) : ?>
					<div class="status red" style="float:left;">Invalid</div>
				<?php endif ?>
				<?php if ($this -> singleOrder['order']['isValidAddress'] == 1) : ?>
					<div class="status green" style="float:left;">Valid</div>
				<?php endif ?>
				<?php if ($this -> singleOrder['order']['isValidAddress'] == 2) : ?>
					<div class="status red" style="float:left;">Partially</div>
				<?php endif ?>
				<?php if ($this -> singleOrder['order']['isValidAddress'] == 3) : ?>
					<div class="status red" style="float:left;">UPS Error</div>
				<?php endif ?>
				<?php if ($this -> singleOrder['order']['isValidAddress'] == 4) : ?>
					<div class="status green" style="float:left;">Valid <span style='font-weight:normal;'>(override)</span></div>
				<?php endif ?>

				<div style="clear:both"></div>
			</div>
			<div class="statusLine" style="margin-bottom:0px;">
				<div class="label">
					Payment Status:
				</div>
				<?php if ($this -> singleOrder['order']['stripeStatus'] == "succeeded") { ?>
					<div class="status green" style="float:left;"><?php echo $this -> singleOrder['order']['stripeStatus'] ?></div>
				<?php } else { ?>
					<div class="status red" style="float:left;"><?php echo $this -> singleOrder['order']['stripeStatus'] ?></div>
				<?php } ?>
				<div style="clear:both"></div>
			</div>
		</div>
		
		
		
		<div class="section" style="padding-bottom: 0px;">
			<div class="sectionHeader">Items Orderded</div>
			
			<div class="orderItemSection">		
			<?php foreach ($this -> singleOrder['items'] as $key => $value) { ?>
	        	<div class="orderItem">
	        		<div><strong><?php echo $value['product_name'] ?></strong></div>
	        		<div>Color: <?php echo $value['Color'] ?></div>
	        		<div>Quantity: <?php echo $value['quantity'] ?></div>
	        	</div>
	        <?php } ?>
	       	</div>
	       	<?php if (!isset($this -> warrantyPopup)) :?>
		       	<div class="receiptContainer">
	            	<a style="text-decoration:none;" href="javascript:void(0);" onclick="CMSController.openPopup('<?php echo PATH ?>cms/receipt/<?php echo $this -> singleOrder['order']['orderID']; ?>')">
	            		<div class="blueButton receiptButton">
	            			View receipt	
	            		</div>
	            	</a>
	            </div>
            <?php endif; ?>
	       	<div style="clear:both"></div>
		</div>	
		<div class="section">
			<div class="sectionHeader">Customer Information</div>
			<div style="clear:both">
				<a style="text-decoration:underline; color:#2a2a2a" href="mailto:<?php echo $this -> singleOrder['order']['email']; ?>">
					<?php echo $this -> singleOrder['order']['email']; ?>
				</a>
			</div>
			<div style="clear:both">
				<?php $areaCode = substr($this -> singleOrder['order']['telephone'], 0, 3);
					  $RestOfPhone1 = substr($this -> singleOrder['order']['telephone'], 3, 3);
					  $RestOfPhone2 = substr($this -> singleOrder['order']['telephone'], 6, 7); 
					$telephone = "(" .$areaCode . ") ". $RestOfPhone1 . " - " . $RestOfPhone2;
					
				?>
					
				<?php echo $telephone ?>
			</div>
			<div class="shippingAddress">
				<div class="shippingAddressSection">
					<strong><?php echo $this -> singleOrder['order']['firstName']; ?> <?php echo $this -> singleOrder['order']['lastName']; ?></strong><br />
					<div id="currentShipping">
						<span id="shippingAddress"><?php echo $this -> singleOrder['order']['address']; ?></span><br />
						<span id="shippingCity"><?php echo $this -> singleOrder['order']['city']; ?></span>, <span id="shippingState"><?php echo $this -> singleOrder['order']['state']; ?></span> <span id="shippingZip"><?php echo $this -> singleOrder['order']['zip']; ?></span><br />
						<?php echo $this -> singleOrder['order']['Country']; ?>		
					</div>
					<div style="background:white;" id="editShippingForm">
						<form id="newShippingAddress" method="post" action="<?php echo PATH ?>cms/changeShippingInfo/<?php echo $this -> singleOrder['order']['orderID']; ?>">
							<div style="margin-bottom:10px;">
								<input type="text" class="newAddress" name="changeAddress" value="<?php echo $this -> singleOrder['order']['address']; ?>" />	
							</div>
							<div style="margin-bottom:10px;">
								
								
								<input type="text" class="newCity" name="changeCity" value="<?php echo $this -> singleOrder['order']['city']; ?>" />
								<input type="text" class="newState" name="changeState" value="<?php echo $this -> singleOrder['order']['state']; ?>" />
								<input type="text" class="newZip" style="" name="changeZip" value="<?php echo $this -> singleOrder['order']['zip']; ?>" />
							</div>
							<div class="buttonHolder">
								<input type="submit" class="blueButton newShipButton" value="Save shipping Information" />
								<button class="cancel whiteButton" onclick="CMSController.closeEditShippingForm(); return false"> Cancel</button>	
							</div>
							<div class="loadingShippingForm"><img src="<?php echo PATH ?>public/images/ajax-loader.gif" /></div>
						</form>
					</div>
					<div style="clear:both"></div>
				</div>
				<?php if (!isset($this -> warrantyPopup)) :?>
					<?php if ($this -> singleOrder['order']['isVoided'] == 0) : ?>
						<div class="shipButtonContainer">
							<?php if ($this -> singleOrder['order']['isFinished'] == 0) :?>
							<?php if(empty($this -> singleOrder['order']['shipmentId'])) : ?>
								<a href="javascript:void(0);" style="text-decoration:none;" onclick="CMSController.openEditShippingForm();">
									<div class="whiteButton editShippingAddressButton">
										Edit Shipping Address	
									</div>
								</a>
							<?php endif; ?>
							
							<a id="mapItLink" href="http://maps.google.com/maps?q=<?php echo str_replace(" ", "+",$this -> singleOrder['order']['address']); ?>,+<?php echo $this -> singleOrder['order']['city']; ?>,+<?php echo $this -> singleOrder['order']['state']; ?>,+<?php echo $this -> singleOrder['order']['zip']; ?>&z=14" target="_blank">
								<div class="whiteButton MapItButton">
									Map It	
								</div>
							</a>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				<div style="clear:both"></div>
			</div>
			
			<div id="changeStatusIndicator" style="<?=($this -> singleOrder['order']['shippingChange'] == 1) ? "display:block;" : ""; ?> margin-top:15px; color:#c30000">
				*shipping address changed on <span id="changedDate"><?php echo $this -> singleOrder['order']['shippingChangeDate']; ?></span>
				<div class="changedShippingRate" style="<?=($this -> singleOrder['order']['newShippingRate']  != 0) ? "display:block;" : ""; ?>">
					<strong>New Shipping Rates:</strong> $<span id="newShippingRates"><?php echo $this -> singleOrder['order']['newShippingRate']; ?> USD</span>
				</div>
				
			</div>
		
			<div style="clear:both"></div>
		</div>
		<?php if ($this -> singleOrder['order']['isVoided'] == 0) :?>
			
			<?php if ($this -> singleOrder['order']['isFinished'] == 0) :?>
				<div class="section">
					<?php $code = Shipments::WithServiceCode($this -> singleOrder['order']['shippingMethodCode']) ?>
					<div class="sectionHeader">Shipping Information - Company: <?php echo $code -> shippingCompany ?></div>
					<?php if (!isset($this -> warrantyPopup)) :?>
						<div style="margin-top:5px; color:#c30000; font-style:italic">
			            		*Note, when creating a shipment order, it is recommended to check your <?php echo $code -> shippingCompany ?> account to see this particular order is in your account.
			            	</div>
						<div style="margin:10px 0px">
							
							<?php if(is_null($this -> singleOrder['order']['shipmentId'])) { 
							
							$shipmentDateLine = "";
							?>
			                        <a style="text-decoration:none;" href="<?php echo PATH; ?>cms/generateShipping/<?php echo $this -> singleOrder['order']['orderID'];?>">
			                        	<div class="greenButton shipmentButton">
			            					Generate Shipment
			            				</div>
			                        </a>
			                    <?php } else{ ?>
			                    	<div style="clear:both">
			                    		<a style="text-decoration:none;" href="javascript:void(0);" onclick="CMSController.openShippingLabelDropDown()">
							            	<div class="blueButton shipmentButton">
							            		View Labels<div class="arrow"></div>
							            	</div>
							            </a>
							            <?php //echo $this->controller->getTrackingUrl($code -> shipmentType, $this -> singleOrder['order']['orderID']); ?>
							            <a style="text-decoration:none;" href="javascript:void(0);" onclick="CMSController.openTrackingPackages()">
											<div class="whiteButton shipmentButton">
												Track Order<div class="arrow"></div>
											</div>
										</a>
				                        
				                        <a href="<?php echo PATH; ?>cms/voidShipment/<?php echo $this -> singleOrder['order']['orderID'];?>" style="text-decoration:none">
				                        	<div class="redButton shipmentButton">
												Void Order
											</div>
											
				                        </a>
							            <div style="clear:both"></div>
			                    	</div>
			                    	
			                    	<div class="printLabelsSection">
						            	<div class="printHeader">Print Shipping Labels</div>
						            	<?php $i = 1 ?>
				                    	<?php foreach($this -> labels as $key => $value) : ?>
				                    		<a style="text-decoration:none;" href="javascript:void(0);" onclick="CMSController.openPopup('<?php echo PATH; ?>cms/shippingLabel/<?php echo $this -> singleOrder['order']['orderID'] . "/" . $key;?>')">
							            		<div class="printLabelBtn shipmentButton">
							            			Package <?php echo $i; ?>
							            		</div>
							            	</a>
							            	<?php $i++  ?>
				                    	<?php endforeach; ?>
				                    	<div style="clear:both"></div>
						            </div>
						            
						            <div class="trackPackagesSection">
						            	<div class="printHeader">Track Packages</div>
						            	<?php $i = 1 ?>
						            	
				                    	<?php foreach($this -> tracking as $key => $value) : ?>
				                    		<?php 
				                    		 		$trackingLink = NULL;
				                    				if($code -> shippingCompany == "UPS") {
				                    					$trackingLink = "http://wwwapps.ups.com/etracking/tracking.cgi?tracknum=" . $value;
				                    				} else if($code -> shippingCompany == "FedEx") {
				                    					$trackingLink = "https://www.fedex.com/apps/fedextrack/?tracknumbers=" . $value. '&language=en&cntry_code=us';
				                    				}
				                    		
				                    		?>
				                    		
				                    		
				                    		<a style="text-decoration:none;" href="<?php echo $trackingLink ?>" target="_blank">
							            		<div class="trackPackageBtn shipmentButton">
							            			Package <?php echo $i; ?>
							            		</div>
							            	</a>
							            	<?php $i++  ?>
				                    	<?php endforeach; ?>
				                    	<div style="clear:both"></div>
						            </div>
			                        
			                    <?php 
			                    $shipmentDateLine = "<div class='line'><div class='label'>Shipment Created:</div><div class='content'>". $this -> recordedTime -> formatDate($this -> singleOrder['order']['shipmentDate']) ."</div><div style='clear:both'></div></div>";
			                    	
			                    } ?>
			            	<div style="clear:both"></div>
						</div>
					<?php endif; ?>
					<div class="shippingInfo">
						<?php if (!isset($this -> warrantyPopup)) :?>
							<?php echo $shipmentDateLine ?>
						<?php endif; ?>
						
						<div class="line">
							<div class="label">
								<span id="originalShippingLabel" <?php if($this -> singleOrder['order']['newShippingRate'] != 0) { echo "style='display:inline' "; } ?>>Original</span> Cost:
							</div>
							<div class="content">
								$<?php echo $this -> singleOrder['order']['shippingRate']; ?> USD
							</div>
							<div style="clear:both"></div>	
						</div>
						<div class="line">
							<div class="label">
								Shipping Method:
							</div>
							<div class="content">
								<?php echo $this -> singleOrder['order']['shippingMethod']; ?>
							</div>
							<div style="clear:both"></div>	
						</div>
					</div>
					
					<div class="packingInfo">
						
							<?php $i = 1 ?>
		                	<?php foreach ($this -> singleOrder['package'] as $key => $value) { ?>
		                		<div style="float:left; margin-right:10px">
		                		 	<strong>Package: <?php echo $i ?></strong><br />
		
		                		 	<?php echo $value['length'] ?> in (Length) <br /> 
		                		 	<?php echo $value['width'] ?> in (Width)<br /> 
		                		 	<?php echo $value['height'] ?> in (Height)<br /> <br />
		                		 	<?php echo $value['weight'] ?> lb (Weight)<br />
		                		</div>
		         			<?php $i++  ?>
		                	<?php } ?>
						<div style="clear:both"></div>
					</div>
				</div>
			<?php endif; ?>
		<div class="section">
			<div class="sectionHeader">User Information</div>
			<div style="font-weight:bold">User Agent</div>
			<?php echo $this -> singleOrder['order']['userAgent']; ?><br /><br/>
			<div style='margin-bottom:10px'>
			<div style="font-weight:bold">Customer IP Address</div>
			<?php echo $this -> singleOrder['order']['customerIDAddress']; ?>

			</div>
			<?php if (!isset($this -> warrantyPopup)) :?>
				<a href="<?php echo IP_LOOKUP . $this -> singleOrder['order']['customerIDAddress']; ?>" style="text-decoration:none;" target="_blank">
					<div class="whiteButton editShippingAddressButton" style="margin-bottom:10px">
						IP Lookup	
					</div>
				</a>
			<?php endif ?>
		</div>
		<?php endif; ?>
	</div>
</div>

 