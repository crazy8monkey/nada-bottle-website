<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/products?Category=<?php echo $_GET['productType']; ?>"><?php echo str_replace("_", " ", $_GET['productType']); ?></a><span>&#8250;</span>
		<span style="font-size:12px; margin: 0px;">
			<?php echo $this -> productSingle['product_name']; ?>
		</span>
	</div>
	<div class="header" style="font-size:16px;">
		Edit Product Inventory
	</div>
	<div class="contentWrapper">
		
		<div class="adminContentWrapper" id="InventorySection">
			<div class="section">
				<?php require 'view/cms/purchase/includes/productSIngle.php' ?>	
			</div>
			
			
				
			<div class="section" style="border-top:none;">
				<div class="sectionHeader">Inventory</div>
				<?php if(!empty($this -> colors)) :?>
				<div class="inventoryList">
					<div class="header" style="border-bottom:1px solid #e9e9e9">
						<div class="colorColumn columnHeader">Colors</div>
						<div class="stockColumn columnHeader" style="text-align:left;"># of Stock</div>
						<div style="clear:both"></div>
					</div>
					<?php foreach ($this -> colors as $key => $value) : ?>
						<div class="colorLine">						
							<div class="colorColumn colorElements">
								<div class="hideMobile">
									<div class="colorBox" style="background:<?php echo $value['hexacode'] ?>"></div>
									<div class="colorName"><?php echo $value['Color'] ?></div>		
								</div>
								
								<div style="clear:both"></div>
								<div class="mobileColor" style="background:<?php echo $value['hexacode'] ?>">
									<?php echo $value['Color'] ?>
								</div>
							</div>
							<form action="<?php echo PATH ?>cms/updateInventory/<?php echo $value['productColor_id'] ?>" method="post" class="inventoryForm">
								<div class="stockFormElement">
										<div class="mobileHeader"># of Stock</div>
										<div class="stockColumnElement">
											<input type="text" name="quantity" value="<?php echo $value['quantityRemaining'] ?>" />
										</div>
										<div class="OutOfStockMessage">
											<div class='text' id="<?php echo $value['productColor_id']?>" <?php echo $value['quantityRemaining'] > 2 ? 'style="display:none;"' : '' ?>>*Out of Stock</div>										
										</div>
										<div class="ajaxButtonContainer">
											<div class="ajaxReloader" id="<?php echo $value['productColor_id']?>">
												<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
											</div>
											<div class="updateButton">
												<input type="submit" class="whiteButton" value="Update" />
											</div>	
										</div>
										
									</div>
							</form>
							<div style="clear:both"></div>
						</div>
					<?php endforeach; ?>
				</div>	
				<?php endif; ?>
				<?php if(empty($this -> colors)) :?>
					<div style="font-size: 16px; padding: 15px 0px; border-top:1px solid #cecece">There is no colors submitted to this product</div>
				<?php endif; ?>		
					
				
			</div>
				
				
		
		
		</div>
	</div>
</div>
