<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="header" style="font-size:16px;">
		Paymemt Logs
	</div>
	
	<div class="adminContentWrapper">
		<div class="LoggingContentContainer">
			<?php 
			setlocale(LC_MONETARY,"en_US");
			$count = null;
        	$prevOrder = null;
			?>	
			
			<?php foreach ($this -> paymentHistory as $key => $value) : ?>
				<?php
					$count ++;
					$showDateHeader = false;
					if($key == 0) {
						$showDateHeader = true;
					} else {
		                if($this -> recordedTime -> formatDate($prevOrder['date']) != $this -> recordedTime -> formatDate($value['date'])) {
		                    $showDateHeader = true;
		                }
					}
		            $prevOrder = $value;
		            
		            if($value['paymentStatus'] == 'Partially Refunded') {
							$label = "Total Refunded:";
						}
				if($value['paymentStatus'] == 'succeeded') {
							$label = "Charge Amount:";
						}
						if($value['paymentStatus'] == 'dispute: funds withdrawn') {
							$label = "Dispute Fee:";
						}
		        
		            //
				?>
				<?php if ($showDateHeader == true) : ?>
				<div class='dateHeader'>
					<div class='line'></div>
					<div class='text'> 
						<?php echo $this -> recordedTime -> formatDate($value['date']) ?> 
					</div>
				</div>
				<?php endif; ?>
			<div class="webHookEvent">
				<div class="title">
					CHARGE: <?php echo $value['paymentStatus'] ?>
				</div>
				<table>
					<tr>
						<td>Order #:</td>
						<td><a href="<?php echo PATH ?>cms/transaction/<?php echo $value['orderID'] ?>"><?php echo $value['orderID'] ?></a></td>
					</tr>
					<tr>
						<td>Payment Link:</td>
						<td><a href="<?php echo STRIPE_PAYMENT_LINK . $value['stripeCheckoutID'] ?>" target='_blank'>Click Here</a></td>
					</tr>
					<tr>
						<td>Time:</td>
						<td><?php echo $value['time'] ?></td>
					</tr>
					<?php if($value['amount'] != 0): ?>
						<tr>
							<td><?php echo $label ?></td>
							<td><?php echo money_format("%10.2n", $value['amount']) ?></td>
						</tr>
					<?php endif; ?> 
					<?php if($value['refundedAmount'] != 0): ?>
						<tr>
							<td>Amount Refunded:</td>
							<td><?php echo money_format("%10.2n", $value['refundedAmount']) ?></td>
						</tr>
					
					<?php endif; ?> 
				</table>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>