<?php
$categoryNameBreadCrumb;
$categoryPageHeader;
$categoryNameInput;
$isCustomizable;
$categorySubmitFormURL;
$editCategoryView = true;

if(isset($this -> categorySingle -> CategoryID)) {
	$categoryNameBreadCrumb = str_replace("_", " ", $this -> categorySingle -> CategoryName);	
	$categoryPageHeader = "Edit Category";
	$categoryNameInput = str_replace("_", " ", $this -> categorySingle -> CategoryName);
	$isCustomizable = $this -> categorySingle -> isCustomizable;
	$categorySubmitFormURL = PATH . "cms/saveCategory/" . $this -> categorySingle -> CategoryID;
	
} else {
	$editCategoryView = false;
	$categorySubmitFormURL = PATH . "cms/saveCategory";
	$categoryNameBreadCrumb = "New Category";
	$categoryPageHeader = "New Category";
	$categoryNameInput = "";
	$isCustomizable = "";
}
?>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/categories">Categories</a><span>&#8250;</span><span id="currentCategoryName" style="font-size:12px;  margin: 0px;"><?php echo $categoryNameBreadCrumb; ?></span>
	</div>
	<div class="header">
		<?php echo $categoryPageHeader ?>
	</div>
	<div class="contentWrapper">
		
		<div class="adminContentWrapper">
		
		<form id="CategoryForm" action="<?php echo $categorySubmitFormURL ?>" method="post">
			<div class="section" style="margin-top:0px;">
				<div class="sectionHeader">Name</div>
			</div>
			<input type="text" value="<?php echo $categoryNameInput ?>" name="categoryName"  />
			<div class="section">
				<div class="sectionHeader">Category Picture</div>
				<?php if($editCategoryView == true) : ?>
					<?php if(!empty($this -> categorySingle -> categoryImage)) : ?>
						<img src="../../view/purchase/category-images/<?php echo $this -> categorySingle -> categoryImage; ?>" style="width:100%;"><br />
						<div style="margin:10px 0px">
							<a href="<?php echo PATH ?>cms/deleteCategoryPhoto/<?php echo $this -> categorySingle -> CategoryID ?>" style='text-decoration:none;'>
								<div class="whiteButton deletePhotoButton">
									Delete Photo
								</div>
							</a>					
							<div style="clear:both"></div>
						</div>
					<?php endif; ?>		
					<?php if(empty($this -> categorySingle -> categoryImage)) : ?>
						<div id="noImage">
							No Image	
						</div>
					<?php endif; ?>	
				<?php endif; ?>
				

				
				<div id="desktopFileUpload">
					<input type='file' name="categoryImage" />
				</div>
				
				<div id="uploadLogoDesktop">Please Upload an image on your desktop</div>
			</div>
			<div class="section">
				<div class="sectionHeader">Specificiations</div>
				<div class="inputLine">
					<div class="permissionsLable">
						Customizable
					</div>
					<div class="radioInput">
						<input type="radio" name="Customizable" value="1"  <?=($isCustomizable == 1) ? "checked" : ""; ?>  />Yes
					</div>
					<div class="radioInput">
						<input type="radio" name="Customizable" value="0"  <?=($isCustomizable == 0) ? "checked" : ""; ?> />No
					</div>
				</div>
			</div>
			
			<div style="margin-top:10px;">
				<div class="submitButton">
					<input type="submit" class="blueButton" value="Save" />
				</div>
				<div class="loadingCategory">
					<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
				</div>
					
				<div style="clear:both"></div>
			</div>
		</form>
		
		</div>
	</div>
</div>
