<script style="text/javascript">
	function confirmDelete() {
		return confirm('Are you sure you want to delete this category? Removing this item deletes all contents related to this category');
	}
</script>

<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="header">
		<div class="text">
			Category List	
		</div>
		<div class="newLink">
			<a href="<?php echo PATH ?>cms/addNewCategory" style="text-decoration:none;">
				<div class="greenButton">
					New Category
				</div>
			</a>
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		<form id="categoryListForm" method="post" action="<?php echo PATH ?>cms/reOrderList/categories">
			<div style="font-size:16px; padding: 10px 0px; ">
				<span id="desktopClick">Click</span><span id="mobileTouch">Touch</span> and drag the category line and re-order to your liking, and click "save order" to save your changes
			</div>
			
			<div style="clear:both; width:100%">
				<div class='submitButton'>
					<input type="submit" class="blueButton" value="Save Order" />
				</div>
				<div class='loadingCategoryList'>
					<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
				</div>
				<div style="clear:both"></div>
			</div>
			
			
					
			
			<div style="border-bottom: 1px solid #EDEDED; margin-top:10px;"></div>
			<div id="categoryList" style="background: #E7E7E7;">
			<?php foreach ($this -> categories as $key => $value) : ?>
				<div class="categoryLine">
					<input type='hidden' name="categoryID[]" class="categoryOrder" value="<?php echo $value['categoryID'] ?>" />
					<div class="dragIcon"></div>
					<div class="name">
						<a href="<?php echo PATH ?>cms/category/<?php echo $value['categoryID'] ?>"><?php echo str_replace("_", " ", $value['ProductCategory']) ?></a>
					</div>
					<div class="editDelete">
						
						<a onclick="return confirmDelete();" href="<?php echo PATH ?>cms/delete/<?php echo $value['categoryID'] ?>/category"><div class="delete"></div></a>
						
					</div>
					<div style="clear:both"></div>
				</div>
			<?php endforeach; ?>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var storeNumber = $(".categoryLine").length;
	//alert(storeNumber);
	$(document).ready(function(){
		$( "#categoryList" ).sortable().disableSelection();
	})
</script>