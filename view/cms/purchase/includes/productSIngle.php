<?php if(empty($this -> productSingle['product_image']) || empty($this -> colors)) :  ?>
	<div style="text-align: center; font-size: 16px; padding: 15px; margin-top:-10px;">
		Product Image/Colors needs to be uploaded in order to publish to your website
	</div>
<?php endif; ?>
<div class="publishedNotification">
	<?php if($this -> productSingle['publishState'] == 0 ) : ?>
		Product Not Published
	<?php endif; ?>
				
	<?php if($this -> productSingle['publishState'] == 1 ) : ?>
		Product Published
	<?php endif; ?>
					
</div>
<div class="filterSection">
	<div class="linkContainer">
	<a href="<?php echo PATH ?>cms/editproduct?ID=<?php echo $this -> productSingle['product_id'] . "&productType=" . $this -> productSingle['product_category'] ?>">
		<div class="link <?php if(isset($this -> productInfoSelectedLink)) {echo "selected"; } ?>">
			Product Info.
		</div>
	</a>
	<a href="<?php echo PATH ?>cms/editcolorlist?ProductID=<?php echo $this -> productSingle['product_id'] . "&productType=" . $this -> productSingle['product_category'] ?>">
		<div class="link <?php if(isset($this -> colorListSelectedLink)) {echo "selected"; } ?>">
			Colors
		</div>
	</a>
	<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceInventory')) : ?>
		<a href="<?php echo PATH ?>cms/editproductinventory?ProductID=<?php echo $this -> productSingle['product_id'] . "&productType=" . $this -> productSingle['product_category'] ?>">
			<div class="link <?php if(isset($this -> SingleInventoryList)) {echo "selected"; } ?>">
				Inventory
			</div>
		</a>
	<?php endif ?>		
	</div>

	<div style="clear:both"></div>
</div>

