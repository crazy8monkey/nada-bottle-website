<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) : ?>
<div class="filterSection">
	<div class="linkContainer">
	<a href="<?php echo PATH ?>cms/transaction/<?php echo $this -> singleOrder['order']['orderID'] ?>">
		<div class="link <?php if(isset($this -> orderInfoSelected)) {echo "selected"; } ?>">
			Order Info.
		</div>
	</a>
	<a href="<?php echo PATH ?>cms/transaction/<?php echo $this -> singleOrder['order']['orderID'] ?>/payments">
		<div class="link <?php if(isset($this -> paymentInfoSelected)) {echo "selected"; } ?>">
			Payment Info.
		</div>
	</a>		
	</div>

	<div style="clear:both"></div>
</div>
<?php endif ?>

