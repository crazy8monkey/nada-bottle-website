<?php
$saveProductFormURL;
$productPageHeader;
$breadCrumbText;
$editProductView = true;
$productName;
$price;
$specOuter;
$specInner;
$specStrap;
$specCap;
$specGrommet;
$width;
$height;	
$length;
$weight;					
$volume;		
$description;


if(isset($this -> productSingle['product_name'])) {
	$saveProductFormURL = PATH . "cms/saveProduct/" . $this -> productSingle['product_category'] . "/" . $this -> productSingle['product_id'];
	$productPageHeader = "Edit Product";
	$breadCrumbText = $this -> productSingle['product_name'];	
	$productName = $this -> productSingle['product_name'];
	$price = $this -> productSingle['price'];
	$specOuter = $this -> productSingle['spec_outer'];
	$specInner = $this -> productSingle['spec_inner'];
	$specStrap = $this -> productSingle['spec_strap'];
	$specCap = $this -> productSingle['spec_cap'];
	$specGrommet = $this -> productSingle['spec_grommet'];
	$width = $this -> productSingle['width'];
	$height = $this -> productSingle['height'];
	$length = $this -> productSingle['length'];
	$weight = $this -> productSingle['weight'];
	$volume = $this -> productSingle['spec_volumne'];
	$description = $this -> productSingle['product_description'];
} else {
	$saveProductFormURL = PATH . "cms/saveProduct/" . $_GET['productType'];
	$editProductView = false;
	$productPageHeader = "New Product";
	$breadCrumbText = "New Product";
	$productName = "";
	$price = "";
	$specOuter = "";
	$specInner = "";
	$specStrap = "";
	$specCap = "";
	$specGrommet = "";
	$width = "";
	$height = "";
	$length = "";
	$weight = "";
	$volume = "";
	$description = "";
}
?>


<!--http://acko.net/blog/farbtastic-jquery-color-picker-plug-in -->
<script type="text/javascript">	
	$(window).resize(function(){
		Globals.textAreaAdjust(".productDescription");
	});
</script>
<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/products?Category=<?php echo $_GET['productType']; ?>"><?php echo str_replace("_", " ", $_GET['productType']); ?></a><span>&#8250;</span>
		<span style="font-size:12px; margin: 0px;">
			<?php echo $breadCrumbText; ?>
		</span>
	</div>
	<div class="header">
		<div class="headerText" style="font-size:16px;" >
			<?php echo $productPageHeader; ?>	
		</div>
		
		<?php if($editProductView == true) : ?>
			<div class="buttonContainer">
				<?php if(!empty($this -> productSingle['product_image']) && !empty($this -> colors)) : ?>
					<div class="publishUnpublishContainer">
						<?php if($this -> productSingle['publishState'] == 0) : ?>
							<a href="<?php echo PATH ?>cms/publishProduct/<?php echo $this -> productSingle['product_id'] ?>" style="text-decoration:none;">
								<div class="whiteButton">
									Publish Product
								</div>
							</a>
						<?php endif; ?>
						
						<?php if($this -> productSingle['publishState'] == 1 ) : ?>
							<a href="<?php echo PATH ?>cms/unPublishProduct/<?php echo $this -> productSingle['product_id'] ?>/ <?php echo $_GET['productType']; ?>" style="text-decoration:none;">
								<div class="whiteButton">
									Unpublish Product
								</div>
							</a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<div class="deleteContainer">
					<form action="<?php echo PATH; ?>cms/delete/<?php echo $this -> productSingle['product_id']; ?>/products" method="post">
						<input type="submit" class="redButton" value="Delete Product">
					</form>				
				</div>
				
			</div> 	
		<?php endif; ?>		
		<div style="clear:both"></div>
	</div>
	<div class="contentWrapper">
		
		<div class="adminContentWrapper">
		
			
			<?php if($editProductView == true) { ?>
			<div class="section">
				<?php require 'view/cms/purchase/includes/productSIngle.php' ?>
			</div>
			<?php } else { ?>
				<div style='border-top: 1px solid #cecece;'></div>
				
			<?php } ?>	
			
			
			<form id="editProductForm" method="post" action="<?php echo $saveProductFormURL ?>">
				<div class="section" style="border-top:none;">
					<div class="sectionHeader">Product Information</div>
					
					<div class="inputLine">
						<div class="label" style="color:#2a2a2a">Product Image</div>
						<?php if($editProductView == true) : ?>
						
							<?php if(!empty($this -> productSingle['product_image'])) : ?>
								<img src="<?php echo PATH ?>view/purchase/product-images/<?php echo $this -> productSingle['product_image']; ?>" style="width:200px;"><br />
							<?php endif; ?>	
							<?php if(empty($this -> productSingle['product_image'])) : ?>
								<div id="noImage">
									No Image	
								</div>
							<?php endif; ?>	
						<?php endif; ?>	
						<div id="desktopFileUpload">
							<input type="file" id="image-upload" name="productImage">	
						</div>
					</div>
					<div id="uploadLogoDesktop">Please Upload an image on your desktop</div>
					<div class="inputLine">
						<div class="label">Product Name</div>
						<input type="text" name="productName" class="required normalInput" value="<?php echo $productName ?>">
					</div>		
					<div class="inputLine">
						<div class="label">Price <span style="font-weight:normal">(i.e 99.99 = $99.99)</span></div>
						<div class="hyperlinkContainer">
							$
						</div>
						<input type="text" name="productPrice" class="required" value="<?php echo $price; ?>">
					</div>
				
				</div>
				
				<div class="section">
					<div class="sectionHeader">Product Specifications</div>
					<div class="inputLine">
						<div class="label">Outer</div>
						<input type="text" name="specOuter" class="required normalInput" value="<?php echo $specOuter; ?>">
					</div>
					
					<div class="inputLine">
						<div class="label">Inner</div>
						<input type="text" name="specInner" class="required normalInput" value="<?php echo $specInner; ?>">
					</div>
					
					<div class="inputLine">
						<div class="label">Strap</div>
						<input type="text" name="specStrap" class="required normalInput" value="<?php echo $specStrap; ?>"> 
					</div>
					
					<div class="inputLine">
						<div class="label">Cap</div>
						<input type="text" name="specCap" class="required normalInput" value="<?php echo $specCap; ?>">
					</div>
					
					<div class="inputLine">
						<div class="label">Grommet</div>
						<input type="text" name="specGrommet" class="required normalInput" value="<?php echo $specGrommet; ?>">
					</div>
				
					<div class="inputLine">
						<div class="label">Width</div>
						<input type="text" name="productWidth" class="required" value="<?php echo $width; ?>"> In
					</div>
					
					<div class="inputLine">
						<div class="label">Height</div>
						<input type="text" name="productHeight" class="required" value="<?php echo $height; ?>"> In
					</div>
					
					<div class="inputLine">
						<div class="label">Depth</div>
						<input type="text" name="productLength" class="required" value="<?php echo $length; ?>"> In
					</div>
					
					<div class="inputLine">
						<div class="label">Weight</div>
						<input type="text" name="productWeight" class="required" value="<?php echo $weight; ?>"> lbs
					</div>
					
					<div class="inputLine">
						<div class="label">Volumne</div>
						<input type="text" name="specVolumne" class="required" value="<?php echo $volume; ?>"> lbs
					</div>
				</div>
				
				<div class="section">
					<div class="sectionHeader">Product Description</div>
					<textarea name="productDescription" onkeyup="Globals.textAreaAdjust(this)" class="productDescription" style="width:100%; max-width:100%; min-height:80px;"><?php echo $description; ?></textarea><br/>
				</div>
				
				<div style="clear:both; width:100%">
					<div class="submitButton">
						<input type="submit" class="blueButton" value="Save Changes">	
					</div>
					<div class="loadingProduct">
						<img src="<?php echo PATH ?>public/images/ajax-loader.gif">
					</div>
					<div style="clear:both"></div>
				</div>
				
			</form>
		</div>
	</div>
</div>
