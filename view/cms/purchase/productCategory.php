<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
	<div style="margin-bottom:20px;" class="adminContentWrapper categoryDropDown">
		<div style="margin-bottom:5px;">
			<strong>Category</strong>
		</div>
		<select onchange="if (this.value) window.location.href=this.value" style="width:100%">
			<?php if(!isset($_GET['Category'])) : ?>
				<option></option>
			<?php endif; ?>			
			<?php foreach ($this -> categories as $key => $value) : ?>
			<option value="<?php echo PATH ?>cms/products?Category=<?php echo $value['ProductCategory'] ?>" <?php echo isset($_GET['Category']) && $_GET['Category'] == $value['ProductCategory'] ? 'selected' : '' ?>>
				<?php echo str_replace("_", " ", $value['ProductCategory']) ?>
			</option>
			<?php endforeach; ?>
		</select>
	</div>
	<?php if(isset($_GET['Category'])) : ?>
	
	<div class="header">
		<div class="category">
			<?php echo str_replace("_", " ", $_GET['Category']) ?> Products	
		</div>
		<div class="newProductbutton">
			<a style="text-decoration:none" href="<?php echo PATH ?>cms/addNewProduct?productType=<?php echo $_GET['Category'] ?>">
				<div class="greenButton">
					New Product	
				</div>
			</a>
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="contentWrapper">
		<?php foreach ($this -> product as $key => $value) { ?>
			<div class="productItem">
				<table style="width:100%;">
					<tr>
						<td class="imageTD">
							<?php if(!empty($value['product_image'])) : ?>
							<img src="../view/purchase/product-images/<?php echo $value['product_image'] ?>" width="100"  />
							<?php endif; ?>
							<?php if(empty($value['product_image'])) : ?>
								<div class="emptyImage">
									No Image
								</div>
							<?php endif; ?>
						</td>
						<td>
							
							<a href="<?php echo PATH ?>cms/editproduct?ID=<?php echo $value['product_id'] ?>&productType=<?php echo $value['product_category'] ?>">
								<?php echo $value['product_name'] ?>	
							</a>
							
						</td>
						<td align="right"> 
							<?php if($value['publishState'] == 1) : ?>
								<span class="publishState">Published</span>
							<?php endif; ?>
							<?php if($value['publishState'] == 0) : ?>
								<span class="publishState">Not Published</span>
							<?php endif; ?>
						</td>
						<td class="priceData" align='right'>$<?php echo $value['price'] ?></td>
					</tr>
				</table>
			</div>
		<?php } ?>
		
		<?php if(empty($this -> product)) : ?>
			<div style="padding:10px 0px;">
				There are no products entered in this category	
			</div>
		<?php endif; ?>
			
	</div>
	<?php endif; ?>
	<?php if(!isset($_GET['Category'])) : ?>
			<div style="font-size: 16px; padding: 5px 0px;">Please select a category</div>
		<?php endif; ?>
</div>
