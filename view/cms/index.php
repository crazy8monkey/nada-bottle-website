<div class="login-box">
	<a href="<?php echo PATH ?>">
		<img src="<?php echo PATH ?>public/images/nadaBottleLogoAdmin.png" style="display:table; margin:0 auto 10px auto;">
	</a>
	<div class="login-input-section">
		<?php if(isset($this -> loginForm)) : ?>
		<div class="padding">
			<div class="loginHeader">
				Login Administration 
			</div>	
		</div>
		<div class="divider"></div>
		<form method="post" id="adminLogin" action="<?php echo PATH ?>login/adminLogin">
			<div class="padding" style="padding-bottom: 5px;">
				<div class="login-label">
					Login
				</div>
				<div style="margin-bottom:10px;" class="input-line">
					<input type="text" name="admin-username">
				</div>	
			</div>
			<div class="padding" style="padding-top: 0px;">
				<div class="login-label">
					Password
				</div>
				<div class="input-line">
					<input type="password" name="admin-password">	
				</div>
			</div>
			<div class="divider"></div>			
			<div class="padding">
				
				<div class="loginButtonSection">
					<div class="loadingForm"><img src="<?php echo PATH ?>public/images/ajax-loader.gif" /></div>
					<div class="inputButton">
						<input type="submit" class="blueButton" value="Login">	
					</div>
					
				</div>
				
				<div class="forgotLinkSection">
					<a href="<?php echo PATH ?>login/forgotPassword">Forgot Password?</a>
				</div>
				
				<div style="clear:both"></div>
			</div>
		</form>
		<?php endif; ?>
		<?php if(isset($this -> forgotPassword)) : ?>
			<div class="padding">
				<div class="loginHeader">
					Forgot Password 
				</div>	
			</div>
			<div class="divider"></div>
			<div class="padding" style="">
				Please enter your email in the inbox below.<br />
				If your email is valid/registered,<br />
				An email will be sent to you to reset your password.
			</div>
			<div class="divider"></div>
			
			
				<form id="resetPassword" method="post" action="<?php echo PATH ?>login/resetPassword">
					<div class="padding" style="padding-bottom: 5px;">
						<div class="login-label">
							Your Email
						</div>
						<div style="margin-bottom:10px;" class="input-line">
							<input type="text" id="forgotEmail" name="forgotPasswordEmailCheck">
						</div>	
					</div>
					<div class="divider"></div>
					<div class="padding">

						<div class="resetPasswordButton">
							<input type="submit" class="blueButton" value="Reset Password">
						</div>
						<div class="loadingForgotPasswordForm">
							<img src="<?php echo PATH ?>public/images/ajax-loader.gif" />
						</div>	
						
						
						<div class="backToLogin">
							<a href="<?php echo PATH ?>login">Back to Login</a>
						</div>
						<div style="clear:both"></div>
					</div>	
				</form>
				
			
		<?php endif; ?>
		
		<?php if(isset($this -> newPassword)) : ?>
			<?php if(isset($_GET['token']) && isset($_GET['userID'])) {
			//$_GET['userID'] == $this -> userInfo['userID'] &&	 
				if($_GET['userID'] == $this -> userInfo['userID'] && $_GET['token'] == $this -> userInfo['securityToken']) {?>
			
			 
			<div class="padding">
				<div class="loginHeader">
					Reset Password 
				</div>	
			</div>
			<div class="divider"></div>
			<form method="post" id="ChangePassword" action="<?php echo PATH ?>login/newPasswordPost">
				<div class="padding" style="padding-bottom: 5px;">
					<div class="login-label">
						New Password
					</div>
					<div style="margin-bottom:10px;" class="input-line">
						<input type="password" class="password" name="newPassword">
					</div>	
				</div>
				<div class="padding" style="padding-top: 0px;">
					<div class="login-label">
						Confirm Password
					</div>
					<div class="input-line">
						<input type="password" class="password" name="ConfirmNewPassword">	
					</div>
				</div>
				<div class="divider"></div>			
				<div class="padding">
					<input type="hidden" name="userID" value="<?php echo $_GET['userID'] ?>" />
					<div class="loginButtonSection">
						<div class="loadingForm"><img src="<?php echo PATH ?>public/images/ajax-loader.gif" /></div>
						<div class="inputButton">
							<input type="submit" class="blueButton" value="Reset Password">	
						</div>			
					</div>
				
					<div style="clear:both"></div>
				</div>
			</form>
			<?php } else { ?>
				<div class="padding">
					This session has been expired, please go to this <a href="<?php echo PATH ?>login/forgotPassword" >link</a> and resend your email to reset your password
				</div>
			<?php } ?>
			<?php } else { ?>
				<div class="padding">
					This session has been expired, please go to this <a href="<?php echo PATH ?>login/forgotPassword" >link</a> and resend your email to reset your password
				</div>
			<?php } ?>
		<?php endif; ?>
	</div>
</div>