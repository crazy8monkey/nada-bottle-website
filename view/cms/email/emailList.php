<?php foreach ($this -> settings as $key => $value) { ?>
				
<div class="adminContent">
	<div class="header">
		Settings
	</div>
	<div class="adminContentWrapper">
		<div class="secondarHeader" style="margin-bottom:10px;">
			<div style="float:left">
				Login Information
			</div>
			<div style="float:left; margin-left:10px;">
				<a class="chagePassword" href="javascript:void(0);" onclick="CMSController.OpenChangePassword()" >Change Password</a>
			</div>
		<div style="clear:both"></div>
			
		</div>
		<div class="settingsContent">
			<form method="post" id="Credentials" action="<?php echo PATH?>cms/usernameUpdate/<?php echo $value["userID"]?>">
				<div class="InputLine">
					<div class="inputLabel">Username:</div>
					<div class="InputText">
						<input type="text" name="userName" value="<?php echo $value['userName']; ?>" />	
					</div>
					<div style="clear:both"></div>
				</div>
				<div class="InputLine">
					<div class="inputLabel">Email:</div>
					<div class="InputText">
						<input type="text" name="email" value="<?php echo $value['email']; ?>" />
							
					</div>
					<div style="clear:both"></div>
				</div>
				<input type="submit" value="submit" />
			</form>			
		</div>
		<div class="secondarHeader">
			Email Settings
		</div>
		<div class="settingsContent">
			<form id="emailSettings" action="<?php echo PATH?>cms/emailSettings/<?php echo $value["userID"]?>" method="post">
				<div class="InputLine" style="margin-top:15px">
					<div class="inputLabel" style="width: 160px;">Contact Inquiries to:</div>
					<div class="InputText">
						<input type="text" name="email" value="<?php echo $value['contactEmail']; ?>" />
					</div>
					<div style="clear:both"></div>
				</div>
				<input type="submit" value="submit" />
			</form>
		</div>
	</div>
</div>
<div class="form-overlay"></div>
<div class="ChangePassword">
	<div class="header" style="position:relative">
		Change Password
		<a href="javascript:void(0);" onclick="CMSController.closeChangePassword()">
			<div class="close-button"></div>	
		</a>
	</div>
	<div style="padding:10px">
		<form method="post" id="ChangePassword" action="<?php echo PATH ?>cms/updatePassword/<?php echo $value["userID"]?>">
			<input type="password" name="updatePassword" style="width: 240px;" />
			<div style="margin-top:10px;">
				<input type="submit" value="submit" />	
			</div>		
		</form>	
	</div>
</div>

<?php } ?>
