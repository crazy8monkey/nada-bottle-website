<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-ecommerce.php' ?>
    <div class="header">
        Tax Report
    </div>
    <div class="adminContentWrapper">
    	
    	<div class="DatePickerForm">
    		<form action="<?php echo PATH ?>cms/taxReport">
	            <div class="inputLine">
	            	<div class="label">
	            		Start Date:	
	            	</div>
	            	 <input type="date" name="dtStart" value="<?php echo $this -> startDate ?>" />	
	            </div>
	            <div class="inputLine">
	            	<div class="label">
	            		End Date:
	            	</div>
	            	<input type="date" name="dtEnd" value="<?php echo $this -> endDate ?>" />
	            </div>
	            
	            <input type="submit" class="blueButton" value="Submit"/>
	        </form>	
    	</div>
       <?php if(!empty($this -> taxReport)) : ?> 
        <table class="taxReportTable">
            <tr>
                <td style="font-weight:bold">
                    State
                </td>
                <td style="font-weight:bold">
                    Amount Due
                </td>
            </tr>

        <?php foreach ($this -> taxReport as $key => $value) { ?>
            <tr>
                <td>
                    <?php echo $value['state'] ?>
                </td>
                <td>
                    $<?php echo $value[0] ?>
                </td>
            </tr>
        <?php } ?>
        </table>
        <?php endif; ?>
        <?php if(empty($this -> taxReport)) : ?> 
        	<div style="font-size: 16px; padding: 10px 0px;">There are no orders to collect taxes from</div>
        <?php endif; ?>
    </div>
</div>