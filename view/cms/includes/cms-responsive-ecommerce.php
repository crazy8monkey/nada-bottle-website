<div style="margin-bottom:20px; margin-top:10px;" class="adminContentWrapper mobileDropdowns">
	<div style="margin-bottom:5px;">
		<strong>eCommerce</strong>
	</div>
	<?php 
	
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	
	$orders = array("orders", "transaction");
	$category = array("categories", "addNewCategory", "category");
	$products = array("products", "editproduct", "editcolorlist", "editcolor", "editproductinventory", "addNewProduct");
	$warranty = array("warranties", "warrantySingle");
	
	function matchesCategory($site, $strings) {
    	foreach ($strings as $matcher) 
    	{
     	    if(strpos($site, $matcher) !== false) 
     	    {
     	        return true;
     	    }
    	}
    	return false;
	}
	
	
	?>
	
	<select onchange="if (this.value) window.location.href=this.value">
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceOrders')) : ?><option <?php echo matchesCategory($url, $orders) ? 'selected' : '' ?> value="<?php echo PATH ?>cms/orders?page=1">Orders</option><?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceCategories')) : ?><option <?php echo matchesCategory($url, $category) ? 'selected' : '' ?> value="<?php echo PATH ?>cms/categories">Categories</option><?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceProducts')) : ?><option <?php echo matchesCategory($url, $products) ? 'selected' : '' ?> value="<?php echo PATH ?>cms/products">Products</option><?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceTaxes')) : ?><option <?php echo strpos($url,'taxReport') !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/taxReport">Taxes</option><?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceWarranty')) : ?><option <?php echo matchesCategory($url, $warranty) ? 'selected' : '' ?> value="<?php echo PATH ?>cms/warranties">Warranties</option><?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommerceInventory')) : ?><option <?php echo strpos($url,'inventory') !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/inventory">Inventory</option><?php endif ?>
		<?php if(isset($_SESSION["user"]) && $_SESSION["user"]->HasPermission('eCommercePaymentLogs')) : ?><option <?php echo strpos($url,'paymentLogs') !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/paymentLogs">Payment Logs</option><?php endif ?>
	</select>
</div>