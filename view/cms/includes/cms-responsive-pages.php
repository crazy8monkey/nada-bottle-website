<div style="margin-bottom:20px; margin-top:10px;" class="adminContentWrapper PageMobileDropDown">
	<div style="margin-bottom:5px;">
		<strong>Pages</strong>
	</div>
	<?php 
	
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	
	
	function matchesCategory($site, $strings) {
    	foreach ($strings as $matcher) 
    	{
     	    if(strpos($site, $matcher) !== false) 
     	    {
     	        return true;
     	    }
    	}
    	return false;
	}
	
	?>
	<select onchange="if (this.value) window.location.href=this.value">
		<optgroup label="Locate Life">
		<option <?php echo matchesCategory($url, array('locatelifeContent')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/locatelifeContent">Locate Life</option>
		<optgroup label="Nada Ordinary Bottle">
		<option <?php echo matchesCategory($url, array('story')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/story">Story</option>
		<option <?php echo matchesCategory($url, array('cleanWater')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/cleanWater">Clean Drinking Water</option>
		<option <?php echo matchesCategory($url, array('thankyou', 'thankYouList')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/thankyou">Thank You</option>
		<option <?php echo matchesCategory($url, array('team', 'teamMember', 'newTeamMember')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/team">Our Team</option>
		<optgroup label="Support">
		<option <?php echo matchesCategory($url, array('contactContent')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/contactContent">Contact</option>
		<option <?php echo matchesCategory($url, array('warranty')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/warranty">Warranty</option>
		<option <?php echo matchesCategory($url, array('faq')) !== false ? 'selected' : '' ?> value="<?php echo PATH ?>cms/faq">FAQ</option>
	</select>
</div>