<div class="adminContent">
	<div class="header">
		<div style="float:left; font-size:16px; padding: 12px 0px 0px 0px;">
			Partners	
		</div>
		<div style="float:right">
			<a style="text-decoration:none;" class="newItem" href="<?php echo PATH ?>cms/addNewPartner">
				<div class="greenButton">
					New Partner	
				</div>
				
			</a>	
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		<table class="parterList">
			
		<?php foreach ($this->partners as $key => $value) { ?>
			<tr>
				<td class="partnerImage">
					<?php if(!empty($value['partnerImage'])) : ?>
						<img src="../view/index/partnersImages/<?php echo $value['partnerImage'] ?>" style="width:200px;" />
					<?php endif; ?>
					<?php if(empty($value['partnerImage'])) : ?>
						<div class="emptyImage">
							No Image
						</div>
					<?php endif; ?>
				</td>
				<td>
					<a style="color:#2a2a2a" href="<?php echo PATH . 'cms/editPartner/' . $value['partnerID']; ?>">
						<?php echo $value['partnerName'] ?>
					</a>
					
				</td>
				<td align="right">
					<?php if($value['publishState'] == 1) : ?>
						<span class="publishState">Published</span>
					<?php endif; ?>
					<?php if($value['publishState'] == 0) : ?>
						<span class="publishState">Not Published</span>
					<?php endif; ?>
				</td>
				<td style="text-align:right;  width: 60px;">
					<form action="<?php echo PATH ?>cms/delete/<?php echo $value['partnerID'] ?>/partners" method="post">
						<?php if(!empty($value['partnerImage'])) :?>
							<input type="hidden" name="hiddenParterImage" value="<?php echo $value['partnerImage'] ?>" />
						<?php endif; ?>
						<input type="submit" class="redButton" value="Delete" />
					</form>	
				</td>
			</tr>
		
		<?php } ?>
		</table>
	</div>
</div>

