<?php
$breadCrumbNameString;
$partnerHeaderString;
$editPartner = true;
$partnerNameForm;
$partnerRedirectLink;
$partnerSaveFormURL;

if(isset($this -> partnerSingle -> partnerID)) {
	$partnerSaveFormURL = PATH . "cms/savePartner/" . $this -> partnerSingle-> partnerID;
	$breadCrumbNameString = $this -> partnerSingle -> name;
	$partnerNameForm = $this -> partnerSingle -> name;
	$partnerRedirectLink = $this -> partnerSingle -> partnerLink;
	$partnerHeaderString = "Edit Partner";
} else {
	$partnerSaveFormURL = PATH . "cms/savePartner";
	$editPartner = false;
	$partnerRedirectLink = "";
	$breadCrumbNameString = "New Partner";
	$partnerHeaderString = "New Partner";
	$partnerNameForm = "";
}
?>
<div class="adminContent">
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/partners">Partners</a><span>&#8250;</span><span style="font-size:12px; margin: 0px;"><?php echo $breadCrumbNameString; ?></span>
	</div>
	<div class="header">
		<div class="headerContainer">
			<?php echo $partnerHeaderString ?>	
		</div>
		<?php if($editPartner == true) : ?>
		
			<div class="publishUnpublishButton">
				<?php if(!empty($this -> partnerSingle -> partnerPhoto)) :  ?>
					<?php if($this -> partnerSingle -> publishState == 0) :?>
						<a style="text-decoration:none" href="<?php echo PATH ?>cms/publishPartner/<?php echo $this -> partnerSingle -> partnerID; ?>">
							<div class="whiteButton publish">Publish Partner</div>
						</a>
					<?php endif; ?>
				<?php endif; ?>
		
			<?php if($this -> partnerSingle -> publishState == 1) :?>
				<a style="text-decoration:none" href="<?php echo PATH ?>cms/unPublishPartner/<?php echo $this -> partnerSingle -> partnerID; ?>">
					<div class="whiteButton publish">Unpublish Partner</div>
				</a>
			<?php endif; ?>
			</div>	
		
			<div class="deleteButton">
				<form action="<?php echo PATH ?>cms/delete/<?php echo $this -> partnerSingle -> partnerID ?>/partners" method="post">
					<?php if(!empty($this -> partnerSingle -> partnerPhoto)) :?>
						<input type="hidden" name="hiddenParterImage" value="<?php echo $this -> partnerSingle -> partnerPhoto ?>" />
					<?php endif; ?>
					<input type="submit" class="redButton" value="Delete Partner" />
				</form>			
			</div> 
		<?php endif; ?>	
		<div style="clear:both"></div>
	</div>
	
	<?php if($editPartner == true) : ?>
		<?php if(empty($this -> partnerSingle -> partnerPhoto)) :  ?>
			<div style="  text-align: center; font-size: 16px; padding: 15px; margin-bottom: -15px;">Partner Logo needs to be uploaded in order to publish to your website</div>
		<?php endif; ?>
	<?php endif; ?>	
	
	<div class="adminContentWrapper PartnerForm">
		<?php if($editPartner == true) : ?>
			<div class="publishedNotification">
				<?php if($this -> partnerSingle -> publishState == 1) :?>
					Partner Published
				<?php endif; ?>
				<?php if($this -> partnerSingle -> publishState == 0) :?>
					Partner Not Published
				<?php endif; ?>
			</div>
		<?php endif; ?>	
		<form id="editPartnerForm" enctype="multipart/form-data" method="post" action="<?php echo $partnerSaveFormURL ?>">
			<div class="inputLine">
				<div class="label">
					Logo
				</div>
				<?php if($editPartner == true) : ?>
					<?php if(!empty($this -> partnerSingle -> partnerPhoto)) :  ?>
						<img src="<?php echo PATH ?>view/index/partnersImages/<?php echo $this -> partnerSingle -> partnerPhoto ?>" style="width:200px;" /><br />
					<?php endif; ?>
					<?php if(empty($this -> partnerSingle -> partnerPhoto)) :  ?>
						<div id="noImage">
							No Image	
						</div>					
					<?php endif; ?>
				<?php endif; ?>	
				<div id="logoImageSection">
					<input type="file" id="image-upload" name="partnerLogoImage" />	
				</div>
				
			</div>
			<div id="uploadLogoDesktop">Please Upload an image on your desktop</div>
			
			<div class="inputLine">
				<div class="label">
					Partner Name
				</div>
				<input type="text" name="partnerName" class="required" style="width: 247px;" value="<?php echo $partnerNameForm ?>" />
			</div>
			
			<div class="inputLine">
				<div class="label">
					Website link
				</div>
				<div class="hyperlinkContainer">
					http://www.
				</div>
				<input type="text" name="partnerHyperLink" class="required" value="<?php echo $partnerRedirectLink ?>" />
			</div>
			<input type="submit" class="blueButton" value="Save Changes" />	
		</form>
		
	</div>
</div>
