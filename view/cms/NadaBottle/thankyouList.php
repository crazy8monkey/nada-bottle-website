<script>
	$(document).ready(function(){
		
		$('#checkAll').click (function () {
		    var checkedStatus = this.checked;
		    $('.supporterChecked').each(function () {
		        $(this).prop('checked', checkedStatus);
		     });
		});
		
		$(".pagination").appendTo(".pagination-section");
		$(".pagination").show();	
		
	});
	function checkCheckedCheckboxes() {
		if($('.supporterChecked').is(':checked')) {
	        return true
    	}
    	else {
 	        alert('You must check at least one box to delete');
    		return false
    	}
	}
</script>


<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-pages.php' ?>
	<?php require 'view/cms/includes/thankYouMenu.php' ?>
	<div class="header" style="font-size:16px;">
		Thank You List
	</div>
	<div class="adminContentWrapper">
		
		<form action="<?php echo PATH ?>cms/deleteMultiple/ThankYou" method="Post">
			<div class="contactLine" style="background: #f0f0f0; height: 30px;">
				<div style="float:left; padding: 7px 0px 5px 0px;">
					<input type="checkbox" id="checkAll" onclick="CMSController.toggleContactCheckBoxes() "/> Select All
				</div>
				<div style="float:right;">
					<input type="submit" class="redButton" value="Delete Checked" onclick="return checkCheckedCheckboxes();"/>
				</div>
				<div style="clear:both"></div>
			</div>
			<?php foreach ($this->ThankYou as $key => $value) : ?>
				<div class="contactLine">
					<div class="indicator" style="top: 100%; left: 0px; right: 0px; display:block"></div>
					<div class="checkboxContainer">
						<input type="checkbox" class="supporterChecked" name="supporters[]" value="<?php echo $value['supporterID']?>" />
					</div>
					<div class="contactName" style="float:left;">
						<?php echo $value['supporterName']  ?>
					</div>	
					<a href="<?php echo PATH ?>cms/delete/<?php echo $value['supporterID'] ?>/thankyou">
						<div class="deleteSupporter"></div>
					</a>
					<div style="clear:both"></div>	
				</div>
				
			<?php endforeach; ?>
		</form>
		
	</div>
	<div class="pagination-section"></div>
</div>

