<div class="adminContent">
	<?php require 'view/cms/includes/cms-responsive-pages.php' ?>
	<?php require 'view/cms/includes/thankYouMenu.php' ?>
	<div class="header">
		Thank You Wall
	</div>
	<div class="adminContentWrapper">
		
		<form action="<?php echo PATH; ?>cms/updateContent/<?php echo $this -> ThankYou -> pageID;?>/ThankYou/" method="Post">
			<div class="headerInput">
				<div class='label'>Page Title</div>
				<input type="text" name="PageHeader" value="<?php echo $this -> ThankYou -> contentHeader;?>" />	
			</div>
			<textarea id="pageContent" name="pageContent"><?php echo $this -> ThankYou -> content;?></textarea>
			<div style="margin-top:15px">
				<input type="submit" class="blueButton" value="Submit" />	
			</div>	
		</form>
		
	</div>
</div>
<script type="text/javascript">
Globals.CKEditorLoad('pageContent', '<?php echo PATH ?>');
</script>

