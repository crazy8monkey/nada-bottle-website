
<div class="adminContent">
	<div class="header">
		<div style="float:left; margin-top: 12px; font-size:16px;">
			Locate Life	
		</div>
		<div style="float:right; margin-left:10px;">
			<a style="text-decoration:none;" href="<?php echo PATH ?>cms/addNewLocation">
				<div class="greenButton">
					New Location
				</div>
			</a>	
		</div>
		<div style="clear:both"></div>
	</div>
	<div class="adminContentWrapper">
		
		<?php if (!empty($this -> locations)) : ?>
		<table class="LocateLifeList" style="width:100%">
			<?php foreach ($this -> locations as $key => $value) { ?>
				<tr>
					<td style="padding-bottom: 30px; padding-right:10px;">
						<a href="<?php echo PATH ?>cms/location/<?php echo $value['id']?>">
							Longitude: <?php echo $value['longitude']; ?> / Latitude: <?php echo $value['latitude']; ?>
						</a>
					</td>
					<td style="padding-bottom: 30px; width: 50px;">
						<a style="text-decoration:none;" href="<?php echo PATH ?>cms/delete/<?php echo $value['id']?>/locations">
							<div class="redButton">
								Delete	
							</div>
						</a>
					</td>
				</tr>
			<?php } ?>
		</table>
		<?php endif; ?>
		<?php if (empty($this -> locations)) : ?>
			<div style="font-size: 16px; padding: 15px 0px;">There are no locations entered</div>
		<?php endif; ?>
	</div>
	
</div>

