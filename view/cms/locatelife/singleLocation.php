<?php


$LocateLifeFormSave;
$coordinates;
$longitudeString;
$latitudeString;
$locationDescription;
$locationNameString;
$locationVideoString;
$locationImage;

$showSlideImages = false;

if(isset($this -> locationSingle -> locationID)) {
	$LocateLifeFormSave = PATH . "cms/saveLocation/" . $this -> locationSingle -> locationID;
	$coordinates = "Longitude: ". $this -> locationSingle -> latitude . "  / Latitude: " . $this -> locationSingle -> longitude;
	$longitudeString = $this -> locationSingle -> longitude;
	$latitudeString = $this -> locationSingle -> latitude;
	$locationDescription = str_replace("\n", '<br />', strip_tags($this -> locationSingle -> description, '<br />'));
	$locationNameString = $this -> locationSingle -> name;
	$showSlideImages = true;
	$locationVideoString = $this -> locationSingle -> locationVideo;
	$locationImage = $this -> locationSingle -> locationImage;
} else {
	$LocateLifeFormSave = PATH . "cms/saveLocation";
	$coordinates = "New Location";
	$longitudeString = "";
	$latitudeString = "";
	$locationDescription = "";
	$locationNameString = "";
	$locationVideoString = "";
	$locationImage = "";
}


?>
<div class="adminContent ">
	
	<div class="transactionBreadCrumb">
		<a href="<?php echo PATH ?>cms/locatelife">Location</a><span>&#8250;</span><?php echo $coordinates ?>
	</div>	
	
	
	<div class="header" id="locationHeader" style="font-size:16px;">
		<?php echo $coordinates; ?>
	</div>
	<div class="adminContentWrapper contentWrapper" style="margin-top:15px">
		<form id="LocationslocateLifeForm" action="<?php echo $LocateLifeFormSave ?>" method="post">
			<div style="margin-bottom:5px;">
				<strong>Longitude</strong>
			</div>
			<input type="text" name="longitude" value="<?php echo $longitudeString ?>" />
			<div style="margin-bottom:5px; margin-top:10px;">
				<strong>Latitude</strong>
			</div>
			<input type="text" name="latitude" value="<?php echo $latitudeString ?>" />
			<div style="margin-bottom:5px; margin-top:10px;">
				<strong>Location</strong>
			</div>
			<input type="text" name="locationName" value="<?php echo $locationNameString ?>" />
			<div style="margin-bottom:5px; margin-top:10px;">
				<strong>Image</strong>
			</div>
			<?php if($showSlideImages == true) : ?>
				<div class="locationImagePreview" style="background:url(<?php echo PATH ?>view/cms/locatelife/location/<?php echo $this -> locationSingle -> locationID . "/" . $locationImage ?>) no-repeat"></div>
			<?php endif; ?>
			<input type="file" name="locationImage" />
			<div style="margin-bottom:5px; margin-top:10px;">
				<strong>Video</strong>
			</div>
			<input type="text" style="padding-left:0px; width:100%" name="locationVideo" value="<?php echo $locationVideoString ?>"/>
			<div style="margin-bottom:5px; margin-top:10px;">
				<strong>Text</strong>
			</div>
			<textarea name="locationInformation" style="min-width:100%; width:100%; max-width:100% padding:0px;"><?php echo $locationDescription ?></textarea>
			<div style="margin-top:10px;">
				<input type="submit" class="blueButton" value="Save Location" />	
			</div>
			
			
		</form>
		
		
		
	</div>
	
</div>


